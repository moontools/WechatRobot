package com.lincoln;

import com.lincoln.gameScore.Game;
import com.lincoln.gameScore.GameMember;
import com.lincoln.reply.AddScoreReply;
import com.lincoln.reply.FindScoreReply;
import com.lincoln.reply.FunReply;
import com.lincoln.reply.PlayReply;
import com.lincoln.wechat.api.enums.AccountType;
import com.lincoln.wechat.api.model.Account;
import com.lincoln.wechat.api.model.Member;
import com.lincoln.wechat.api.model.WeChatMessage;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;

public class AutoReply {
    public static Map<String,FunReply> funReplyMap = new HashMap<>();//需要进行代码计算的回复
    public static Map<String,String> config = new HashMap<>();//简单的文本回复
    static {
        config.put("上分","上分成功!");
        config.put("下分","下分成功");
        config.put("使用方法","1.上分xx(xx为需要上的分数)\n2.下分\n3.查询\n4.下注xx(xx为下注的数字,十以内,会扣除100积分,下中奖励10000分)");
        funReplyMap.put("上分",new AddScoreReply());
        funReplyMap.put("下注",new PlayReply());
        funReplyMap.put("查询",new FindScoreReply());
    }
    private static FunReply findFunReply(String text){
        int i = text.indexOf(":");
        if(i != -1){
            text = text.substring(i);
        }
        for(String name:funReplyMap.keySet()){
            if(text.indexOf(name)==0){
                return funReplyMap.get(name);
            }
        }
        return null;
    }
    public static String reply(WeChatMessage message){
        String memberId = message.sendMemberId();
        if(memberId == null){
            return null;
        }
        GameMember gameMember = Game.gameMemberMap.get(memberId);
        if(gameMember == null){
            GameMember newGameMember = new GameMember();
            newGameMember.setId(memberId);
            Account group = TestBoot.boot.api().findGroupById(message.getFromUserName());
            if(group.getAccountType()== AccountType.TYPE_GROUP) {
                Member member = group.findMemberById(memberId);
                if(member==null){
                    return null;
                }
                newGameMember.setMember(member);
                Game.gameMemberMap.put(newGameMember.getId(), newGameMember);
            }
        }
        FunReply reply = findFunReply(message.getText());
        if(reply != null){
            return reply.reply(message);
        }
        String stringReply = config.get(message.getText());
        if(stringReply!=null){
            return stringReply;
        }
        return null;
    }
}
