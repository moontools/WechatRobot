package com.lincoln.reply;

import com.lincoln.gameScore.Game;
import com.lincoln.gameScore.GameMember;
import com.lincoln.wechat.api.model.Account;
import com.lincoln.wechat.api.model.Member;
import com.lincoln.wechat.api.model.WeChatMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AddScoreReply implements FunReply {
    public static String content = "上分";
    @Override
    public String reply(WeChatMessage message) {
        String id=message.sendMemberId();
        if(id ==null){
            return null;
        }
        GameMember gameMember = Game.gameMemberMap.get(id);
        if(gameMember == null){
            return null;
        }
        try{
            int score = Integer.valueOf(message.getText().replaceAll(content,""));
            gameMember.setScore(gameMember.getScore()+score);
            Member member = gameMember.getMember();
            return "@"+member.getNickName()+" 上分成功!余额:"+gameMember.getScore();
        }catch (Exception e){
            log.error("上分失败");
            return null;
        }
    }
}
