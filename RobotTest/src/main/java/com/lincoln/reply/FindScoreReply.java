package com.lincoln.reply;

import com.lincoln.gameScore.Game;
import com.lincoln.gameScore.GameMember;
import com.lincoln.wechat.api.model.Member;
import com.lincoln.wechat.api.model.WeChatMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FindScoreReply implements FunReply {
    public static String content = "查询";
    @Override
    public String reply(WeChatMessage message) {
        String id=message.sendMemberId();
        if(id ==null){
            return null;
        }
        GameMember gameMember = Game.gameMemberMap.get(id);
        if(gameMember == null){
            return null;
        }
        try{
            Member member = gameMember.getMember();
            return "@"+member.getNickName()+" 余额:"+gameMember.getScore();
        }catch (Exception e){
            log.error("查询失败");
            return null;
        }
    }
}
