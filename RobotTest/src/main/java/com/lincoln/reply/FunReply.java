package com.lincoln.reply;

import com.lincoln.wechat.api.model.WeChatMessage;

//需要进行代码执行得出的回复
public interface FunReply {
    String reply(WeChatMessage message);
}
