package com.lincoln.reply;

import com.lincoln.gameScore.Game;
import com.lincoln.gameScore.GameMember;
import com.lincoln.gameScore.GameMemberResult;
import com.lincoln.wechat.api.model.Member;
import com.lincoln.wechat.api.model.WeChatMessage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlayReply implements FunReply {
    public static String content = "下注";
    @Override
    public String reply(WeChatMessage message) {
        String id=message.sendMemberId();
        if(id ==null){
            return null;
        }
        GameMember gameMember = Game.gameMemberMap.get(id);
        if(gameMember == null){
            return null;
        }
        Member member = gameMember.getMember();
        if(gameMember.getScore()<100){
            return "下注失败,余额不足!";
        }
        try{
            int result = Integer.valueOf(message.getText().replaceAll(content,""));
            GameMemberResult gameMemberResult = new GameMemberResult();
            gameMemberResult.setId(id);
            gameMemberResult.setNum(result);
            Game.gameMemberResultMap.put(id,gameMemberResult);
            gameMember.setScore(gameMember.getScore()-100);
            return "@"+member.getNickName()+" 下注成功!余额:"+gameMember.getScore();
        }catch (Exception e){
            log.error("下注失败!");
            return null;
        }
    }
}
