package com.lincoln.gameScore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameMemberResult {
    String id;
    int num;
}
