package com.lincoln.gameScore;

import com.lincoln.TestBoot;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Slf4j
public class Game extends Thread{
    public static Map<String,GameMember> gameMemberMap = new HashMap<>();
    public static Map<String,GameMemberResult> gameMemberResultMap = new HashMap<>();

    @Override
    public void run() {
        while(true) {
            if (gameMemberResultMap.size() > 0) {//不是第一轮
                int result = new Random().nextInt(9);
                for (String id : gameMemberResultMap.keySet()) {
                    GameMemberResult gameMemberResult = gameMemberResultMap.get(id);
                    if (gameMemberResult != null && gameMemberResult.getNum() == result) {
                        GameMember gameMember = gameMemberMap.get(id);
                        if (gameMember != null) {
                            gameMember.setScore(gameMember.getScore() + 10000);//赢10000分
                        }
                    }
                }
                for (String name : TestBoot.groupNos) {
                    TestBoot.boot.sendMsgByName(name, "本轮结束,结果为:" + result);
                }
            }
            gameMemberResultMap = new HashMap<>();
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
