package com.lincoln.gameScore;

import com.lincoln.wechat.api.model.Member;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GameMember {
    private String id;
    private Member member;
    private int score;
}
