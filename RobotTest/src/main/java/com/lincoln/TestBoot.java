package com.lincoln;

import com.lincoln.gameScore.Game;
import com.lincoln.wechat.WeChatBot;
import com.lincoln.wechat.api.annotation.Bind;
import com.lincoln.wechat.api.constant.Config;
import com.lincoln.wechat.api.enums.MsgType;
import com.lincoln.wechat.api.model.WeChatMessage;
import com.lincoln.wechat.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
@Slf4j
public class TestBoot extends WeChatBot {
    public static TestBoot boot = new TestBoot(Config.me().autoLogin(true).showTerminal(true));
    public static Game game = new Game();
    public static List<String> groupNos = new ArrayList<>();

    static {
        groupNos.add("微信BC");
    }
    public TestBoot(Config config) {
        super(config);
    }

    @Bind(msgType = MsgType.ALL)
    public void handleText(WeChatMessage message) {
        if(message.isGroup()){//群消息
            for(String groupNo : groupNos){
                if(groupNo.equals(message.getFromNickName())){
                    log.info("接收到群 [{}] 的消息: {}",message.getName(),message.getText());
                    String reply = AutoReply.reply(message);
                    if(reply!=null){
                        this.sendMsg(message.getFromUserName(), reply);
                    }
                    break;
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        game.start();
        boot.start();
    }
}
