package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Member(用户)实体类
 *
 * @author keyboard
 * <p>
 * name :用户名
 * <p>
 * open_id : 微信openId
 * <p>
 * score : 用户可以提现的积分
 * <p>
 * surplus_score : 总的充值金额,此字段只会增加不会减少
 * <p>
 * phone : 手机号
 * @version 2014-11-26 上午11:54:46
 */
@Getter
@Setter
@Entity(name = "member")
public class Member extends BaseEntity implements Serializable {
    public static String entityName() {
        return "member";
    }

    public final static int STATUS_NORMAL = 0;//正常
    public final static int STATUS_BAN = 1;//封禁
    public final static int STATUS_BOT = 2;//假人
    public final static int STATUS_BOT_BAN = 3;//假人且被踢出

    public final static int SORT_TOP = 1;//置顶
    public final static int SORT_NORMAL = 2;//正常
    public final static int SORT_BOTTOM = 3;//置底

    @Id
    private Long id;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;
    /**
     * `id` bigint(100) NOT null,		                # 微信attrStatus
     * `create_time` datetime not null,			        # 创建时间
     * `update_time` datetime not null,			        # 修改时间
     *   `nick_name` varchar(20) default null,          # 花名
     * `score` decimal(10,2) default 0,                  # 总充值
     * `earn` decimal(10,2) default 0,                   # 总赢取
     * `all_use` decimal(10,2) default 0,                    # 已使用
     * `withdraw` decimal(10,2) default 0,               # 总提现
     * `status` int(1) default 0,                        # 状态:0正常1踢出2假人
     */

    @Column(name = "nick_name")
    private String nickName;

    @Column(name = "score")
    private Double score;

    @Column(name = "earn")
    private Double earn;

    @Column(name = "all_use")
    private Double allUse;

    @Column(name = "withdraw")
    private Double withdraw;

    @Column(name = "status")
    private Integer status;

    @Column(name = "sort")
    private Integer sort;

//    @Formula("score + earn - all_use - withdraw")
//    private Double money;
    /**
     * 计算余额
     *
     * @return
     */
    @Transient
    public Double getMoney() {
        double score = this.score == null ? 0 : this.score;
        double earn = this.earn == null ? 0 : this.earn;
        double allUse = this.allUse == null ? 0 : this.allUse;
        double withdraw = this.withdraw == null ? 0 : this.withdraw;
        return score + earn - allUse - withdraw;
    }

    //==============================================================================
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

