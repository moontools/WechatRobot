package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * member # 用户
 * 
 * @author lincoln
 * 
 * @version 2014-11-26 上午11:54:46
 */
@Getter
@Setter
@Entity(name = "wx_message")
public class WxMessage extends BaseEntity implements Serializable {
	public static String entityName(){
		return "wxMessage";
	}

	@Id
	private Long id;//	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "update_time")
	private Date updateTime;
	/**
	 `fromUserName` varchar(200) default null,     # fromUserName
	 `fromNickName` varchar(200) default null,     # fromNickName
	 `mineUserName` varchar(200) default null,     # fromNickName
	 `mineNickName` varchar(200) default null,     # fromNickName
	 `toUserName` varchar(200) default null,       # fromNickName
	 `content` text default null,                  # content
	 `text` text default null,                     # text
	 */

	@Column(name = "fromUserName")
	private String fromUserName;

	@Column(name = "fromNickName")
	private String fromNickName;

	@Column(name = "mineUserName")
	private String mineUserName;

	@Column(name = "mineNickName")
	private String mineNickName;

	@Column(name = "toUserName")
	private String toUserName;

	@Column(name = "content")
	private String content;

	@Column(name = "text")
	private String text;

	/**
	 * 分割content取得讨论组成员的Name
	 * @return
	 */
	public String getGroupMemberId(){
		if(content!=null&&content.length()>0){
			return content.substring(0,content.indexOf(":<br/>"));
		}
		return "";
	}

	//==============================================================================
	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}

