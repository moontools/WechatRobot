package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Betting(下注)实体类
 *
 * @author lincoln
 *
 * @version 2014-11-26 上午11:54:46
 */
@Getter
@Setter
@Entity(name = "betting")
public class Betting extends BaseEntity implements Serializable {
    public static String entityName(){
        return "betting";
    }

    public static final int RESULT_LOSE = 0;//输
    public static final int RESULT_WIN = 1;//赢


    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     * `member` bigint(100) default null,            # 用户id
     `game` int(8) default null,                   # 游戏id
     `score` decimal(10,2) default 0,              # 金额
     `content` text default null,                  # 下注内容
     `result` int default null,                    # 下注结果,0表示输,1表示赢
     `win` decimal(10,2) default 0,                # 总赢取,为0表示输了
     )
     */

    @ManyToOne
    @JoinColumn(name = "member")
    @NotFound(action = NotFoundAction.IGNORE)
    private Member member;

    @ManyToOne
    @JoinColumn(name = "game")
    @NotFound(action = NotFoundAction.IGNORE)
    private Game game;

    @Column(name = "score")
    private Double score;

    @Column(name = "content")
    private String content;
    /*
      0:未中奖,
      1:中奖
     */
    @Column(name = "result")
    private int result;

    @Column(name = "win")
    private Double win;

    //==============================================================================
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

