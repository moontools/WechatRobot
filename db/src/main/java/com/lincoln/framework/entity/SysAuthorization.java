package com.lincoln.framework.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * table : sys_authorization # 角色菜单授权关联表
 * 
 * @author zwm
 */
@Entity(name = "sys_authorization")
public class SysAuthorization implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "role_id")
	private SysRole sysRole;

	@ManyToOne
	@JoinColumn(name = "menu_id")
	private SysMenu sysMenu;

	@Column(name = "button_rights")
	private String buttonRights;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SysRole getSysRole() {
		return sysRole;
	}

	public void setSysRole(SysRole sysRole) {
		this.sysRole = sysRole;
	}

	public SysMenu getSysMenu() {
		return sysMenu;
	}

	public void setSysMenu(SysMenu sysMenu) {
		this.sysMenu = sysMenu;
	}

	public String getButtonRights() {
		return buttonRights;
	}

	public void setButtonRights(String buttonRights) {
		this.buttonRights = buttonRights;
	}
	
	
}
