package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Withdraw(下分)实体类
 *
 * @author keyboard
 *
 * m_id :下注的会员
 *
 * extract_score : 下分金额
 *
 * source : 在哪个平台下分,如:支付宝,微信
 *
 * approval : 下分订单状态:已拒绝、已处理、待处理
 *
 * @version 2014-11-26 上午11:54:46
 */
@Setter
@Getter
@Entity(name = "withdraw")
public class Withdraw extends BaseEntity implements Serializable {
    public static final int STATUS_WAIT = 0;//未处理
    public static final int STATUS_YES = 1;//批准
    public static final int STATUS_NO = 2;//拒绝

    public static final String SOURCE_ALI = "支付宝";
    public static final String SOURCE_WECHAT = "微信";

    public static String entityName(){
        return "extract";
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     `member` bigint(100) default null,            # 用户id
     `score` decimal(10,2) default 0,              # 提现金额
     `source` varchar(100) default null,           # 提现到
     `status` int(1) default 0,                    # 状态:0未处理1批准2拒绝
     */

    @ManyToOne
    @JoinColumn(name = "member")
    @NotFound(action = NotFoundAction.IGNORE)
    private Member member;

    @Column(name = "score")
    private Double score;

    @Column(name = "source")
    private String source;

    @Column(name = "status")
    private Integer status;

    //==============================================================================
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

