package com.lincoln.framework.entity;

import com.lincoln.framework.utils.StringUtilsEx;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * table : sys_menu # 菜单表
 * 
 * @author zwm
 */
@Entity(name = "sys_menu")
public class SysMenu implements Serializable {

	/**
	 * 菜单状态:0-隐藏
	 */
	public static final int STATUS_HIDE = 0;

	/**
	 * 菜单状态:1-启用
	 */
	public static final int STATUS_ALLOW = 1;

	/**
	 * 菜单状态:-1 -禁用
	 */
	public static final int STATUS_FORBIDDEN = -1;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent_id")
	@NotFound(action = NotFoundAction.IGNORE)
	private SysMenu parentMenu;

	@Column(name = "level")
	private int level;

	@Column(name = "name")
	private String name;

	@Column(name = "css_class")
	private String cssClass;

	@Column(name = "uri")
	private String uri;

	@Column(name = "sort")
	private int sort;

	@Column(name = "button1")
	private String button1;

	@Column(name = "button2")
	private String button2;

	@Column(name = "button3")
	private String button3;

	@Column(name = "button4")
	private String button4;

	@Column(name = "button5")
	private String button5;

	@Column(name = "button6")
	private String button6;

	@Column(name = "button7")
	private String button7;

	@Column(name = "button8")
	private String button8;

	@Column(name = "button9")
	private String button9;

	@Column(name = "button10")
	private String button10;

	@Column(name = "button11")
	private String button11;

	@Column(name = "button12")
	private String button12;

	@Column(name = "button13")
	private String button13;

	@Column(name = "button14")
	private String button14;

	@Column(name = "button15")
	private String button15;

	@Column(name = "button16")
	private String button16;

	@Column(name = "button17")
	private String button17;

	@Column(name = "button18")
	private String button18;

	@Column(name = "button19")
	private String button19;

	@Column(name = "button20")
	private String button20;

	@Column(name = "status")
	private int status;

	@Column(name = "remark")
	private String remark;

	@Column(name = "add_time")
	private Date addTime;

	@Column(name = "update_time")
	private Date updateTime;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "parentMenu", targetEntity = SysMenu.class)
	@OrderBy("sort asc")
	private List<SysMenu> childMenus;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "sysMenu", targetEntity = SysAuthorization.class)
	private List<SysAuthorization> authorizations;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SysMenu getParentMenu() {
		return parentMenu;
	}

	public void setParentMenu(SysMenu parentMenu) {
		this.parentMenu = parentMenu;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getButton1() {
		return button1;
	}

	public void setButton1(String button1) {
		this.button1 = button1;
	}

	public String getButton2() {
		return button2;
	}

	public void setButton2(String button2) {
		this.button2 = button2;
	}

	public String getButton3() {
		return button3;
	}

	public void setButton3(String button3) {
		this.button3 = button3;
	}

	public String getButton4() {
		return button4;
	}

	public void setButton4(String button4) {
		this.button4 = button4;
	}

	public String getButton5() {
		return button5;
	}

	public void setButton5(String button5) {
		this.button5 = button5;
	}

	public String getButton6() {
		return button6;
	}

	public void setButton6(String button6) {
		this.button6 = button6;
	}

	public String getButton7() {
		return button7;
	}

	public void setButton7(String button7) {
		this.button7 = button7;
	}

	public String getButton8() {
		return button8;
	}

	public void setButton8(String button8) {
		this.button8 = button8;
	}

	public String getButton9() {
		return button9;
	}

	public void setButton9(String button9) {
		this.button9 = button9;
	}

	public String getButton10() {
		return button10;
	}

	public void setButton10(String button10) {
		this.button10 = button10;
	}

	public String getButton11() {
		return button11;
	}

	public void setButton11(String button11) {
		this.button11 = button11;
	}

	public String getButton12() {
		return button12;
	}

	public void setButton12(String button12) {
		this.button12 = button12;
	}

	public String getButton13() {
		return button13;
	}

	public void setButton13(String button13) {
		this.button13 = button13;
	}

	public String getButton14() {
		return button14;
	}

	public void setButton14(String button14) {
		this.button14 = button14;
	}

	public String getButton15() {
		return button15;
	}

	public void setButton15(String button15) {
		this.button15 = button15;
	}

	public String getButton16() {
		return button16;
	}

	public void setButton16(String button16) {
		this.button16 = button16;
	}

	public String getButton17() {
		return button17;
	}

	public void setButton17(String button17) {
		this.button17 = button17;
	}

	public String getButton18() {
		return button18;
	}

	public void setButton18(String button18) {
		this.button18 = button18;
	}

	public String getButton19() {
		return button19;
	}

	public void setButton19(String button19) {
		this.button19 = button19;
	}

	public String getButton20() {
		return button20;
	}

	public void setButton20(String button20) {
		this.button20 = button20;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public List<SysAuthorization> getAuthorizations() {
		return authorizations;
	}

	public void setAuthorizations(List<SysAuthorization> authorizations) {
		this.authorizations = authorizations;
	}

	public List<SysMenu> getChildMenus() {
		return childMenus;
	}

	public void setChildMenus(List<SysMenu> childMenus) {
		this.childMenus = childMenus;
	}
	
	public List<String> getMenuButtonsList() throws Exception{
		List<String> menuButtons = new ArrayList<String>();
		for (int i=0; i<20; i++) {
			String methodName = "getButton" + (i+1);
			String buttonName = (String)SysMenu.class.getDeclaredMethod(methodName).invoke(this);
			
			if (!StringUtilsEx.isNullOrEmpty(buttonName)) {
				menuButtons.add(buttonName);
			} else {
				break;
			}
		}
		
		return menuButtons;
	}

}
