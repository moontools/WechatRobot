package com.lincoln.framework.entity;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;

/**
 * table : sys_admin # 系统管理员数据表
 * 
 * @author zwm
 */
@Entity(name = "sys_admin")
public class SysAdmin {

	/**
	 * 管理员状态：0、停用
	 */
	public static final int STATUS_DISABLE = 0;
	
	/**
	 * 管理员状态：1、启用
	 */
	public static final int STATUS_ENABLE = 1;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "password")
	private String password;

	@Column(name = "md5_password")
	private String md5Password;
	
	/**
	 * 使用者
	 */
	@Column(name = "real_name")
	private String realName;

	@ManyToOne
	@JoinColumn(name = "role_id")
	@NotFound(action = NotFoundAction.IGNORE)
	private SysRole sysRole;

	@Column(name = "status")
	private int status;

	@Column(name = "phone_num")
	private String phoneNum;

	@Column(name = "remark")
	private String remark;

	@Column(name = "last_login_time")
	private Date lastLoginTime;

	@Column(name = "last_login_ip")
	private String lastLoginIp;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMd5Password() {
		return md5Password;
	}

	public void setMd5Password(String md5Password) {
		this.md5Password = md5Password;
	}

	public SysRole getSysRole() {
		return sysRole;
	}

	public void setSysRole(SysRole sysRole) {
		this.sysRole = sysRole;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
	
}
