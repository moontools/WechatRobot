package com.lincoln.framework.entity;

import java.util.Date;

/**
 * Created by Lincoln on 2017/10/18.
 */
public abstract class BaseEntity {
    public abstract Long getId();
    public abstract void setId(Long id);
    public abstract Date getCreateTime();
    public abstract void setCreateTime(Date createTime);
    public abstract void setUpdateTime(Date updateTime);
    public abstract Date getUpdateTime();
    public static String entityName(){
        return null;
    }
}
