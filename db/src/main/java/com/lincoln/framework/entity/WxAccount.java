package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * 微信用户
 *
 * @author biezhi
 * @date 2018/1/19
 */
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "wx_account")
public class WxAccount extends BaseEntity implements Serializable {
    public static String entityName(){
        return "wxAccount";
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    private String alias;

    private Integer appAccountFlag;

    private Long attrStatus;

    private Long chatRoomId;

    private String city;

    private Integer contactFlag;

    private String displayName;

    /**
     * 群id
     */
    private String encryChatRoomId;

    /**
     * 微信头像URL
     */
    private String headImgUrl;

    private Integer hideInputBarFlag;

    private Integer isOwner;

    /**
     * 微信头像URL
     */
    private String keyWord;

    private Integer memberCount;

    /**
     * 微信昵称
     */
    private String nickName;

    private Long ownerUin;

    private String pyInitial;

    private String pyQuanPin;

    private String province;

    /**
     * 备注名
     */
    private String remarkName;

    private String remarkPYInitial;

    private String remarkPYQuanPin;

    /**
     * 性别
     */
    private Integer sex;

    private String signature;

    private Integer snsFlag;

    private Integer starFriend;

    private Integer statues;

    private Long uin;

    private Integer uniFriend;

    /**
     * 用户唯一标识
     */
    private String userName;

    private Integer verifyFlag;

    public static boolean equals(WxAccount wxAccount,WxAccount wxAccount1) {
        try {
            Field[] fields = WxAccount.class.getDeclaredFields();
            for (Field field : fields) {
                if ("id".equals(field.getName()) || "createTime".equals(field.getName()) || "updateTime".equals(field.getName())) {
                    continue;
                }
                Object o = field.get(wxAccount);
                Object o1 = field.get(wxAccount1);
                if (o == null && o1 == null) {
                    continue;
                }
                if (o != null && o1 == null) {
                    return false;
                }
                if (o == null && o1 != null) {
                    return false;
                }
                if (o.equals(o1)) {
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //==============================================================================
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
