package com.lincoln.framework.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * table : sys_admin_log # 管理员操作日志表
 * 
 * @author jared
 */
@Entity(name = "sys_admin_log")
public class SysAdminLog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	/**
	 * 系统管理员信息
	 */
	@ManyToOne
	@JoinColumn(name = "sys_admin_id")
	private SysAdmin sysAdmin;
	
	/**
	 * 操作内容
	 */
	@Column(name = "content")
	private String content;
	
	/**
	 * 操作ip地址
	 */
	@Column(name = "ip_address")
	private String ipAddress;
	
	/**
	 * 操作时间
	 */
	@Column(name = "operate_time")
	private Date operateTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public SysAdmin getSysAdmin() {
		return sysAdmin;
	}

	public void setSysAdmin(SysAdmin sysAdmin) {
		this.sysAdmin = sysAdmin;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

}
