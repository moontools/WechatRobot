package com.lincoln.framework.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * table : sys_role # 角色表
 * 
 * @author zwm
 */
@Entity(name = "sys_role")
public class SysRole implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "remark")
	private String remark;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = SysAuthorization.class, mappedBy = "sysRole", cascade = CascadeType.ALL)
	private List<SysAuthorization> authorizations;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<SysAuthorization> getAuthorizations() {
		return authorizations;
	}

	public void setAuthorizations(List<SysAuthorization> authorizations) {
		this.authorizations = authorizations;
	}
	
	/**
	 * 将授权信息编码
	 * @return
	 */
	public String getAuthorizationCode(){
		String authorizationCode = ",";
		for (SysAuthorization sysAuthorization : this.getAuthorizations()) {
			authorizationCode +=  sysAuthorization.getSysMenu().getId() + ",";
			if (sysAuthorization.getButtonRights().contains("1")) {
				String[] sysAuthorizationRights = sysAuthorization.getButtonRights().split(",");
				for (int i=0; i<20; i++) {
					if (sysAuthorizationRights[i].equals("1")) {
						authorizationCode +=  sysAuthorization.getSysMenu().getId() + "_" + (i+1) + ",";
					}
				}
			}
		}
		return authorizationCode;
	}
}
