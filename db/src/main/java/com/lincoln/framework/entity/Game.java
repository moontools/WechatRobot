package com.lincoln.framework.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Game(游戏)实体类
 *
 * @author lincoln
 *
 * @version 2014-11-26 上午11:54:46
 */
@Getter
@Setter
@Entity(name = "game")
public class Game extends BaseEntity implements Serializable {
    public static String entityName(){
        return "game";
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    /**
     `game_no` int(20) default null,               # 游戏期数
     `start_time` datetime default null,           # 开始时间
     `end_time` datetime default null,             # 截止时间
     `result1` int default null,                   # 结果1
     `result2` int default null,                   # 结果2
     `result3` int default null,                   # 结果3
     `win` decimal(10,2) default 0,                # 总盈亏
     )
     */

    @Column(name = "game_no")
    private Long gameNo;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @Column(name = "result1")
    private Integer result1;

    @Column(name = "result2")
    private Integer result2;

    @Column(name = "result3")
    private Integer result3;

    @Column(name = "win")
    private Double win;

    //==============================================================================
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}

