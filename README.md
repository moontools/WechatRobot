#微信机器人

1、https://gitee.com/lincolnking/WechatRobot.git 注册账号后，通知我提交加入项目开发人员

2、	changelog、initbasedata、chineseregion 3个初始化内容 

	changelog 项目所有数据库变更提交该文件，防止数据库同步问题，注释格式：# yyyy-MM-dd 某人 删除、提交了***** 
	initbasedata 初始化内容，用于频繁变更初始化数据，如菜单数据 
	chineseregion 中国地区表，初始化即可

3、	目录对应代码

    com.lincoln.framework.controller.sys 系统端controller
	com.lincoln.framework.controller.api api controller
	com.lincoln.framework.entity 持久化实体类 
	com.lincoln.framework.bean 封装实体类 
	com.lincoln.framework.service 忽略了dao层，直接将业务逻辑service与dao合并为service层 
	com.lincoln.framework.task 定时任务 
	com.lincoln.framework.task.schedule 延时任务
4、图片上传注意事项
	
	由于系统上传都是临时文件/temp，所以需要转移至正式目录/uploads，遇到富文本编辑器内容，可使用一下方法直接替换转移
	WebUtils.updateImage
	
	其他可直接参看代码

5、编码规范，

	a、所有方法类都增加注释，有利于后续维护修改
	
6、扩展性
    
    1.机器人能够扩展cmd,方法是调用CmdContext中的loadJar,将jar包加载到程序中,扩展机器人自动回复的判断
    2.扩展爬虫脚本,实现ReptileHotModule并且包在com.lincoln.Client.reptile
    3.(todo)扩展游戏玩法
7、待完成

    1.扩展游戏玩法
    2.将cmd与reptile加入game中一起打包扩展
    3.一个jar包不仅限于实现一种扩展
    4.扩展插件自动查错
    5.界面分页可抽出自定
	