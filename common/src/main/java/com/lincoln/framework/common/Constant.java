package com.lincoln.framework.common;

/**
 * 常用的常量命令使用
 * @author jared
 *
 */
public class Constant {

	/**
	 * 分页显示数据条数 10-条
	 */
	public static final int PAGE_COUNT = 10;
	
	/**
	 * 分页显示数据条数 12-条
	 */
	public static final int PAGE_COUNT_TWELVE = 12;
	
	/**
	 * 临时文件目录
	 */
	public static final String TEMP_DIR = "/temp";
	
	/**
	 * ueditor临时上传目录目录
	 */
	public static final String UEDITOR_TEMP_DIR = "/ueditor/jsp/upload/image";
	
	/**
	 * 上传文件目录
	 */
	public static final String UPLOAD_DIR = "/uploads";
	
	/**
	 * 上传下载附件目录
	 */
	public static final String UPLOAD_DOWNLOAD_DIR = "/uploads/sys/downloadfile";
	
	/**
	 * session中保存系统管理员登录信息的key
	 */
	public static final String SESSION_SYS_ADMIN_LOGIN = "session_sys_admin_login";
	
	/**
	 * session中保存系统会员登录信息的key
	 */
	public static final String SESSION_MEMBER_LOGIN = "session_member_login";
	
	/**
	 * session中保存系统管理员访问uri路径，用于页面校验权限
	 */
	public static final String SYS_REQUEST_URI = "sysRequestUri";
	
	/**
	 * 访问路径菜单id
	 */
	public static final String SYS_REQUEST_MENU_ID = "sysRequestMenuId";
	
	/**
	 * 访问路径父类菜单id
	 */
	public static final String SYS_REQUEST_PARENT_MENU_ID = "sysRequestParentMenuId";
	
	/**
	 * 异常类型：未登录
	 */
	public static String ERRORTYPE_UNLOGIN = "unlogin";
	
	/**
	 * 异常类型：会员未登录
	 */
	public static String ERRORTYPE_MEMBER_UNLOGIN = "memberUnlogin";
	
	/**
	 * 异常类型：会员不允许
	 */
	public static String ERRORTYPE_MEMBER_DIABLE = "memberDisable";
	
	/**
	 * 店铺重写request attribute保存店铺id的key
	 */
	public static String REQUEST_ATTRIBUTE_SHOPID = "request_attribute_shopid";
	
	/**
	 * rmb与网币倍率
	 */
	public final static int NETCURRENCY_RATE = 10; 
	
	/**
	 * 资讯列表显示概要长度90个字符
	 */
	public final static int INFORMARION_LIST_SUMMARY_LENGTH = 90;
	
	
	/**
	 * 国际化常量，中文
	 */
	public final static String LOCALE_ZH = "zh_CN";
	
	/**
	 * 国际化常量：英文
	 */
	public final static String LOCALE_EN = "en_US";
}
