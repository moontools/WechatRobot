package com.lincoln.framework.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 * md5的加密类
 * @author AXZH
 *
 */
public class MD5 {
	private static String byte2hex(byte[] bytes){
		StringBuffer buf=new StringBuffer("");
		for (int i=0;i<bytes.length;i++){
			String t=Integer.toHexString(bytes[i]>=0?bytes[i]:(bytes[i]+256));
			if(t.length()<2) t="0"+t;
			buf.append(t.toUpperCase());
		}
		return buf.toString();
	}
	public static String hex_md5(String s){
		MessageDigest md5_dig = null;
		try {
			md5_dig = MessageDigest.getInstance("md5");
		} catch (NoSuchAlgorithmException e) {
			// do nothing , it is impossible!!
			
		}
		byte[] bytes=s.getBytes();
		md5_dig.update(bytes);
		return byte2hex(md5_dig.digest());
	}
	
	private MessageDigest md5_dig;
	
	public MD5(){
		try {
			md5_dig = MessageDigest.getInstance("md5");
		} catch (NoSuchAlgorithmException e) {
		}
	}
	
	public void update(String s){
		byte[] bytes=s.getBytes();
		md5_dig.update(bytes);
	}
	
	public String hexdigest(){
		return byte2hex(md5_dig.digest());
	}
	public static void main(String[] args) throws Exception{
//		System.out.println(MD5.hex_md5("www.dg999.com8djfka4239l@!d293a!Asdf"));
		

		
		String te = new sun.misc.BASE64Encoder().encode("神雕侠侣".getBytes("GBK"));
		
		byte[] td = new sun.misc.BASE64Decoder().decodeBuffer(te);
		
		
		System.out.println(te);
		System.out.println(new String(td, "GBK"));
		
		System.out.println(new sun.misc.BASE64Encoder().encode("sdxng".getBytes("GBK")));
		System.out.println(new sun.misc.BASE64Encoder().encode("sdxng".getBytes("utf8")));
		System.out.println(new sun.misc.BASE64Encoder().encode("神雕侠侣".getBytes("GBK")));
		System.out.println(new sun.misc.BASE64Encoder().encode("神雕侠侣".getBytes("utf8")));
		
		
	}
}
