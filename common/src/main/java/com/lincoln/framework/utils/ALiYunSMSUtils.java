package com.lincoln.framework.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Created on 17/6/7.
 * 短信API产品的DEMO程序,工程中包含了一个SmsDemo类，直接通过
 * 执行main函数即可体验短信产品API功能(只需要将AK替换成开通了云通信-短信产品功能的AK即可)
 * 工程依赖了2个jar包(存放在工程的libs目录下)
 * 1:aliyun-java-sdk-core.jar
 * 2:aliyun-java-sdk-dysmsapi.jar
 *
 * 备注:Demo工程编码采用UTF-8
 * 国际短信发送请勿参照此DEMO
 */
public class ALiYunSMSUtils {
    protected static Logger logger = LoggerFactory.getLogger(ALiYunSMSUtils.class);
    static class Code{
        private String code;
        private Date date;
        public Code(int code){
            this.code = String.valueOf(code);
            this.date = new Date();
        }
        public Code(String code){
            this.code = code;
            this.date = new Date();
        }
        public String getCode() {
            return code;
        }
        public Date getDate() {
            return date;
        }
    }
    private static Map<String,Code> registerCode = new ConcurrentHashMap<>();

    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
    static final String accessKeyId = "LTAIx8lg3dFVR21E";
    static final String accessKeySecret = "rMQsN6MMFkhn9H90gynQ7dOhz4aHQ5";
    static final String sms_code = "SMS_120375910";

    /**
     * 发送注册短信验证码,0为失败,1为成功,2为时间内不可重复发送
     * @param phone
     * @return
     */
    public static int SendRegisterSms(String phone) throws Exception {
        Code code1 = registerCode.get(phone);
        if(code1 != null&&((new Date().getTime()-code1.getDate().getTime())<60*1000)){
            return 2;
        }
        SendSmsResponse sendSmsResponse;
        int code = 1 + (int)(Math.random() * ((999999 - 1) + 1));
        logger.info("注册验证码为:"+code);

        //测试用
        registerCode.put(phone,new Code(code));
        if(!ConfigSetting.SMS_OPEN) {
            throw new Exception("测试注册返回短信验证码为:" + code);
        }
        try {
            sendSmsResponse = sendSms(phone, sms_code, "{\"code\":\""+code+"\"}");
        }catch (Exception e){
            return 0;
        }
        if(sendSmsResponse.getCode().equals("OK")){
            registerCode.put(phone,new Code(code));
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * 发送找回密码短信验证码,0为失败,1为成功,2为时间内不可重复发送
     * @param phone
     * @return
     */
    public static int SendForgetSms(String phone) throws Exception {
        Code code1 = registerCode.get(phone);
        if(code1 != null&&((new Date().getTime()-code1.getDate().getTime())<60*1000)){
            return 2;
        }
        SendSmsResponse sendSmsResponse;
        int code = 1 + (int)(Math.random() * ((999999 - 1) + 1));
        logger.info("忘记密码验证码为:"+code);
        //测试用
        registerCode.put(phone,new Code(code));
        if(!ConfigSetting.SMS_OPEN) {
            throw new Exception("测试忘记密码返回短信验证码为:" + code);
        }
        try {
            sendSmsResponse = sendSms(phone, sms_code, "{\"code\":\""+code+"\"}");
        }catch (Exception e){
            return 0;
        }
        if(sendSmsResponse.getCode().equals("OK")){
            registerCode.put(phone,new Code(code));
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * 发送修改密码短信验证码,0为失败,1为成功,2为时间内不可重复发送
     * @param phone
     * @return
     */
    public static int SendModifyPasswordMobileCodeSms(String phone) throws Exception {
        Code code1 = registerCode.get(phone);
        if(code1 != null&&((new Date().getTime()-code1.getDate().getTime())<60*1000)){
            return 2;
        }
        SendSmsResponse sendSmsResponse;
        int code = 1 + (int)(Math.random() * ((999999 - 1) + 1));

        registerCode.put(phone,new Code(code));
        logger.info("修改密码:"+code);

        if(!ConfigSetting.SMS_OPEN) {
            throw new Exception("测试修改密码返回短信验证码为:" + code);
        }

        try {
            sendSmsResponse = sendSms(phone, "SMS_80190114", "{\"code\":\""+code+"\"}");
        }catch (Exception e){
            return 0;
        }
        if(sendSmsResponse.getCode().equals("OK")){
            registerCode.put(phone,new Code(code));
            return 1;
        }else{
            return 0;
        }
    }

    public static boolean CheckRegisterSms(String phone,String code){
        Code code1 = registerCode.get(phone);
        if((code1!=null)&&code1.getCode().equals(code)){
            registerCode.remove(phone);
            return true;
        }else{
            return false;
        }
    }

    public static SendSmsResponse sendSms(String phone,String templateCode,String json) throws ClientException {
        if(StringUtilsEx.isNullOrEmpty(templateCode)){
            templateCode = sms_code;
        }
        if(StringUtilsEx.isNullOrEmpty(json)){
            json = "{\"name\":\"Tom\", \"code\":\"123\"}";
        }
        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象-具体描述见控制台-文档部分内容
        SendSmsRequest request = new SendSmsRequest();
        //必填:待发送手机号
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName("金典实");
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(templateCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        request.setTemplateParam(json);

        //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");

        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        request.setOutId("yourOutId");

        //hint 此处可能会抛出异常，注意catch
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

        return sendSmsResponse;
    }


    public static QuerySendDetailsResponse querySendDetails(String bizId) throws ClientException {

        //可自助调整超时时间
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        //初始化acsClient,暂不支持region化
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);

        //组装请求对象
        QuerySendDetailsRequest request = new QuerySendDetailsRequest();
        //必填-号码
        request.setPhoneNumber("15000000000");
        //可选-流水号
        request.setBizId(bizId);
        //必填-发送日期 支持30天内记录查询，格式yyyyMMdd
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        request.setSendDate(ft.format(new Date()));
        //必填-页大小
        request.setPageSize(10L);
        //必填-当前页码从1开始计数
        request.setCurrentPage(1L);

        //hint 此处可能会抛出异常，注意catch
        QuerySendDetailsResponse querySendDetailsResponse = acsClient.getAcsResponse(request);

        return querySendDetailsResponse;
    }

    public static void main(String[] args) throws ClientException, InterruptedException {
//        System.out.println(SendRegisterSms("18650021838"));
        Scanner scanneer = new Scanner(System.in);
        String phone = scanneer.nextLine();
        String code = scanneer.nextLine();
        System.out.println(phone);
        System.out.println(code);
        System.out.println(CheckRegisterSms(phone,code));

//        //发短信
//        SendSmsResponse response = sendSms("18650021838","","");
//        System.out.println("短信接口返回的数据----------------");
//        System.out.println("Code=" + response.getCode());
//        System.out.println("Message=" + response.getMessage());
//        System.out.println("RequestId=" + response.getRequestId());
//        System.out.println("BizId=" + response.getBizId());
//
//        Thread.sleep(3000L);
//
//        //查明细
//        if(response.getCode() != null && response.getCode().equals("OK")) {
//            QuerySendDetailsResponse querySendDetailsResponse = querySendDetails(response.getBizId());
//            System.out.println("短信明细查询接口返回数据----------------");
//            System.out.println("Code=" + querySendDetailsResponse.getCode());
//            System.out.println("Message=" + querySendDetailsResponse.getMessage());
//            int i = 0;
//            for(QuerySendDetailsResponse.SmsSendDetailDTO smsSendDetailDTO : querySendDetailsResponse.getSmsSendDetailDTOs())
//            {
//                System.out.println("SmsSendDetailDTO["+i+"]:");
//                System.out.println("Content=" + smsSendDetailDTO.getContent());
//                System.out.println("ErrCode=" + smsSendDetailDTO.getErrCode());
//                System.out.println("OutId=" + smsSendDetailDTO.getOutId());
//                System.out.println("PhoneNum=" + smsSendDetailDTO.getPhoneNum());
//                System.out.println("ReceiveDate=" + smsSendDetailDTO.getReceiveDate());
//                System.out.println("SendDate=" + smsSendDetailDTO.getSendDate());
//                System.out.println("SendStatus=" + smsSendDetailDTO.getSendStatus());
//                System.out.println("Template=" + smsSendDetailDTO.getTemplateCode());
//            }
//            System.out.println("TotalCount=" + querySendDetailsResponse.getTotalCount());
//            System.out.println("RequestId=" + querySendDetailsResponse.getRequestId());
//        }

    }
}
