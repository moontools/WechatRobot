package com.lincoln.framework.utils;

import com.lincoln.framework.spring.SpringContextUtil;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志处理类
 * 
 * @author zwm
 */
public abstract class FrameworkLogger {

    /** 日志操作对象 **/
    private  Logger _logger;

    private Class<?> cls;

    /**
     * 为了保持代码统一，不提供new创建对象方式
     * 
     * @param cls
     */
    private FrameworkLogger(Class<?> cls, String loggerName) {
//        PropertyConfigurator.configure(StringUtilsEx.getContextPath() + "log4j.properties");
        //分模块了,配置文件被编译在了db的jar包中
        PropertyConfigurator.configure(this.getClass().getClassLoader().getResource("log4j.properties"));
        _logger =LoggerFactory.getLogger(loggerName);
        this.cls = cls;
    }
    
    /**
     * debug日志
     * @param msg
     */
    public void debug(String msg){
        this.log(msg);
    }

    /**
     * 日志记录
     */
    public void log(String msg) {
    }

    public Class<?> getCls() {
        return cls;
    }

    public void setCls(Class<?> cls) {
        this.cls = cls;
    }

    /**
     * 异常日志
     */
    public void error(String msg) {
    }

    /**
     * 异常日志
     */
    public void error(String msg, Throwable e) {
    }

    /**
     * 获取日志操作对象
     */
    public Logger getLogger() {
        return this._logger;
    }

    /**
     * 静态方法获取日志类
     * 
     * @param cls
     * @return SocketLogger
     */
    private static FrameworkLogger initLogger(Class<?> cls, String loggerName) {
        return new FrameworkLogger(cls, loggerName) {
            
            @Override
            public void log(String msg) {
                Logger logger = this.getLogger();
                logger.debug(this.getCls().getName() + " debug:" + msg);
            }

            @Override
            public void error(String msg) {
                Logger logger = this.getLogger();
                logger.error(this.getCls().getName() + " error:" + msg);
            }

            @Override
            public void error(String msg, Throwable e) {
                Logger logger = this.getLogger();
                logger.error(this.getCls().getName() + " error:" + msg, e);
            }
            
            /**
             * 取错误信息前40个字符作为邮件标题
             * @param content
             * @return
             */
            private String getEmailTitle(String content) {
            	int titleLength = 40;
            	if (content != null && content.length() > titleLength) {
            		return content.substring(0, titleLength - 1);
            	} else {
            		return content;
            	}
            }
        };
    }

    /**
     * 静态方法获取api相关日志类
     * 
     * @param cls
     * @return SocketLogger
     */
    public static FrameworkLogger initApiLogger(Class<?> cls) {
        String loggerName = "framework.api";
        return initLogger(cls, loggerName);
    }

    /**
     * 静态方法获取微信相关日志类
     * 
     * @param cls
     * @return SocketLogger
     */
    public static FrameworkLogger initWechatLogger(Class<?> cls) {
        String loggerName = "framework.wechat";
        return initLogger(cls, loggerName);
    }
    
    /**
     * 静态方法获取支付日志类
     * 
     * @param cls
     * @return SocketLogger
     */
    public static FrameworkLogger initPayLogger(Class<?> cls) {
    	String loggerName = "framework.pay";
    	return initLogger(cls, loggerName);
    }

    /**
     * 静态方法获取framework日志类
     * 
     * @param cls
     * @return SocketLogger
     */
    public static FrameworkLogger initFrameworkLogger(Class<?> cls) {
        String loggerName = "framework.framework";
        return initLogger(cls, loggerName);
    }
}