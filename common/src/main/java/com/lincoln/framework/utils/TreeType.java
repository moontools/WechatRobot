package com.lincoln.framework.utils;

import java.util.List;

/**
 * 树型结构实现接口，继承后，通过TreeUtils可以进行自动排序分组。生成树结构
 * 
 * @author 哨子
 */
public interface TreeType<T> {

    /**
     * 根目录编号ID 0
     */
    public static final int ROOT_ELEMENT_ID = 0;

    /**
     * 返回分类编号
     * 
     * @return
     */
    public int getId();

    /**
     * 获取父类编号
     * 
     * @return
     */
    public int getParentId();

    /**
     * 获取排序值，值越小排前面
     * 
     * @return
     */
    public int getSort();
    
    /**
     * 获取所有子节点信息
     * 
     * @return 返回子节点列表
     */
    public List<T> getChildren();

    /**
     * 获取所有子节点信息
     * 
     * @param elementList
     */
    public void setChildren(List<T> elementList);

    /**
     * 节点等级
     */
    public void setLevel(int level);

    /**
     * 获取节点等级
     * 
     * @return
     */
    public int getLevel();
}
