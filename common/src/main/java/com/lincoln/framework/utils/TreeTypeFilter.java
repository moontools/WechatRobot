package com.lincoln.framework.utils;


/**
 * 提供生成tree或者扁平列表时的过滤功能，可以排除某些多余的
 * 
 * @author 哨子
 */
public interface TreeTypeFilter<T>{
    
    /**
     * 如果需要排除返回true
     * @param t
     * @return
     */
    public boolean isExclude(TreeType<T> t);
}
