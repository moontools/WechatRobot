package com.lincoln.framework.utils;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 快递100接口工具
 * Created by Lincoln on 2017/12/23.
 */
public class KuaiDi100Utils {
    protected static Logger logger = LoggerFactory.getLogger(KuaiDi100Utils.class);

    public static final String id = "d6bd77ebd3e3ec31";
    public static final String uri = "https://www.kuaidi100.com/query";

    /**
     * 查询快递
     *
     * @param mailType 快递公司类型
     * @param no       快递单号
     * @return
     */
    public static String query(int mailType, String no) {
        String com = "";
        if (mailType == 1) {
            com = "shunfeng";
        }
        if (mailType == 2) {
            com = "yuantong";
        }
        if (mailType == 3) {
            com = "shentong";
        }
        if (StringUtilsEx.isNullOrEmpty(com)) {
            return "";
        }
        return query(com, no);
    }

    /**
     * 查询快递
     *
     * @param com 快递公司名称
     * @param no  快递单号
     * @return
     */
    public static String query(String com, String no) {
        if (StringUtilsEx.isNullOrEmpty(no)) {
            return "";
        }
        return HttpUtils.sendGet(uri, "type=" + com + "&postid=" + no);
    }

    public static JSONObject parseJSON(String json){
        return JSONObject.parseObject(json);
    }

    public static void main(String[] args) {
        logger.info(query(2, "887164895482703908"));
    }
}
