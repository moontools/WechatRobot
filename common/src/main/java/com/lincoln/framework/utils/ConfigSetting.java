package com.lincoln.framework.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 系统配置类
 * 
 * @author Administrator
 */
public class ConfigSetting {

	private static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(ConfigSetting.class);

	public static String DOMAIN = "";

	/**
	 * 系统版本控制参数
	 */
	public static String SYS_VERSION;

	/**
	 * icp备案号
	 */
	public static String ICP_NUMBER = "";

	/**
	 * api校验key
	 */
	public static String API_CHECKSUM_KEY = "";

	/**
	 * 短信验证码开关
	 */

	public static boolean SMS_OPEN = true;
	static {
		loadSettingConfig();
	}

	/**
	 * 获取系统配置
	 */
	public static void loadSettingConfig() {
		logger.log("从properties配置文件中获取参数信息......start");
		try {
			PropertiesHelper.initProperties("config.properties");

			DOMAIN = PropertiesHelper.getProperty("DOMAIN");

			SYS_VERSION = PropertiesHelper.getProperty("SYS_VERSION");

			API_CHECKSUM_KEY = PropertiesHelper.getProperty("API_CHECKSUM_KEY");

			SMS_OPEN = Boolean.valueOf(PropertiesHelper.getProperty("SMS_OPEN"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
		logger.log("从properties配置文件中获取参数信息......end");
	}
}

/**
 * 读取配置文件辅助类
 * 
 * @author Administrator
 */
class PropertiesHelper {

	private static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(PropertiesHelper.class);

	/**
	 * 解析config.properties配置文件
	 */
	private static Map<String, String> properties = new HashMap<String, String>();

	/**
	 * 初始化配置文件
	 */
	public static void initProperties(String propertiesFileName) {
		try {
			logger.log(propertiesFileName);
			InputStream is = ConfigSetting.class.getClassLoader().getResourceAsStream(propertiesFileName);
			InputStreamReader isr;
			isr = new InputStreamReader(is, "utf-8");
			Properties p = new Properties();
			p.load(isr);
			Enumeration<?> nameEnum = p.propertyNames();
			while (nameEnum.hasMoreElements()) {
				String name = (String) nameEnum.nextElement();
				String value = p.getProperty(name);
				logger.log(name + "=>" + value);
				properties.put(name, value);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage(), e);
		}
	}

	/**
	 * 获取wechatSpider.properties配置文件属性值
	 * 
	 * @param propertyName
	 * @return
	 */
	public static String getProperty(String propertyName) {
		String value = properties.get(propertyName);
		if (value == null) {
			throw new RuntimeException("config.properties：【" + propertyName + "】取值失败!");
		}
		return value;
	}
}
