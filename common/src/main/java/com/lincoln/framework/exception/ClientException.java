package com.lincoln.framework.exception;

/**
 * 客户端异常，提交至前端显示，不需要发送异常邮件，一般为ajax异常
 * 
 * @author zwm
 * 
 * @version 2016-2-25 下午3:02:17
 */
public class ClientException extends Exception {
	
	private String errorType;
	
	private boolean needRefresh = false;

	public ClientException(String msg) {
		super(msg);
	}

	public ClientException(String msg, String errorType) {
		super(msg);
		this.errorType = errorType;
	}
	
	public ClientException(String msg, String errorType,boolean needRefresh) {
		super(msg);
		this.errorType = errorType;
		this.needRefresh = needRefresh;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public boolean isNeedRefresh() {
		return needRefresh;
	}

	public void setNeedRefresh(boolean needRefresh) {
		this.needRefresh = needRefresh;
	}

}
