package com.lincoln.framework.exception;

/**
 * 系统异常，需要发送异常邮件
 * 
 * @author zwm
 * 
 * @version 2016-2-25 下午3:02:17
 */
public class SystemException extends Exception {

	private String errorType;

	private boolean needRefresh = false;

	public SystemException(String msg) {
		super(msg);
	}

	public SystemException(String msg, String errorType) {
		super(msg);
		this.errorType = errorType;
	}

	public SystemException(String msg, String errorType, boolean needRefresh) {
		super(msg);
		this.errorType = errorType;
		this.needRefresh = needRefresh;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public boolean isNeedRefresh() {
		return needRefresh;
	}

	public void setNeedRefresh(boolean needRefresh) {
		this.needRefresh = needRefresh;
	}

}
