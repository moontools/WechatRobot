package com.lincoln.framework.exception;


/**
 * 文件处理异常
 * @author rubekid
 *
 * Data	2015年7月13日下午5:18:04  
 *
 */
public class FileException extends SystemException {
	public FileException(String message){
		super(message);
	}
}
