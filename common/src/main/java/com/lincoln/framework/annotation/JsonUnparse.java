package com.lincoln.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * json不处理
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonUnparse {

}
