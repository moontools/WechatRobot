package com.lincoln.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * json处理 和manytoone一起用
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonParse {

}
