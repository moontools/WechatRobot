package com.lincoln.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class Application {
    public static Map<String, User> users = new HashMap<>();

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @ResponseBody
    @RequestMapping(value = "/login", produces = "application/json")
    public int login(HttpServletRequest request) {
        String name = request.getParameter("name");
        if (name == null || "".equals(name)) {
            return 0;
        }
        String password = request.getParameter("password");
        if (password == null || "".equals(password)) {
            return 0;
        }
        User user = users.get(name);
        if(user == null){
            return 0;
        }
        if (!password.equals(user.getPassword())) {
            return 0;
        }
        if (user.getDate() == null) {
            return 0;
        }
        if (user.getDate().getTime() < new Date().getTime()) {
            return 0;
        }
        return 1;
    }

    @ResponseBody
    @RequestMapping(value = "/add", produces = "application/json")
    public int add(HttpServletRequest request) {
        String name = request.getParameter("name");
        if (name == null || "".equals(name)) {
            return 0;
        }
        String password = request.getParameter("password");
        if (password == null || "".equals(password)) {
            return 0;
        }
        User user = new User();
        String day = request.getParameter("day");
        if (day == null || "".equals(day)) {
            return 0;
        }
        int dday = 0;
        try {
            dday = Integer.parseInt(day);
        } catch (Exception e) {
            return 0;
        }
        user.setName(name);
        user.setPassword(password);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + dday);//让日期加1
        user.setDate(calendar.getTime());
        users.put(user.getName(), user);
        return 1;
    }

    public class User {
        String name;
        String password;
        Date date;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }
}
