package com.lincoln.framework.service;

import com.lincoln.framework.entity.SysMenu;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 系统菜单service
 * 
 * @author zwm
 */
@Service
@Transactional
public class SysMenuService {

	private SimpleHibernateTemplate<SysMenu, Integer> sysMenuDao;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		sysMenuDao = new SimpleHibernateTemplate<SysMenu, Integer>(sessionFactory, SysMenu.class);
	}
	
	/**
	 * 保存
	 * @param sysMenu
	 */
	public void saveSysMenu(SysMenu sysMenu) {
		if (sysMenu.getId() == 0) {
			sysMenu.setAddTime(new Date());
		}
		
		sysMenu.setUpdateTime(new Date());
		sysMenuDao.save(sysMenu);
	}
	/**
	 * 通过url取menuId
	 * @param url
	 */
	public SysMenu findMenuByUri(String url) {
		List<SysMenu> list = sysMenuDao.findBy("uri",url);
		if(list==null||list.size()==0){
			return null;
		}
		return list.get(0);
	}
	
	/**
	 * 删除
	 * @param sysMenu
	 */
	public void deleteSysMenu(SysMenu sysMenu) {
		sysMenuDao.delete(sysMenu);
	}
	
	/**
	 * 根据id查询
	 * @param id
	 * @return
	 */
	public SysMenu getSysMenuById(int id) {
		return sysMenuDao.get(id);
	}

	/**
	 * 根据父类id、菜单等级查询菜单信息
	 * 
	 * @return List<SysMenu>
	 */
	public List<SysMenu> findByParentIdAndLevel(int parentId, int level) {
		Criteria criteria = sysMenuDao.createCriteria();
		if (parentId == 0) {
			criteria.add(Restrictions.or(Restrictions.isNull("parentMenu"),Restrictions.eq("parentMenu.id", 0)));
		} else {
			criteria.add(Restrictions.eq("parentMenu.id", parentId));
		}
		if (level > -1) {
			criteria.add(Restrictions.eq("level", level));
		}
		criteria.addOrder(Order.asc("sort"));
		return sysMenuDao.findByCriteria(criteria);
	}
	
	/**
	 * 查询菜单树
	 * @return
	 */
	public List<SysMenu> findAllInTree(){
		List<SysMenu> sysMenuListOrgin = this.findByParentIdAndLevel(0, 0);
		List<SysMenu> sysMenuListFormat = new ArrayList<SysMenu>();
		for (SysMenu menuItem : sysMenuListOrgin) {
			sysMenuListFormat.add(menuItem);
			for(SysMenu childMenuItem : menuItem.getChildMenus()) {
				sysMenuListFormat.add(childMenuItem);
			}
		}
		return sysMenuListFormat;
	}
	
	/**
	 * 查询是否菜单重名
	 * @param id
	 * @param parentId
	 * @param name
	 * @return
	 */
	public boolean checkExistByName(int id, int parentId, String name){
		Criteria criteria = sysMenuDao.createCriteria();
		criteria.add(Restrictions.ne("id", id));
		criteria.add(Restrictions.eq("parentMenu.id", parentId));
		criteria.add(Restrictions.eq("name", name));
		return sysMenuDao.findByCriteria(criteria).size() > 0;
	}
	
	/**
	 * 查询是否链接重复
	 * @param id
	 * @param uri
	 * @return
	 */
	public boolean checkExistByUri(int id, String uri){
		Criteria criteria = sysMenuDao.createCriteria();
		criteria.add(Restrictions.ne("id", id));
		criteria.add(Restrictions.eq("uri", uri));
		return sysMenuDao.findByCriteria(criteria).size() > 0;
	}
}
