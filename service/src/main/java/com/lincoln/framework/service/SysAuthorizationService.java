package com.lincoln.framework.service;

import com.lincoln.framework.entity.SysAuthorization;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色菜单授权service
 * 
 * @author zwm
 */
@Service
@Transactional
public class SysAuthorizationService {

	private SimpleHibernateTemplate<SysAuthorization, Integer> sysAuthorizationDao;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		sysAuthorizationDao = new SimpleHibernateTemplate<SysAuthorization, Integer>(sessionFactory,
				SysAuthorization.class);
	}
	
	/**
	 * 删除
	 * @param sysAuthorization
	 */
	public void delete(SysAuthorization sysAuthorization) {
		sysAuthorizationDao.delete(sysAuthorization);
	}
	
	/**
	 * 删除列表
	 * @param sysAuthorizationList
	 */
	public void deleteSysAuthorizationList(List<SysAuthorization> sysAuthorizationList) {
		for (SysAuthorization sysAuthorization : sysAuthorizationList) {
			this.delete(sysAuthorization);
		}
		sysAuthorizationDao.getSession().flush();
	}

}
