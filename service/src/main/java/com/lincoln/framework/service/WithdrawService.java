package com.lincoln.framework.service;

import com.lincoln.framework.entity.Member;
import com.lincoln.framework.entity.Withdraw;
import com.lincoln.framework.entity.Withdraw;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.utils.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户充值(上分)Service
 * Created by Lincoln on 2017/10/18.
 */
@Service
@Transactional
public class WithdrawService extends BaseService<Withdraw> {
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        dao = new SimpleHibernateTemplate<>(sessionFactory, Withdraw.class);
    }

    @Autowired
    MemberService memberService;

    /**
     * 查询数据
     *
     * @param criteria
     * @param parameters
     * @return
     */
    public Criteria createCriteria(Criteria criteria, Map<String, Object> parameters) {
        if (!StringUtilsEx.isNullOrEmpty((String) parameters.get("memberId"))) {
            criteria.add(Restrictions.eq("member.id", parameters.get("memberId")));
        }
        if (!StringUtilsEx.isNullOrEmpty((String) parameters.get("score"))) {
            criteria.add(Restrictions.eq("score", parameters.get("score")));
        }
        if (!StringUtilsEx.isNullOrEmpty((String) parameters.get("status"))) {
            criteria.add(Restrictions.eq("status", parameters.get("status")));
        }
        if (!StringUtilsEx.isNullOrEmpty((String) parameters.get("source"))) {
            criteria.add(Restrictions.eq("source", parameters.get("source")));
        }
        return criteria;
    }

    /**
     * 查询当天上分情况
     */
    public List<Withdraw> getTodayWithdraw(Long memberId) {
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.ge("createTime", DateUtils.getTodayDateStart()));
        criteria.add(Restrictions.le("createTime", DateUtils.getTodayDateEnd()));
        if (memberId != null && memberId > 0) {
            criteria.add(Restrictions.eq("member.id", memberId));
        }
        criteria.add(Restrictions.eq("status", Withdraw.STATUS_YES));
        return this.findList(criteria);
    }

    public double sumTodayWithdraw(Long memberId) {
        List<Withdraw> list = getTodayWithdraw(memberId);
        double result = 0;
        for (Withdraw extract : list) {
            if (extract.getScore() != null && extract.getScore() >= 0) {
                result += extract.getScore();
            }
        }
        return result;
    }

    /**
     * 查出所有未处理的订单
     *
     * @return
     */
    public List<Withdraw> findAllNoDealWithdraw() {
        Criteria criteria = this.dao.createCriteria();
        criteria.addOrder(Order.desc("createTime"));
        criteria.add(Restrictions.eq("status", Withdraw.STATUS_WAIT));
        return this.findList(criteria);
    }

    /**
     * 批准
     *
     * @param withdrawId
     */
    public void allow(long withdrawId,String source) {
        Withdraw withdraw = getById(withdrawId);
        if (withdraw == null || withdraw.getStatus() != Withdraw.STATUS_WAIT) {
            return;
        }
        withdraw.setStatus(Withdraw.STATUS_YES);
        withdraw.setSource(source);
        this.dao.save(withdraw);
    }

    /**
     * 拒绝
     *
     * @param withdrawId
     */
    public void refuse(long withdrawId) {
        Withdraw withdraw = getById(withdrawId);
        if (withdraw == null || withdraw.getStatus() != Withdraw.STATUS_WAIT) {
            return;
        }
        withdraw.setStatus(Withdraw.STATUS_NO);
        Member member = withdraw.getMember();
        if (member == null) {
            return;
        }
        member.setScore(member.getScore() + withdraw.getScore());
        memberService.save(member);
        this.dao.save(withdraw);
    }
}
