package com.lincoln.framework.service;

import com.lincoln.framework.entity.Member;
import com.lincoln.framework.entity.Recharge;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.utils.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 用户充值(上分)Service
 * Created by Lincoln on 2017/10/18.
 */
@Service
@Transactional
public class RechargeService extends BaseService<Recharge> {
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        dao = new SimpleHibernateTemplate<>(sessionFactory, Recharge.class);
    }
    @Autowired
    MemberService memberService;

    /**
     * 查询数据
     * @param criteria
     * @param parameters
     * @return
     */
    public Criteria createCriteria(Criteria criteria, Map<String, Object> parameters) {
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("member"))){
            criteria.add(Restrictions.eq("member",parameters.get("member")));
        }
        if(parameters.get("score")!=null){
            criteria.add(Restrictions.eq("score",parameters.get("score")));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("status"))){
            criteria.add(Restrictions.eq("status",parameters.get("status")));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("pay"))){
            criteria.add(Restrictions.eq("pay",parameters.get("pay")));
        }
        return criteria;
    }

    /**
     * 查询当天上分情况
     */
    public List<Recharge> getTodayRecharge(Long memberId){
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.ge("createTime", DateUtils.getTodayDateStart()));
        criteria.add(Restrictions.le("createTime", DateUtils.getTodayDateEnd()));
        if(memberId != null && memberId>0){
            criteria.add(Restrictions.eq("member.id", memberId));
        }
        criteria.add(Restrictions.eq("status", Recharge.STATUS_YES));
        return this.findList(criteria);
    }
    public double sumTodayRecharge(Long memberId){
        List<Recharge> list = getTodayRecharge(memberId);
        double result = 0;
        for(Recharge extract:list){
            if(extract.getScore()!=null&&extract.getScore()>=0) {
                result += extract.getScore();
            }
        }
        return result;
    }

    /**
     * 查出所有未处理的订单
     * @return
     */
    public List<Recharge> findAllNoDealRecharge(){
        Criteria criteria = this.dao.createCriteria();
        criteria.addOrder(Order.desc("createTime"));
        criteria.add(Restrictions.eq("status", Recharge.STATUS_WAIT));
        return this.findList(criteria);
    }

    /**
     * 批准
     * @param rechargeId
     */
    public void allow(long rechargeId,String source){
        Recharge recharge = getById(rechargeId);
        if(recharge == null||recharge.getStatus() != Recharge.STATUS_WAIT){
            return;
        }
        recharge.setStatus(Recharge.STATUS_YES);
        recharge.setPay(source);
        Member member = recharge.getMember();
        if(member == null){
            return;
        }
        member.setScore(member.getScore() + recharge.getScore());
        memberService.save(member);
        this.dao.save(recharge);
    }

    /**
     * 拒绝
     * @param rechargeId
     */
    public void refuse(long rechargeId){
        Recharge recharge = getById(rechargeId);
        if(recharge == null||recharge.getStatus() != Recharge.STATUS_WAIT){
            return;
        }
        recharge.setStatus(Recharge.STATUS_NO);
        this.dao.save(recharge);
    }
}
