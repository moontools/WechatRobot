package com.lincoln.framework.service;

import com.lincoln.framework.entity.WxAccount;
import com.lincoln.framework.entity.WxMessage;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import com.lincoln.framework.utils.StringUtilsEx;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 分类
 * Created by Lincoln on 2017/10/18.
 */
@Service
@Transactional
public class WxAccountService extends BaseService<WxAccount>{
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        dao = new SimpleHibernateTemplate<>(sessionFactory, WxAccount.class);
    }

    public WxAccount getByAttrStatus(Long attrStatus) {
        if(attrStatus == null){
            return null;
        }
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.eq("attrStatus",attrStatus));
        List<WxAccount> list = findList(criteria);
        if(list!=null&&list.size()>0){
            return list.get(0);
        }
        return null;
    }

    public void evict(WxAccount wxAccount){
        this.dao.evict(wxAccount);
    }
}
