package com.lincoln.framework.service;

import com.lincoln.framework.entity.SysRole;
import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 系统角色service
 * 
 * @author zwm
 */
@Service
@Transactional
public class SysRoleService {

	private SimpleHibernateTemplate<SysRole, Integer> sysRoleDao;

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		sysRoleDao = new SimpleHibernateTemplate<SysRole, Integer>(sessionFactory, SysRole.class);
	}
	
	/**
	 * 保存
	 * @param sysRole
	 */
	public void saveSysRole(SysRole sysRole) {
		sysRoleDao.save(sysRole);
		sysRoleDao.getSession().flush();
	}
	
	/**
	 * 删除
	 * @param sysRole
	 */
	public void deleteSysRole(SysRole sysRole) {
		sysRoleDao.delete(sysRole);
	}

	/**
	 * 查询所有系统角色
	 */
	public List<SysRole> findAll() {
		return sysRoleDao.findAll();
	}
	
	/**
	 * 根据id获取系统角色信息
	 * @param id
	 * @return
	 */
	public SysRole getSysRoleById(int id) {
		return sysRoleDao.get(id);
	}
	
	/**
	 * 通过名称获取系统角色
	 * @param name
	 * @return
	 */
	public SysRole getSysRoleByName(String name) {
		return sysRoleDao.findByUnique("name", name);
	}
	
	/**
	 * 查询分页数据
	 * @param page
	 * @return
	 */
	public Page<SysRole> findPage(Page<SysRole> page) {
		return sysRoleDao.findAll(page);
	}
	
	/**
	 * 判断是否名称重复
	 * @param id
	 * @param name
	 * @return
	 */
	public boolean checkExistByName(int id, String name) {
		Criteria criteria = sysRoleDao.createCriteria();
		criteria.add(Restrictions.ne("id", id));
		criteria.add(Restrictions.eq("name", name));
		return sysRoleDao.findByCriteria(criteria).size() > 0;
	}
}
