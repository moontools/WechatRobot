package com.lincoln.framework.service;

import com.lincoln.framework.entity.Game;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 游戏
 * Created by Lincoln on 2017/10/18.
 */
@Service
@Transactional
public class GameService extends BaseService<Game> {
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        dao = new SimpleHibernateTemplate<>(sessionFactory, Game.class);
    }

    /**
     * 查询数据
     *
     * @param criteria
     * @param parameters
     * @return
     */
    public Criteria createCriteria(Criteria criteria, Map<String, Object> parameters) {
        if (parameters.get("gameNo") != null) {
            criteria.add(Restrictions.eq("gameNo", parameters.get("gameNo")));
        }
        if (parameters.get("win") != null) {
            criteria.add(Restrictions.eq("win", parameters.get("win")));
        }
        return criteria;
    }

    /**
     * 根据gameNo查询
     *
     * @param gameNo
     * @return
     */
    public Game getGameByNo(long gameNo) {
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.eq("gameNo", gameNo));
        List<Game> list = this.dao.findByCriteria(criteria);
        if (list == null || list.size() <= 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * 根据gameNo查询
     *
     * @param gameNo
     * @param isOpen 是否已经开奖
     * @return
     */
    public Game getGameByNo(long gameNo,boolean isOpen) {
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.eq("gameNo", gameNo));
        if(!isOpen){
            criteria.add(Restrictions.isNull("result1"));
            criteria.add(Restrictions.isNull("result2"));
            criteria.add(Restrictions.isNull("result3"));
            criteria.add(Restrictions.isNull("endTime"));
        }
        List<Game> list = this.dao.findByCriteria(criteria);
        if (list == null || list.size() <= 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public List<Game> findAll() {
        Criteria criteria = this.dao.createCriteria();
        criteria.addOrder(Order.desc("createTime"));
        return findList(criteria);
    }

    /**
     * 删除所有未结束的游戏
     */
    public void deleteAllNoEnd() {
        Criteria criteria = this.dao.createCriteria();
        criteria.add(Restrictions.isNull("endTime"));
        List<Game> list = findList(criteria);
        for (Game game : list) {
            delete(game);
        }
        criteria = this.dao.createCriteria();
        criteria.add(Restrictions.or(Restrictions.isNull("result1"),Restrictions.isNull("result2"),Restrictions.isNull("result3")));
        list = findList(criteria);
        for (Game game : list) {
            delete(game);
        }
    }

    /**
     * 获取最新的游戏
     *
     * @return
     */
    public Game getNewestGame() {
        Criteria criteria = this.dao.createCriteria();
        criteria.addOrder(Order.desc("createTime"));
        List<Game> list = this.dao.findByCriteria(criteria);
        if (list == null || list.size() <= 0) {
            return null;
        } else {
            return list.get(0);
        }
    }

    /**
     * 获取最近15场有数据的
     *
     * @return
     */
    public List<Game> findNearlyGame() {
        Criteria criteria = this.dao.createCriteria();
        criteria.addOrder(Order.desc("createTime"));
        criteria.add(Restrictions.isNotNull("result1"));
        criteria.add(Restrictions.isNotNull("result2"));
        criteria.add(Restrictions.isNotNull("result3"));
        List<Game> list = this.dao.findByCriteria(criteria);
        if (list.size() > 15) {
            return list.subList(0, 15);
        }
        return list;
    }
}
