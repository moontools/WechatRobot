package com.lincoln.framework.service;

import com.lincoln.framework.entity.WxMessage;
import com.lincoln.framework.hibernate.SimpleHibernateTemplate;
import com.lincoln.framework.utils.StringUtilsEx;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * 分类
 * Created by Lincoln on 2017/10/18.
 */
@Service
@Transactional
public class WxMessageService extends BaseService<WxMessage>{
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        dao = new SimpleHibernateTemplate<>(sessionFactory, WxMessage.class);
    }

    public Criteria createCriteria(Criteria criteria, Map<String, Object> parameters) {
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("fromUserName"))){
            criteria.add(Restrictions.like("fromUserName","%"+parameters.get("fromUserName")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("fromNickName"))){
            criteria.add(Restrictions.like("fromNickName","%"+parameters.get("fromNickName")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("mineUserName"))){
            criteria.add(Restrictions.like("mineUserName","%"+parameters.get("mineUserName")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("mineNickName"))){
            criteria.add(Restrictions.like("mineNickName","%"+parameters.get("mineNickName")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("toUserName"))){
            criteria.add(Restrictions.like("toUserName","%"+parameters.get("toUserName")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("content"))){
            criteria.add(Restrictions.like("content","%"+parameters.get("content")+"%"));
        }
        if(!StringUtilsEx.isNullOrEmpty((String)parameters.get("text"))) {
            criteria.add(Restrictions.like("text", "%" + parameters.get("text") + "%"));
        }
        return criteria;
    }
}
