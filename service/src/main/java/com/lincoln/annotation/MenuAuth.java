package com.lincoln.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 菜单校验
 * 
 * @author zwm
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MenuAuth {
	
	/**
	 * 不校验菜单按钮权限
	 */
	public static final int MENU_BUTTON_NO = -1;

	/**
	 * 菜单按钮1
	 */
	public static final int MENU_BUTTON_1 = 1;

	/**
	 * 菜单按钮2
	 */
	public static final int MENU_BUTTON_2 = 2;

	/**
	 * 菜单按钮3
	 */
	public static final int MENU_BUTTON_3 = 3;

	/**
	 * 菜单按钮4
	 */
	public static final int MENU_BUTTON_4 = 4;

	/**
	 * 菜单按钮5
	 */
	public static final int MENU_BUTTON_5 = 5;

	/**
	 * 菜单按钮6
	 */
	public static final int MENU_BUTTON_6 = 6;

	/**
	 * 菜单按钮7
	 */
	public static final int MENU_BUTTON_7 = 7;

	/**
	 * 菜单按钮8
	 */
	public static final int MENU_BUTTON_8 = 8;

	/**
	 * 菜单按钮9
	 */
	public static final int MENU_BUTTON_9 = 9;

	/**
	 * 菜单按钮10
	 */
	public static final int MENU_BUTTON_10 = 10;

	/**
	 * 菜单按钮11
	 */
	public static final int MENU_BUTTON_11 = 11;

	/**
	 * 菜单按钮12
	 */
	public static final int MENU_BUTTON_12 = 12;

	/**
	 * 菜单按钮13
	 */
	public static final int MENU_BUTTON_13 = 13;

	/**
	 * 菜单按钮14
	 */
	public static final int MENU_BUTTON_14 = 14;

	/**
	 * 菜单按钮15
	 */
	public static final int MENU_BUTTON_15 = 15;

	/**
	 * 菜单按钮16
	 */
	public static final int MENU_BUTTON_16 = 16;

	/**
	 * 菜单按钮17
	 */
	public static final int MENU_BUTTON_17 = 17;

	/**
	 * 菜单按钮18
	 */
	public static final int MENU_BUTTON_18 = 18;

	/**
	 * 菜单按钮19
	 */
	public static final int MENU_BUTTON_19 = 19;

	/**
	 * 菜单按钮20
	 */
	public static final int MENU_BUTTON_20 = 20;

	/**
	 * 需要校验的按钮
	 * 
	 * @return int
	 */
	int buttonRight() default MENU_BUTTON_NO;

}
