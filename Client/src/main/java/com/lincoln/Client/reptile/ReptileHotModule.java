package com.lincoln.Client.reptile;

import com.lincoln.Client.context.SettingContext;
import com.lincoln.Client.utils.HotModule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReptileHotModule extends HotModule {
    /**
     * 模块构造函数结构
     *
     * @return
     */
    public Class[] getClassDefine() {
        return new Class[]{};
    }

    @Override
    public boolean filter(String className) {
        return className.indexOf("BaseReptile.class") == -1;
    }

    @Override
    public Map<String, Class> loadDefaultJar() {
        Map<String, Class> map = new HashMap<>();
        List<String> jars = readAllFile(getPath());
        for (String path : jars) {
            map.putAll(loadJar(path, getPackageName()));
        }
        return map;
    }

    /**
     * 获取cmd扩展jar包的位置
     *
     * @return
     */
    public static String getPath() {
        // TODO: 2018/4/19 通过配置方式或者读取同运行目录下的,完成后再做
        return SettingContext.getMainDir() + "reptiles/";
    }

    public static String getPackageName() {
        return "com.lincoln.Client.reptile";
    }
}
