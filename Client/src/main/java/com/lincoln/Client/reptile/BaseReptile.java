package com.lincoln.Client.reptile;

import java.util.HashMap;
import java.util.Map;

/**
 * 基本爬虫
 */
public abstract class BaseReptile {
    //结果集
    private Map<String, Object> result = new HashMap<>();

    /**
     * 获取结果
     *
     * @param key
     * @param <T>
     * @return
     */
    public <T> T getResult(String key) {
        return (T) result.get(key);
    }

    public Map<String, Object> getResult() {
        return result;
    }

    /**
     * 设置结果
     * @param key
     * @param object
     */
    public void setResult(String key, Object object) {
        result.put(key, object);
    }
    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    /**
     * 爬虫实现
     */
    public abstract void reptile();
}
