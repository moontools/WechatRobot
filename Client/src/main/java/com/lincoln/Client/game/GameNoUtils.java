package com.lincoln.Client.game;

import java.util.Date;

public class GameNoUtils {
    /**
     * 获得当前北京28的gameNo
     */
    public static long getBeiJingCurrGameNo() {
        long time = new Date().getTime();
        long gameNo = time / (5 * 60 * 1000) - 4199123;
        return gameNo;
    }

    /**
     * 获得当前北京28的剩余开奖时间
     */
    public static long getBeiJingCurrGameLeftTime() {
        long time = new Date().getTime();
        long gameNo = time % (5 * 60 * 1000);
        return gameNo;
    }
}
