package com.lincoln.Client.Robot;

import com.google.zxing.NotFoundException;
import com.lincoln.Client.context.FrameContext;
import com.lincoln.Client.context.RobotContext;
import com.lincoln.wechat.WeChatBot;
import com.lincoln.wechat.api.constant.Config;

public class ThreadRobot extends WeChatBot implements Runnable {
    public String key;
    public ThreadRobot(String key,Config config) {
        super(config);
        this.key = key;
    }

    @Override
    public void run() {
        try {
            this.start();
        }catch (NotFoundException e){
            e.printStackTrace();
            FrameContext.getLoginFrame().dispose();
        }catch (NullPointerException e){
            throw e;
        } catch(Exception e){
            e.printStackTrace();
            this.setRunning(false);
        }
    }
    /**
     * 启动后主线程干的事，子类可重写
     */
    public void other(){
        while (true){
            if(!this.isRunning()){
                System.out.println("机器人下线");
                RobotContext.removeRobot(key);
                break;
            }else{
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
