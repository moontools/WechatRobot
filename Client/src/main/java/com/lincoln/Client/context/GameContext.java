package com.lincoln.Client.context;

import com.lincoln.Client.game.Beijing28Game;
import com.lincoln.framework.entity.*;
import com.lincoln.framework.service.BettingService;
import com.lincoln.framework.service.GameService;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.spring.SpringContextUtil;
import com.lincoln.wechat.utils.StringUtils;

import javax.swing.*;
import java.util.*;

//游戏上下文
public class GameContext {

    //当前游戏期数
    public static Beijing28Game currentGame;

    //当前下注数
    public static int bettingNum = 0;
    //下一次开奖时间
    public static Date nextDate;
    //是否允许下注
    public static boolean canBetting = false;
    public static boolean beforeStopBetting = false;//停猜前状态位
    //开关
    public static boolean canPlay = false;

    public static boolean autoStopBetting = true;//自动停猜

    public static boolean isBeiJingBeiYong = false;//北京28备用

    /**
     * 开始新游戏
     */
    public static void start() {
        init();
        //直接开始
        currentGame = new Beijing28Game();
        canBetting = true;
        canPlay = true;
        autoStopBetting = true;
        currentGame.start();
        FrameContext.main.mainNorthPanel.sysRun.setText("系统关闭");

    }

    /**
     * 系统关闭
     */
    public static void stop() {
        canPlay = false;
        canBetting = false;
        FrameContext.main.mainNorthPanel.sysRun.setText("系统启动");
    }

    /**
     * 初始化
     */
    public static void init() {
        BettingService bettingService = SpringContextUtil.getBean(BettingService.class);
        //在数据库中找到所有未处理的下注,并将其设为未中奖
        bettingService.unWinAllNoDeal();
    }

    /**
     * 停猜前XX(设置值)秒执行
     */
    public static void beforeStopBetting() {
        if (beforeStopBetting && canBetting) {
            beforeStopBetting = false;
            int beforeStopTime = Integer.parseInt(SettingContext.getInofoSetting().getProperty("tingcai.kaijiangqian", "0"));//为0表示不执行
            //发送停猜前XX秒提示文本
            String tingcai_qian_text_ = SettingContext.getInofoSetting().getProperty("tingcai.qian.text", "");
            if (beforeStopTime > 0 && tingcai_qian_text_ != null && !tingcai_qian_text_.equals("")) {
                RobotContext.sendMsg(dealMsg(tingcai_qian_text_));
            }
        }
    }

    /**
     * 停止下注(停猜)
     */
    public static void stopBetting() {
        if (canBetting) {
            canBetting = false;
            //发送开奖前XX秒提示
            String tingcai_kaijiangqian_ = SettingContext.getInofoSetting().getProperty("tingcai.kaijiangqian.text", "");
            if (tingcai_kaijiangqian_ != null && !tingcai_kaijiangqian_.equals("")) {
                RobotContext.sendMsg(dealMsg(tingcai_kaijiangqian_));
            }
            sendStopBettingMsg();
        }
    }

    /**
     * 发送停猜后提示
     */
    public static void sendStopBettingMsg() {
        String tingcai_tingcaihou_text_ = SettingContext.getInofoSetting().getProperty("tingcai.tingcaihou.text", "");
        boolean tingcai_tingcaihou_ = Boolean.valueOf(SettingContext.getInofoSetting().getProperty("tingcai.tingcaihou", "false"));
        if (tingcai_tingcaihou_ && tingcai_tingcaihou_text_ != null && !tingcai_tingcaihou_text_.equals("")) {
            RobotContext.sendMsg(dealMsg(tingcai_tingcaihou_text_));
        }
    }

    /**
     * 手动停猜
     */
    public static void doStopBetting() {
        if (canBetting) {
            canBetting = false;
            autoStopBetting = false;
            sendStopBettingMsg();
        }
    }

    /**
     * 开奖
     *
     * @param result1
     * @param result2
     * @param result3
     */
    public static void kaijiang(int result1, int result2, int result3) {
        currentGame.kaijiang(result1, result2, result3);
    }

    /**
     * 开奖前0秒执行

     */
    public static void beforeOpenGame() {
        String kaijiang_qian_ = SettingContext.getInofoSetting().getProperty("kaijiang.qian", "");
        if (kaijiang_qian_ != null && !kaijiang_qian_.equals("")) {
            RobotContext.sendMsg(dealMsg(kaijiang_qian_));
        }
    }

    /**
     * 开奖信息
     */
    public static void openGame() {
        String kaijiang_info_ = SettingContext.getInofoSetting().getProperty("kaijiang.info", "");
        if (kaijiang_info_ != null && !kaijiang_info_.equals("")) {
            RobotContext.sendMsg(dealMsg(kaijiang_info_));
        }
    }

    /**
     * 发送信息设置-积分
     */
    public static void sendMemberScoreInfo() {
        //发送积分列表
        try {
            String score_text = SettingContext.getInofoSetting().getProperty("score.text", "");
            if (!"".equals(score_text)) {
                RobotContext.sendMsg(GameContext.dealMsg(score_text));
            }
        } catch (Exception e1) {
            JOptionPane.showMessageDialog(new JFrame().getContentPane(), "请检查积分设置中积分设置！", "系统信息", JOptionPane.ERROR_MESSAGE);
        }
        boolean score_auto = SettingContext.getInfoSettingProperties("score.auto", false);
        if (!score_auto) {
            return;
        }
        RobotContext.sendMsg(GameContext.dealMsg("{玩家列表}"));
    }

    /**
     * 处理占位符
     *
     * @param msg
     * @return
     */
    public static String dealMsg(String msg) {
        BettingService bettingService = SpringContextUtil.getBean(BettingService.class);
        GameService gameService = SpringContextUtil.getBean(GameService.class);
        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
        if (msg.indexOf("{游戏期号}") != -1) {
            long result = GameContext.getGame().getGameNo();
            msg = replaceAll(msg, "{游戏期号}", String.valueOf(result));
        }
        if (msg.indexOf("{开奖结果1}") != -1) {
            int result = GameContext.getGame().getResult1();
            msg = replaceAll(msg, "{开奖结果1}", String.valueOf(result));
        }
        if (msg.indexOf("{开奖结果2}") != -1) {
            int result = GameContext.getGame().getResult2();
            msg = replaceAll(msg, "{开奖结果2}", String.valueOf(result));
        }
        if (msg.indexOf("{开奖结果3}") != -1) {
            int result = GameContext.getGame().getResult3();
            msg = replaceAll(msg, "{开奖结果3}", String.valueOf(result));
        }
        if (msg.indexOf("{开奖结果求和}") != -1) {
            int result = GameContext.getGame().getResult1() + GameContext.getGame().getResult2() + GameContext.getGame().getResult3();
            msg = replaceAll(msg, "{开奖结果求和}", String.valueOf(result));
        }
        if (msg.indexOf("{下注情况}") != -1) {
            List<Betting> bettings = bettingService.queryBettingByGame(GameContext.getGame().getId());
            String result = "\n";
            for (Betting betting : bettings) {
                Member member = betting.getMember();
                result += "@" + member.getNickName() + "--" + betting.getContent() + "--" + betting.getScore() + "\n";
            }
            msg = replaceAll(msg, "{下注情况}", result);
        }
        if (msg.indexOf("{在线玩家列表}") != -1) {
            List<Betting> bettings = bettingService.queryBettingByGame(GameContext.getGame().getId());
            Map<Long, Member> map = new HashMap<>();
            for (Betting betting : bettings) {
                Member member = betting.getMember();
                map.put(member.getId(), member);
            }
            String result = "";
            for (Member member : map.values()) {
                result += "@" + member.getNickName() + "--" + member.getMoney() + "\n";
            }
            msg = replaceAll(msg, "{在线玩家列表}", result);
        }
        if (msg.indexOf("{玩家列表}") != -1) {
            List<Member> members = memberService.findAll();
            String result = "";
            for (Member member : members) {
                if (member.getStatus() == Member.STATUS_BAN || member.getStatus() == Member.STATUS_BOT_BAN) {
                    continue;
                }
                result += "@" + member.getNickName() + "--" + member.getMoney() + "\n";
            }
            msg = replaceAll(msg, "{玩家列表}", result);
        }
        if (msg.indexOf("{开奖结果}") != -1) {
            List<Betting> bettings = bettingService.queryBettingByGame(GameContext.getGame().getId());
            String result = "\n";
            for (Betting betting : bettings) {
                if (betting.getResult() == Betting.RESULT_WIN) {
                    Member member = betting.getMember();
                    result += "@" + member.getNickName() + "--" + betting.getWin() + "\n";
                }
            }
            msg = replaceAll(msg, "{开奖结果}", result);
        }
        if (msg.indexOf("{历史数据}") != -1) {
            List<Game> games = gameService.findNearlyGame();
            String result = "";
            for (Game game : games) {
                result += " " + (game.getResult1() + game.getResult2() + game.getResult3());
            }
            msg = replaceAll(msg, "{历史数据}", result);
        }
        if (msg.indexOf("{在线人数}") != -1) {
            String result = String.valueOf(GameContext.bettingNum);
            msg = replaceAll(msg, "{在线人数}", result);
        }
        if (msg.indexOf("{开奖倒计时}") != -1) {
            String result = String.valueOf(GameContext.currentGame.timeLeft);
            msg = replaceAll(msg, "{开奖倒计时}", result);
        }
        if (msg.indexOf("{在线人数}") != -1) {
            List<Betting> bettings = bettingService.queryBettingByGame(GameContext.getGame().getId());
            Map<Long, Member> map = new HashMap<>();
            for (Betting betting : bettings) {
                Member member = betting.getMember();
                map.put(member.getId(), member);
            }
            String result = String.valueOf(map.size());
            msg = replaceAll(msg, "{在线人数}", result);
        }
        if (msg.indexOf("{本期赔率}") != -1) {
            Map<String, Double> map = dealResult(GameContext.currentGame.game.getResult1(), GameContext.currentGame.game.getResult2(), GameContext.currentGame.game.getResult3());
            String result = "";
            for (String c : map.keySet()) {
                result += c + ":" + map.get(c) + "\n";
            }
            msg = replaceAll(msg, "{本期赔率}", result);
        }
        // TODO: 2018/4/26 11111
        return msg;
    }

    /**
     * 判断是不是豹子
     *
     * @param result1
     * @param result2
     * @param result3
     * @return
     */
    public static boolean isBaozi(int result1, int result2, int result3) {
        if (result1 == result2 && result2 == result3) {
            return true;
        }
        return false;
    }

    /**
     * 判断是不是对子
     *
     * @param result1
     * @param result2
     * @param result3
     * @return
     */
    public static boolean isDuizi(int result1, int result2, int result3) {
        if ((result1 == result2 || result2 == result3 || result1 == result3) && isBaozi(result1, result2, result3) == false) {
            return true;
        }
        return false;
    }

    /**
     * 判断是不是顺子
     *
     * @param result1
     * @param result2
     * @param result3
     * @return
     */
    public static boolean isShunzi(int result1, int result2, int result3) {
        int[] a = new int[3];
        a[0] = result1;
        a[1] = result2;
        a[2] = result3;
        a = bubbleSort(a, a.length);
        result1 = a[0];
        result2 = a[1];
        result3 = a[2];
        if (result1 == 0 && result2 == 1 && result3 == 2) {
            return false;
        }
        if (result2 - result1 == 1 && result3 - result2 == 1) {
            return true;
        }
        return false;
    }

    /**
     * 处理中奖结果(传入开奖结果,然后处理得到开奖结果)
     *
     * @param result1
     * @param result2
     * @param result3
     */
    public static Map<String, Double> dealResult(int result1, int result2, int result3) {
        int sum = result1 + result2 + result3;
        boolean baozi = isBaozi(result1, result2, result3);
        boolean duizi = isDuizi(result1, result2, result3);
        boolean shunzi = isShunzi(result1, result2, result3);
        double daxiaodanshuang = SettingContext.getInfoSettingProperties("daxiaodanshuang.peilv.text", 1);//大小单双赔率

        Map<String, Double> map = new HashMap<>();
        List<String> result = new ArrayList<>();
        if (sum >= 0 && sum <= 13) {
            result.add("小");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("小") == null) {
                //这里不需要判断总注
                map.put("小", daxiaodanshuang);
            }
        }
        if (sum >= 14 && sum <= 27) {
            result.add("大");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("大") == null) {
                //这里不需要判断总注
                map.put("大", daxiaodanshuang);
            }
        }
        if (sum >= 22 && sum <= 27) {
            result.add("极大");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("极大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("极大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("极大", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("极大") == null) {
                //这里不需要判断总注
                map.put("极大", SettingContext.getInfoSettingProperties("daxiaodanshuang.jida", 1));
            }
        }
        if (sum >= 0 && sum <= 5) {
            result.add("极小");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("极小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("极小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("极小", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("极小") == null) {
                //这里不需要判断总注
                map.put("极小", SettingContext.getInfoSettingProperties("daxiaodanshuang.jixiao", 1));
            }
        }
        if (sum % 2 == 1) {
            result.add("单");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("单", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("单", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("单", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("单") == null) {
                //这里不需要判断总注
                map.put("单", daxiaodanshuang);
            }
        }
        if (sum % 2 == 0) {
            result.add("双");
            if (baozi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.check", false)) {
                map.put("双", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.check", false)) {
                map.put("双", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.check", false)) {
                map.put("双", SettingContext.getInfoSettingProperties("daxiaodanshuang.right.shunzi.peilv", 1));
            }
            if (map.get("双") == null) {
                //这里不需要判断总注
                map.put("双", daxiaodanshuang);
            }
        }
        if (sum >= 0 && sum <= 13 && sum % 2 == 1) {
            result.add("小单");
            if (baozi && SettingContext.getInfoSettingProperties("zuhe.baozi.check", false)) {
                map.put("小单", SettingContext.getInfoSettingProperties("zuhe.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("zuhe.duizi.check", false)) {
                map.put("小单", SettingContext.getInfoSettingProperties("zuhe.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("zuhe.shunzi.check", false)) {
                map.put("小单", SettingContext.getInfoSettingProperties("zuhe.shunzi.peilv", 1));
            }
            if (map.get("小单") == null) {
                //这里不需要判断总注
                if (SettingContext.getInfoSettingProperties("zuhe.suanfa1.check", false)) {//使用算法1
                    if ((sum == 13 || sum == 14) && SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.check", false) && (sum == 13 || sum == 14)) {//配置了13 14
                        map.put("小单", SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.text", 1));
                    } else {
                        map.put("小单", SettingContext.getInfoSettingProperties("zuhe.suanfa1.tongyi", 1));
                    }
                } else if (SettingContext.getInfoSettingProperties("zuhe.suanfa2.check", false)) {//使用算法2
                    map.put("小单", SettingContext.getInfoSettingProperties("zuhe.suanfa2.1", 1));
                } else {//没配置
                    map.put("小单", 1.0);
                }
            }
        }
        if (sum >= 0 && sum <= 13 && sum % 2 == 0) {
            result.add("小双");
            if (baozi && SettingContext.getInfoSettingProperties("zuhe.baozi.check", false)) {
                map.put("小双", SettingContext.getInfoSettingProperties("zuhe.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("zuhe.duizi.check", false)) {
                map.put("小双", SettingContext.getInfoSettingProperties("zuhe.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("zuhe.shunzi.check", false)) {
                map.put("小双", SettingContext.getInfoSettingProperties("zuhe.shunzi.peilv", 1));
            }
            if (map.get("小双") == null) {
                //这里不需要判断总注
                if (SettingContext.getInfoSettingProperties("zuhe.suanfa1.check", false)) {//使用算法1
                    if ((sum == 13 || sum == 14) && SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.check", false)) {//配置了13 14
                        map.put("小双", SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.text", 1));
                    } else {
                        map.put("小双", SettingContext.getInfoSettingProperties("zuhe.suanfa1.tongyi", 1));
                    }
                } else if (SettingContext.getInfoSettingProperties("zuhe.suanfa2.check", false)) {//使用算法2
                    map.put("小双", SettingContext.getInfoSettingProperties("zuhe.suanfa2.2", 1));
                } else {//没配置
                    map.put("小双", 1.0);
                }
            }
        }
        if (sum >= 14 && sum <= 27 && sum % 2 == 1) {
            result.add("大单");
            if (baozi && SettingContext.getInfoSettingProperties("zuhe.baozi.check", false)) {
                map.put("大单", SettingContext.getInfoSettingProperties("zuhe.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("zuhe.duizi.check", false)) {
                map.put("大单", SettingContext.getInfoSettingProperties("zuhe.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("zuhe.shunzi.check", false)) {
                map.put("大单", SettingContext.getInfoSettingProperties("zuhe.shunzi.peilv", 1));
            }
            if (map.get("大单") == null) {
                //这里不需要判断总注
                if (SettingContext.getInfoSettingProperties("zuhe.suanfa1.check", false)) {//使用算法1
                    if ((sum == 13 || sum == 14) && SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.check", false)) {//配置了13 14
                        map.put("大单", SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.text", 1));
                    } else {
                        map.put("大单", SettingContext.getInfoSettingProperties("zuhe.suanfa1.tongyi", 1));
                    }
                } else if (SettingContext.getInfoSettingProperties("zuhe.suanfa2.check", false)) {//使用算法2
                    map.put("大单", SettingContext.getInfoSettingProperties("zuhe.suanfa2.2", 1));
                } else {//没配置
                    map.put("大单", 1.0);
                }
            }
        }
        if (sum >= 14 && sum <= 27 && sum % 2 == 0) {
            result.add("大双");
            if (baozi && SettingContext.getInfoSettingProperties("zuhe.baozi.check", false)) {
                map.put("大双", SettingContext.getInfoSettingProperties("zuhe.baozi.peilv", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("zuhe.duizi.check", false)) {
                map.put("大双", SettingContext.getInfoSettingProperties("zuhe.duizi.peilv", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("zuhe.shunzi.check", false)) {
                map.put("大双", SettingContext.getInfoSettingProperties("zuhe.shunzi.peilv", 1));
            }
            if (map.get("大双") == null) {
                //这里不需要判断总注
                if (SettingContext.getInfoSettingProperties("zuhe.suanfa1.check", false)) {//使用算法1
                    if ((sum == 13 || sum == 14) && SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.check", false)) {//配置了13 14
                        map.put("大双", SettingContext.getInfoSettingProperties("zuhe.suanfa1.1314.text", 1));
                    } else {
                        map.put("大双", SettingContext.getInfoSettingProperties("zuhe.suanfa1.tongyi", 1));
                    }
                } else if (SettingContext.getInfoSettingProperties("zuhe.suanfa2.check", false)) {//使用算法2
                    map.put("大双", SettingContext.getInfoSettingProperties("zuhe.suanfa2.1", 1));
                } else {//没配置
                    map.put("大双", 1.0);
                }
            }
        }
        if (SettingContext.getInfoSettingProperties("dandian.tongyi.check", false)) {//统一配置
            if (baozi && SettingContext.getInfoSettingProperties("dandian.baozi.check", false)) {//配置了豹子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("dandian.baozi.text", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("dandian.duizi.check", false)) {//配置了对子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("duizi.duizi.text", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("dandian.shunzi.check", false)) {//配置了顺子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("dandian.shunzi.text", 1));
            }
        } else if (SettingContext.getInfoSettingProperties("dandian.dandu.check", false)) {//单独配置
            String peilvstr = SettingContext.getInfoSettingProperties("dandian.dandu.text", "");
            try {
                String[] peilvs = peilvstr.split("\n");
                for (String peilv : peilvs) {
                    String[] kv = peilv.split("=");
                    if (Integer.parseInt(kv[0]) == sum) {
                        map.put("特" + sum, Double.parseDouble(kv[1]));
                    }
                }
            } catch (Exception e) {
                map.put("特" + sum, 1.0);
            }
            if (baozi && SettingContext.getInfoSettingProperties("dandian.baozi.check", false)) {//配置了豹子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("dandian.baozi.text", 1));
            }
            if (duizi && SettingContext.getInfoSettingProperties("dandian.duizi.check", false)) {//配置了对子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("duizi.duizi.text", 1));
            }
            if (shunzi && SettingContext.getInfoSettingProperties("dandian.shunzi.check", false)) {//配置了顺子
                map.put("特" + sum, SettingContext.getInfoSettingProperties("dandian.shunzi.text", 1));
            }
        } else {
            map.put("特" + sum, 1.0);
        }
        return map;
    }

    //冒泡
    public static int[] bubbleSort(int[] a, int n) {
        int i, j;

        for (i = 0; i < n; i++) {//表示n次排序过程。
            for (j = 1; j < n - i; j++) {
                if (a[j - 1] > a[j]) {//前面的数字大于后面的数字就交换
                    //交换a[j-1]和a[j]
                    int temp;
                    temp = a[j - 1];
                    a[j - 1] = a[j];
                    a[j] = temp;
                }
            }
        }
        return a;
    }

    /**
     * 替换所有
     *
     * @param str
     * @param a
     * @param b
     * @return
     */
    public static String replaceAll(String str, String a, String b) {
        while (str.indexOf(a) != -1) {
            str = str.replace(a, b);
        }
        return str;
    }

    /**
     * 获取当前游戏期数
     *
     * @return
     */
    public static Game getGame() {
        if (currentGame != null) {
            return currentGame.game;
        }
        return null;
    }

    /**
     * 用户批量下注
     *
     * @param memberId 用户id
     * @param gameId   游戏id
     * @param contents 下注内容
     * @param scores   分数
     * @return
     */
    public static String doBettings(long memberId, long gameId, List<String> contents, List<Double> scores) {
        if (contents == null || scores == null || contents.size() == 0 || scores.size() == 0 || contents.size() != scores.size()) {
            return "系统错误";
        }
        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
        GameService gameService = SpringContextUtil.getBean(GameService.class);
        BettingService bettingService = SpringContextUtil.getBean(BettingService.class);
        Member member = memberService.getById(memberId);
        Game game = gameService.getById(gameId);
        List<Betting> bettings = bettingService.queryBettingByGameAndMember(game.getId(), member.getId());
        double allScore0 = 0;
        for (double score : scores) {
            allScore0 += score;
        }
        try {
            if (game.getId() != GameContext.currentGame.game.getId()) {
                throw new Exception("下注超时");
            }
        } catch (Exception e) {
            return "下注超时";
        }
        if (member.getMoney() < allScore0) {
            return "余额不足!";
        }
        List<String> allContents = new ArrayList<>();
        List<String> allTe = new ArrayList<>();
        for (String content : contents) {
            if (StringUtils.isNotEmpty(content)) {
                allContents.add(content);
                if (content.startsWith("特")) {
                    boolean flag = true;
                    for (String te : allTe) {
                        if (te.equals(content)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        allTe.add(content);
                    }
                }
            } else {
                return "指令错误!";
            }
        }
        double allScore = allScore0;
        for (Betting betting : bettings) {
            allContents.add(betting.getContent());
            allScore += betting.getScore();
            if (betting.getContent().startsWith("特")) {
                boolean flag = true;
                for (String te : allTe) {
                    if (te.equals(betting.getContent())) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    allTe.add(betting.getContent());
                }
            }
        }
        boolean dashuang = false;//大双
        boolean dadan = false;//大单
        boolean xiaoshuang = false;//小双
        boolean xiaodan = false;//小单
        boolean da = false;//大
        boolean xiao = false;//小
        boolean dan = false;//单
        boolean shuang = false;//双
        for (String content : allContents) {
            if (content.equals("大双")) {
                dashuang = true;
            }
            if (content.equals("大单")) {
                dadan = true;
            }
            if (content.equals("小双")) {
                xiaoshuang = true;
            }
            if (content.equals("小单")) {
                xiaodan = true;
            }
            if (content.equals("大")) {
                da = true;
            }
            if (content.equals("小")) {
                xiao = true;
            }
            if (content.equals("单")) {
                dan = true;
            }
            if (content.equals("双")) {
                shuang = true;
            }
        }
        //禁止杀组合:杀组合意思是总的四门组合全下，或者下到三门
        if (SettingContext.getRuleSettingProperties("jinzhishazuhe", false)) {
            if (SettingContext.getRuleSettingProperties("jinzhishazuhe.zongzu.check", false) && allScore > SettingContext.getRuleSettingProperties("jinzhishazuhe.zongzu.text", 0)) {
            } else {
                //判断杀组合
                if ((dashuang ? 1 : 0) + (dadan ? 1 : 0) + (xiaoshuang ? 1 : 0) + (xiaodan ? 1 : 0) >= 3) {
                    cancelMemberAllBetting(member.getId());
                    if (SettingContext.getRuleSettingProperties("jinzhishazuhe.zongzu.check", false)) {
                        return "总注不到" + SettingContext.getRuleSettingProperties("jinzhishazuhe.zongzu.text", 0) + "以上,禁止杀组合,本期猜猜全部无效,请重新猜猜!";
                    } else {
                        return "禁止杀组合,本期猜猜全部无效,请重新猜猜!";
                    }
                }
            }
        }
        //禁止反向组合:反向组合意思是小单大双，或者小双大单
        if (SettingContext.getRuleSettingProperties("jinzhifanxiangzuhe", false)) {
            if (SettingContext.getRuleSettingProperties("jinzhifanxiangzuhe.zongzu.check", false) && allScore > SettingContext.getRuleSettingProperties("jinzhifanxiangzuhe.zongzu.text", 0)) {
            } else {
                //判断杀组合
                if ((xiaodan && dashuang) || (xiaoshuang && dadan)) {
                    cancelMemberAllBetting(member.getId());
                    if (SettingContext.getRuleSettingProperties("jinzhifanxiangzuhe.zongzu.check", false)) {
                        return "总注不到" + SettingContext.getRuleSettingProperties("jinzhifanxiangzuhe.zongzu.text", 0) + "以上,禁止反向组合,本期猜猜全部无效,请重新猜猜!";
                    } else {
                        return "禁止反向组合,本期猜猜全部无效,请重新猜猜!";
                    }
                }
            }
        }
        //禁止同向组合:同向组合小双小单，或大双大单
        if (SettingContext.getRuleSettingProperties("jinzhitongxiangzuhe", false)) {
            if (SettingContext.getRuleSettingProperties("jinzhitongxiangzuhe.zongzu.check", false) && allScore > SettingContext.getRuleSettingProperties("jinzhitongxiangzuhe.zongzu.text", 0)) {
            } else {
                //判断杀组合
                if ((xiaoshuang && xiaodan) || (dashuang && dadan)) {
                    cancelMemberAllBetting(member.getId());
                    if (SettingContext.getRuleSettingProperties("jinzhitongxiangzuhe.zongzu.check", false)) {
                        return "总注不到" + SettingContext.getRuleSettingProperties("jinzhitongxiangzuhe.zongzu.text", 0) + "以上,禁止同向组合,本期猜猜全部无效,请重新猜猜!";
                    } else {
                        return "禁止同向组合,本期猜猜全部无效,请重新猜猜!";
                    }
                }
            }
        }
        //禁止大小单双反向:大小单双反向，大或小，单或双
        if (SettingContext.getRuleSettingProperties("jinzhidaxiaodanshuangfanxiang", false)) {
            if (SettingContext.getRuleSettingProperties("jinzhidaxiaodanshuangfanxiang.zongzu.check", false) && allScore > SettingContext.getRuleSettingProperties("jinzhidaxiaodanshuangfanxiang.zongzu.text", 0)) {
            } else {
                //判断杀组合
                if ((da && xiao) || (dan && shuang)) {
                    cancelMemberAllBetting(member.getId());
                    if (SettingContext.getRuleSettingProperties("jinzhidaxiaodanshuangfanxiang.zongzu.check", false)) {
                        return "总注不到" + SettingContext.getRuleSettingProperties("jinzhidaxiaodanshuangfanxiang.zongzu.text", 0) + "以上,禁止大小单双反向组合,本期猜猜全部无效,请重新猜猜!";
                    } else {
                        return "禁止大小单双反向组合,本期猜猜全部无效,请重新猜猜!";
                    }
                }
            }
        }
        //单点数字每期最多
        if (SettingContext.getRuleSettingProperties("dandianshuzi.meiqizuiduo.check", false) && SettingContext.getRuleSettingProperties("dandianshuzi.meiqizuiduo.text", 0) > 0) {
            if (allTe.size() > SettingContext.getRuleSettingProperties("dandianshuzi.meiqizuiduo.text", 0)) {
                return "单点数字每期最多" + SettingContext.getRuleSettingProperties("andianshuzi.meiqizuiduo.text", 0) + "个!";
            }
        }
        for (int i = 0; i < contents.size(); i++) {
            String result = doBetting(member, game, contents.get(i), scores.get(i), bettings);
            if (!"OK".equals(result)) {
                return "猜猜" + contents.get(i) + "时" + result;
            }
        }
        member.setAllUse(member.getAllUse() + allScore0);
        memberService.save(member);
        for (int i = 0; i < contents.size(); i++) {
            Betting betting = new Betting();
            betting.setMember(member);
            betting.setGame(game);
            betting.setScore(scores.get(i));
            betting.setContent(contents.get(i));
            bettingService.save(betting);
        }

        return "下注成功!";
    }

    /**
     * 下注
     *
     * @param member   用户id
     * @param game     游戏id
     * @param content  下注内容
     * @param score    分数
     * @param bettings 当前游戏下注记录
     * @return 返回聊天结果
     */
    public static String doBetting(Member member, Game game, String content, double score, List<Betting> bettings) {
        if (score <= 0) {
            return "最低需要" + SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1) + "分";
        }
        //设置了单注最低限制
        if (SettingContext.getRuleSettingProperties("danzhuzuidi.check", false)) {
            if (score < SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1)) {
                return "最低需要" + SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1) + "分";
            }
        }

        if (content.startsWith("特")) {
            //设置了单点数字单注封顶且未设置按单点数字总注计算
            if (SettingContext.getRuleSettingProperties("dandianshuzi.check", false) && !SettingContext.getRuleSettingProperties("dandianshuzizongzhu.check", false) && SettingContext.getRuleSettingProperties("dandianshuzi.text", 0) > 0) {
                if (score > SettingContext.getRuleSettingProperties("dandianshuzi.text", 0)) {
                    return "单点数字封顶" + SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1) + "分";
                }
            }
        }

        if ("大".equals(content) || "小".equals(content) || "单".equals(content) || "双".equals(content)) {
            //设置了大小单双单注封顶
            if (SettingContext.getRuleSettingProperties("daxiaodanshuang.check", false) && SettingContext.getRuleSettingProperties("daxiaodanshuang.text", 0) > 0) {
                if (score > SettingContext.getRuleSettingProperties("daxiaodanshuang.text", 0)) {
                    return "大小单双封顶" + SettingContext.getRuleSettingProperties("daxiaodanshuang.text", 1) + "分";
                }
            }
        }

        if ("大单".equals(content) || "大双".equals(content) || "小单".equals(content) || "小双".equals(content)) {
            //设置了组合单注封顶
            if (SettingContext.getRuleSettingProperties("zuhe.check", false) && SettingContext.getRuleSettingProperties("zuhe.text", 0) > 0) {
                if (score > SettingContext.getRuleSettingProperties("zuhe.text", 0)) {
                    return "组合封顶" + SettingContext.getRuleSettingProperties("zuhe.text", 1) + "分";
                }
            }
        }

        if ("极大".equals(content) || "极小".equals(content)) {
            //设置了极大极小单注封顶
            if (SettingContext.getRuleSettingProperties("jidajixiao.check", false) && SettingContext.getRuleSettingProperties("jidajixiao.text", 0) > 0) {
                if (score > SettingContext.getRuleSettingProperties("jidajixiao.text", 0)) {
                    return "极大极小封顶" + SettingContext.getRuleSettingProperties("jidajixiao.text", 1) + "分";
                }
            }
        }
        try {
            if (game.getId() != GameContext.currentGame.game.getId()) {
                throw new Exception("下注超时");
            }
        } catch (Exception e) {
            return "下注超时";
        }
        if (member.getMoney() < score) {
            return "余额不足!";
        }
        if (content.startsWith("特")) {
            double teAll = 0;
            for (Betting betting : bettings) {
                if (betting.getContent() != null && betting.getContent().startsWith("特")) {
                    teAll += betting.getScore();
                }
            }
            //设置了单点数字单注封顶且未设置按单点数字总注计算
            if (SettingContext.getRuleSettingProperties("dandianshuzi.check", false) && SettingContext.getRuleSettingProperties("dandianshuzizongzhu.check", false) && SettingContext.getRuleSettingProperties("dandianshuzi.text", 0) > 0) {
                if ((teAll + score) > SettingContext.getRuleSettingProperties("dandianshuzi.text", 0)) {
                    return "单点数字总注封顶" + SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1) + "分";
                }
            }
        }
        if (SettingContext.getRuleSettingProperties("zongzhu.check", false) && SettingContext.getRuleSettingProperties("zongzhu.text", 0) > 0) {
            double all = 0;
            for (Betting betting : bettings) {
                if (betting.getScore() != null && betting.getScore() > 0) {
                    all += betting.getScore();
                }
            }
            if (all + score > SettingContext.getRuleSettingProperties("zongzhu.text", 0)) {
                return "总注封顶" + SettingContext.getRuleSettingProperties("danzhuzuidi.text", 1) + "分";
            }
        }
        return "OK";
    }

    /**
     * 取消用户当轮游戏所有下注
     *
     * @param memberId
     */
    public static void cancelMemberAllBetting(long memberId) {
        BettingService bettingService = SpringContextUtil.getBean(BettingService.class);
        bettingService.deleteByGameIdAndMemberId(GameContext.getGame().getId(), memberId);
    }
}
