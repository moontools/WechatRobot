package com.lincoln.Client.context;

import com.lincoln.Client.cmd.AbstractBaseCmd;

import java.io.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

/**
 * 配置上下文
 */
public class SettingContext {
    private static String infoSettingPath = getSettingDir() + "infoSetting.properties";//信息设置路径
    private static Properties infoSetting;//信息设置

    public static Properties getInofoSetting() {
        if (infoSetting == null) {
            infoSetting = loadFileProperties(infoSettingPath);
        }
        return infoSetting;
    }

    public static String getInfoSettingProperties(String key, String defaultValue) {
        String result = defaultValue;
        try {
            result = getInofoSetting().getProperty(key, defaultValue);
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static double getInfoSettingProperties(String key, double defaultValue) {
        double result = defaultValue;
        try {
            result = Double.parseDouble(getInofoSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static boolean getInfoSettingProperties(String key, boolean defaultValue) {
        boolean result = defaultValue;
        try {
            result = Boolean.parseBoolean(getInofoSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    /**
     * 加入信息设置
     *
     * @param key
     * @param value
     */
    public static void setInfoSetting(String key, String value) {
        Properties infoSetting = getInofoSetting();
        infoSetting.setProperty(key, value);
        writeFileProperties(infoSetting, infoSettingPath);
        SettingContext.infoSetting = infoSetting;
    }

    private static String ruleSettingPath = getSettingDir() + "ruleSetting.properties";//规则设置路径
    private static Properties ruleSetting;//规则设置

    public static Properties getRuleSetting() {
        if (ruleSetting == null) {
            ruleSetting = loadFileProperties(ruleSettingPath);
        }
        return ruleSetting;
    }

    public static String getRuleSettingProperties(String key, String defaultValue) {
        String result = defaultValue;
        try {
            result = getRuleSetting().getProperty(key, defaultValue);
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static double getRuleSettingProperties(String key, double defaultValue) {
        double result = defaultValue;
        try {
            result = Double.parseDouble(getRuleSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static boolean getRuleSettingProperties(String key, boolean defaultValue) {
        boolean result = defaultValue;
        try {
            result = Boolean.parseBoolean(getRuleSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    /**
     * 加入规则设置
     *
     * @param key
     * @param value
     */
    public static void setRuleSetting(String key, String value) {
        Properties ruleSetting = getRuleSetting();
        ruleSetting.setProperty(key, value);
        writeFileProperties(ruleSetting, ruleSettingPath);
        SettingContext.ruleSetting = ruleSetting;
    }


    private static String soundSettingPath = getSettingDir() + "soundSetting.properties";//规则设置路径
    private static Properties soundSetting;//规则设置

    public static Properties getSoundSetting() {
        if (soundSetting == null) {
            soundSetting = loadFileProperties(soundSettingPath);
        }
        return soundSetting;
    }

    public static String getSoundSettingProperties(String key, String defaultValue) {
        String result = defaultValue;
        try {
            result = getSoundSetting().getProperty(key, defaultValue);
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static double getSoundSettingProperties(String key, double defaultValue) {
        double result = defaultValue;
        try {
            result = Double.parseDouble(getSoundSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public static boolean getSoundSettingProperties(String key, boolean defaultValue) {
        boolean result = defaultValue;
        try {
            result = Boolean.parseBoolean(getSoundSetting().getProperty(key, ""));
            return result;
        } catch (Exception e) {
            return result;
        }
    }

    /**
     * 加入声音设置
     *
     * @param key
     * @param value
     */
    public static void setSoundSetting(String key, String value) {
        Properties soundSetting = getSoundSetting();
        soundSetting.setProperty(key, value);
        writeFileProperties(soundSetting, soundSettingPath);
        SettingContext.soundSetting = soundSetting;
        AbstractBaseCmd.flash();
    }

    /**
     * 删除声音设置
     *
     * @param key
     */
    public static void removeSoundSetting(String key) {
        Properties soundSetting = getSoundSetting();
        soundSetting.remove(key);
        writeFileProperties(soundSetting, soundSettingPath);
        SettingContext.soundSetting = soundSetting;
    }


    /**
     * 获取工作目录
     *
     * @return
     */
    public static String getMainDir() {
        System.out.println(System.getProperty("java.class.path"));
        return "C:/Users/Lincoln/Desktop/微信机器人成品/";
    }

    /**
     * 获取配置目录
     */
    public static String getSettingDir() {
        return getMainDir() + "setting/";
    }

    /***
     * 读取文件系统中的配置
     * @param path
     * @return
     */
    public static Properties loadFileProperties(String path) {
        Properties properties = new Properties();
        try {
            URL url = new URL((path.startsWith("file:") ? "" : "file:") + path);
            URLConnection connection = url.openConnection();
            connection.connect();
            InputStream in = connection.getInputStream();
            properties.load(new InputStreamReader(in, "utf-8"));
            in.close();
        } catch (Exception e) {
            return properties;
        }
        return properties;
    }

    public static void writeFileProperties(Properties properties, String path) {
        try {
            FileOutputStream out = new FileOutputStream(path);
            properties.store(new OutputStreamWriter(out, "utf-8"), null);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 读取jar中的配置
     *
     * @param path
     * @return
     */
    public static Properties loadJarProperties(String path) {
        Properties properties = new Properties();
        try {
            URL url = new URL((path.startsWith("jar:") ? "" : "jar:") + path);
            JarURLConnection connection = (JarURLConnection) url.openConnection();
            connection.connect();
            InputStream in = connection.getInputStream();
            properties.load(in);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }
}
