package com.lincoln.Client.context;

import com.lincoln.Client.cmd.BaseCmd;
import com.lincoln.framework.entity.WxAccount;
import com.lincoln.framework.entity.WxMessage;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.service.WxAccountService;
import com.lincoln.framework.service.WxMessageService;
import com.lincoln.framework.spring.BeanUtils;
import com.lincoln.framework.spring.SpringContextUtil;
import com.lincoln.wechat.api.annotation.Bind;
import com.lincoln.wechat.api.enums.MsgType;
import com.lincoln.wechat.api.model.Account;
import com.lincoln.wechat.api.model.Member;
import com.lincoln.wechat.api.model.WeChatMessage;

import java.util.Date;

//消息工厂,消息由此分发处理
public class MessageFactory {
    public static WxMessageService wxMessageService = SpringContextUtil.getBean("wxMessageService");
    public static WxAccountService wxAccountService = SpringContextUtil.getBean("wxAccountService");
    public static MemberService memberService = SpringContextUtil.getBean("memberService");

    @Bind(msgType = MsgType.TEXT)
    public static void handleText(WeChatMessage message) {
        String group = SettingContext.getRuleSettingProperties("GROUP_NAME","");
        if (message.isGroup() && group.equals(message.getFromNickName())) {
            RobotContext.groupId = message.getFromUserName();
            //保存消息记录
            WxMessage wxMessage = new WxMessage();
            wxMessage.setId(Long.valueOf(message.getId()));
            wxMessage.setText(message.getText());
            wxMessage.setContent(message.getRaw().getContent());
            wxMessage.setCreateTime(new Date(message.getRaw().getCreateTime()));
            wxMessage.setFromNickName(message.getFromNickName());
            wxMessage.setFromUserName(message.getFromUserName());
            wxMessage.setMineNickName(message.getMineNickName());
            wxMessage.setMineUserName(message.getMineUserName());
            wxMessage.setToUserName(message.getToUserName());
            wxMessageService.save(wxMessage);

            //作为好友的account
            Account account = RobotContext.getNewestRobot().api().getAccountById(wxMessage.getGroupMemberId());
            //群名片account
            Member member = RobotContext.getNewestRobot().api().findGroupById(wxMessage.getFromUserName()).findMemberById(wxMessage.getGroupMemberId());
            com.lincoln.framework.entity.Member m = null;
            if(account!=null){
                m = memberService.getById(account.getAttrStatus());
            }else if(member!=null){
                m = memberService.getById(member.getAttrStatus());
            }

            if(m == null){
                m = new com.lincoln.framework.entity.Member();
                if(account!=null){
                    m.setId(account.getAttrStatus());
                }else if(member!=null){
                    m.setId(member.getAttrStatus());
                }
                m.setEarn(0.0);
                m.setAllUse(0.0);
                m.setScore(0.0);
                if(account!=null){
                    m.setNickName(account.getNickName());
                }else if(member!=null){
                    m.setNickName(member.getNickName());
                }
                m.setWithdraw(0.0);
                m.setStatus(0);
                memberService.save(m);
            }
            BaseCmd baseCmd = CmdContext.getBaseCmd(m.getNickName(), member.getAttrStatus(), wxMessage.getText());
            baseCmd.replyMsg();
        }
    }

    /**
     * 消息测试用
     */
    public static void handleText(String wxName, long attrStatus, String content) {

//        if(content.startsWith("ce")){
//            GameContext.dealResult(1,2,3);
//            GameContext.dealResult(1,2,4);
//            GameContext.dealResult(5,5,5);
//            GameContext.dealResult(6,6,6);
//            GameContext.dealResult(8,9,9);
//            GameContext.dealResult(1,9,1);
//            GameContext.dealResult(5,6,4);
//            GameContext.dealResult(1,3,5);
//            GameContext.dealResult(3,4,6);
//            GameContext.dealResult(3,4,7);
//            return;
//        }

        com.lincoln.framework.entity.Member member = memberService.getById(attrStatus);
        if (member == null) {
            member = new com.lincoln.framework.entity.Member();
            member.setId(attrStatus);
            member.setNickName(wxName.substring(0,2));
            member.setEarn(0.0);
            member.setAllUse(0.0);
            member.setScore(0.0);
            member.setWithdraw(0.0);
            member.setStatus(0);
            memberService.save(member);
        }
        BaseCmd baseCmd = CmdContext.getBaseCmd(wxName, attrStatus, content);
        baseCmd.replyMsgTest();
    }
}
