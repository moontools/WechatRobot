package com.lincoln.Client.context;

import com.lincoln.Client.frames.*;
import lombok.Getter;

import javax.swing.*;

@Getter
public class FrameContext {
    //主窗口
    public static MainFrame main = new MainFrame();
    //登陆窗口
    public static LoginFrame login;
    //账号管理窗口
    public static AccountManageFrame accountManage;

    //登陆
    public static FirstFrame firstFrame;

    public static JFrame getInstance() {
        if (main == null) {
            main = new MainFrame();
        }
        return main;
    }

    public static JFrame getLoginFrame() {
        login = new LoginFrame();
        return login.getFrame();
    }

    public static JFrame getAccountManageFrame() {
        if (accountManage == null) {
            accountManage = new AccountManageFrame();
        }
        return accountManage.getFrame();
    }

    /**
     * 刷新查回审核列表
     */
    public static void flashMainEastPanelAuditList() {
        main.mainEastPanel.flashMainEastPanelAuditList();
    }

    /**
     * 添加一条查回审核列表数据
     *
     * @param id    查或者回的订单id
     * @param name  用户花名
     * @param type  "查"或者"回"
     * @param score 分数
     */
    public static void addMainEastPanelAuditList(long id, String name, String type, double score) {
        main.mainEastPanel.addMainEastPanelAuditList(id, name, type, score);
    }
}
