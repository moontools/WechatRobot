package com.lincoln.Client.context;

import com.lincoln.Client.reptile.BaseReptile;
import com.lincoln.Client.reptile.ReptileHotModule;
import com.lincoln.framework.spring.SpringContextUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sun.net.www.protocol.file.FileURLConnection;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;

/**
 * 爬虫控制器(热加载方式)
 * 加载北京28 + 加拿大28
 */
public class ReptileContext {
    public static ReptileHotModule reptileHotModule = new ReptileHotModule();
    //加载配置文件
    public static Properties properties = new Properties();

    //北京28默认爬虫
    public static String REPTILE_BEIJING_DEFAULT = "KaiCaiBeijingAPI";
    //#北京28备用爬虫
    public static String REPTILE_BEIJING_BEIYONG = "KaiCaiBeijingAPI";
    //
    public static boolean isBeiyong = false;


    //#加拿大28默认爬虫
    public static String REPTILE_CANADA_DEFAULT = "KaiCaiCanadaAPI";
    //#加拿大28备用爬虫
    public static String REPTILE_CANADA_BEIYONG = "KaiCaiCanadaAPI";

    static {
        try {//+"client.properties"
            String classPath = ReptileContext.class.getResource("").getPath();
            try {
                URL url = new URL((classPath.startsWith("file:") ? "" : "file:") + classPath.replaceAll("com/lincoln/Client/context/", "") + "client.properties");
                URLConnection connection = url.openConnection();
                connection.connect();
                InputStream in = connection.getInputStream();
                properties.load(in);
                in.close();
            } catch (Exception e) {
                URL url = new URL((classPath.startsWith("jar:") ? "" : "jar:") + classPath.replaceAll("com/lincoln/Client/context/", "") + "client.properties");
                JarURLConnection connection = (JarURLConnection) url.openConnection();
                connection.connect();
                InputStream in = connection.getInputStream();
                properties.load(in);
                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (StringUtils.isNotEmpty(properties.getProperty("REPTILE.BEIJING_DEFAULT"))) {
            REPTILE_BEIJING_DEFAULT = properties.getProperty("REPTILE.BEIJING_DEFAULT");
        }
        if (StringUtils.isNotEmpty(properties.getProperty("REPTILE.BEIJING_BEIYONG"))) {
            REPTILE_BEIJING_BEIYONG = properties.getProperty("REPTILE.BEIJING_BEIYONG");
        }
        if (StringUtils.isNotEmpty(properties.getProperty("REPTILE.CANADA_DEFAULT"))) {
            REPTILE_CANADA_DEFAULT = properties.getProperty("REPTILE.CANADA_DEFAULT");
        }
        if (StringUtils.isNotEmpty(properties.getProperty("REPTILE.CANADA_BEIYONG"))) {
            REPTILE_CANADA_BEIYONG = properties.getProperty("REPTILE.CANADA_BEIYONG");
        }
    }

    public static void main(String[] args) {
        BaseReptile baseReptile = getReptile("DaShen28BeijingAPI");
        baseReptile.reptile();
    }

    /**
     * 传入key,获取对应的爬虫
     *
     * @return
     */
    public static BaseReptile getReptile(String key) {
        BaseReptile baseReptile = reptileHotModule.loadModuleObject(reptileHotModule.getModule(key), new Object[]{});
        return baseReptile;
    }

    /**
     * 获取北京28默认爬虫
     *
     * @return
     */
    public static BaseReptile getBeijing28Reptile() {
        if (getReptile(REPTILE_BEIJING_DEFAULT) != null) {
            return getReptile(REPTILE_BEIJING_DEFAULT);
        } else {
            return getReptile(REPTILE_BEIJING_BEIYONG);
        }
    }

    /**
     * 获取北京28备用爬虫
     *
     * @return
     */
    public static BaseReptile getBeijing28BeiYongReptile() {
        if (getReptile(REPTILE_BEIJING_BEIYONG) != null) {
            isBeiyong = true;
            return getReptile(REPTILE_BEIJING_BEIYONG);
        } else {
            return null;
        }
    }
}
