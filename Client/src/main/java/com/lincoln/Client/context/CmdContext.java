package com.lincoln.Client.context;

import com.lincoln.Client.cmd.AbstractBaseCmd;
import com.lincoln.Client.cmd.BaseCmd;
import com.lincoln.Client.cmd.CmdHotModule;
import com.lincoln.framework.spring.SpringContextUtil;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 微信消息解析器
 */
public class CmdContext {
    public static CmdHotModule cmdHotModule = new CmdHotModule();

    public static void main(String[] args) {
        SpringContextUtil springContextUtil = new SpringContextUtil();
        springContextUtil.setApplicationContext(new ClassPathXmlApplicationContext("classpath:applicationContext-client.xml"));

        BaseCmd baseCmd = getBaseCmd("@@bd6051243292fb227c7e7b1929571b3f00a10c1da7573cdafff929c1b50c2336", new Long(176613), "大100");
        baseCmd.replyMsgTest();
    }

    /**
     * 传入微信消息,自动获取对应的解析器
     *
     * @param groupName
     * @param attrStatus
     * @param content
     * @return
     */
    public static BaseCmd getBaseCmd(String groupName, Long attrStatus, String content) {
        // TODO: 2018/4/18 对content进行简单的判断,从而筛选大部分无用信息

        //通过挂载的cmd
        for (String key : cmdHotModule.getModules().keySet()) {
            Object[] objects = new Object[]{groupName, attrStatus, content};
            BaseCmd baseCmd = cmdHotModule.loadModuleObject(cmdHotModule.getModule(key), objects);
            if (baseCmd.compare()) {
                return baseCmd;
            }
        }
        return new AbstractBaseCmd(groupName, attrStatus, content);
    }
}
