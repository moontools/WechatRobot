package com.lincoln.Client;

import com.lincoln.Client.context.FrameContext;
import com.lincoln.Client.context.GameContext;
import com.lincoln.Client.context.MessageFactory;
import com.lincoln.Client.frames.FirstFrame;
import com.lincoln.framework.entity.WxAccount;
import com.lincoln.framework.entity.WxMessage;
import com.lincoln.framework.service.WxMessageService;
import com.lincoln.framework.spring.SpringContextUtil;
import com.lincoln.wechat.api.model.WeChatMessage;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.*;
import java.util.Scanner;

public class Client {
    /**
     * 加载Spring
     */
    public static void loadSpring() {
        SpringContextUtil springContextUtil = new SpringContextUtil();
        springContextUtil.setApplicationContext(new ClassPathXmlApplicationContext("classpath:applicationContext-client.xml"));
    }

    public static void main(String[] args) {
        loadSpring();
        FrameContext.firstFrame = new FirstFrame();
        FrameContext.firstFrame.setVisible(true);
        new ChatTest().start();

        ////测试用
        /*测试右侧查回审核
        MessageFactory.handleText("小明", 1, "查100");
        MessageFactory.handleText("小明", 1, "查1000");
        MessageFactory.handleText("小明", 1, "回100");
        MessageFactory.handleText("小明", 1, "回1000");
        */
    }

    public static class ChatTest extends Thread{
        @Override
        public void run() {
            while (true){
                Scanner sc=new Scanner(System.in);
                System.out.println("模拟聊天：");
                String m=sc.next();
                MessageFactory.handleText("小明",1,m);
            }
        }
    }
}
