package com.lincoln.Client.frames;

import com.lincoln.Client.context.GameContext;
import com.lincoln.Client.context.RobotContext;
import com.lincoln.Client.context.SettingContext;
import com.lincoln.Client.context.SoundContext;
import com.lincoln.framework.entity.Betting;
import com.lincoln.framework.entity.Game;
import com.lincoln.framework.entity.Member;
import com.lincoln.framework.service.BettingService;
import com.lincoln.framework.service.GameService;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.spring.SpringContextUtil;
import com.lincoln.wechat.utils.StringUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

/**
 * Created by Lincoln on 2018/4/15.
 */
public class MainCenterPanel extends JPanel {
    public JTabbedPane jTabbedPane = new JTabbedPane(JTabbedPane.TOP);
    public Down down = new Down();
    public UserList userList = new UserList();
    public OddSetting oddSetting = new OddSetting();
    public Rule rule = new Rule();
    public Prompt prompt = new Prompt();
    public UsePrompt usePrompt = new UsePrompt();
    JPanel prompt_Panel = new JPanel();
    public MessageSetting messageSetting = new MessageSetting();
    public GameList gameList = new GameList();

    public MainCenterPanel() {
        super(new BorderLayout());
        prompt_Panel.add(prompt);
        prompt_Panel.add(usePrompt, BorderLayout.WEST);
        jTabbedPane.addTab("玩家列表", userList);
        jTabbedPane.addTab("开奖数据", gameList);
        jTabbedPane.addTab("信息设置", messageSetting);
        jTabbedPane.addTab("赔率设置", oddSetting);
        jTabbedPane.addTab("游戏规则", rule);
        jTabbedPane.addTab("语音提示", prompt_Panel);
        JPanel jPanel = new JPanel();
        jTabbedPane.addTab("使用协议", new Attention());
        this.add(BorderLayout.NORTH, jTabbedPane);
        this.add(BorderLayout.CENTER, down);
    }

    public void flashAllUser() {
        userList.flashAllUser();
    }

    //使用协议
    public class Attention extends JPanel {
        public void paintComponent(Graphics g) {
            int x = 0, y = 0;
            String classPath = MainCenterPanel.class.getResource("").getPath();
            URL url = null;
            try {
                url = new URL((classPath.startsWith("jar:") ? "" : "jar:") + classPath.replaceAll("com/lincoln/Client/frames/", "") + "background.png");
                JarURLConnection connection = (JarURLConnection) url.openConnection();
                connection.connect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            ImageIcon icon = new ImageIcon(url);
            g.drawImage(icon.getImage(), x, y, getSize().width, getSize().height, this);// 图片会自动缩放
            //    g.drawImage(icon.getImage(), x, y,this);//图片不会自动缩放
        }
    }

    //开奖数据
    public class GameList extends JPanel {
        Vector title = new Vector();
        DefaultTableModel model = new DefaultTableModel();
        JTable jTable = new JTable(model);
        JScrollPane jScrollPane = new JScrollPane(jTable);

        public GameList() {
            super();
            this.setLayout(new BorderLayout());
            this.add(jScrollPane, BorderLayout.CENTER);
            title.add("期号");
            title.add("开奖时间");
            title.add("3X9");
            title.add("28");
            title.add("开奖结果");
            title.add("输赢");
            title.add("备注");
            Vector data = ((DefaultTableModel) jTable.getModel()).getDataVector();
            model.setDataVector(data, title);
            flashGameList();
        }

        public void flashGameList() {
            this.model.getDataVector().clear();
            GameService gameService = SpringContextUtil.getBean(GameService.class);
            java.util.List<Game> gameList = gameService.findAll();
            for (Game game : gameList) {
                if (game.getResult1() == null || game.getResult2() == null || game.getResult3() == null) {
                    this.model.addRow(new Object[]{game.getGameNo(), formateTime(game.getEndTime()), "未开奖", "未开奖", "", game.getWin(), ""});

                } else {
                    this.model.addRow(new Object[]{game.getGameNo(), formateTime(game.getEndTime()), game.getResult1() + "+" + game.getResult3() + "+" + game.getResult3(), game.getResult1() + game.getResult2() + game.getResult3(), "", game.getWin(), ""});
                }
            }
        }

        public String formateTime(Date date) {
            if (date == null) {
                return "";
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            return simpleDateFormat.format(date);
        }
    }

    //刷新开奖数据
    public void flashGameList() {
        gameList.flashGameList();
    }

    //玩家列表
    public class UserList extends JPanel {
        Vector title = new Vector();
        DefaultTableModel model = new DefaultTableModel();
        JTable jTable = new JTable(model);
        JScrollPane jScrollPane = new JScrollPane(jTable);

        //        JTable jTable = new JTable(new Object[100][8],new String[]{"序号","昵称","积分","上轮赢分","本轮总分","猜猜内容","状态","最后猜猜"});
//        JScrollPane jScrollPane = new JScrollPane(jTable);
        public UserList() {
            super();
            this.setLayout(new BorderLayout());
            this.add(jScrollPane, BorderLayout.CENTER);
            title.add("序号");
            title.add("昵称");
            title.add("积分");
            title.add("上轮赢分");
            title.add("本轮总分");
            title.add("猜猜内容");
            title.add("状态");
            title.add("最后猜猜");
            Vector data = ((DefaultTableModel) jTable.getModel()).getDataVector();
            model.setDataVector(data, title);

            jTable.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(MouseEvent evt) {
                    //判断是否为鼠标的BUTTON3按钮，BUTTON3为鼠标右键
                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        //通过点击位置找到点击为表格中的行
                        int focusedRowIndex = jTable.rowAtPoint(evt.getPoint());
                        if (focusedRowIndex == -1) {
                            return;
                        }
                        //将表格所选项设为当前右键点击的行
                        long id = (long) jTable.getValueAt(jTable.getSelectedRow(), 0);
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id);
                        down.id.setText("id:" + id);
                        down.userName.setText(member.getNickName());
                        down.score.setText("" + member.getMoney());
                        if (member.getStatus() == Member.STATUS_BOT || member.getStatus() == Member.STATUS_BOT_BAN) {
                            down.robot.setSelected(true);
                        } else {
                            down.robot.setSelected(false);
                        }
                        if (member.getStatus() == Member.STATUS_BAN || member.getStatus() == Member.STATUS_BOT_BAN) {
                            down.remove.setText("恢复玩家");
                        } else {
                            down.remove.setText("踢出玩家");
                        }
                    }
                    if(evt.getButton() == MouseEvent.BUTTON3){
                        long id = (long) jTable.getValueAt(jTable.getSelectedRow(), 0);
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id);
                        if (member == null) {
                            return;
                        }
                        if (member.getStatus() == Member.STATUS_BAN || member.getStatus() == Member.STATUS_BOT_BAN) {
                            if (member.getStatus() == Member.STATUS_BAN) {
                                member.setStatus(Member.STATUS_NORMAL);
                            } else if (member.getStatus() == Member.STATUS_BOT_BAN) {
                                member.setStatus(Member.STATUS_BOT);
                            }
                            memberService.save(member);
                            down.remove.setText("踢出玩家");
                        } else {
                            if (member.getStatus() == Member.STATUS_NORMAL) {
                                member.setStatus(Member.STATUS_BAN);
                            } else if (member.getStatus() == Member.STATUS_BOT) {
                                member.setStatus(Member.STATUS_BOT_BAN);
                            }
                            memberService.save(member);
                            down.remove.setText("恢复玩家");
                        }
                        flashAllUser();
                    }
                }
            });

            flashAllUser();
        }

        /**
         * 刷新用户列表
         */
        public void flashAllUser() {
            MemberService memberService = SpringContextUtil.getBean(MemberService.class);
            BettingService bettingService = SpringContextUtil.getBean(BettingService.class);
            GameService gameService = SpringContextUtil.getBean(GameService.class);
            java.util.List<Member> list = memberService.findAllMemberOrderByScore();
            this.model.getDataVector().clear();
            for (Member member : list) {
                Betting lastBetting = bettingService.getLastBetting(member.getId());//最后一次下注
                Game game = gameService.getGameByNo(GameContext.currentGame == null ? 0 : GameContext.currentGame.gameNo);//当前游戏
                Game lastGame = gameService.getGameByNo(GameContext.currentGame == null ? 0 : GameContext.currentGame.lastGameNo);//上一轮游戏
                java.util.List<Betting> currentList = bettingService.queryBettingByGameAndMember((game == null ? 0 : game.getId()), member.getId());
                java.util.List<Betting> lastList = bettingService.queryBettingByGameAndMember((lastGame == null ? 0 : lastGame.getId()), member.getId());
                double lastWin = 0;
                double nowUse = 0;
                String content = "";
                String lastContent = "";
                if (lastBetting != null) {
                    lastContent = lastBetting.getContent();
                }
                if (currentList != null && currentList.size() > 0) {
                    for (Betting b : currentList) {
                        content += b.getContent() + "||";
                        nowUse += b.getScore();
                    }
                }
                if (lastList != null && lastList.size() > 0) {
                    for (Betting b : currentList) {
                        if (b.getWin() > 0) {
                            lastWin += b.getWin();
                        }
                    }
                }
                addMember(member, lastWin, nowUse, content, lastContent);
            }
        }

        /**
         * 新增一条记录
         *
         * @param member
         * @param lastWin     上轮赢分
         * @param nowUse      本轮总分
         * @param content     猜猜内容
         * @param lastContent 最后猜猜
         */
        public void addMember(Member member, double lastWin, double nowUse, String content, String lastContent) {
            String status = "正常";
            switch (member.getStatus()) {
                case Member.STATUS_NORMAL:
                    status = "正常";
                    break;
                case Member.STATUS_BAN:
                    status = "禁用";
                    break;
                case Member.STATUS_BOT:
                    status = "假人";
                    break;
                case Member.STATUS_BOT_BAN:
                    status = "假人禁用";
                    break;
            }
            this.model.addRow(new Object[]{member.getId(), member.getNickName(), member.getMoney(), lastWin, nowUse, content, status, lastContent});
        }
    }

    //信息设置
    public class MessageSetting extends JPanel {
        JTabbedPane jTabbedPane = new JTabbedPane(JTabbedPane.TOP);

        JPanel tingcai = new JPanel(new GridLayout(1, 3));
        JTextField tingcai_qian = new JTextField(3);//停猜前xx秒提示
        JTextArea tingcai_qian_text = new JTextArea();
        JTextField tingcai_kaijiangqian = new JTextField(3);//开奖前xx秒停猜提示信息
        JTextArea tingcai_kaijiangqian_text = new JTextArea();
        JCheckBox tingcai_tingcaihou = new JCheckBox("停猜后内容审核信息:");
        JTextArea tingcai_tingcaihou_text = new JTextArea();

        JPanel kaijiang = new JPanel(new GridLayout(1, 3));
        JTextArea kaijiang_qian = new JTextArea();//开奖前
        JTextArea kaijiang_info = new JTextArea();//开奖信息
        JTextArea kaijiang_lishi = new JTextArea();//历史信息

        JPanel ad = new JPanel(new GridLayout(1, 5));

        JPanel test = new JPanel(new BorderLayout());
        JTextArea test_text = new JTextArea();

        JPanel score = new JPanel(new BorderLayout());
        JTextArea score_text = new JTextArea();

        /**
         * 加载积分设置
         */
        public void loadScoreProperties() {
            JScrollPane jScrollPane = new JScrollPane(score_text);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            score_text.setText(SettingContext.getInofoSetting().getProperty("score.text", ""));
            score.add(BorderLayout.NORTH, new JLabel("积分列表信息:"));
            score.add(BorderLayout.CENTER, jScrollPane);
            score_text.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("score.text", score_text.getText());
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("score.text", score_text.getText());
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("score.text", score_text.getText());
                }
            });
        }

        /**
         * 加载停彩设置
         */
        public void loadTingcaiProperties() {
            tingcai_qian_text.setColumns(25);
            tingcai_kaijiangqian_text.setColumns(25);
            tingcai_tingcaihou_text.setColumns(25);
            tingcai_qian_text.setRows(25);
            tingcai_kaijiangqian_text.setRows(25);
            tingcai_tingcaihou_text.setRows(25);

            JPanel jPanel = new JPanel(new BorderLayout());
            JPanel jPanel1 = new JPanel();
            jPanel1.add(new JLabel("停猜前"));
            jPanel1.add(tingcai_qian);
            jPanel1.add(new JLabel("提示信息:"));
            jPanel.add(BorderLayout.NORTH, jPanel1);
            JScrollPane jScrollPane = new JScrollPane(tingcai_qian_text);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            tingcai.add(jPanel);

            jPanel = new JPanel(new BorderLayout());
            jPanel1 = new JPanel();
            jPanel1.add(new JLabel("开奖前"));
            jPanel1.add(tingcai_kaijiangqian);
            jPanel1.add(new JLabel("停猜提示信息:"));
            jPanel.add(BorderLayout.NORTH, jPanel1);
            jScrollPane = new JScrollPane(tingcai_kaijiangqian_text);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            tingcai.add(jPanel);

            jPanel = new JPanel(new BorderLayout());
            jPanel1 = new JPanel();
            jPanel1.add(tingcai_tingcaihou);
            jPanel.add(BorderLayout.NORTH, jPanel1);
            jScrollPane = new JScrollPane(tingcai_tingcaihou_text);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            tingcai.add(jPanel);

            Properties properties = SettingContext.getInofoSetting();
            String tingcai_qian_ = properties.getProperty("tingcai.qian", "");
            String tingcai_qian_text_ = properties.getProperty("tingcai.qian.text", "");
            String tingcai_kaijiangqian_ = properties.getProperty("tingcai.kaijiangqian", "");
            String tingcai_kaijiangqian_text_ = properties.getProperty("tingcai.kaijiangqian.text", "'");
            String tingcai_tingcaihou_ = properties.getProperty("tingcai.tingcaihou", "false");
            String tingcai_tingcaihou_text_ = properties.getProperty("tingcai.tingcaihou.text", "");
            this.tingcai_qian.setText(tingcai_qian_);
            this.tingcai_qian_text.setText(tingcai_qian_text_);
            this.tingcai_kaijiangqian.setText(tingcai_kaijiangqian_);
            this.tingcai_kaijiangqian_text.setText(tingcai_kaijiangqian_text_);
            this.tingcai_tingcaihou.setSelected("true".equalsIgnoreCase(tingcai_tingcaihou_) ? true : false);
            this.tingcai_tingcaihou_text.setText(tingcai_tingcaihou_text_);
            this.tingcai_qian.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian", tingcai_qian.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian", tingcai_qian.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian", tingcai_qian.getText());
                }
            });
            this.tingcai_qian_text.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian.text", tingcai_qian_text.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian.text", tingcai_qian_text.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.qian.text", tingcai_qian_text.getText());
                }
            });
            this.tingcai_kaijiangqian.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian", tingcai_kaijiangqian.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian", tingcai_kaijiangqian.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian", tingcai_kaijiangqian.getText());
                }
            });
            this.tingcai_kaijiangqian_text.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian.text", tingcai_kaijiangqian_text.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian.text", tingcai_kaijiangqian_text.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.kaijiangqian.text", tingcai_kaijiangqian_text.getText());
                }
            });
            this.tingcai_tingcaihou.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("tingcai.tingcaihou", String.valueOf(tingcai_tingcaihou.isSelected()));
                }
            });
            this.tingcai_tingcaihou_text.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.tingcaihou.text", tingcai_tingcaihou_text.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.tingcaihou.text", tingcai_tingcaihou_text.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("tingcai.tingcaihou.text", tingcai_tingcaihou_text.getText());
                }
            });
        }

        /**
         * 加载开奖设置
         */
        public void loadKaijiangProperties() {
            kaijiang_qian.setColumns(25);
            kaijiang_info.setColumns(25);
            kaijiang_lishi.setColumns(25);

            JPanel jPanel = new JPanel(new BorderLayout());
            jPanel.add(BorderLayout.NORTH, new JLabel("开奖前信息:"));
            JScrollPane jScrollPane = new JScrollPane(kaijiang_qian);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            kaijiang.add(jPanel);

            jPanel = new JPanel(new BorderLayout());
            jPanel.add(BorderLayout.NORTH, new JLabel("开奖信息:"));
            jScrollPane = new JScrollPane(kaijiang_info);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            kaijiang.add(jPanel);

            jPanel = new JPanel(new BorderLayout());
            jPanel.add(BorderLayout.NORTH, new JLabel("历史信息:"));
            jScrollPane = new JScrollPane(kaijiang_lishi);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jPanel.add(BorderLayout.CENTER, jScrollPane);
            kaijiang.add(jPanel);

            Properties properties = SettingContext.getInofoSetting();
            String kaijiang_qian_ = properties.getProperty("kaijiang.qian", "");
            kaijiang_qian.setText(kaijiang_qian_);
            this.kaijiang_qian.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.qian", kaijiang_qian.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.qian", kaijiang_qian.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.qian", kaijiang_qian.getText());
                }
            });
            String kaijiang_info_ = properties.getProperty("kaijiang.info", "");
            kaijiang_info.setText(kaijiang_info_);
            this.kaijiang_info.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.info", kaijiang_info.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.info", kaijiang_info.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.info", kaijiang_info.getText());
                }
            });
            String kaijiang_lishi_ = properties.getProperty("kaijiang.lishi", "");
            kaijiang_lishi.setText(kaijiang_lishi_);
            this.kaijiang_lishi.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.lishi", kaijiang_lishi.getText());
                }

                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.lishi", kaijiang_lishi.getText());
                }

                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("kaijiang.lishi", kaijiang_lishi.getText());
                }
            });
        }

        /**
         * 加载广告设置
         */
        public void loadAdProperties() {
            ad.add(loadAdProoerties(1));
            ad.add(loadAdProoerties(2));
            ad.add(loadAdProoerties(3));
            ad.add(loadAdProoerties(4));
            ad.add(loadAdProoerties(5));
        }

        public JPanel loadAdProoerties(int i) {
            JTextField ad_cycle = new JTextField(3);//广告1周期
            ad_cycle.setText(SettingContext.getInofoSetting().getProperty("ad." + i + ".cycle", ""));
            JTextArea ad = new JTextArea();//广告1
            ad.setText(SettingContext.getInofoSetting().getProperty("ad." + i + ".text", ""));
            ad.setRows(25);
            ad.setColumns(15);
            JScrollPane jScrollPane = new JScrollPane(ad);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

            JPanel jPanel = new JPanel(new BorderLayout());
            JPanel jPanel1 = new JPanel();
            jPanel1.add(new JLabel("广告" + i + "发送周期:"));
            jPanel1.add(ad_cycle);
            jPanel.add(BorderLayout.NORTH, jPanel1);
            jPanel.add(jScrollPane);
            ad_cycle.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".cycle", ad_cycle.getText());
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".cycle", ad_cycle.getText());
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".cycle", ad_cycle.getText());
                }
            });
            ad.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".text", ad.getText());
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".text", ad.getText());
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("ad." + i + ".text", ad.getText());
                }
            });
            return jPanel;
        }

        /**
         * 加载测试消息
         */
        public void loadTestProperties() {
            JScrollPane jScrollPane = new JScrollPane(test_text);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            test_text.setText(SettingContext.getInofoSetting().getProperty("test.text", ""));
            test.add(BorderLayout.NORTH, new JLabel("测试消息"));
            test.add(BorderLayout.CENTER, jScrollPane);
            test_text.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("test.text", test_text.getText());
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("test.text", test_text.getText());
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    SettingContext.setInfoSetting("test.text", test_text.getText());
                }
            });
        }

        public MessageSetting() {
            loadTingcaiProperties();
            loadKaijiangProperties();
            loadAdProperties();
            loadScoreProperties();
            loadTestProperties();
            jTabbedPane.addTab("停猜", tingcai);
            jTabbedPane.addTab("开奖", kaijiang);
            jTabbedPane.addTab("积分", score);
            jTabbedPane.addTab("广告", ad);
            jTabbedPane.addTab("测试", test);
            this.add(jTabbedPane);
        }
    }

    //赔率设置
    public class OddSetting extends JPanel {
        JPanel jPanelNorth = new JPanel(new FlowLayout());
        JPanel jPanelCenter = new JPanel(new GridLayout(1, 2));
        JPanel jPanelSouth = new JPanel();

        JPanel dandianshuzi = new JPanel(new BorderLayout());//单点数字
        JPanel dandian_tongyi_jpanel = new JPanel(new FlowLayout());
        JCheckBox dandian_tongyipeilv = new JCheckBox("统一赔率");
        JTextField dandian_tongyi = new JTextField();
        JPanel dandian_dandu_jpanel = new JPanel(new FlowLayout());
        JCheckBox dandian_dandu_check = new JCheckBox("单独赔率:");
        JTextArea dandian_dandu = new JTextArea();

        JPanel dandian_baozi = new JPanel(new BorderLayout());//单点数字旁边的豹子
        JCheckBox dandian_baozi_baozi = new JCheckBox("豹子");
        JTextField dandian_baozi_baozi_text = new JTextField();
        JCheckBox dandian_baozi_duizi = new JCheckBox("对子");
        JTextField dandian_baozi_duizi_text = new JTextField();
        JCheckBox dandian_baozi_shunzi = new JCheckBox("顺子");
        JTextField dandian_baozi_shunzi_text = new JTextField();

        JPanel daxiaodanshuang = new JPanel(new GridLayout(1, 2));//大小单双
        JTextField daxiaodanshuang_peilv = new JTextField(3);//赔率
        JTextField daxiaodanshuang_jida = new JTextField(3);//极大赔率
        JTextField daxiaodanshuang_jixiao = new JTextField(3);//极小赔率
        JCheckBox daxiaodanshuang_zongzhu1_check = new JCheckBox("总注>");//总注1
        JTextField daxiaodanshuang_zongzhu1 = new JTextField(3);//总注1
        JTextField daxiaodanshuang_zongzhu1_peilv = new JTextField(3);//总注1赔率
        JCheckBox daxiaodanshuang_zongzhu2_check = new JCheckBox("总注>");//总注2
        JTextField daxiaodanshuang_zongzhu2 = new JTextField(3);//总注2
        JTextField daxiaodanshuang_zongzhu2_peilv = new JTextField(3);//总注2赔率
        JCheckBox daxiaodanshuang_zongzhu3_check = new JCheckBox("总注>");//总注3
        JTextField daxiaodanshuang_zongzhu3 = new JTextField(3);//总注3
        JTextField daxiaodanshuang_zongzhu3_peilv = new JTextField(3);//总注3赔率
        JPanel daxiaodanshuang_right = new JPanel(new GridLayout(3, 1));//大小单双设置豹子赔率
        JCheckBox daxiaodanshuang_right_baozi = new JCheckBox("出豹子时,大小单双一律");
        JTextField daxiaodanshuang_right_baozi_peilv = new JTextField(3);
        JCheckBox daxiaodanshuang_right_duizi = new JCheckBox("出对子时,大小单双一律");
        JTextField daxiaodanshuang_right_duizi_peilv = new JTextField(3);
        JCheckBox daxiaodanshuang_right_shunzi = new JCheckBox("出顺子时,大小单双一律");
        JTextField daxiaodanshuang_right_shunzi_peilv = new JTextField(3);

        JPanel zuhe = new JPanel();
        JPanel zuhe_suanfa1 = new JPanel(new FlowLayout());
        JCheckBox zuhe_suanfa1_check = new JCheckBox("算法(一):");
        JTextField zuhe_suanfa1_tongyi = new JTextField(3);
        JCheckBox zuhe_suanfa1_check_13_14 = new JCheckBox("13 14");
        JTextField zuhe_suanfa1_13_14 = new JTextField(3);

        JPanel zuhe_suanfa2 = new JPanel();
        JCheckBox zuhe_suanfa2_check = new JCheckBox("算法(二):");
        JTextField zuhe_suanfa2_1 = new JTextField(3);
        JTextField zuhe_suanfa2_2 = new JTextField(3);

        JCheckBox zuhe_zongzhu_1 = new JCheckBox("总注>");
        JTextField zuhe_zongzhu_1_text = new JTextField(3);
        JTextField zuhe_zongzhu_1_text1 = new JTextField(3);
        JCheckBox zuhe_zongzhu_2 = new JCheckBox("总注>");
        JTextField zuhe_zongzhu_2_text = new JTextField(3);
        JTextField zuhe_zongzhu_2_text1 = new JTextField(3);
        JCheckBox zuhe_zongzhu_3 = new JCheckBox("总注>");
        JTextField zuhe_zongzhu_3_text = new JTextField(3);
        JTextField zuhe_zongzhu_3_text1 = new JTextField(3);

        JCheckBox zuhe_baozi = new JCheckBox("出豹子时,组合一律");
        JTextField zuhe_baozi_peilv = new JTextField(3);
        JCheckBox zuhe_duizi = new JCheckBox("出对子时,组合一律");
        JTextField zuhe_duizi_peilv = new JTextField(3);
        JCheckBox zuhe_shunzi = new JCheckBox("出顺子时,组合一律");
        JTextField zuhe_shunzi_peilv = new JTextField(3);

        public OddSetting() {
            super(new BorderLayout());
            dandianshuzi.setBorder(BorderFactory.createTitledBorder("单点数字"));
            dandian_tongyipeilv.setSelected("true".equalsIgnoreCase(SettingContext.getInofoSetting().getProperty("dandian.tongyi.check", "false")) ? true : false);
            dandian_tongyipeilv.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("dandian.tongyi.check", String.valueOf(dandian_tongyipeilv.isSelected()));
                    if (dandian_tongyipeilv.isSelected()) {
                        dandian_dandu_check.setSelected(false);
                        SettingContext.setInfoSetting("dandian.dandu.check", "false");
                    } else {
                        dandian_dandu_check.setSelected(true);
                        SettingContext.setInfoSetting("dandian.dandu.check", "true");
                    }
                }
            });
            dandian_tongyi_jpanel.add(dandian_tongyipeilv);
            dandian_tongyi.setColumns(2);
            setTextFieldChangeAction(dandian_tongyi, "dandian.tongyi.peilv");
            dandian_tongyi_jpanel.add(dandian_tongyi);
            dandian_tongyi_jpanel.add(new JLabel("倍"));
            dandianshuzi.add(BorderLayout.WEST, dandian_tongyi_jpanel);

            dandian_dandu_check.setSelected("true".equalsIgnoreCase(SettingContext.getInofoSetting().getProperty("dandian.dandu.check", "false")) ? true : false);
            dandian_dandu_check.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("dandian.dandu.check", String.valueOf(dandian_dandu_check.isSelected()));
                    if (dandian_tongyipeilv.isSelected()) {
                        dandian_tongyipeilv.setSelected(false);
                        SettingContext.setInfoSetting("dandian.tongyi.check", "false");
                    } else {
                        dandian_tongyipeilv.setSelected(true);
                        SettingContext.setInfoSetting("dandian.tongyi.check", "true");

                    }
                }
            });
            dandian_dandu_jpanel.add(BorderLayout.WEST, dandian_dandu_check);
            dandian_dandu.setColumns(10);
            dandian_dandu.setRows(5);
            setTextAreaChangeAction(dandian_dandu, "dandian.dandu.text");
            JScrollPane jScrollPane = new JScrollPane(dandian_dandu);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            dandian_dandu_jpanel.add(BorderLayout.EAST, jScrollPane);
            dandianshuzi.add(dandian_dandu_jpanel);
            jPanelNorth.add(dandianshuzi);

            dandian_baozi.setBorder(BorderFactory.createTitledBorder("豹子"));
            JPanel jPanel1 = new JPanel(new FlowLayout());
            setCheckBoxClickAction(dandian_baozi_baozi, "dandian.baozi.check");
            jPanel1.add(dandian_baozi_baozi);
            dandian_baozi_baozi_text.setColumns(3);
            setTextFieldChangeAction(dandian_baozi_baozi_text, "dandian.baozi.text");
            jPanel1.add(dandian_baozi_baozi_text);
            jPanel1.add(new JLabel("倍"));
            dandian_baozi.add(BorderLayout.NORTH, jPanel1);

            jPanel1 = new JPanel(new FlowLayout());
            setCheckBoxClickAction(dandian_baozi_duizi, "dandian.duizi.check");
            jPanel1.add(dandian_baozi_duizi);
            dandian_baozi_duizi_text.setColumns(3);
            setTextFieldChangeAction(dandian_baozi_duizi_text, "dandian.duizi.text");
            jPanel1.add(dandian_baozi_duizi_text);
            jPanel1.add(new JLabel("倍"));
            dandian_baozi.add(BorderLayout.CENTER, jPanel1);

            jPanel1 = new JPanel(new FlowLayout());
            setCheckBoxClickAction(dandian_baozi_shunzi, "dandian.shunzi.check");
            jPanel1.add(dandian_baozi_shunzi);
            dandian_baozi_shunzi_text.setColumns(3);
            setTextFieldChangeAction(dandian_baozi_shunzi_text, "dandian.shunzi.text");
            jPanel1.add(dandian_baozi_shunzi_text);
            jPanel1.add(new JLabel("倍(012中,890不中)"));
            dandian_baozi.add(BorderLayout.SOUTH, jPanel1);
            jPanelNorth.add(dandian_baozi);


            //大小单双
            jPanelCenter.setBorder(BorderFactory.createTitledBorder("大小单双"));
            JPanel jPanel = new JPanel(new GridLayout(3, 2));
            jPanel1 = new JPanel(new FlowLayout());
            jPanel1.add(new JLabel("赔率:"));
            daxiaodanshuang_peilv.setColumns(3);
            setTextFieldChangeAction(daxiaodanshuang_peilv, "daxiaodanshuang.peilv.text");
            jPanel1.add(daxiaodanshuang_peilv);
            jPanel1.add(new JLabel("倍"));
            jPanel.add(jPanel1);
            jPanel1 = new JPanel();
            setCheckBoxClickAction(daxiaodanshuang_zongzhu1_check, "daxiaodanshuang.zongzhu1.check");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu1, "daxiaodanshuang.zongzhu1.text");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu1_peilv, "daxiaodanshuang.zongzhu1.peilv");
            jPanel1.add(daxiaodanshuang_zongzhu1_check);
            jPanel1.add(daxiaodanshuang_zongzhu1);
            jPanel1.add(new JLabel("时,13 14"));
            jPanel1.add(daxiaodanshuang_zongzhu1_peilv);
            jPanel1.add(new JLabel("倍"));
            jPanel.add(jPanel1);
            //极大
            jPanel1 = new JPanel();
            jPanel1.add(new JLabel("极大:"));
            jPanel1.add(daxiaodanshuang_jida);
            setTextFieldChangeAction(daxiaodanshuang_jida, "daxiaodanshuang.jida");
            jPanel.add(jPanel1);
            jPanel1 = new JPanel(new FlowLayout());
            setCheckBoxClickAction(daxiaodanshuang_zongzhu2_check, "daxiaodanshuang.zongzhu2.check");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu2, "daxiaodanshuang.zongzhu2.text");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu2_peilv, "daxiaodanshuang.zongzhu2.peilv");
            jPanel1.add(daxiaodanshuang_zongzhu2_check);
            jPanel1.add(daxiaodanshuang_zongzhu2);
            jPanel1.add(new JLabel("时,13 14"));
            jPanel1.add(daxiaodanshuang_zongzhu2_peilv);
            jPanel1.add(new JLabel("倍"));
            jPanel.add(jPanel1);
            //极小
            jPanel1 = new JPanel();
            jPanel1.add(new JLabel("极小:"));
            jPanel1.add(daxiaodanshuang_jixiao);
            setTextFieldChangeAction(daxiaodanshuang_jixiao, "daxiaodanshuang.jixiao");
            jPanel.add(jPanel1);
            jPanel1 = new JPanel(new FlowLayout());
            setCheckBoxClickAction(daxiaodanshuang_zongzhu3_check, "daxiaodanshuang.zongzhu3.check");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu3, "daxiaodanshuang.zongzhu3.text");
            setTextFieldChangeAction(daxiaodanshuang_zongzhu3_peilv, "daxiaodanshuang.zongzhu3.peilv");
            jPanel1.add(daxiaodanshuang_zongzhu3_check);
            jPanel1.add(daxiaodanshuang_zongzhu3);
            jPanel1.add(new JLabel("时,13 14"));
            jPanel1.add(daxiaodanshuang_zongzhu3_peilv);
            jPanel1.add(new JLabel("倍"));
            jPanel.add(jPanel1);
            jPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            daxiaodanshuang.add(jPanel);
            daxiaodanshuang_right.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            setCheckBoxClickAction(daxiaodanshuang_right_baozi, "daxiaodanshuang.right.baozi.check");
            setTextFieldChangeAction(daxiaodanshuang_right_baozi_peilv, "daxiaodanshuang.right.baozi.peilv");
            daxiaodanshuang_right.add(daxiaodanshuang_right_baozi);
            daxiaodanshuang_right.add(daxiaodanshuang_right_baozi_peilv);
            daxiaodanshuang_right.add(new JLabel("倍"));
            setCheckBoxClickAction(daxiaodanshuang_right_duizi, "daxiaodanshuang.right.duizi.check");
            setTextFieldChangeAction(daxiaodanshuang_right_duizi_peilv, "daxiaodanshuang.right.duizi.peilv");
            daxiaodanshuang_right.add(daxiaodanshuang_right_duizi);
            daxiaodanshuang_right.add(daxiaodanshuang_right_duizi_peilv);
            daxiaodanshuang_right.add(new JLabel("倍"));
            setCheckBoxClickAction(daxiaodanshuang_right_shunzi, "daxiaodanshuang.right.shunzi.check");
            setTextFieldChangeAction(daxiaodanshuang_right_shunzi_peilv, "daxiaodanshuang.right.shunzi.peilv");
            daxiaodanshuang_right.add(daxiaodanshuang_right_shunzi);
            daxiaodanshuang_right.add(daxiaodanshuang_right_shunzi_peilv);
            daxiaodanshuang_right.add(new JLabel("倍"));
            daxiaodanshuang.add(daxiaodanshuang_right);
            jPanelCenter.add(daxiaodanshuang);
            jPanelCenter.add(daxiaodanshuang_right);

            GridBagLayout gridBagLayout = new GridBagLayout();
            zuhe.setLayout(gridBagLayout);
            zuhe.setBorder(BorderFactory.createTitledBorder("组合"));

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.gridheight = 1;
            gbc.gridwidth = 1;
            zuhe_suanfa1.setBorder(BorderFactory.createTitledBorder("算法(一)"));
            zuhe_suanfa1_check.setSelected("true".equalsIgnoreCase(SettingContext.getInofoSetting().getProperty("zuhe.suanfa1.check", "false")) ? true : false);
            zuhe_suanfa1_check.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("zuhe.suanfa1.check", String.valueOf(zuhe_suanfa1_check.isSelected()));
                    if (zuhe_suanfa1_check.isSelected()) {
                        zuhe_suanfa2_check.setSelected(false);
                        SettingContext.setInfoSetting("zuhe.suanfa2.check", "false");
                    } else {
                        zuhe_suanfa2_check.setSelected(true);
                        SettingContext.setInfoSetting("zuhe.suanfa2.check", "true");
                    }
                }
            });
            setTextFieldChangeAction(zuhe_suanfa1_tongyi, "zuhe.suanfa1.tongyi");
            setCheckBoxClickAction(zuhe_suanfa1_check_13_14, "zuhe.suanfa1.1314.check");
            setTextFieldChangeAction(zuhe_suanfa1_13_14, "zuhe.suanfa1.1314.text");
            zuhe_suanfa1.add(zuhe_suanfa1_check);
            zuhe_suanfa1.add(new JLabel("统一赔率:"));
            zuhe_suanfa1.add(zuhe_suanfa1_tongyi);
            zuhe_suanfa1.add(new JLabel("倍"));
            zuhe_suanfa1.add(zuhe_suanfa1_check_13_14);
            zuhe_suanfa1.add(zuhe_suanfa1_13_14);
            zuhe_suanfa1.add(new JLabel("倍"));
            gridBagLayout.setConstraints(zuhe_suanfa1, gbc);
            zuhe.add(zuhe_suanfa1);

            gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 1;
            gbc.gridheight = 2;
            gbc.gridwidth = 1;
            zuhe_suanfa2.setBorder(BorderFactory.createTitledBorder("算法(二)"));
            zuhe_suanfa2_check.setSelected("true".equalsIgnoreCase(SettingContext.getInofoSetting().getProperty("zuhe.suanfa2.check", "false")) ? true : false);
            zuhe_suanfa2_check.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("zuhe.suanfa2.check", String.valueOf(zuhe_suanfa2_check.isSelected()));
                    if (zuhe_suanfa2_check.isSelected()) {
                        zuhe_suanfa1_check.setSelected(false);
                        SettingContext.setInfoSetting("zuhe.suanfa1.check", "false");
                    } else {
                        zuhe_suanfa1_check.setSelected(true);
                        SettingContext.setInfoSetting("zuhe.suanfa1.check", "true");
                    }
                }
            });
            setTextFieldChangeAction(zuhe_suanfa2_1, "zuhe.suanfa2.1");
            setTextFieldChangeAction(zuhe_suanfa2_2, "zuhe.suanfa2.2");
            zuhe_suanfa2.add(zuhe_suanfa2_check);
            zuhe_suanfa2.add(new JLabel("大双,小单:"));
            zuhe_suanfa2.add(zuhe_suanfa2_1);
            zuhe_suanfa2.add(new JLabel("倍"));
            zuhe_suanfa2.add(new JLabel("小双,大单"));
            zuhe_suanfa2.add(zuhe_suanfa2_2);
            zuhe_suanfa2.add(new JLabel("倍"));
            gridBagLayout.setConstraints(zuhe_suanfa2, gbc);
            zuhe.add(zuhe_suanfa2);

            gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 3;
            gbc.gridheight = 3;
            gbc.gridwidth = 1;
            jPanel = new JPanel(new GridLayout(3, 1));
            JPanel jPanel2 = new JPanel();

            setCheckBoxClickAction(zuhe_zongzhu_1, "zuhe.zongzhu.1.check");
            setTextFieldChangeAction(zuhe_zongzhu_1_text, "zuhe.zongzhu.1.text");
            setTextFieldChangeAction(zuhe_zongzhu_1_text1, "zuhe.zongzhu.1.text1");
            jPanel2.add(zuhe_zongzhu_1);
            jPanel2.add(zuhe_zongzhu_1_text);
            jPanel2.add(new JLabel("时,13 14"));
            jPanel2.add(zuhe_zongzhu_1_text1);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            jPanel2 = new JPanel();
            setCheckBoxClickAction(zuhe_zongzhu_2, "zuhe.zongzhu.2.check");
            setTextFieldChangeAction(zuhe_zongzhu_2_text, "zuhe.zongzhu.2.text");
            setTextFieldChangeAction(zuhe_zongzhu_2_text1, "zuhe.zongzhu.2.text1");
            jPanel2.add(zuhe_zongzhu_2);
            jPanel2.add(zuhe_zongzhu_2_text);
            jPanel2.add(new JLabel("时,13 14"));
            jPanel2.add(zuhe_zongzhu_2_text1);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            jPanel2 = new JPanel();
            setCheckBoxClickAction(zuhe_zongzhu_3, "zuhe.zongzhu.3.check");
            setTextFieldChangeAction(zuhe_zongzhu_3_text, "zuhe.zongzhu.3.text");
            setTextFieldChangeAction(zuhe_zongzhu_3_text1, "zuhe.zongzhu.3.text1");
            jPanel2.add(zuhe_zongzhu_3);
            jPanel2.add(zuhe_zongzhu_3_text);
            jPanel2.add(new JLabel("时,13 14"));
            jPanel2.add(zuhe_zongzhu_3_text1);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            gridBagLayout.setConstraints(jPanel, gbc);
            zuhe.add(jPanel);

            gbc = new GridBagConstraints();
            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.gridheight = 6;
            gbc.gridwidth = 1;
            jPanel = new JPanel(new GridLayout(6, 1));
            jPanel.add(new JLabel(""));
            jPanel.add(new JLabel(""));
            jPanel.add(new JLabel(""));
            jPanel2 = new JPanel();
            setCheckBoxClickAction(zuhe_baozi, "zuhe.baozi.check");
            setTextFieldChangeAction(zuhe_baozi_peilv, "zuhe.baozi.peilv");
            jPanel2.add(zuhe_baozi);
            jPanel2.add(zuhe_baozi_peilv);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            jPanel2 = new JPanel();
            setCheckBoxClickAction(zuhe_duizi, "zuhe.duizi.check");
            setTextFieldChangeAction(zuhe_duizi_peilv, "zuhe.duizi.peilv");
            jPanel2.add(zuhe_duizi);
            jPanel2.add(zuhe_duizi_peilv);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            jPanel2 = new JPanel();
            setCheckBoxClickAction(zuhe_shunzi, "zuhe.shunzi.check");
            setTextFieldChangeAction(zuhe_shunzi_peilv, "zuhe.shunzi.peilv");
            jPanel2.add(zuhe_shunzi);
            jPanel2.add(zuhe_shunzi_peilv);
            jPanel2.add(new JLabel("倍"));
            jPanel.add(jPanel2);
            gridBagLayout.setConstraints(jPanel, gbc);
            zuhe.add(jPanel);

            jPanelSouth.add(zuhe);

            this.add(BorderLayout.NORTH, jPanelNorth);
            this.add(BorderLayout.CENTER, jPanelCenter);
            this.add(BorderLayout.SOUTH, jPanelSouth);
        }
    }

    public class Down extends JPanel {
        JPanel kefu = new JPanel(new BorderLayout());
        JPanel btns = new JPanel(new FlowLayout());
        JPanel jPanel = new JPanel(new FlowLayout());
        JLabel id = new JLabel("id:");
        JTextField userName = new JTextField();
        JButton top = new JButton("置顶");
        JButton bottom = new JButton("置底");
        JButton cancelTop = new JButton("取消置顶/底");
        JButton remove = new JButton("踢出玩家");
        JCheckBox robot = new JCheckBox("假人");
        JTextField score = new JTextField();
        JButton changeScore = new JButton("修改");

        public JButton stopCai_ = new JButton("停猜");
        public JButton stopCai = new JButton("手动开奖");
        JButton checkContent = new JButton("内容核对");
        JButton fieldWin = new JButton("填入开奖");
        JButton fabujifenliebiao = new JButton("发布积分列表");
        JCheckBox auitoFaBu = new JCheckBox("自动发布积分列表");
        JButton testMsg = new JButton("测试消息");
        JButton changeName = new JButton("改名");

        public Down() {
            super(new BorderLayout());
            jPanel.add(id);
            jPanel.add(new JLabel("昵称:"));
            userName.setColumns(10);
            jPanel.add(userName);
            jPanel.add(changeName);
            jPanel.add(top);
            jPanel.add(bottom);
            jPanel.add(cancelTop);
            jPanel.add(remove);
            jPanel.add(robot);
            jPanel.add(new JLabel("积分:"));
            score.setColumns(10);
            jPanel.add(score);
            jPanel.add(changeScore);
            kefu.add(BorderLayout.NORTH, jPanel);

            top.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member != null) {
                            member.setSort(Member.SORT_TOP);
                            memberService.save(member);
                        }
                        flashAllUser();
                    }
                }
            });
            bottom.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member != null) {
                            member.setSort(Member.SORT_BOTTOM);
                            memberService.save(member);
                        }
                        flashAllUser();
                    }
                }
            });
            cancelTop.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member != null) {
                            member.setSort(Member.SORT_NORMAL);
                            memberService.save(member);
                        }
                        flashAllUser();
                    }
                }
            });

            //改名
            changeName.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member != null && StringUtils.isNotEmpty(userName.getText()) && !userName.getText().equals(member.getNickName())) {
                            member.setNickName(userName.getText());
                            memberService.save(member);
                        }
                        flashAllUser();
                    }
                }
            });

            //改分
            changeScore.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    Double score_ = null;
                    try {
                        score_ = Double.parseDouble(score.getText());
                    } catch (Exception ee) {
                        score_ = null;
                    }
                    if (id_ > 0 && score != null) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member != null && member.getMoney() != score_) {
                            if (member.getMoney() > score_) {
                                member.setAllUse(member.getAllUse() + (member.getMoney() - score_));
                            } else {
                                member.setScore(member.getScore() + (score_ - member.getMoney()));
                            }
                            memberService.save(member);
                        }
                        flashAllUser();
                    }
                }
            });

            remove.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member == null) {
                            return;
                        }
                        if (member.getStatus() == Member.STATUS_BAN || member.getStatus() == Member.STATUS_BOT_BAN) {
                            if (member.getStatus() == Member.STATUS_BAN) {
                                member.setStatus(Member.STATUS_NORMAL);
                            } else if (member.getStatus() == Member.STATUS_BOT_BAN) {
                                member.setStatus(Member.STATUS_BOT);
                            }
                            memberService.save(member);
                            down.remove.setText("踢出玩家");
                        } else {
                            if (member.getStatus() == Member.STATUS_NORMAL) {
                                member.setStatus(Member.STATUS_BAN);
                            } else if (member.getStatus() == Member.STATUS_BOT) {
                                member.setStatus(Member.STATUS_BOT_BAN);
                            }
                            memberService.save(member);
                            down.remove.setText("恢复玩家");
                        }
                        flashAllUser();
                    }
                }
            });

            robot.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    long id_ = 0;
                    try {
                        id_ = Long.parseLong(id.getText().replaceAll("id:", ""));
                    } catch (Exception ee) {
                        id_ = 0;
                    }
                    if (id_ > 0) {
                        MemberService memberService = SpringContextUtil.getBean(MemberService.class);
                        Member member = memberService.getById(id_);
                        if (member.getStatus() == Member.STATUS_BOT) {
                            member.setStatus(Member.STATUS_NORMAL);
                            robot.setSelected(false);
                        } else if (member.getStatus() == Member.STATUS_NORMAL) {
                            member.setStatus(Member.STATUS_BOT);
                            robot.setSelected(true);
                        } else if (member.getStatus() == Member.STATUS_BAN) {
                            member.setStatus(Member.STATUS_BOT_BAN);
                            robot.setSelected(true);
                        } else if (member.getStatus() == Member.STATUS_BOT_BAN) {
                            member.setStatus(Member.STATUS_BAN);
                            robot.setSelected(false);
                        }
                        memberService.save(member);
                        flashAllUser();
                    }
                }
            });



            kefu.setBorder(BorderFactory.createTitledBorder("客服"));
            this.add(BorderLayout.NORTH, kefu);

            stopCai_.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (GameContext.autoStopBetting) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "自动开奖不能手动停猜！", "系统信息", JOptionPane.ERROR_MESSAGE);
                    } else {
                        GameContext.doStopBetting();
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "停猜成功！", "系统信息", JOptionPane.QUESTION_MESSAGE);
                    }
                }
            });

            stopCai.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!GameContext.canPlay) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "游戏未启动！", "系统信息", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    if (GameContext.autoStopBetting) {
                        if (GameContext.canBetting) {
                            JOptionPane.showMessageDialog(new JFrame().getContentPane(), "请先停猜,然后再切换为自动开奖！", "系统信息", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        GameContext.autoStopBetting = false;
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "切换手动开奖成功！", "系统信息", JOptionPane.QUESTION_MESSAGE);
                        stopCai.setText("自动开奖");
                    } else {
                        try {
                            String str = JOptionPane.showInputDialog("请填入上一期开奖结果(英文逗号分隔,注意:请确保在正确开奖时间手动开奖):");
                            String[] strings = str.split(",");
                            if (strings.length != 3) {
                                throw new Exception("滴滴滴滴");
                            }
                            int result1 = Integer.parseInt(strings[0]);
                            int result2 = Integer.parseInt(strings[1]);
                            int result3 = Integer.parseInt(strings[2]);
                            GameContext.kaijiang(result1, result2, result3);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            JOptionPane.showMessageDialog(new JFrame().getContentPane(), "输入格式有误!\n请重新输入！", "系统信息", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                        GameContext.autoStopBetting = true;
                        GameContext.start();
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "切换自动开奖成功！", "系统信息", JOptionPane.QUESTION_MESSAGE);
                        stopCai.setText("手动开奖");
                    }
                }
            });
            btns.add(stopCai_);
            btns.add(stopCai);
            checkContent.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!GameContext.canPlay) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "游戏未启动！", "系统信息", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    RobotContext.sendMsg(GameContext.dealMsg("当前下注情况:{下注情况}"));
                    JOptionPane.showMessageDialog(new JFrame().getContentPane(), "发送成功！", "系统信息", JOptionPane.QUESTION_MESSAGE);
                }
            });
            btns.add(checkContent);
            fieldWin.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!GameContext.canPlay) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "游戏未启动！", "系统信息", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    if (GameContext.autoStopBetting) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "请先手动开奖！", "系统信息", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    if (GameContext.canBetting) {
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "请先手动停猜！", "系统信息", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    try {
                        String str = JOptionPane.showInputDialog("请填入开奖结果(英文逗号分隔,注意:请确保在正确开奖时间手动开奖):");
                        if (StringUtils.isEmpty(str)) {
                            return;
                        }
                        String[] strings = str.split(",");
                        if (strings.length != 3) {
                            throw new Exception("滴滴滴滴");
                        }
                        int result1 = Integer.parseInt(strings[0]);
                        int result2 = Integer.parseInt(strings[1]);
                        int result3 = Integer.parseInt(strings[2]);
                        GameContext.kaijiang(result1, result2, result3);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        JOptionPane.showMessageDialog(new JFrame().getContentPane(), "输入格式有误!\n请重新输入！", "系统信息", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
            btns.add(fieldWin);
            fabujifenliebiao.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    RobotContext.sendMsg(GameContext.dealMsg("{玩家列表}"));
                }
            });
            btns.add(fabujifenliebiao);
            String score_auto = SettingContext.getInofoSetting().getProperty("score.auto", "false");
            this.auitoFaBu.setSelected("true".equalsIgnoreCase(score_auto) ? true : false);
            this.auitoFaBu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SettingContext.setInfoSetting("score.auto", String.valueOf(auitoFaBu.isSelected()));
                }
            });
            btns.add(auitoFaBu);
            btns.add(testMsg);
            this.add(BorderLayout.CENTER, btns);
        }
    }

    //规则1
    public class Rule extends JPanel {
        JPanel jPanelNorth = new JPanel(new FlowLayout());

        JPanel dandianshuzi = new JPanel(new BorderLayout());//单点数字
        JPanel dandian_tongyi_jpanel = new JPanel(new FlowLayout());
        JCheckBox minimum = new JCheckBox("单注最低");
        JTextField dandian_tongyi = new JTextField();
        JPanel dandian_dandu_jpanel = new JPanel(new FlowLayout());

        /**
         * ---------------start:单注封顶-------------
         */
        JPanel xiane = new JPanel(new GridLayout(10, 1));
        JLabel fengding = new JLabel("单注封顶:");
        JCheckBox shuzi = new JCheckBox("单点数字");
        JTextField shuzikuang = new JTextField();
        JCheckBox dandianshuzizongzhu = new JCheckBox("按单点数字总注计算");
        JPanel dandian_fengding_jpanel = new JPanel(new FlowLayout());

        /**---------------end:单注封顶-------------*/


        /**
         * ---------------start:总注封顶-------------
         */


        /**
         * ---------------start:大小-总注-------------
         */

        JCheckBox daxiaodanshuang = new JCheckBox("大小单双");
        JCheckBox zuhe = new JCheckBox("组合");
        JCheckBox jidajixiao = new JCheckBox("极大极小");
        JCheckBox baozi = new JCheckBox("豹子");
        JCheckBox duizi = new JCheckBox("对子");
        JCheckBox shunzi = new JCheckBox("顺子");
        JCheckBox zongzhu = new JCheckBox("总注");

        JTextField daxiaodanshuang_text = new JTextField();
        JTextField zuhe_text = new JTextField();
        JTextField jidajixiao_text = new JTextField();
        JTextField baozi_text = new JTextField();
        JTextField duizi_text = new JTextField();
        JTextField shunzi_text = new JTextField();
        JTextField zongzhu_text = new JTextField();

        JPanel j1 = new JPanel();
        JPanel j2 = new JPanel();
        JPanel j3 = new JPanel();
        JPanel j4 = new JPanel();
        JPanel j5 = new JPanel();
        JPanel j6 = new JPanel();
        JPanel j7 = new JPanel();

        Nixiang nixiang = new Nixiang();

        /**
         * ---------------end:大小-顺子-------------
         */

        public Rule() {
            super(new BorderLayout());
            dandianshuzi.setBorder(BorderFactory.createTitledBorder("猜猜分数限额"));
            dandian_tongyi_jpanel.add(minimum);
            dandian_tongyi.setColumns(4);
            setRuleCheckBoxClickAction(minimum, "danzhuzuidi.check");
            setRuleTextFieldChangeAction(dandian_tongyi, "danzhuzuidi.text");

            dandian_tongyi_jpanel.add(dandian_tongyi);
            dandian_tongyi_jpanel.add(new JLabel("起"));
            dandianshuzi.add(BorderLayout.WEST, dandian_tongyi_jpanel);

            /**---------------start:单注封顶-------------*/

            dandian_fengding_jpanel.add(fengding);
            dandian_fengding_jpanel.add(shuzi);
            shuzikuang.setColumns(4);

            setRuleCheckBoxClickAction(shuzi, "dandianshuzi.check");
            setRuleTextFieldChangeAction(shuzikuang, "dandianshuzi.text");

            dandian_fengding_jpanel.add(shuzikuang);
            JPanel jPanelzongshu = new JPanel();
            setRuleCheckBoxClickAction(dandianshuzizongzhu, "dandianshuzizongzhu.check");

            jPanelzongshu.add(dandianshuzizongzhu);
            xiane.add(dandian_fengding_jpanel);
            xiane.add(jPanelzongshu);

            /**---------------end:单注封顶-------------*/

            /**---------------start:大小-总注-------------*/
            daxiaodanshuang_text.setColumns(4);
            zuhe_text.setColumns(4);
            jidajixiao_text.setColumns(4);
            baozi_text.setColumns(4);
            duizi_text.setColumns(4);
            shunzi_text.setColumns(4);
            zongzhu_text.setColumns(4);

            setRuleCheckBoxClickAction(daxiaodanshuang, "daxiaodanshuang.check");
            setRuleTextFieldChangeAction(daxiaodanshuang_text, "daxiaodanshuang.text");
            setRuleCheckBoxClickAction(zuhe, "zuhe.check");
            setRuleTextFieldChangeAction(zuhe_text, "zuhe.text");
            setRuleCheckBoxClickAction(jidajixiao, "jidajixiao.check");
            setRuleTextFieldChangeAction(jidajixiao_text, "jidajixiao.text");
            setRuleCheckBoxClickAction(baozi, "baozi.check");
            setRuleTextFieldChangeAction(baozi_text, "baozi.text");
            setRuleCheckBoxClickAction(duizi, "duizi.check");
            setRuleTextFieldChangeAction(duizi_text, "duizi.text");
            setRuleCheckBoxClickAction(shunzi, "shunzi.check");
            setRuleTextFieldChangeAction(shunzi_text, "shunzi.text");
            setRuleCheckBoxClickAction(zongzhu, "zongzhu.check");
            setRuleTextFieldChangeAction(zongzhu_text, "zongzhu.text");

            j1.add(daxiaodanshuang);
            j1.add(daxiaodanshuang_text);

            j2.add(zuhe);
            j2.add(zuhe_text);

            j3.add(jidajixiao);
            j3.add(jidajixiao_text);

            j4.add(baozi);
            j4.add(baozi_text);

            j5.add(duizi);
            j5.add(duizi_text);

            j6.add(shunzi);
            j6.add(shunzi_text);

            j7.add(zongzhu);
            j7.add(zongzhu_text);

            xiane.add(j1);
            xiane.add(j2);
            xiane.add(j3);
            xiane.add(j4);
            xiane.add(j5);
            xiane.add(j6);
            xiane.add(j7);

            /**---------------end:大小-顺子-------------*/


            dandianshuzi.add(dandian_dandu_jpanel, BorderLayout.NORTH);
            dandianshuzi.add(xiane, BorderLayout.SOUTH);

            jPanelNorth.add(dandianshuzi);

            this.add(jPanelNorth, BorderLayout.WEST);
            this.add(nixiang, BorderLayout.EAST);
        }
    }

    //规则2
    public class Nixiang extends JPanel {
        JPanel jPanelNorth = new JPanel(new FlowLayout());

        JPanel dandianshuzi = new JPanel(new BorderLayout());//单点数字
        JPanel dandian_tongyi_jpanel = new JPanel(new FlowLayout());
        JPanel main = new JPanel(new GridLayout(13, 1));

        /**
         * [禁止杀组合]组件
         */
        JCheckBox jinzhishazuhe = new JCheckBox("禁止杀组合");
        JPanel zongzhudayu_panel = new JPanel();
        JCheckBox jinzhishazuhe_zongzhu_check = new JCheckBox("总注大于");
        JTextField jinzhishazuhe_zongzhu_text = new JTextField();
        JLabel liewai = new JLabel("时列外");

        /**
         * 禁止反向组合组件
         */
        JCheckBox jinzhifanxiangzuhe = new JCheckBox("禁止反向组合");
        JPanel zongzhudayu_panel1 = new JPanel();
        JCheckBox jinzhifanxiangzuhe_zongzhu_check = new JCheckBox("总注大于");
        JTextField jinzhifanxiangzuhe_zongzhu_text = new JTextField();
        JLabel liewai1 = new JLabel("时列外");

        /**
         * 禁止同向组合组件
         */
        JCheckBox jinzhitongxiangzuhe = new JCheckBox("禁止同向组合");
        JPanel zongzhudayu_panel2 = new JPanel();
        JCheckBox jinzhitongxiangzuhe_zongzhu_check = new JCheckBox("总注大于");
        JTextField jinzhitongxiangzuhe_zongzhu_text = new JTextField();
        JLabel liewai2 = new JLabel("时列外");

        /**
         * 禁止大小单双反向组件
         */
        JCheckBox jinzhidaxiaodanshuangfanxiang = new JCheckBox("禁止大小单双反向");
        JPanel zongzhudayu_panel3 = new JPanel();
        JCheckBox jinzhidaxiaodanshuangfanxiang_zongzhu_check = new JCheckBox("总注大于");
        JTextField jinzhidaxiaodanshuangfanxiang_zongzhu_text = new JTextField();
        JLabel liewai3 = new JLabel("时列外");

        /**
         * 单点数字,每期不同点数最多xx个组件
         */
        JPanel dandian = new JPanel();
        JCheckBox dandianshuzi_meiqizuiduo_check = new JCheckBox("单点数字,每期不同点数最多");
        JTextField dandianshuzi_meiqizuiduo_text = new JTextField();
        JLabel ge = new JLabel("个");

        public Nixiang() {
            super(new BorderLayout());
            dandianshuzi.setBorder(BorderFactory.createTitledBorder("逆向组合"));
            dandianshuzi.add(BorderLayout.WEST, dandian_tongyi_jpanel);

            /**
             * 禁止杀组合组件加入
             */
            setRuleCheckBoxClickAction(jinzhishazuhe, "jinzhishazuhe");
            setRuleCheckBoxClickAction(jinzhishazuhe_zongzhu_check, "jinzhishazuhe.zongzu.check");
            setRuleTextFieldChangeAction(jinzhishazuhe_zongzhu_text, "jinzhishazuhe.zongzu.text");
            main.add(jinzhishazuhe);
            zongzhudayu_panel.add(jinzhishazuhe_zongzhu_check);
            jinzhishazuhe_zongzhu_text.setColumns(4);
            zongzhudayu_panel.add(jinzhishazuhe_zongzhu_text);
            zongzhudayu_panel.add(liewai);
            main.add(zongzhudayu_panel);
            /**
             * 禁止反向组合组件加入
             */
            setRuleCheckBoxClickAction(jinzhifanxiangzuhe, "jinzhifanxiangzuhe");
            setRuleCheckBoxClickAction(jinzhifanxiangzuhe_zongzhu_check, "jinzhifanxiangzuhe.zongzu.check");
            setRuleTextFieldChangeAction(jinzhifanxiangzuhe_zongzhu_text, "jinzhifanxiangzuhe.zongzu.text");
            main.add(jinzhifanxiangzuhe);
            zongzhudayu_panel1.add(jinzhifanxiangzuhe_zongzhu_check);
            jinzhifanxiangzuhe_zongzhu_text.setColumns(4);
            zongzhudayu_panel1.add(jinzhifanxiangzuhe_zongzhu_text);
            zongzhudayu_panel1.add(liewai1);
            main.add(zongzhudayu_panel1);
            /**
             * 禁止同向组合组件加入
             */
            setRuleCheckBoxClickAction(jinzhitongxiangzuhe, "jinzhitongxiangzuhe");
            setRuleCheckBoxClickAction(jinzhitongxiangzuhe_zongzhu_check, "jinzhitongxiangzuhe.zongzu.check");
            setRuleTextFieldChangeAction(jinzhitongxiangzuhe_zongzhu_text, "jinzhitongxiangzuhe.zongzu.text");
            main.add(jinzhitongxiangzuhe);
            zongzhudayu_panel2.add(jinzhitongxiangzuhe_zongzhu_check);
            jinzhitongxiangzuhe_zongzhu_text.setColumns(4);
            zongzhudayu_panel2.add(jinzhitongxiangzuhe_zongzhu_text);
            zongzhudayu_panel2.add(liewai2);
            main.add(zongzhudayu_panel2);

            /**
             * 说明
             */
            JLabel weifanss = new JLabel("违反上述规则时:");
            weifanss.setForeground(Color.red);
            JLabel weifanss1 = new JLabel("@玩家 总注不到####以上,禁止xx组合,本期猜猜全部无效,请重新猜猜!");
            weifanss1.setForeground(Color.red);
            JPanel gz = new JPanel();
            gz.add(weifanss1);
            main.add(weifanss);
            main.add(gz);

            dandianshuzi.add(main);

            /**
             * 禁止大小单双反向组件添加
             */
            setRuleCheckBoxClickAction(jinzhidaxiaodanshuangfanxiang, "jinzhidaxiaodanshuangfanxiang");
            setRuleCheckBoxClickAction(jinzhidaxiaodanshuangfanxiang_zongzhu_check, "jinzhidaxiaodanshuangfanxiang.zongzu.check");
            setRuleTextFieldChangeAction(jinzhidaxiaodanshuangfanxiang_zongzhu_text, "jinzhidaxiaodanshuangfanxiang.zongzu.text");
            main.add(jinzhidaxiaodanshuangfanxiang);
            zongzhudayu_panel3.add(jinzhidaxiaodanshuangfanxiang_zongzhu_check);
            jinzhidaxiaodanshuangfanxiang_zongzhu_text.setColumns(4);
            zongzhudayu_panel3.add(jinzhidaxiaodanshuangfanxiang_zongzhu_text);
            zongzhudayu_panel3.add(liewai3);
            main.add(zongzhudayu_panel3);

            /**
             * 单点数字,每期不同点数最多xx个组件加入
             */
            setRuleCheckBoxClickAction(dandianshuzi_meiqizuiduo_check, "dandianshuzi.meiqizuiduo.check");
            setRuleTextFieldChangeAction(dandianshuzi_meiqizuiduo_text, "dandianshuzi.meiqizuiduo.text");
            dandian.add(dandianshuzi_meiqizuiduo_check);
            dandianshuzi_meiqizuiduo_text.setColumns(4);
            dandian.add(dandianshuzi_meiqizuiduo_text);
            dandian.add(ge);
            main.add(dandian);

            jPanelNorth.add(dandianshuzi);

            this.add(BorderLayout.NORTH, jPanelNorth);
        }
    }

    //语音提示
    public class Prompt extends JPanel {
        /**
         * 游戏事件提示声组件
         */
        Vector title = new Vector();
        DefaultTableModel model = new DefaultTableModel();
        JTable resourceFrame = new JTable(model);
        TishiSound tishiSound = new TishiSound();

        public void flashData() {
            this.model.getDataVector().clear();
            this.model.addRow(new Object[]{"查回", SettingContext.getSoundSettingProperties("sound.default.chahui", TishiSound.NONE)});
            this.model.addRow(new Object[]{"微信掉线", SettingContext.getSoundSettingProperties("sound.default.weixindiaoxian", TishiSound.NONE)});
            this.model.addRow(new Object[]{"开奖失败", SettingContext.getSoundSettingProperties("sound.default.kaijiangshibai", TishiSound.NONE)});
            this.model.addRow(new Object[]{"封盘失败", SettingContext.getSoundSettingProperties("sound.default.fengpanshibai", TishiSound.NONE)});
            this.resourceFrame.setRowSelectionInterval(0, 0);
        }

        public Prompt() {
            super(new BorderLayout());
            this.setBorder(BorderFactory.createTitledBorder("游戏事件提示声"));
            this.title.add("操作");
            this.title.add("声音");
            Vector data = ((DefaultTableModel) resourceFrame.getModel()).getDataVector();
            this.model.setDataVector(data, title);
            flashData();
            JScrollPane jScrollPane = new JScrollPane(resourceFrame);
            jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            this.add(BorderLayout.CENTER, jScrollPane);
            this.add(BorderLayout.SOUTH, tishiSound);
            resourceFrame.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(MouseEvent evt) {
                    //判断是否为鼠标的BUTTON3按钮，BUTTON3为鼠标右键
                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        //通过点击位置找到点击为表格中的行
                        int focusedRowIndex = resourceFrame.rowAtPoint(evt.getPoint());
                        if (focusedRowIndex == -1) {
                            return;
                        }
                        //将表格所选项设为当前右键点击的行
                        String type = (String) resourceFrame.getValueAt(resourceFrame.getSelectedRow(), 0);
                        String sound = (String) resourceFrame.getValueAt(resourceFrame.getSelectedRow(), 1);
                        tishiSound.setSelectSound(type, sound);
                    }
                }
            });

            tishiSound.jComboBox.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        String s = (String) tishiSound.jComboBox.getSelectedItem();
                        if ("查回".equals(tishiSound.getCurrType())) {
                            SettingContext.setSoundSetting("sound.default.chahui", s);
                        }
                        if ("微信掉线".equals(tishiSound.getCurrType())) {
                            SettingContext.setSoundSetting("sound.default.weixindiaoxian", s);
                        }
                        if ("开奖失败".equals(tishiSound.getCurrType())) {
                            SettingContext.setSoundSetting("sound.default.kaijiangshibai", s);
                        }
                        if ("封盘失败".equals(tishiSound.getCurrType())) {
                            SettingContext.setSoundSetting("sound.default.fengpanshibai", s);
                        }
                        flashData();
                    }
                }
            });
        }
    }

    /**
     * 提示声音
     */
    public class TishiSound extends JPanel {
        public static final String NONE = "---- 无 ----";
        public JComboBox jComboBox = new JComboBox();
        JButton jButton = new JButton("播放");
        String type = "";

        public TishiSound() {
            super(new BorderLayout());
            JPanel jPanel = new JPanel();
            java.util.List<String> sounds = SoundContext.getSoundList();
            jComboBox.addItem(NONE);
            for (String sound : sounds) {
                jComboBox.addItem(sound);
            }
            jPanel.add(jComboBox);
            jButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SoundContext.play((String) jComboBox.getSelectedItem());
                }
            });
            jPanel.add(jButton);
            this.add(BorderLayout.NORTH, new JLabel("提示声音"));
            this.add(BorderLayout.SOUTH, jPanel);
        }

        public String getSelectSound() {
            return (String) jComboBox.getSelectedItem();
        }

        public void setSelectSound(String type, String sound) {
            this.type = type;
            jComboBox.setSelectedIndex(0);
            jComboBox.setSelectedItem(sound);
        }

        public String getCurrType() {
            return type;
        }
    }

    //特定语音提示
    public class UsePrompt extends JPanel {
        JTextField yourenshuo = new JTextField(25);
        JButton yourenshuo_btn = new JButton("添加");

        TishiSound tishiSound = new TishiSound();
        JTextField zidonghuifu = new JTextField();//自动回复
        JButton change = new JButton("修改");

        Vector title = new Vector();
        DefaultTableModel model = new DefaultTableModel();
        JTable list = new JTable(model);
        JScrollPane tableList = new JScrollPane(list);
        JButton delete = new JButton("删除");

        public UsePrompt() {
            super(new BorderLayout());
            this.setBorder(BorderFactory.createTitledBorder("特定消息提示音"));
            JPanel jPanel = new JPanel(new BorderLayout());
            JPanel jPanel1 = new JPanel(new BorderLayout());
            jPanel1.add(BorderLayout.NORTH, new Label("群里有人说(多条之间用|分割)"));
            JPanel jPanel2 = new JPanel();
            jPanel2.add(yourenshuo);
            jPanel2.add(yourenshuo_btn);
            jPanel1.add(BorderLayout.SOUTH, jPanel2);
            jPanel.add(BorderLayout.NORTH, jPanel1);
            jPanel1 = new JPanel();
            jPanel1.add(tishiSound);
            jPanel2 = new JPanel();
            jPanel2.add(BorderLayout.NORTH, new JLabel("自动回复:"));
            JPanel jPanel3 = new JPanel();
            zidonghuifu.setColumns(15);
            jPanel3.add(zidonghuifu);
            jPanel3.add(change);
            jPanel2.add(BorderLayout.SOUTH, jPanel3);
            jPanel1.add(BorderLayout.NORTH, jPanel2);
            jPanel.add(BorderLayout.SOUTH, jPanel1);
            this.add(BorderLayout.NORTH, jPanel);

            jPanel = new JPanel();
            title.add("内容");
            title.add("提示声音");
            title.add("自动回复");
            Vector data = ((DefaultTableModel) list.getModel()).getDataVector();
            model.setDataVector(data, title);
            jPanel.add(tableList);
            jPanel.add(delete);
            this.add(BorderLayout.CENTER, jPanel);

            jPanel = new JPanel(new BorderLayout());
            jPanel.add(BorderLayout.NORTH, new JLabel("若只要回复内容,不要声音提示,则请将\"提示声音\"设为\"(无)\""));
            jPanel.add(BorderLayout.SOUTH, new JLabel("若只要声音提示,不要回复内容,则请将\"自动回复\"设为空"));
            this.add(BorderLayout.SOUTH, jPanel);

            list.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(MouseEvent evt) {
                    //判断是否为鼠标的BUTTON3按钮，BUTTON3为鼠标右键
                    if (evt.getButton() == MouseEvent.BUTTON1) {
                        //通过点击位置找到点击为表格中的行
                        int focusedRowIndex = list.rowAtPoint(evt.getPoint());
                        if (focusedRowIndex == -1) {
                            return;
                        }
                        //将表格所选项设为当前右键点击的行
                        String type = (String) list.getValueAt(list.getSelectedRow(), 0);
                        String sound = (String) list.getValueAt(list.getSelectedRow(), 1);
                        String reply = (String) list.getValueAt(list.getSelectedRow(), 2);
                        zidonghuifu.setText(reply);
                        yourenshuo.setText(type);
                        tishiSound.setSelectSound(type, sound);
                    }
                }
            });
//            tishiSound.jComboBox.addItemListener(new ItemListener() {
//                @Override
//                public void itemStateChanged(ItemEvent e) {
//                    if (e.getStateChange() == ItemEvent.SELECTED) {
//                        String s = (String) tishiSound.jComboBox.getSelectedItem();
//                        SettingContext.setSoundSetting("yourenshuo." + yourenshuo.getText() + ".sound", s);
//                        flashData();
//                    }
//                }
//            });
            change.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (yourenshuo.getText() != null && !"".equals(yourenshuo.getText())) {
                        SettingContext.setSoundSetting("yourenshuo." + yourenshuo.getText() + ".reply", zidonghuifu.getText());
                        SettingContext.setSoundSetting("yourenshuo." + yourenshuo.getText() + ".content", "1");
                        SettingContext.setSoundSetting("yourenshuo." + yourenshuo.getText() + ".sound", tishiSound.getSelectSound());
                        flashData();
                    }
                }
            });
            delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (list.getSelectedRow() != -1) {
                        String content = (String) list.getValueAt(list.getSelectedRow(), 0);
                        SettingContext.removeSoundSetting("yourenshuo." + content + ".reply");
                        SettingContext.removeSoundSetting("yourenshuo." + content + ".content");
                        SettingContext.removeSoundSetting("yourenshuo." + content + ".sound");
                        flashData();
                    }
                }
            });
            yourenshuo_btn.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String content = yourenshuo.getText();
                    SettingContext.setSoundSetting("yourenshuo." + content + ".content", "1");
                    SettingContext.setSoundSetting("yourenshuo." + content + ".sound", tishiSound.getSelectSound());
                    SettingContext.setSoundSetting("yourenshuo." + content + ".reply", zidonghuifu.getText());
                    flashData();
                }
            });
            flashData();
        }

        /**
         * 刷新列表
         */
        public void flashData() {
            this.model.getDataVector().clear();
            Properties properties = SettingContext.getSoundSetting();
            for (Object key : properties.keySet()) {
                String k = (String) key;
                if (k.startsWith("yourenshuo.") && k.endsWith(".content")) {
                    k = k.replace("yourenshuo.", "");
                    String content = k.replace(".content", "");
                    String sound = SettingContext.getSoundSettingProperties("yourenshuo." + content + ".sound", TishiSound.NONE);
                    String reply = SettingContext.getSoundSettingProperties("yourenshuo." + content + ".reply", "");
                    this.model.addRow(new Object[]{content, sound, reply});
                }
            }
        }
    }

    public static void setCheckBoxClickAction(JCheckBox jCheckBox, String propertiesName) {
        jCheckBox.setSelected("true".equalsIgnoreCase(SettingContext.getInofoSetting().getProperty(propertiesName, "false")) ? true : false);
        jCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingContext.setInfoSetting(propertiesName, String.valueOf(jCheckBox.isSelected()));
            }
        });
    }

    public static void setTextFieldChangeAction(JTextField jTextField, String propertiesName) {
        jTextField.setText(SettingContext.getInofoSetting().getProperty(propertiesName));
        jTextField.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextField.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextField.getText());
            }

            public void changedUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextField.getText());
            }
        });
    }

    public static void setTextAreaChangeAction(JTextArea jTextArea, String propertiesName) {
        jTextArea.setText(SettingContext.getInofoSetting().getProperty(propertiesName));
        jTextArea.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextArea.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextArea.getText());
            }

            public void changedUpdate(DocumentEvent e) {
                SettingContext.setInfoSetting(propertiesName, jTextArea.getText());
            }
        });
    }

    //==================================================================================================
    public static void setRuleTextAreaChangeAction(JTextArea jTextArea, String propertiesName) {
        jTextArea.setText(SettingContext.getRuleSetting().getProperty(propertiesName));
        jTextArea.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextArea.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextArea.getText());
            }

            public void changedUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextArea.getText());
            }
        });
    }

    public static void setRuleCheckBoxClickAction(JCheckBox jCheckBox, String propertiesName) {
        jCheckBox.setSelected(SettingContext.getRuleSettingProperties(propertiesName, false));
        jCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SettingContext.setRuleSetting(propertiesName, String.valueOf(jCheckBox.isSelected()));
            }
        });
    }

    public static void setRuleTextFieldChangeAction(JTextField jTextField, String propertiesName) {
        jTextField.setText(SettingContext.getRuleSetting().getProperty(propertiesName));
        jTextField.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextField.getText());
            }

            public void removeUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextField.getText());
            }

            public void changedUpdate(DocumentEvent e) {
                SettingContext.setRuleSetting(propertiesName, jTextField.getText());
            }
        });
    }
}
