package com.lincoln.Client.frames;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Lincoln on 2018/4/15.
 */
public class MainSouthPanel extends JPanel {

    private String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private String time;
    private int ONE_SECOND = 1000;

    JPanel top = new JPanel(new FlowLayout());
    JPanel down = new JPanel(new FlowLayout());
    JLabel j1 = new JLabel("当前猜猜:第xxxxxx期");
    JLabel j2 = new JLabel("距离开奖:等待结果...");
    JLabel j3 = new JLabel("上轮开奖:***");
    JLabel j4 = new JLabel("输赢:###");
    JLabel j5 = new JLabel("今日总输赢:###");
    JButton buttonSwitch = new JButton("开启");
    JLabel j6 = new JLabel("停止");
    JLabel j7 = new JLabel("2018-04-02");
    public MainSouthPanel(){
        super();
        this.setLayout(new BorderLayout());
        top.add(j1);
        top.add(j2);
        top.add(j3);
        top.add(j4);
        top.add(j5);
        top.add(buttonSwitch);
        this.add(BorderLayout.NORTH,top);
        down.add(j6);
        configTimeArea();
        down.add(j7);
        this.add(BorderLayout.SOUTH,down);
    }

    private void configTimeArea() {
        java.util.Timer tmr = new Timer();
        tmr.scheduleAtFixedRate(new JLabelTimerTask(),new Date(), ONE_SECOND);
    }

    protected class JLabelTimerTask extends TimerTask {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DEFAULT_TIME_FORMAT);
        @Override
        public void run() {
            time = dateFormatter.format(Calendar.getInstance().getTime());
            j7.setText(time);
        }
    }

}


