package com.lincoln.Client.frames;

import com.lincoln.Client.context.FrameContext;
import com.lincoln.Client.context.RobotContext;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.*;

public class AccountManageFrame extends BaseFrame {
    private JFrame jFrame = new JFrame();
    private JPanel panel1;
    private JButton button1;
    private JPanel accountList;

    public AccountManageFrame() {
        panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        button1 = new JButton("添加账号");
        accountList = new JPanel();
        panel1.add("South",button1);
        panel1.add("Center",accountList);
        jFrame.setTitle("账号管理");
        jFrame.setContentPane(panel1);
        jFrame.setSize(200,400);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameContext.getLoginFrame().setVisible(true);
            }
        });
    }

    @Override
    public JFrame getFrame() {
        flashFrame();
        return jFrame;
    }

    /**
     * 刷新窗口组件
     */
    public void flashFrame() {
        accountList.removeAll();
        for (String key : RobotContext.bots.keySet()) {
            AccountComponent accountComponent = new AccountComponent(RobotContext.getRobot(key).api().icon(), RobotContext.getRobot(key).getSession().getNickName());
            accountComponent.setSize(accountList.getWidth(), accountList.getHeight() / RobotContext.bots.size());
            accountComponent.flashIconSize(RobotContext.bots.size());
            accountList.add(accountComponent);
        }
        accountList.repaint();
        accountList.setVisible(true);
    }

    /*
        账号管理组件(单个账号)
         */
    public class AccountComponent extends JPanel {
        ImageIcon imageIcon;//原始的头像数据
        JLabel icon = new JLabel();//微信头像
        JLabel name = new JLabel();//微信名

        public void flashIconSize(int size){
            int width = getWidth();
            int height = getHeight();
            if(height/size>width){
                this.icon.setIcon(new ImageIcon(imageIcon.getImage().getScaledInstance(width/2, width/2, Image.SCALE_DEFAULT)));
            }else{
                this.icon.setIcon(new ImageIcon(imageIcon.getImage().getScaledInstance(height/size, height/size, Image.SCALE_DEFAULT)));
            }
        }

        public AccountComponent(File icon, String name) {
            super();
            InputStream input = null;
            try {
                input = new FileInputStream(icon);
                byte[] byt = new byte[input.available()];
                input.read(byt);
                imageIcon = new ImageIcon(byt);
                this.icon.setText("");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            this.name.setText(name);
            this.icon.setIcon(imageIcon);
            super.add(this.icon);
            super.add(this.name);
        }
    }
}
