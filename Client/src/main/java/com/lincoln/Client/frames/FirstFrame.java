package com.lincoln.Client.frames;

import com.lincoln.Client.context.FrameContext;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;

public class FirstFrame extends JFrame {
    public NameTextField name = new NameTextField("用户名:", 20);
    public NameTextField password = new NameTextField("密码:", 20);
    public JButton login = new JButton("登陆");

    public FirstFrame() {
        super();
        this.setSize(330,240);
        JPanel jPanel = new JPanel(new GridLayout(3, 1));
        jPanel.add(name);
        jPanel.add(password);
        jPanel.add(login);
        login.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (name.jTextField.getText() == null || "".equals(name.jTextField.getText())) {
                    return;
                }
                if (password.jTextField.getText() == null || "".equals(password.jTextField.getText())) {
                    return;
                }
                HttpClient client = new DefaultHttpClient();
                HttpGet get = new HttpGet("http://localhost:8080/login?name="+name.jTextField.getText()+"&password="+password.jTextField.getText());
                try {
                    HttpResponse res = client.execute(get);
                    if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                        HttpEntity entity = res.getEntity();
                        if (entity != null) {
                            InputStream in = entity.getContent();
                            StringBuffer out = new StringBuffer();
                            byte[] b = new byte[4096];
                            for (int n; (n = in.read(b)) != -1; ) {
                                out.append(new String(b, 0, n));
                            }
                            String result = out.toString();
                            if(result.equals("1")){
                                FrameContext.firstFrame.setVisible(false);
                                FrameContext.getInstance().setVisible(true);
                            }else{
                                JOptionPane.showMessageDialog(new JFrame().getContentPane(), "登陆失败！", "系统信息", JOptionPane.ERROR_MESSAGE);
                            }
                            System.out.print(result);
                        }
                    }
                } catch (Exception e1) {
                    throw new RuntimeException(e1);
                } finally {
                    //关闭连接 ,释放资源
                    client.getConnectionManager().shutdown();
                }
            }
        });
        this.setContentPane(jPanel);
    }

    public class NameTextField extends JPanel {
        public JLabel jLabel = new JLabel();
        public JTextField jTextField = new JTextField();

        public NameTextField(String name, int columns) {
            super(new FlowLayout());
            jLabel.setText(name);
            this.add(jLabel);
            jTextField.setColumns(columns);
            this.add(jTextField);
        }
    }

    public static void main(String[] args) {
        FirstFrame firstFrame = new FirstFrame();
        firstFrame.setVisible(true);
    }
}
