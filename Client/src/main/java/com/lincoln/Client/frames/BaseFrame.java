package com.lincoln.Client.frames;

import javax.swing.*;

public abstract class BaseFrame {
    public abstract JFrame getFrame();
}
