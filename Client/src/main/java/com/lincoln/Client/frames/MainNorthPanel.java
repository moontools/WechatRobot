package com.lincoln.Client.frames;

import com.lincoln.Client.context.FrameContext;
import com.lincoln.Client.context.GameContext;
import com.lincoln.Client.context.SettingContext;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lincoln on 2018/4/15.
 */
//north
public class MainNorthPanel extends JPanel {

    /**---------------------START:[截图实现需要用到的变量]--------------------*/

    /**
     * 图像文件前缀
     */
    private String fileName = "GUI";
    /**
     * 图像文件后缀
     */
    private String imageFormat = "png";
    /**
     * 图像保存路径
     */
    private String imageSavePath = "d:\\";

    /**
     * ---------------------END:[截图实现需要用到的变量]--------------------
     */

    public JButton runWx = new JButton();//启动微信按钮
    public JButton sysRun = new JButton();//系统启动
    public JButton checkTime = new JButton();//时间校准
    public JButton screenShot = new JButton();//一键截图
    public JButton setGroup = new JButton("设置讨论组名");//设置讨论组名
    public JLabel groupName = new JLabel();//讨论组名

    public MainNorthPanel() {
        super();
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        this.setLayout(flowLayout);
        runWx.setText("启动微信");
        runWx.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FrameContext.getAccountManageFrame().setVisible(true);
            }
        });
//        runWx.setVerticalTextPosition(SwingConstants.BOTTOM);//
//        runWx.setHorizontalTextPosition(SwingConstants.CENTER);//
//        System.out.println(System.getProperty("user.dir")+"/src/resource/wx.jpg");
//        runWx.setIcon(new ImageIcon(System.getProperty("user.dir")+"/src/resource/wx.jpg"));
        sysRun.setText("系统启动");
        sysRun.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (GameContext.canPlay) {
                    GameContext.stop();
                } else {
                    GameContext.start();
                }
            }
        });
        checkTime.setText("时间校准");
        checkTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimpleDateFormat simdate = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat simtime = new SimpleDateFormat("HH:mm:ss");
                Date date = getNetworkTime("http://www.baidu.com");
                String stime = simtime.format(date);
                String sdate = simdate.format(date);
                System.out.println("北京时间:" + sdate + " " + stime);
                Runtime run = Runtime.getRuntime();
                try {
                    run.exec("cmd /c time " + stime);
                    run.exec("cmd /c date " + sdate);
                    System.out.println("设置成功");
                    JOptionPane.showMessageDialog(new JFrame().getContentPane(), "校准成功！", "系统信息", JOptionPane.QUESTION_MESSAGE);
                } catch (IOException e1) {
                    System.out.println(e1.getMessage());
                }
            }
        });
        screenShot.setText("一键截图");
        setGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String str = JOptionPane.showInputDialog("请填入讨论组名:");
                    SettingContext.setRuleSetting("GROUP_NAME",str);
                    groupName.setText(SettingContext.getRuleSettingProperties("GROUP_NAME",""));
                } catch (Exception e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(new JFrame().getContentPane(), "输入格式有误!\n请重新输入！", "系统信息", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        });
        super.add(runWx);
        super.add(sysRun);
        super.add(checkTime);
        super.add(screenShot);
        super.add(groupName);
        groupName.setText(SettingContext.getRuleSettingProperties("GROUP_NAME",""));
        super.add(setGroup);

        /**
         * 一键截图按钮监听器
         */
        screenShot.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                snapShot();
            }
        });

    }

    public static Date getNetworkTime(String url1) {
        try {
            URL url = new URL(url1);
            URLConnection urlc = url.openConnection();
            urlc.connect();
            long time = urlc.getDate();
            Date date = new Date(time);
            return date;
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * ------------------------------START:[截图实现]-------------------------------
     **/

    public void snapShot() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        /**
         * 获取屏幕相关信息
         */
        Dimension screen = toolkit.getScreenSize();
        /**
         * 对屏幕进行拍照
         */
        try {
            /**
             * 拷贝屏幕的一个BufferedImage对象
             */
            BufferedImage screenshot = (new Robot()).createScreenCapture(
                    new Rectangle(FrameContext.main.getLocation().x, FrameContext.main.getLocation().y
                            , FrameContext.main.getWidth(), FrameContext.main.getHeight())
            );
            System.out.println("Frame-------->" + FrameContext.main.getWidth());
            System.out.println("Frame-------->" + FrameContext.main.getLocation().x);
            /**
             * 根据文件前缀变量和后缀变量自动生成文件名
             */
            String name = imageSavePath + fileName + "." + imageFormat;
            File file = new File(name);
            System.out.println("SAVE FILE = " + name);
            /**
             * 将screenshot图像写入图像文件
             */
            ImageIO.write(screenshot, imageFormat, file);
        } catch (Exception e) {
            System.err.println(MainNorthPanel.class.getName() + ": 图像文件写入失败");
        }
    }

    /**------------------------------END:[截图实现]-------------------------------**/

}
