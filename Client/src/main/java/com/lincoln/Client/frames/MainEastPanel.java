package com.lincoln.Client.frames;

import com.lincoln.Client.context.FrameContext;
import com.lincoln.Client.context.RobotContext;
import com.lincoln.framework.entity.Recharge;
import com.lincoln.framework.entity.Withdraw;
import com.lincoln.framework.service.RechargeService;
import com.lincoln.framework.service.WithdrawService;
import com.lincoln.framework.spring.SpringContextUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Vector;

/**
 * Created by Lincoln on 2018/4/15.
 */
public class MainEastPanel extends JPanel {
    AuditList auditList = new AuditList();

    public MainEastPanel() {
        super();
        this.setLayout(new BorderLayout());
        this.add(BorderLayout.NORTH, new JLabel("查回审核:"));
        this.add(BorderLayout.CENTER, auditList);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        JPanel top = new JPanel(new FlowLayout());
        jPanel.add(BorderLayout.NORTH, top);
        jPanel.add(BorderLayout.CENTER, new JLabel("<html>在表中点鼠标右键<br>选择 同意 或 拒绝<br>*查:分数为正<br>*回:分数为负</html>"));
        this.add(BorderLayout.SOUTH, jPanel);
        flashMainEastPanelAuditList();
    }

    //查回审核列表
    public class AuditList extends JPanel {
        JPopupMenu jPopupMenu = new JPopupMenu();//表格右键菜单
        Vector title = new Vector();
        DefaultTableModel model = new DefaultTableModel();
        JTable jTable = new JTable(model);
        JScrollPane jScrollPane = new JScrollPane(jTable);

        public AuditList() {
            super();
            this.setLayout(new BorderLayout());
            this.add(jScrollPane, BorderLayout.CENTER);
            title.add("id");
            title.add("全名");
            title.add("类型");
            title.add("分数");
            Vector data = ((DefaultTableModel) jTable.getModel()).getDataVector();
            model.setDataVector(data, title);

            //初始化右键菜单
            JMenuItem allow_ali = new JMenuItem();//同意为支付宝到账
            allow_ali.setText("同意(支付宝)");
            allow_ali.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    int row = jTable.getSelectedRow();
                    long id = (long) jTable.getValueAt(row, 0);
                    String nickName = (String) jTable.getValueAt(row, 1);
                    String type = (String) jTable.getValueAt(row, 2);
                    double score = (double) jTable.getValueAt(row, 3);
                    if ("查".equals(type)) {//充值订单
                        RechargeService rechargeService = SpringContextUtil.getBean(RechargeService.class);
                        Recharge recharge = rechargeService.getById(id);
                        if (recharge == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            rechargeService.allow(recharge.getId(),Recharge.SOURCE_ALI);
                            JOptionPane.showMessageDialog(null, "查分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            RobotContext.sendMsg("@" + nickName + " " + "查分" + score + "成功!");
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    } else {//提现订单
                        WithdrawService withdrawService = SpringContextUtil.getBean(WithdrawService.class);
                        Withdraw withdraw = withdrawService.getById(id);
                        if (withdraw == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            withdrawService.allow(withdraw.getId(),Withdraw.SOURCE_ALI);
                            JOptionPane.showMessageDialog(null, "回分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            RobotContext.sendMsg("@" + nickName + " " + "回分" + score + "成功!");
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    }
                }
            });
            jPopupMenu.add(allow_ali);
            JMenuItem allow_wx = new JMenuItem();//同意微信到账
            allow_wx.setText("同意(微信)");

            allow_wx.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    int row = jTable.getSelectedRow();
                    long id = (long) jTable.getValueAt(row, 0);
                    String nickName = (String) jTable.getValueAt(row, 1);
                    String type = (String) jTable.getValueAt(row, 2);
                    double score = (double) jTable.getValueAt(row, 3);
                    if ("查".equals(type)) {//充值订单
                        RechargeService rechargeService = SpringContextUtil.getBean(RechargeService.class);
                        Recharge recharge = rechargeService.getById(id);
                        if (recharge == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            rechargeService.allow(recharge.getId(),Recharge.SOURCE_WECHAT);
                            RobotContext.sendMsg("@" + nickName + " " + "查分" + score + "成功!");
                            JOptionPane.showMessageDialog(null, "查分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    } else {//提现订单
                        WithdrawService withdrawService = SpringContextUtil.getBean(WithdrawService.class);
                        Withdraw withdraw = withdrawService.getById(id);
                        if (withdraw == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            withdrawService.allow(withdraw.getId(),Withdraw.SOURCE_WECHAT);
                            RobotContext.sendMsg("@" + nickName + " " + "回分" + score + "成功!");
                            JOptionPane.showMessageDialog(null, "回分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    }
                }
            });
            jPopupMenu.add(allow_wx);

            JMenuItem allow_yhx = new JMenuItem();//同意银行卡到账
            allow_wx.setText("同意(银行卡)");

            allow_wx.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    int row = jTable.getSelectedRow();
                    long id = (long) jTable.getValueAt(row, 0);
                    String nickName = (String) jTable.getValueAt(row, 1);
                    String type = (String) jTable.getValueAt(row, 2);
                    double score = (double) jTable.getValueAt(row, 3);
                    if ("查".equals(type)) {//充值订单
                        RechargeService rechargeService = SpringContextUtil.getBean(RechargeService.class);
                        Recharge recharge = rechargeService.getById(id);
                        if (recharge == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            rechargeService.allow(recharge.getId(),Recharge.SOURCE_WECHAT);
                            RobotContext.sendMsg("@" + nickName + " " + "查分" + score + "成功!");
                            JOptionPane.showMessageDialog(null, "查分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    } else {//提现订单
                        WithdrawService withdrawService = SpringContextUtil.getBean(WithdrawService.class);
                        Withdraw withdraw = withdrawService.getById(id);
                        if (withdraw == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            withdrawService.allow(withdraw.getId(),Withdraw.SOURCE_WECHAT);
                            RobotContext.sendMsg("@" + nickName + " " + "回分" + score + "成功!");
                            JOptionPane.showMessageDialog(null, "回分成功!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    }
                }
            });
            jPopupMenu.add(allow_yhx);

            JMenuItem refuse = new JMenuItem();//拒绝
            refuse.setText("拒绝");

            refuse.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    int row = jTable.getSelectedRow();
                    long id = (long) jTable.getValueAt(row, 0);
                    String nickName = (String) jTable.getValueAt(row, 1);
                    String type = (String) jTable.getValueAt(row, 2);
                    double score = (double) jTable.getValueAt(row, 3);
                    if ("查".equals(type)) {//充值订单
                        RechargeService rechargeService = SpringContextUtil.getBean(RechargeService.class);
                        Recharge recharge = rechargeService.getById(id);
                        if (recharge == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            rechargeService.refuse(recharge.getId());
                            RobotContext.sendMsg("@" + nickName + " " + "查分" + score + "失败!");
                            JOptionPane.showMessageDialog(null, "查分失败!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    } else {//提现订单
                        WithdrawService withdrawService = SpringContextUtil.getBean(WithdrawService.class);
                        Withdraw withdraw = withdrawService.getById(id);
                        if (withdraw == null) {
                            JOptionPane.showMessageDialog(null, "订单未找到!", "ERROR_MESSAGE", JOptionPane.ERROR_MESSAGE);
                        } else {
                            withdrawService.refuse(withdraw.getId());
                            RobotContext.sendMsg("@" + nickName + " " + "回分" + score + "失败!");
                            JOptionPane.showMessageDialog(null, "回分失败!", "成功", JOptionPane.PLAIN_MESSAGE);
                            flashMainEastPanelAuditList();
                            FrameContext.main.mainCenterPanel.flashAllUser();
                        }
                    }
                }
            });
            jPopupMenu.add(refuse);

            jTable.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(MouseEvent evt) {
                    //判断是否为鼠标的BUTTON3按钮，BUTTON3为鼠标右键
                    if (evt.getButton() == MouseEvent.BUTTON3) {
                        //通过点击位置找到点击为表格中的行
                        int focusedRowIndex = jTable.rowAtPoint(evt.getPoint());
                        if (focusedRowIndex == -1) {
                            return;
                        }
                        //将表格所选项设为当前右键点击的行
                        jTable.setRowSelectionInterval(focusedRowIndex, focusedRowIndex);
                        //弹出菜单
                        jPopupMenu.show(jTable, evt.getX(), evt.getY());
                    }
                }
            });
        }
    }

    /**
     * 刷新查回审核列表
     */
    public void flashMainEastPanelAuditList() {
        this.auditList.model.getDataVector().clear();
        //查出未处理充值订单
        RechargeService rechargeService = SpringContextUtil.getBean(RechargeService.class);
        java.util.List<Recharge> rechargeList = rechargeService.findAllNoDealRecharge();
        for (Recharge recharge : rechargeList) {
            addMainEastPanelAuditList(recharge.getId(), recharge.getMember().getNickName(), "查", recharge.getScore());
        }

        //查出未处理提现订单
        WithdrawService withdrawService = SpringContextUtil.getBean(WithdrawService.class);
        java.util.List<Withdraw> withdrawList = withdrawService.findAllNoDealWithdraw();
        for (Withdraw withdraw : withdrawList) {
            addMainEastPanelAuditList(withdraw.getId(), withdraw.getMember().getNickName(), "回", withdraw.getScore());
        }
    }

    /**
     * 添加一条数据
     *
     * @param id    查或者回的订单id
     * @param name  用户花名
     * @param type  "查"或者"回"
     * @param score 分数
     */
    public void addMainEastPanelAuditList(long id, String name, String type, double score) {
        this.auditList.model.addRow(new Object[]{id, name, type, score});
    }
}
