package com.lincoln.Client.frames;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by Lincoln on 2018/4/15.
 */
public class MainFrame extends JFrame {
    public MainNorthPanel mainNorthPanel = new MainNorthPanel();
    public MainCenterPanel mainCenterPanel = new MainCenterPanel();
    public MainEastPanel mainEastPanel = new MainEastPanel();
    public MainSouthPanel mainSouthPanel = new MainSouthPanel();

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }

    JPanel jPanel = new JPanel();//最大的panel

    public MainFrame() {
        this.setSize(1300, 900);
        jPanel.setLayout(new BorderLayout());
        this.setContentPane(jPanel);
        jPanel.add(BorderLayout.NORTH, mainNorthPanel);
        jPanel.add(BorderLayout.WEST, mainCenterPanel);
        jPanel.add(BorderLayout.CENTER, mainEastPanel);
        jPanel.add(BorderLayout.SOUTH, mainSouthPanel);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosed(e);
                System.exit(0);
            }
        });
    }
}
