package com.lincoln.Client.cmd;

import com.lincoln.Client.context.GameContext;
import com.lincoln.Client.context.RobotContext;
import com.lincoln.framework.entity.Betting;
import com.lincoln.framework.entity.Member;
import com.lincoln.framework.entity.Recharge;
import com.lincoln.framework.entity.Withdraw;
import com.lincoln.framework.service.*;
import com.lincoln.framework.spring.SpringContextUtil;

//指令解析基类
public abstract class BaseCmd {
    private WxAccountService wxAccountService;

    public WxAccountService wxAccountService() {
        if (wxAccountService == null) {
            this.wxAccountService = SpringContextUtil.getBean(WxAccountService.class);
        }
        return wxAccountService;
    }

    public void wxAccountService(WxAccountService wxAccountService) {
        this.wxAccountService = wxAccountService;
    }

    private MemberService memberService;

    public MemberService memberService() {
        if (memberService == null) {
            this.memberService = SpringContextUtil.getBean(MemberService.class);
        }
        return memberService;
    }

    public void memberService(MemberService memberService) {
        this.memberService = memberService;
    }

    private RechargeService rechargeService;

    public RechargeService rechargeService() {
        if (rechargeService == null) {
            this.rechargeService = SpringContextUtil.getBean(RechargeService.class);
        }
        return rechargeService;
    }

    public void rechargeService(RechargeService rechargeService) {
        this.rechargeService = rechargeService;
    }

    private WithdrawService withdrawService;

    public WithdrawService withdrawService() {
        if (withdrawService == null) {
            this.withdrawService = SpringContextUtil.getBean(WithdrawService.class);
        }
        return withdrawService;
    }

    public void withdrawService(WithdrawService rechargeService) {
        this.withdrawService = withdrawService;
    }

    private Member member;

    public Member member() {
        return member;
    }

    public void member(Member member) {
        this.member = member;
    }

    public boolean compareMemberMoney(double score) {
        return getMember().getMoney() >= score;
    }

    public void decMemberScore(double score) {
        member.setAllUse(member.getAllUse() + score);
        memberService().save(member);
        return;
    }

    private String reply;


    public void reply(String reply) {
        this.reply = reply;
    }

    /**
     * 获取回复信息
     *
     * @return
     */
    public String reply() {
        if (reply == null) {
            return "";
        }
        return reply;
    }

    private String groupName;
    private String content;

    public String content() {
        return content;
    }

    public void content(String content) {
        this.content = content;
    }

    private Long attrStatus;//微信来源
    private String[] args;//参数

    public void args(String[] args) {
        this.args = args;
    }

    public String[] args() {
        return this.args;
    }

    private String errMsg = "";//错误信息

    public String errMsg() {
        return this.errMsg;
    }

    public void errMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public BaseCmd(String groupName, Long attrStatus, String content) {
        this.groupName = groupName;
        this.content = content;
        this.attrStatus = attrStatus;
    }

    /**
     * 比较是否符合该cmd规则
     *
     * @return
     */
    public abstract boolean compare();

    /**
     * 根据content解析出参数列表
     *
     * @return
     */
    public abstract void parseArgs();

    /**
     * 传入参数列表后执行
     *
     * @return 执行成功或失败
     */
    public abstract boolean doCmd();


    public String getErrMsg() {
        return this.errMsg;
    }

    /**
     * 获取当前命令关联的平台用户
     *
     * @return
     */
    public Member getMember() {
        if (memberService == null) {
            memberService = SpringContextUtil.getBean(MemberService.class);
        }
        if (member == null) {
            member = memberService.getById(attrStatus);
        }
        return member;
    }

    /**
     * 新增member
     */
    public void insertMember() {
        Member member = new Member();
        member.setId(attrStatus);
        member.setScore(0.0);
        member.setEarn(0.0);
        member.setAllUse(0.0);
        member.setWithdraw(0.0);
        member.setStatus(Member.STATUS_NORMAL);
        memberService.save(member);
        this.member = member;
    }

    /**
     * 回复结果
     */
    public void replyMsg() {
        if (doCmd()) {
            if (reply()!=null&&!"".equals(reply())) {
                String r = "@" + getMember().getNickName() + " " + reply();
                RobotContext.sendMsg(r);
            }
        } else {
            if (errMsg!=null&&!"".equals(errMsg)) {
                String r = "@" + getMember().getNickName() + " " + errMsg;
                RobotContext.sendMsg(r);
            }
        }

    }

    /**
     * 回复结果
     */
    public void replyMsgTest() {
        if (doCmd()) {
            String r = "@" + attrStatus + " " + reply();
            System.out.println(content + "回复:" + r);
        } else {
            String r = "@" + attrStatus + " " + errMsg;
            System.out.println(content + "回复:" + r);
        }

    }

    public static final String[] numStr = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};

    /**
     * 将字符串切割成字符串+数字
     *
     * @param str 如AA123
     * @return {"AA","123"}
     */
    public String[] cutStrNum(String str) {
        if (str == null || "".equals(str)) {
            return new String[]{"", ""};
        }
        int firstNum = -1;
        for (int i = 0; i < str.length(); i++) {
            String s = str.substring(i, i + 1);
            try {
                int x = Integer.parseInt(s);
                firstNum = i;
                break;
            } catch (Exception e) {

            }
        }
        if (firstNum == -1) {
            return new String[]{str, ""};
        }
        String[] result = new String[2];
        result[0] = str.substring(0, firstNum);
        result[1] = str.substring(firstNum);
        return result;
    }

    /**
     * 将字符串切割成数字+字符串
     *
     * @param str 如AA123
     * @return {"AA","123"}
     */
    public String[] cutNumStr(String str) {
        int firstNum = -1;
        for (int i = 0; i < str.length(); i++) {
            String s = str.substring(i, i + 1);
            try {
                int x = Integer.parseInt(s);
            } catch (Exception e) {
                firstNum = i;
                break;
            }
        }
        if (firstNum == -1) {
            return new String[]{str, ""};
        }
        String[] result = new String[2];
        result[0] = str.substring(0, firstNum);
        result[1] = str.substring(firstNum);
        return result;
    }

    /**
     * 判断字符串是否为空
     *
     * @return
     */
    public static boolean isEmpty(String str) {
        if (str == null || "".equals(str)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断字符串是否为空
     *
     * @return
     */
    public static boolean isNotEmpty(String str) {
        if (str == null || "".equals(str)) {
            return false;
        } else {
            return true;
        }
    }

    //====================Withdraw=======================================
    public static int Withdraw_STATUS_WAIT = 0;

    public Withdraw newWithdraw(Member member) {
        Withdraw withdraw = new Withdraw();
        withdraw.setMember(member);
        return withdraw;
    }

    public Withdraw newWithdraw(Member member, double score) {
        Withdraw withdraw = new Withdraw();
        withdraw.setMember(member);
        return withdraw;
    }

    public Withdraw newWithdraw(Member member, double score, String source) {
        Withdraw withdraw = new Withdraw();
        withdraw.setMember(member);
        withdraw.setScore(score);
        withdraw.setSource(source);
        return withdraw;
    }

    public Withdraw newWithdraw(Member member, double score, String source, int status) {
        Withdraw withdraw = new Withdraw();
        withdraw.setMember(member);
        withdraw.setScore(score);
        withdraw.setSource(source);
        withdraw.setStatus(status);
        return withdraw;
    }

    public long insertWithdraw(Member member, double score, String source, int status) {
        Withdraw withdraw = newWithdraw(member, score, source, status);
        withdrawService().saveOnly(withdraw);
        return withdraw.getId();
    }

    //====================Recharge=======================================
    public static int Recharge_STATUS_WAIT = 0;

    public Recharge newRecharge(Member member) {
        Recharge recharge = new Recharge();
        recharge.setMember(member);
        return recharge;
    }

    public Recharge newRecharge(Member member, double score) {
        Recharge recharge = new Recharge();
        recharge.setMember(member);
        return recharge;
    }

    public Recharge newRecharge(Member member, double score, String pay) {
        Recharge recharge = new Recharge();
        recharge.setMember(member);
        recharge.setScore(score);
        recharge.setPay(pay);
        return recharge;
    }

    public Recharge newRecharge(Member member, double score, String pay, int status) {
        Recharge recharge = new Recharge();
        recharge.setMember(member);
        recharge.setScore(score);
        recharge.setPay(pay);
        recharge.setStatus(status);
        return recharge;
    }

    public long insertRecharge(Member member, double score, String pay, int status) {
        Recharge recharge = newRecharge(member, score, pay, status);
        rechargeService().save(recharge);
        return recharge.getId();
    }

    //====================Betting=======================================
    public Betting newBetting(double score) {
        Betting betting = new Betting();
        betting.setMember(getMember());
        betting.setGame(GameContext.getGame());
        betting.setContent(content());
        betting.setScore(score);
        return betting;
    }

    public long insertBetting(double score) {
        Betting betting = newBetting(score);
        bettingService().save(betting);
        return betting.getId();
    }

    public long insertBetting(double score, String content) {
        Betting betting = newBetting(score);
        betting.setContent(content);
        bettingService().save(betting);
        return betting.getId();
    }

    private BettingService bettingService;

    public BettingService bettingService() {
        if (bettingService == null) {
            this.bettingService = SpringContextUtil.getBean(BettingService.class);
        }
        return bettingService;
    }

    public void bettingService(BettingService bettingService) {
        this.bettingService = bettingService;
    }
}
