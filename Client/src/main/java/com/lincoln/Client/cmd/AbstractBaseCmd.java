package com.lincoln.Client.cmd;

import com.lincoln.Client.context.SettingContext;
import com.lincoln.Client.context.SoundContext;
import com.lincoln.Client.frames.MainCenterPanel;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//未找到的cmd,用来执行自动回复提醒
public class AbstractBaseCmd extends BaseCmd {

    public AbstractBaseCmd(String groupName, Long attrStatus, String content) {
        super(groupName, attrStatus, content);
    }

    @Override
    public boolean compare() {
        return false;
    }

    @Override
    public void parseArgs() {

    }

    @Override
    public boolean doCmd() {
        if (content() != null && sounds.get(content()) != null&& !MainCenterPanel.TishiSound.NONE.equals(sounds.get(content()))){
            SoundContext.play(sounds.get(content()));
        }
        if (content() != null && replys.get(content()) != null){
            reply(replys.get(content()));
            return true;
        }
        return false;
    }

//    @Override
//    public void replyMsg() {
//        System.out.println("未匹配!");
//    }
//
//    public void replyMsgTest() {
//        System.out.println(content() + " " + "未匹配!");
//    }

    public static Map<String, String> sounds = new HashMap<>();
    public static Map<String, String> replys = new HashMap<>();

    static {
        flash();
    }

    public static void flash() {
        sounds = new HashMap<>();
        replys = new HashMap<>();
        Properties properties = SettingContext.getSoundSetting();
        for (Object key : properties.keySet()) {
            String k = (String) key;
            if (k.startsWith("yourenshuo.") && k.endsWith(".content")) {
                k = k.replace("yourenshuo.", "");
                String content = k.replace(".content", "");
                String sound = SettingContext.getSoundSettingProperties("yourenshuo." + content + ".sound", MainCenterPanel.TishiSound.NONE);
                String reply = SettingContext.getSoundSettingProperties("yourenshuo." + content + ".reply", "");
                String[] contents = content.split("\\|");
                for (String msg : contents) {
                    sounds.put(msg, sound);
                    replys.put(msg, reply);
                }
            }
        }
    }
}
