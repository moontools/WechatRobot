<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html lang="zh-cmn-Hans">
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
	name="viewport">
<meta name="apple-mobile-web-app-capable" content="yes" />
<!-- 是否启用 WebApp 全屏模式，删除苹果默认的工具栏和菜单栏-->
<meta name="apple-itunes-app"
	content="app-id=myAppStoreID, affiliate-data=myAffiliateData, app-argument=myURL">
<!-- 启用360浏览器的极速模式(webkit) -->
<meta name="renderer" content="webkit">
<!-- 避免IE使用兼容模式 -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- 不让百度转码 -->
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!-- uc强制竖屏 -->
<meta name="screen-orientation" content="portrait">
<!-- QQ强制竖屏 -->
<meta name="x5-orientation" content="portrait">
<!-- UC强制全屏 -->
<meta name="full-screen" content="yes">
<!-- QQ强制全屏 -->
<meta name="x5-fullscreen" content="true">
<!-- windows phone 点击无高光 -->
<meta name="msapplication-tap-highlight" content="no">
<link href="/style/mobile/reset.css" rel="stylesheet">
<link href="/style/mobile/css_${pageLocale }.css" rel="stylesheet">
<script src="/js/jquery-1.7.2.min.js"></script>
</head>
<body class="gray_bg">
	<div class="caution">
		<div class="caution_img_box">
			<img src="/images/mobile/danger.png" />
		</div>
		<h5>${exception.getMessage() }</h5>
		<div class="b_btn">
			<c:if test="${errorType == 'unlogin' || errorType == 'memberUnlogin' || errorType == 'memberDisable' }">
				<a href="/login/toLogin.htm" class="btn_out"><spring:message code="common.exception.backlogin" /></a>
			</c:if>
			<c:if test="${errorType != 'unlogin' && errorType != 'memberUnlogin' && errorType != 'memberDisable' }">
				<a href="javascript:;" onclick="history.go(-1);" class="btn_out"><spring:message code="common.exception.back" /></a>
			</c:if>
		</div>
	</div>
</body>
</html>
