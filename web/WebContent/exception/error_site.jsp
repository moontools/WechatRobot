<%@page import="com.lincoln.framework.utils.ParameterChecker"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>异常页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/style/site/reset.css" rel="stylesheet">
<link href="/style/site/css_${pageLocale }.css" rel="stylesheet">
<body>
<div class="w_1180">
<div class="prompt_out">
<img src="/images/site/404.png"/>
<div class="prompt_out_body">
<h1>异常提示</h1>
<p>${exception.getMessage() }</p>
<div class=" clearfix"> <a href="/" class="btn_blue_b2">返回</a> </div></div>
</div>

</div>
</body>
</html>