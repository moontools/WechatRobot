<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>异常页面</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/style/sys/bootstrap.min.css" rel="stylesheet">
<link href="/style/sys/font-awesome/f-style/font-awesome.css"
	rel="stylesheet">
<link href="/style/sys/animate.css" rel="stylesheet">
<link href="/style/sys/style.css" rel="stylesheet">
</head>

<body class="gray-bg">
	<div class="middle-box text-center animated fadeInDown">
		<h1>500</h1>
		<h3 class="font-bold">网络异常</h3>
		<div class="error-desc">
			<a href="/" class="btn btn-primary m-t">返回</a>
		</div>
	</div>
</body>

</html>
