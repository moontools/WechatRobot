<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<ex:javascript src="/script/common/promptbox.js"></ex:javascript>

<div class="pop_box3" id="prompt_confirm" style="display: none;">
	<div class="title">
		<span class="f_l"><fmt:message key="common.dialog.title"/></span> <a href="javascript:;" class="close f_r dialog-cancel"></a>
	</div>
	<div class="pop_body">
		<div class="pop_tablecell">
			<div class="pop_img">
				<i class="pop_judge pop_help"></i>
			</div>
			<div class="judge_r">
				<h1 class="prompt_message"></h1>
			</div>
		</div>
		<div class="pop_btn_r">
			<a href="javascript:;" class="btn_pop_ok dialog-close"><fmt:message key="common.dialog.confirm"/></a>
			<a href="javascript:;" class="pop_btn_no dialog-cancel"><fmt:message key="common.dialog.concel" /></a>
		</div>
	</div>
</div>

<div class="pop_box3" id="prompt_error" style="display: none;">
	<div class="title">
		<span class="f_l"><fmt:message key="common.dialog.title"/></span> <a href="javascript:;" class="close f_r dialog-cancel"></a>
	</div>
	<div class="pop_body">
		<div class="pop_tablecell">
			<div class="pop_img">
				<i class="pop_judge pop_no"></i>
			</div>
			<div class="judge_r">
				<h1 class="prompt_message"></h1>
			</div>
		</div>
		<div class="pop_btn_r">
			<a href="javascript:;" class="btn_pop_ok dialog-close"><fmt:message key="common.dialog.confirm"/></a>
		</div>
	</div>
</div>

<div class="pop_box3" id="prompt_alert" style="display: none;">
	<div class="title">
		<span class="f_l"><fmt:message key="common.dialog.title"/></span> <a href="javascript:;" class="close f_r dialog-cancel"></a>
	</div>
	<div class="pop_body">
		<div class="pop_tablecell">
			<div class="pop_img">
				<i class="pop_judge pop_warn"></i>
			</div>
			<div class="judge_r">
				<h1 class="prompt_message"></h1>
			</div>
		</div>
		<div class="pop_btn_r">
			<a href="javascript:;" class="btn_pop_ok dialog-close"><fmt:message key="common.dialog.confirm"/></a>
		</div>
	</div>
</div>

<div class="pop_box3" id="prompt_success" style="display: none;">
	<div class="title">
		<span class="f_l"><fmt:message key="common.dialog.title"/></span> <a href="javascript:;" class="close f_r dialog-cancel"></a>
	</div>
	<div class="pop_body">
		<div class="pop_tablecell">
			<div class="pop_img">
				<i class="pop_judge pop_ok"></i>
			</div>
			<div class="judge_r">
				<h1 class="prompt_message"></h1>
			</div>
		</div>
		<div class="pop_btn_r">
			<a href="javascript:;" class="btn_pop_ok dialog-close"><fmt:message key="common.dialog.confirm"/></a>
		</div>
	</div>
</div>