<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<ex:javascript src="/script/common/promptbox.js"></ex:javascript>
<div class="pop_box" id="prompt_wait" style="display: none;">
	<div class="pop_icon">
		<div class="sk-spinner sk-spinner-cube-grid">
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
			<div class="sk-cube"></div>
		</div>
	</div>
	<h2 class="prompt_message">上传中</h2>
</div>