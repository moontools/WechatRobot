/**
 * 全选按钮事件
 */
function CheckboxAll(config){
	 this.config=config;
	 this.init(config);	
}
CheckboxAll.prototype={
	init: function(config) {
		this.wrapper=config.wrapper;
		//可配置chkAll 和 chkMe 以兼容一个容器内有多个 checkbox 列
		this.classChkAll=config.classChkAll || ".chkAll";
		this.classChkMe=config.classChkMe || ".chkMe";
		this.extendCheckMe=config.extendCheckMe;
		this.extendCheckAll=config.extendCheckAll;
		this.bindEvent();
	},
	bindEvent: function(){
		var _this=this;
		var wrapperJQ=this.wrapper;
		var checherAll = wrapperJQ.find(this.classChkAll + ":checkbox");
		checherAll.each(function() {
			this.wrapper=wrapperJQ;
			this.classChkMe = _this.classChkMe;
			this.classChkAll = _this.classChkAll;
			this.extendCheckAll=_this.extendCheckAll;
			this.onclick = _this.checkAll;
		});
		wrapperJQ.find(this.classChkMe + ":checkbox").each(function() {
			this.wrapper=wrapperJQ;
			this.classChkMe = _this.classChkMe;
			this.classChkAll = _this.classChkAll;
			this.extendCheckMe=_this.extendCheckMe;
			this.onclick = _this.checkMe;
		});
		checherAll.filter(":checked").click();
	},
	checkAll: function() {
		var all_checked = false;
		var enabledChkMeJQ = this.wrapper.find(this.classChkMe + ":checkbox:enabled");
		if (enabledChkMeJQ.size() == enabledChkMeJQ.filter(":checked").size()) {
			all_checked = true;
		}
		if (all_checked) {
			this.checked = false;
			this.wrapper.find(this.classChkMe + ":checkbox").each(function() {
				this.checked = false;
			});
		} else {
			this.checked = true;
			enabledChkMeJQ.each(function() {
				this.checked = true;
			});
		}
		if(typeof(this.extendCheckAll)=="function"){
			this.extendCheckAll();
		}
	},
	/**
	 * 选项选择事件
	 */
	checkMe : function() {
		var chkAllJQ = this.wrapper.find(this.classChkAll + ":checkbox:enabled");
		if (!this.checked) {
			chkAllJQ.each(function() {
				this.checked = false;
			});
		} else {
			var all_checked = false;
			var enabledChkMeJQ = this.wrapper.find(this.classChkMe + ":checkbox:enabled");
			if (enabledChkMeJQ.size() == enabledChkMeJQ.filter(":checked").size()) {
				all_checked = true;
			}
			if (all_checked) {
				chkAllJQ.each(function() {
					this.checked = true;
				});
			}
		}
		if(typeof(this.extendCheckMe)=="function"){
			this.extendCheckMe();
		}
	},
	
	/**
	 * 获取选中的个数
	 */
	getCheckedSize: function(){
		return this.wrapper.find(this.classChkMe+ ":checkbox:checked").size();
	},
	/**
	 * 获取选项总数
	 */
	getTotalSize:function(){
		return this.wrapper.find(this.classChkMe + ":checkbox").size();
	},	
	/**
	 * 判断是否全选
	 */
	isCheckAll: function(){
		return this.getCheckedSize() == this.getTotalSize();
	},
	/**
	 * 获取选中的值数组
	 */
	getCheckedArray:function(){
		var data = [];
		this.wrapper.find(this.classChkMe+ ":checkbox:checked").each(function(){
			data.push($(this).val());
		});
		return data;
	},
	/**
	 * 获取选中的值字符串 以, 风格
	 */
	getCheckedString:function(){
		return this.getCheckedArray().join(',');
	}
}

