/**
 * 中国地区选择联动
 */
function ChineseRegion (config) {
	this.config = {
		// 地区下拉框
		provinceSelect : null,
		citySelect : null,
		areaSelect : null,
		regionValueEl : null,
		// 默认选中显示值
		defaultProvinceShow : "请选择省份",
		defaultCityShow : "请选择城市",
		defaultAreaShow : "请选择地区",
		// 初始化地区值
		defaultRegion : "",
		dataUrl : ""
	};
	
	this.init = function(config) {
		this.config = $.extend(this.config, config);

		this.initDefault();

		this.bindEvent();
		
	};
	
	this.initDefault = function() {
		var defaultData = this.getRegionData(this.config.defaultRegion,	true , 0);
		if (defaultData.defaultProvince) {
			if(this.config.defaultRegion){
				this.initProvince(this.config.defaultRegion);
				this.initCity(this.config.defaultRegion);
				this.initArea(this.config.defaultRegion);
			}
		} else {
			this.initProvince();
			this.appendOption(this.config.citySelect,"" , this.config.defaultCityShow);
			if(typeof this.config.citySelect.selectRender == "function"){
				this.config.citySelect.selectRender();
			}
			this.appendOption(this.config.areaSelect,"" , this.config.defaultAreaShow);
			if(typeof this.config.areaSelect.selectRender == "function"){
				this.config.areaSelect.selectRender();
			}
		}
	};
	
	
	this.bindEvent = function() {
		this.bindProvinceEvent();
		this.bindCityEvent();
		this.bindAreaEvent();
	};
	
	this.bindProvinceEvent = function() {
		var _this = this;
		this.config.provinceSelect.bind("change", function() {
			if($(this).find("option[value='"+ $(this).val() +"']").text() == _this.config.defaultProvinceShow){
				
				_this.config.citySelect.html("");
				_this.config.areaSelect.html("");
				
				_this.appendOption(_this.config.citySelect,"" , _this.config.defaultCityShow);
				if(typeof _this.config.citySelect.selectRender == "function"){
					_this.config.citySelect.selectRender();
				}
				_this.appendOption(_this.config.areaSelect,"" , _this.config.defaultAreaShow);
				if(typeof _this.config.areaSelect.selectRender == "function"){
					_this.config.areaSelect.selectRender();
				}
				
				_this.config.regionValueEl.val("");
			} else {
				_this.config.citySelect.html("");
				_this.config.areaSelect.html("");
				
				_this.appendOption(_this.config.areaSelect,"" , _this.config.defaultAreaShow);
				if(typeof _this.config.areaSelect.selectRender == "function"){
					_this.config.areaSelect.selectRender();
				}
				
				var region = $(this).val();
				_this.config.regionValueEl.val(region);
				_this.initCity(region);
				
			}
			
		});
		this.config.provinceSelect.bind("click", function() {
			if ($(this).find("option").length == 0) {
				_this.initProvince();
			}
		});
	};
	
	this.bindCityEvent = function() {
		var _this = this;
		this.config.citySelect.bind("change", function() {
			if($(this).find("option[value='"+ $(this).val() +"']").text() == _this.config.defaultCityShow){
				_this.config.areaSelect.html("");
				
				_this.appendOption(_this.config.areaSelect,"" , _this.config.defaultAreaShow);
				if(typeof _this.config.areaSelect.selectRender == "function") {
					_this.config.areaSelect.selectRender();
				}
				
				_this.config.regionValueEl.val(_this.config.provinceSelect.val());
			} else {
				_this.config.areaSelect.html("");
				var region = $(this).val();
				_this.config.regionValueEl.val(region);
				_this.initArea(region);
			}
		});
		this.config.citySelect.bind("click", function() {
			if(_this.config.provinceSelect.find("option").length == 0 || _this.config.provinceSelect.val() == ""){
				showError("请选择省份");
				return;
			}
			if ($(this).find("option").length == 0) {
				_this.initCity();
			}
		});
	};
	
	this.bindAreaEvent = function() {
		var _this = this;
		this.config.areaSelect.bind("change", function() {
			if($(this).find("option[value='"+ $(this).val() +"']").text() == _this.config.defaultAreaShow){
				_this.config.areaSelect.val("");
				_this.config.regionValueEl.val(_this.config.citySelect.val());
			} else {
				_this.config.regionValueEl.val($(this).val());
			}
		});
		this.config.areaSelect.bind("click", function() {
			if(_this.config.provinceSelect.find("option").length == 0 || _this.config.provinceSelect.val() == ""){
				showError("请选择省份");
				return;
			}
			if(_this.config.citySelect.find("option").length == 0 || _this.config.citySelect.val() == ""){
				showError("请选择城市");
				return;
			}
			if ($(this).find("option").length == 0) {
				_this.initArea();
			}
		});
	};
	
	this.initProvince = function(region){
		var _this = this;
		var data = _this.getRegionData(region || _this.config.defaultRegion, false , 1);
		if (data.province) {
			_this.appendOption(_this.config.provinceSelect, "",	_this.config.defaultProvinceShow);
			for ( var i = 0; i < data.province.length; i++) {
				_this.appendOption(_this.config.provinceSelect,data.province[i].id, data.province[i].name);
			}
			if((region || "") && region.length > 0){
				_this.config.provinceSelect.val(region.substr(0,2));
			} else {
				_this.config.provinceSelect.find("option:first").prop("selected", true);
			}
			if(typeof _this.config.provinceSelect.selectRender == "function"){
				_this.config.provinceSelect.selectRender();
			}
		}
	};
	
	this.initCity = function(region){
		var _this = this;
		var data = _this.getRegionData(region || _this.config.defaultRegion, false , 2);
		if (data.city) {
			_this.appendOption(_this.config.citySelect, "",	_this.config.defaultCityShow);
			for ( var i = 0; i < data.city.length; i++) {
				_this.appendOption(_this.config.citySelect,data.city[i].id, data.city[i].name);
			}
			if((region || "") && region.length > 2){
				_this.config.citySelect.val(region.substr(0,4));
			} else {
				_this.config.citySelect.find("option:first").prop("selected", true);
			}
			if(typeof _this.config.citySelect.selectRender == "function"){
				_this.config.citySelect.selectRender();
			}
		}
	};
	
	this.initArea = function (region){
		var _this = this;
		var data = _this.getRegionData(region || _this.config.defaultRegion, false , 3);
		if (data.area) {
			_this.appendOption(_this.config.areaSelect, "",	_this.config.defaultAreaShow);
			for ( var i = 0; i < data.area.length; i++) {
				_this.appendOption(_this.config.areaSelect,data.area[i].id, data.area[i].name);
			}
			if((region || "") && region.length > 4){
				_this.config.areaSelect.val(region.substr(0,6));
			} else {
				_this.config.areaSelect.find("option:first").prop("selected", true);
			}
			if(typeof _this.config.areaSelect.selectRender == "function"){
				_this.config.areaSelect.selectRender();
			}
		}
	};
	
	this.getRegionData = function(region, isDefault , level) {
		var _this = this;
		var data = {};

		$.ajax({
			dataType : "json",
			url : _this.config.dataUrl,
			type : 'post',
			async : false,
			data : {
				"chineseRegion" : region,
				"level" : level,
				"isDefault" : (isDefault ? 1 : 0)
			},
			success : function(result) {
				data = result.data;
			}
		});
		return data;
	};
	
	this.appendOption = function(selectEl, value, show) {
		var option = $("<option value='" + value + "' title='"+ show +"'>" + show + "</option>");
		selectEl.append(option);
	}
	
	return this;
} 

