/**
 * ueditor封装使用
 */
(function($) {

	$.fn.ueditor = function(options, afterInit) {
		var _this = $(this);
		require([ "ueditor" ], function() {
			var editor = UE.getEditor(_this.attr("id"), options);
			if (afterInit && typeof afterInit == "function") {
				editor.addListener("ready", function() {
					afterInit(editor);
				});
			}
		});
	};

	$.fn.simpleueditor = function(options, afterInit) {
		var _this = $(this);
		return _this.ueditor($.extend({
			toolbars : [ [ 'undo', 'redo', '|', 'bold', 'italic', 'underline',
					'fontborder', 'strikethrough', 'superscript', 'subscript',
					'removeformat', 'formatmatch', 'autotypeset', 'blockquote',
					'pasteplain', '|', 'forecolor', 'backcolor',
					'insertorderedlist', 'insertunorderedlist', 'selectall',
					'cleardoc', '|', 'rowspacingtop', 'rowspacingbottom',
					'lineheight', '|', 'customstyle', 'paragraph',
					'fontfamily', 'fontsize', '|', 'justifyleft',
					'justifycenter', 'justifyright', 'justifyjustify', '|',
					'touppercase', 'tolowercase' ] ],
			enableAutoSave : false,
			elementPathEnabled : false,
			wordCount : false,
			scaleEnabled : true,
			autoFloatEnabled : false
		}, options), afterInit);
	};
	$.fn.sueditor = function(options, afterInit) {
		var _this = $(this);
		return _this.ueditor($.extend({
			toolbars : [ [ 'undo', 'redo'] ],
			enableAutoSave : false,
			elementPathEnabled : false,
			wordCount : false,
			scaleEnabled : true,
			autoFloatEnabled : false
		}, options), afterInit);
	};

})(jQuery);