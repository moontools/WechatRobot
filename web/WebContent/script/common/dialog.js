/**
 * 简单弹出框
 */
(function($) {
	var simpleDialog = function(options) {
		this.options = options;
		this.dialogDiv = options.dialogEl;
		this.coverEl = options.coverEl
				|| $('<div class="tran_layer auto_create" style="display: none;"></div>');
		if(this.coverEl.hasClass("auto_create")) {
			$(document).find("body").append(this.coverEl);
		}
		this.closeClass = options.closeClass;

		if (this.dialogDiv.find("." + this.closeClass).length > 0) {
			this.dialogDiv.find("." + this.closeClass).click(function() {
				_this.closeDialog();
			});
		}
		var _this = this;
		// 兼容自适应
		$(window).bind("scroll", function() {
			if (_this.timeout) {
				clearTimeout(_this.timeout);
			}
			_this.timeout = setTimeout(function() {
				if (_this.dialogDiv.is(":visible")) {
					_this.setPostion();
				}
			}, 30);
		});
	};

	simpleDialog.prototype.showDialog = function() {
		this.setPostion();
		this.dialogDiv.show();
		this.coverEl.show();
	};
	simpleDialog.prototype.closeDialog = function() {
		this.dialogDiv.hide();
		clearTimeout(this.timeout);
		this.coverEl.hide();
		this.reset();
		this.onClose();
	};
	simpleDialog.prototype.onClose = function() {
		// 重写
	};
	simpleDialog.prototype.reset = function() {
		this.dialogDiv.find("input").val("");
		this.dialogDiv.find("textarea").val("");
	};
	
	simpleDialog.prototype.setPostion = function() {
		var width = this.dialogDiv.outerWidth();
		var height = this.dialogDiv.outerHeight();
		var top = $(document).scrollTop();
		var css = {
			marginTop : (top - height / 2) + "px",
			marginLeft : -width / 2 + "px",
			top : "50%",
			left : "50%"
		};
		this.dialogDiv.css(css);
	};

	$.fn.simpleDialog = function(options) {
		options = options || {};
		var _options = $.extend({}, {
			dialogEl : $(this)
		}, options);
		this.dialog = new simpleDialog(_options);
		return this.dialog;
	};

})(window.jQuery);