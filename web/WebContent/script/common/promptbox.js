function showWait(msg){
	return showDialog("prompt_wait", msg, "" , null, null, null);
}

function showConfirmDialog(msg, onClosed, onCancel) {
	return showDialog("prompt_confirm", msg, "", null, onClosed, onCancel);
}

function showSuccessDialog(msg,onClosed) {
	return showDialog("prompt_success", msg, "" , null, onClosed, null);
}

function showAlertDialog(msg) {
	return showDialog("prompt_alert", msg, "" , null, null, null);
}

function showErrorDialog(msg,onClosed) {
	return showDialog("prompt_error", msg, "" , null, onClosed, null);
}

/**
 * 对话框显示
 * @param el	对话框id
 * @param classname	输出提示div特定class
 * @param message	提示消息
 * @param onClosed	关闭后处理
 * @param options	提示框配置信息
 */
function showDialog(el,message,remarks,options, onClosed, onCancel){
	var htmlEncode = function(str){
		var div = document.createElement("div");
		div.appendChild(document.createTextNode(str));
		return div.innerHTML.replace(/"/g, "&quot;").replace(/\\'/g, "&apos;");
	}
	if(!options){
		options = {dialogWidth:364,dialogHeight:169};
	}else{
		$.extend(options,{dialogWidth:364,dialogHeight:169});
	}
	message = !options.escape || "" ? message : htmlEncode(message);
	Dialog.prototype.onClosed = onClosed?onClosed:function(){};
	Dialog.prototype.onCanceled = onCancel?onCancel:function(){};
	var alertDialog = new Dialog(el,options);
	var dialogMessage = $('#'+el).find('.prompt_message');
	if(dialogMessage.length >= 1){
		dialogMessage.html(message);
	}
	var dialogRemarks=$('#'+el).find('.prompt_remarks');
	if(dialogRemarks.length >= 1){
		dialogMessage.html(remarks == '' ? ('<h2>' + message + '</h2>') : ('<h3>' + message + '</h3>'));
		dialogRemarks.html(remarks);
	}
	
	alertDialog.show();
	return alertDialog;
}

/**
 * 弹出对话框
 * @param options
 */
var Dialog = function(element, options) {
	var _this = this;
	
	this.content = null;    // 显示的内容Div
    this.cover = null;     // 遮盖页面内容的iframe
    this.coverDiv = null;   // 遮盖iframe的div 在iframe上拖拽会卡 so。。。

    this.dialogClass = null;
    this.closeClass = "dialog-close";
    this.cancelClass = 'dialog-cancel';

    this.isVisible = false; // 当前状态是否显示
    
    this.options = {
    	opacity: 0.5,
    	position : 'center', // 显示位置 默认‘center’显示在页面中间
	    className : false,   // 要给dialog添加的class 多个class以空格分开
	    needEsc : false,      // 是否需要esc关闭功能 模拟 主要用于模拟alert
	    needEnter:false,	  // 是否需要enter关闭功能 
	    needCover : true,	 // 是否需要一个遮盖页面的iframe
	    zIndex: 9999,        // 对话框深度控制
	    isShow : false,       // 是否立即显示
	    focusEl : null,	 //关闭后焦点定位元素
	    dialogWidth : 364,	//弹出对话框宽度
	    dialogHeight : 169,	//弹出对话框高度
	    closeSubmitName : "确 定"
    };
	
	$.extend(this.options,options);

    this.content = $('#'+element);
    $(document.body).append(this.content);
    
    // set close herf & button event
    this.content.find("."+this.closeClass).each(function(index,el){
    	$(el).focus;
    	$(el).unbind('click');
    	$(el).bind('click',function(){
    		_this.close();
    	});
    });
    
    // set cancel herf & button event
    this.content.find("."+this.cancelClass).each(function(index,el){
    	$(el).unbind('click');
    	$(el).bind('click',function(){
    		_this.cancel();
    	});
    });

    this.createCover();
    
    // set esc event
    if (this.options.needEsc) this.setEscEvent();
    if (this.options.needEnter) this.setEnterEvent();

    //show the dialog
    if (this.options.isShow) this.show();
};

Dialog.prototype = {
	    /**
		 *	关闭 flow div
		 */
	    close : function(){
	        this.hide();
	        this.onClosed();
	    },
	    /**
	     * 点击取消
	     */
	    cancel : function(){
	    	this.hide();
	    	this.onCanceled();
	    },
	    
	    hide : function(){
	    	// cancel esc event
	        this.removeEscEvent();
	        this.removeEnterEvent();
	        if (!this.onClose()) return;
	        this.content.hide();
	        if(this.cover){
	        	this.cover.remove();
	        }
	        if(this.coverDiv){
	        	this.coverDiv.remove();
	        }
	        this.isVisible = false;
	        $(window).unbind("resize", this.resize);
	    },

	    /**
		 *	关闭时执行的方法
		 * return false 关闭方法将不被执行
		 */
	    onClose : function(){
	        return true;
	    },

	    /**
		 *	关闭后执行的方法
		 */
	    onClosed : function(){
	        return true;
	    },
	    /**
	     * 点击取消后执行
	     */
	    onCanceled : function(){
	    	return true;
	    },
	    
	    /**
		 *	显示 default : on the center of page
		 */
	    show : function(event) {
	    	if (event) Event.stop(event);
	    	
	    	this.isVisible = true;
	    	
	    	this.setDialogSize();
	        this.content.show();
	        this.setPosition(event);

	        this.showCover();
	        
	        //聚焦在 tabIndex 最小的输入项或者按钮上
	        this.focus();
	        this.addEscEvent();
	        this.addEnterEvent();
	        
	        var _this = this;
			// 兼容自适应
			$(window).bind("scroll", function() {
				if (_this.timeout) {
					clearTimeout(_this.timeout);
				}
				_this.timeout = setTimeout(function() {
					if (_this.content.is(":visible")) {
						_this.setPosition();
					}
				}, 30);
			});
			
	        $(window).bind("resize", {target:this}, this.resize);
	    },
	    /**
	     * 设置对话框大小
	     */
	    setDialogSize : function(){
	    	/*this.content.css({
	    		'width' : this.options.dialogWidth+"px",
	    		'height' : this.options.dialogHeight+"px"
	    	});*/
	    },

	    /**
		 *	设置Esc关闭事件
		 */
	    setEscEvent : function() {
	        this._closeHandler = function(event) {
	    		if (event.keyCode == Event.KEY_ESC) this.close();
	    	}.bindAsEventListener(this);
	    }, 
	    
	    /**
	     * 添加 ESC 事件
	     */
	    addEscEvent : function() {
	    	// set esc event
	        if (this.options.needEsc) {
	        	document.observe('keypress', this._closeHandler);
	        }
	    }, 
	    
	    /**
	     * 移除 ESC 事件
	     */
	    removeEscEvent : function() {
	    	if (this.options.needEsc) {
	    		document.stopObserving('keypress', this._closeHandler);
	    	}
	    },
	    
	    /**
		 *	设置回车事件
		 */
	    setEnterEvent : function() {
	    	var _this = this;
	        this._enterHandler = function(event) {
	    		if (event.keyCode == 13) {
	    			_this.close();
	    			return false;
	    		}
	    	};
	    }, 
	    
	    /**
	     * 添加回车事件
	     */
	    addEnterEvent : function(){
	    	if (this.options.needEnter) {
	    		$(document).bind("keypress", this._enterHandler);
	    	}
	    },
	    /**
	     * 移除 回车事件
	     */
	    removeEnterEvent : function() {
	    	if (this.options.needEnter) {
	    		$(document).unbind("keypress", this._enterHandler);
	    	}
	    },

	    /**
	     *  设置显示的位置
	     */
	    setPosition : function(event){
	        if (typeof this.options.position == "object") {
				var pos = this.options.position;
				this.content.css({
	                "top": pos.y + "px",
	                "left": pos.x + "px"
	            });
			} else {
	            this.setTop();
	            this.setLeft();
	        }
	    },

	    /**
		 *	set style.top
		 */
	    setTop : function() {
	        this.content.css({'top' : "50%","marginTop" : ($(document).scrollTop() - this.content.outerHeight() / 2) + "px" });
	    },

	    /**
		 *	set style.left default center
		 */
	    setLeft : function() {
	    	var width = this.content.outerWidth();
			var css = {
				marginLeft : -width / 2 + "px",
				left : "50%"
			};
			this.content.css(css);
	    },

	    focus : function() {
	    	var focusEle=this.content.find('.'+this.closeClass);
	    	if(focusEle.length==0){
	    		focusEle=this.content.find('.'+this.cancelClass);
	    	}
	    	focusEle.focus();
	    },

	    toString : function(){
	        return "i am a flowdiv";
	    },
	    
	    /**
	     * 创建消息显示区
	     */
	    createContent : function(){
	    	
	    },
	        
	    /**
		 * 创建遮盖页面内容的iframe
		 */
	   createCover : function(){
			var zIndex = this.options.zIndex;
			var coverInstance = this._getCoverInstance();
			this.cover = coverInstance.find("iframe").clone().addClass(this.coverClass).css({
				'z-index': zIndex
			});
			this.coverDiv = coverInstance.find("div").clone().css({
				'z-index': zIndex
			});
			
			this.content.css({
				'z-index' : zIndex
			});
			
			this.content.before(this.cover);
			this.cover.after(this.coverDiv);
	   }, 
	   
	   /**
	    * 貌似这样会快一些。。。
	    * 如果每次新建对话框都创建会很浪费时间（特别在IE下效率很差），所以这里在第一次创建的时候就直接插到 doc 节点树上，
	    * 以后创建就直接从这里拿就好了
	    */
	   _getCoverInstance: function() {
		   	var instance = $("#__this_cover");
		   	if (instance.length == 0) {
		   		instance = $('<div id="__this_cover" style="display:none;"></div>');
		       	$(document.body).append(instance);
		       	
		       	var coverHtml = '<iframe frameborder="0" style="background:#000000;top: 0px; left: 0px; position: absolute; display: none; opacity:' +  this.options.opacity + '; filter:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0,finishOpacity=100);filter:alpha(opacity='+ this.options.opacity * 100 +');"></iframe>';
		       	coverHtml += '<div style="top: 0px; left: 0px; position: absolute; display: none; background:#000000;opacity:' +  this.options.opacity + '; filter:progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0,finishOpacity=100);filter:alpha(opacity='+ this.options.opacity * 100 +');"></div>';
		       	instance.append($(coverHtml));
		   	} else {
		   		instance.find("iframe").css("opacity",this.options.opacity);
		   		instance.find("iframe").css("filter",'alpha(opacity='+ this.options.opacity * 100 +')');
		   		instance.find("div").css("opacity",this.options.opacity);
		   		instance.find("div").css("filter",'alpha(opacity='+ this.options.opacity * 100 +')');
		   	}
		   	return instance;
	   },
	   
	   /**
	    * 显示覆盖层
	    */
	   showCover : function(){
		   if (this.options.needCover){
	       		var width = $(document).width() > document.body.scrollWidth ? $(document).width() : document.body.scrollWidth;
	       		/*var height = $(document).height();
	       		if (document.documentElement.clientHeight < document.documentElement.offsetHeight &&  $(document).height() > document.body.scrollHeight){
	       			height = document.body.scrollHeight;
	       		} */
				var height = $(document).height() > document.body.scrollHeight ? $(document).height() : document.body.scrollHeight;
	       		
	       		this.cover.css({
	               'height' : height + 'px',
	               'width' : width + 'px'
	           });

	       		this.coverDiv.css({
	               'height' : height + 'px',
	               'width' : width + 'px'
	           });

	       		this.cover.show();
	       		this.coverDiv.show();
	       }
	   },
	   resize:function(e){
		   var _this = e.data.target;
	    	if(_this.content.is(":visible")){
	    		_this.setPosition();
	    		_this.showCover();
	    	}
	    }
};