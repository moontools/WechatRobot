/**
 * 浏览器辅助函数
 */
var BrowserHelper = {};
(function(){
	var ua = navigator.userAgent.toLowerCase();
	var check = function(r){
		return r.test(ua);
	};
	(function(browser){
		browser.isOpera = check(/opera/);
		browser.isIE = !browser.isOpera && check(/(msie\s|trident.*rv:)([\w.]+)/);
		browser.isIE6 = browser.isIE && check(/msie 6/);
		browser.isIE7 = browser.isIE && check(/msie 7/);
		browser.isIE8 = browser.isIE && check(/msie 8/);
		browser.isIE9 = browser.isIE && check(/msie 9/);
		browser.isIE11 = browser.isIE && check(/Trident/i);
		browser.isChrome = check(/chrome/);
		browser.isSafari = check(/safari/);
		browser.isUcbrowser = check(/ucbrowser/);
		browser.isIE7Mode = browser.isIE7 || (browser.isIE &&  document.documentMode && document.documentMode == 7);
	})(BrowserHelper);
})();

/**
 * html特殊字符转义
 * 
 * @param str
 * @returns
 */
function htmlEncode(str) {
	if(str == ""||str == null||str == undefined)return "";
	var div = document.createElement("div");
	div.appendChild(document.createTextNode(str));
	return div.innerHTML.replace(/"/g, "&quot;").replace(/'/g, "&#39;");
}

/**
 * 去标签
 * @param str
 * @returns
 */
function htmlDecode(str, isText) {
	if(str == ""||str == null||str == undefined)return "";
	var div = document.createElement("div");
	if(isText){ //兼容纯文本换行
		div = document.createElement("pre");
	}
	div.innerHTML = str;
	return div.innerText || div.textContent;
}

/**
 * 国际化语言配置
 * @param key
 * @param defaultValue
 */
function getLanguage( key, defaultValue) {
	return typeof language == "undefined" ? defaultValue
			: (language[key] || defaultValue);
}

function initListSubmitForm(){
	if ($("form").find("input[type='submit']").length <= 0) {
		$("form input").keypress(function(event){
			if(event.keyCode == 13){
				$("form").submit();
			}
		});
	}
}

/**
 * 日期对象转字符串表示
 * @param dataObj
 * @returns {String}
 */
function getDateStr(dataObj) {
	//一位数字前填充“0”
	function fixNumber(num) {
		return (num >= 10 ? "" : "0") + num;
	}
	
	var year = dataObj.year + 1900,
		mouth = dataObj.month + 1,
		date = dataObj.date,
		hour = dataObj.hours,
		minute = dataObj.minutes,
		second = dataObj.seconds;
	
	return year + "-" + fixNumber(mouth) + "-" + fixNumber(date) + " " +
			fixNumber(hour) + ":" + fixNumber(minute) + ":" + fixNumber(second);
}

/**
 * 一些插件的封装
 */
(function($){
	
	/**
	 * 设置输入框提示信息
	 * @param options
	 * @author rubekid
	 */
	var supportPlaceHolder = (function(){
		var input = document.createElement("input");
		return "placeholder" in input;
	})();


	function setPlaceHolder(target, options){
		if(!target || target.get(0)._initTip){
			return false;
		}
		
		options = options ||{};
		var tipText = options.tipText || target.attr("placeholder") ||"";

		if(supportPlaceHolder){
			target.attr("placeholder", tipText);
			target.data("placeholder", tipText);
			target.bind({
				focus: function(){
					$(this).removeAttr("placeholder");
				},
				blur:function(){
					$(this).attr("placeholder", target.data("placeholder"));
				}
			});
			return ;
		}
		
		var color = options.color||"#999999";
		var inputPrompt = target.clone();
		var name =  target.attr("name");
		
		target.removeAttr("placeholder");
		target.attr("data-tip", tipText);
		target.show();
		if(inputPrompt.attr("type") != "text" && inputPrompt.get(0).tagName != "TEXTAREA"){
			inputPrompt.attr("type","text");
		}
		inputPrompt.removeAttr("id");
		inputPrompt.removeAttr("name");
		inputPrompt.removeAttr("maxlength");
		inputPrompt.removeAttr("placeholder");
		inputPrompt.attr("data-focus", "filter");
		inputPrompt.css({"color":color});
		//过滤特殊类名
		if(options.filterClass){
			inputPrompt.removeClass(options.filterClass.replace(/,/g, " "));
		}
		target.get(0)._initTip = true;
		
		var init = function(){
			if($.trim(target.val())==""){
				target.val("");
				target.after(inputPrompt);
				target.hide();
				inputPrompt.val(target.attr("data-tip")); //每次赋值，避免页面赋值造成错误
				inputPrompt.bind({
					focus:function(){
						if(options.immediately || ""){
							inputPrompt.detach();
							target.show().focus();
						} else {
							setTimeout(function(){
								inputPrompt.detach();
								target.show().focus();
							});
						}
					}
				});
			}
		};
		target.bind({
			blur: function(){
				init();
			},
			focus:function(){
				$this = $(this);
				var text = $this.attr("placeholder") || $this.attr("data-tip");
				$this.removeAttr("placeholder");
				$this.attr("data-tip", text);
				inputPrompt.detach();
				$(this).show();
			},
			_reset:function(){
				if($.trim(target.val())==""){
					init();
				}
				else{
					target.show();
					if(inputPrompt){
						inputPrompt.hide();
						inputPrompt.remove();
					}
				}
			},
			removePlaceHolder:function(){
				$(this).show();
				inputPrompt.remove();
			}
		});
		init();
	}
	
	 /**
     * 设置输入灰色提示值
     * @author rubekid
     */
    $.fn.setPlaceHolder = function(options){
    	return this.each(function(){
    		var _options = $.extend({}, options||{});
    		setPlaceHolder($(this), _options);
    	});
    };
    $(document).ready(function(){
    	/**
    	 * 设置输入框的提示值
    	 */
    	$("input,textarea[placeholder]").not('input[type="hidden"]').setPlaceHolder();
    });
	
	/**
	 * 封装echarts
	 */
	$.fn.exEcharts = function(options) {
		var _this = $(this);
		require(["echarts"], function(echarts){
			var myChart = echarts.init(_this[0]);
			myChart.setOption(options);
		});
	};
})(jQuery);
