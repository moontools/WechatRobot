
/**
 * 转到指定页面
 * @param desc 目标地址
 * @data 需要传递的数据
 */
var go2Page = function(dest,data){
	var params  = "";
	if(data){
		params= $.param(data);
		if(dest.indexOf("?")!= -1){
			params = "&"+ params;
		}else{
			params = "?" + params;
		}
	}
	document.location=dest + params;
}

/*
 * 刷新当前页面
 */
var refreshCurrentPage = function(){
	go2Page(document.location);
}

/**
 * 返回上一页
 */
var goBack = function(){
	history.go(-1);
}

/**
 * 初始化页面链接
 */
var resultPage;
var initSkipResultPage = function(formId){
	resultPage = {};
	resultPage.formId = formId;
    resultPage.skip2Page=function(pageNo){
		var url = "";
		var addr = $("#"+this.formId).attr("action");
		if(addr.indexOf("?") != -1){
			url = addr + "&pageNo="+pageNo;
		}else{
			url = addr + "?pageNo="+pageNo;
		}
		go2Page(url);
	}
}

function jumpPage(){
	var pageNo = $.trim($("#pageNum").val());
	if(parseInt(pageNo)!=pageNo || pageNo <= 0){
		showError(getLanguage("site.page.error.pageno","请输入正常的页数"));
		return false;
	}
	resultPage.skip2Page(pageNo);
}

/**
 * 转到排序连接
 */
var sortcolumn = function(formId,orderBy){
	var _this = $("#"+"column_"+formId+"_"+(orderBy.replace(/\./g,"_")));
	var order = "desc";
	if(_this.html().indexOf("fa-caret-up") != -1){
		order = "asc";	
	}
	var url = $("#"+formId).attr("action");
	if(url.indexOf("?") != -1){
		url += "&order="+order+"&orderBy="+orderBy;
	}else{
		url += "?order="+order+"&orderBy="+orderBy;
	}
	go2Page(url);
}

/**
 * 前台转到排序连接
 */
var sitesortcolumn = function(formId,orderBy,order){
	var _this = $("#"+"column_"+formId+"_"+(orderBy.replace(/\./g,"_")));
	var url = $("#"+formId).attr("action");
	if(url.indexOf("?") != -1){
		url += "&order="+order+"&orderBy="+orderBy;
	}else{
		url += "?order="+order+"&orderBy="+orderBy;
	}
	go2Page(url);
}


