function checkPhone(num) {
	return checkMobile(num) || checkTelephone(num);
}

function checkMobile(num) {
	var re = /^1[3,4,5,7,8]\d{9}$/;
	return re.test(num);
}

function checkTelephone(num) {
	var phoneReg = /(^\+86\.\d{3,5}\d{6,8}$)|(^\d{3}((\d-)|(-\d)|\d|-)\d{3}(\d|-|)\d{3}(\d|)$)/;
	return phoneReg.test(num);
}

/**
 * 判断一个数是否为0或者正整数
 * @param number
 * @returns {Boolean}
 */
function checkNumber(number){
	var re = /^\d+$/;
	return number == 0 || re.test(number);
}

/**
 * 检验密码强度
 * @param password
 */
function checkStrength(password){
	var strength = 0;
	if (password.match(/[a-z]+/i)) {
		strength ++;
	}
	if (password.match(/[0-9]+/)) {
		strength ++;
	}
	if (password.match(/[\/,.~!@#$%^&*()\[\]_+\-=\:\";'\{\}\|\\><\?]+/)) {
		strength ++;
	}
	return strength;
}

/**
 * 检查是否为邮箱格式
 * @param email
 * @returns
 */
function checkEmail(email){
	var re = /^[&~#$*%\u4e00-\u9fa5_0-9a-z\-\.\/\\]+@([\u4e00-\u9fa5-a-z0-9]+\.){1,5}[\u4e00-\u9fa5a-z]+$/i;
	return re.test(email);
}
/**
 * 检测是否为url格式
 * @param url
 * @returns
 */
function checkUrl(url) {
	var re = /^((requireOpenId:)?((http|https|ftp):\/\/)?([\w\u4e00-\u9fa5\-]+\.)+[\w\u4e00-\u9fa5\-]+(:\d+)?(\/[\w\u4e00-\u9fa5\-\.\/?\@\%\!\&=\+\~\:\#\;\,]*)?)$/ig;
	return re.test(url);
}

/**
 * 检测是否为qq
 * @param num
 * @returns
 */
function checkQQ(num){
	var re = /^[1-9][0-9]{5,10}$/;
	return re.test(num);
}

/**
 * 判断是否为英文字符
 * @param num
 * @returns
 */
function checkEnglishWord(str){
	var re = /^[a-zA-Z]$/;
	return re.test(str);
}
