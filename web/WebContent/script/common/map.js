//2016-03-18
//jared
(function(){ 
	
	function Map(config){
		this.init(config);
	}
	
	Map.prototype = {
		init : function(config){
			var defaults = {
				mapDivId : null ,// 可视区域id
				lngEl : null,// 经度原素
				latEl : null,// 纬度原素
				addressEl : null , // 地址描述原素
				lng : null,
				lat : null
			};
			this.config = $.extend(defaults , config);
			this.curentPosition = new GLatLng(this.config.lat,this.config.lng );
			this.initMap();
			this.initMyGeo();
			this.initMarker();
			this.bindEvent();
		} ,
		initMap : function(){
			this.map = new GMap2(document.getElementById(this.config.mapDivId));
			this.map.enableScrollWheelZoom();
			this.map.enableDragging();
			this.map.enableContinuousZoom();
			this.map.setCenter(this.curentPosition, 18);
			this.map.panTo(this.curentPosition);
		},
		initMarker : function(){
			var icon = new GIcon(G_DEFAULT_ICON);
			icon.image = "/images/common/marker.png";
			this.marker = new GMarker(this.curentPosition, {draggable:true,icon:icon});
			this.marker.enableDragging();
			this.map.addOverlay(this.marker); // 将标注添加到地图中
		},
		initMyGeo : function(){
			var _this = this;
			this.myGeo = new GClientGeocoder();	//实例化一个地址解析器
		} , 
		bindEvent : function(){
			var _this = this;
			google.maps.event.addListener(this.marker,"dragend", function(e) {
				document.getElementById(_this.config.lngEl).value = e.lng();
				document.getElementById(_this.config.latEl).value = e.lat();
				_this.myGeo.getLocations(new GLatLng(e.lat(), e.lng()),function(result) {
					if (result.Status.code == G_GEO_SUCCESS){
						document.getElementById(_this.config.addressEl).value = result.Placemark[0].address;
						
						var position = new GLatLng(e.lat(),e.lng());
						_this.marker.setLatLng(position);
						if(_this.config.markerChange){
							_this.config.markerChange(e.lng() , e.lat() , result["Placemark"][0].formatted_address);
						}
						_this.map.panTo(position);
					}
				});
			});
			
			google.maps.event.addListener(this.map,"dragend",function showInfo() {
				var cp = _this.map.getCenter();
				_this.myGeo.getLocations(new GLatLng(cp.lat(), cp.lng()),function(result) {
					if (result.Status.code == G_GEO_SUCCESS){
						document.getElementById(_this.config.addressEl).value = result.Placemark[0].address;
						document.getElementById(_this.config.lngEl).value = cp.lng();
						document.getElementById(_this.config.latEl).value = cp.lat();
						
						var position = new GLatLng(cp.lat(), cp.lng());
						_this.marker.setLatLng(position);
						if(_this.config.markerChange){
							_this.config.markerChange(cp.lng() , cp.lat() , result["Placemark"][0].address);
						}
						_this.map.panTo(position);
					}
				});
			});
			// 
			google.maps.event.addListener(this.map,"dragging", function showInfo() {
				var cp = _this.map.getCenter();
				var position = new GLatLng(cp.lat(), cp.lng());
				_this.marker.setLatLng(position);
				_this.map.setCenter(position, _this.map.getZoom());
				_this.map.panTo(position);
			});
		},
		focusToAddress : function(address){
			var _this = this;
			//实例化一个地址解析
			geocoder = new GClientGeocoder();
			geocoder.getLocations(address, function(result){
				// 显示结果
                if (result.Status.code == G_GEO_SUCCESS){
							   lat = result.Placemark[0].Point.coordinates[1]; // lat
							   lng = result.Placemark[0].Point.coordinates[0]; // lng
                               address = result.Placemark[0].address; // 地址
							   point = new GLatLng(lat,lng);
							   _this.marker.setLatLng(point);
								if(_this.config.markerChange){
									_this.config.markerChange(point.lng() , point.lat() , address);
								}
								document.getElementById(_this.config.lngEl).value = lng;
								document.getElementById(_this.config.latEl).value = lat;
								_this.map.setCenter(point, _this.map.getZoom());
								_this.map.panTo(point);
				}
			});//搜索的核心方法
		},
		focusToLocalAddress : function(ip){
			var _this = this;
			$.getScript("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip="+ip,  function(){   
				var country = remote_ip_info["country"];  
				var prov = remote_ip_info["province"];        
				var city = remote_ip_info["city"];           
				var thisAddess=country+prov+city;
				//实例化一个地址解析
				geocoder = new GClientGeocoder();
				geocoder.getLocations(thisAddess, function(result){
					// 显示结果
	                if (result.Status.code == G_GEO_SUCCESS){
	                	lat = result.Placemark[0].Point.coordinates[1]; // lat
						lng = result.Placemark[0].Point.coordinates[0]; // lng
						point = new GLatLng(lat,lng);
						_this.marker.setLatLng(point);
						_this.map.setCenter(point, _this.map.getZoom());
						_this.map.panTo(point);
					}
				});//搜索的核心方法
				 
			});
		}
	}
	
	
	
	var googleMap = window.googleMap = {
		init : function(config){
			return new Map(config);
		}
	};

	



	if(window.jQuery || window.Zepto){
		
		
		(function($) {
			'use strict';
			/**
			 * 谷歌地图
			 * e.g:
			 *	$("#navigate_map").googleMap({
			 *		searchBtn:"mapSearch",
			 *		longitudeTarget:"longitude",
			 *		latitudeTarget:"latitude",
			 *		addressTarget:"address",
			 *		lng:"118.183358",
			 *		lat:"24.489911"
			 *	});
			 * @author jared
			 */
			$.fn.googleMap = function(options) {
				var target = this.eq(0);
				var self = target.get(0);
				var settings = $.extend({}, options);
				if(self._initMap){
					return false;
				}
				self._initMap = true;

				if(typeof self.id ==  "undefined"){
					self.id = "mapDivId_" + (new Date()).getTime();
				}
				
				var config = {mapDivId:self.id , lngEl : settings.longitudeTarget , latEl : settings.latitudeTarget , addressEl : settings.addressTarget};
				var navigate = settings.lng;
				if(navigate){
					config.lng = settings.lng;
					config.lat = settings.lat;
				} else {
					config.lng = null;
					config.lat = null;
					config.address = null;
				}
				// 位置修改触发
				config.markerChange = function(longitude,latitude,address){
					document.getElementById(config.lngEl).value = longitude;
					document.getElementById(config.latEl).value = latitude;
					if(typeof settings.markerChange == "function"){
						settings.markerChange(longitude,latitude,address);
					}
				};
				var map = googleMap.init(config);
				
				if(config.addressEl){
					if(config.lng == null  && config.lat == null){
						map.focusToLocalAddress(settings.ip);
					}
					//搜索按钮事件
					$("#" + settings.searchBtn).bind("click",function(){
						map.focusToAddress(document.getElementById(config.addressEl).value);
					});
					//输入回车事件
					$("#" +config.addressEl).keypress(function(e) {
						if (e.keyCode == 13) {
							map.focusToAddress(document.getElementById(config.addressEl).value);
						}
						return true;
					});
				}
				return target;
			};
		})(window.jQuery || window.Zepto);
	}

})();