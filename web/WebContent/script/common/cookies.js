
/**
 * cookie方法封装
 */
(function($){
	
	/**
	 * 设置cookie
	 * name => value
	 */
	$.fn.setCookie = function(name,value,path) {
		var Days = 30;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + "=" + escape(value) + ";expires="
				+ exp.toGMTString() + (path ? ";path=/" : "");
	};
	
	/**
	 * 根据名称获取cookie
	 */
	$.fn.getCookie = function(name) {
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg)) {
			return unescape(arr[2]);
		} else {
			return null;
		}
	};
	
	/**
	 * 删除cookie
	 */
	$.fn.delCookie = function(name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = $(this).getCookie(name);
		if (cval != null) {
			document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
		}
	};
})(jQuery);

