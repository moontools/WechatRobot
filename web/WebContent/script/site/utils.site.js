/**
 * ajax请求封装支持提示用户操作过期等常见错误
 */ 
function ajaxRequest(options){
	options = options || {};
	return $.ajax({
		type: options.type || "POST",
		async: options.async === undefined || options.async == true,
		dataType: options.dataType || "json",
		url: options.url || "",
		data: options.data || {},
		success: function(jsonResult, textStatus){
			if (typeof options.success == "function") {
				if (jsonResult.type == "failure" && jsonResult.data.errorType == "memberUnlogin") {
					showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
						location.href = "/login/toLogin.htm?needBack=1";
					});
					return false;
				}
				options.success(jsonResult, textStatus);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			if (textStatus == "error") {
				showError(getLanguage("site.member.login.timeout","登录超时，请重新登录"), function(){
					location.href = "/login/toLogin.htm?needBack=1";
				});
			} else {
				if (typeof options.error == "function") {
					options.error(XMLHttpRequest, textStatus, errorThrown);
				}
			}
		},
		complete: function(XMLHttpRequest, textStatus){
			if (typeof options.complete == "function") {
				options.complete(XMLHttpRequest, textStatus);
			}
		}
	});
}

/**
 * 显示提示
 * @param msg
 */
function showTip(msg){
	//TODO
	alert(msg);
}

/**
 * 显示成功信息
 * @param msg
 * @param callback
 */
function showSuccess(msg, callback){
	showSuccessDialog(msg,callback);
}

/**
 * 显示错误信息
 * @param msg
 * @param callback
 */
function showError(msg, callback){
	showErrorDialog(msg, callback);
}

/**
 * 显示确认信息
 * @param msg
 * @param callback
 */
function showConfirm(msg, callback){
	showConfirmDialog(msg, callback, null);
}

/**
 * 显示输入信息
 * @param msg
 * @param callback
 */
function showInput(msg, callback){
	//TODO
}

/**
 * 一些插件的封装
 */
(function($){
	/**
	 * 封装表单提交ajaxSubmit
	 */
	$.fn.exAjaxSubmit = function(options){
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxSubmit({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (jsonResult.type != "success" && jsonResult.data.errorType == "memberUnlogin") {
						showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
							location.href = "/login/toLogin.htm?needBack=1";
						});
					} else {
						if (typeof options.success == "function") {
							options.success(jsonResult, textStatus);
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError(getLanguage("site.member.login.timeout","登录超时，请重新登录"), function(){
							location.href = "/login/toLogin.htm?needBack=1";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	/**
	 * 封装ajax表单绑定
	 */
	$.fn.exAjaxForm = function(options) {
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxForm({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (jsonResult.type != "success" && jsonResult.data.errorType == "memberUnlogin") {
						showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
							location.href = "/login/toLogin.htm?needBack=1";
						});
					} else {
						if (typeof options.success == "function") {
							options.success(jsonResult, textStatus);
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError(getLanguage("site.member.login.timeout","登录超时，请重新登录"), function(){
							location.href = "/login/toLogin.htm?needBack=1";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	/**
	 * 封装validate
	 */
	$.fn.exValidate = function(options){
		var _this = $(this);
		
		//默认配置
		options = $.extend({
			errorElement: "span",
			errorClass: "error",
			errorPlacement: function(error, element) {
				element.parent("div.sm_l").after(error);
			}
		}, options);
		
		require(["validate"], function(){
			$.validator.addMethod("trim", function(value, element) {
				$(element).val($.trim(value));
				return true;
			});
			
			_this.validate(options);
		});
		return _this;
	};
	
	/**
	 * 封装datepicker
	 */
	$.fn.exDatepicker = function(options){
		var _options = options || {};
		if(! _options.hasOwnProperty('lang') && window.pageLocale){
			_options.lang = (window.pageLocale == "" || window.pageLocale == "zh_CN" ? 'zh-cn' : 'en');
		}
		
		var _this = $(this);
		_this.prop("readonly", options == null || options.readOnly == true)
		
		require(["datepicker97"], function(){
			_this.bind("focus", function(){
				WdatePicker(_options);
			});
		});
		return _this;
	};
})(jQuery);