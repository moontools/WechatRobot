var language = {
	'site.page.error.pageno' : "Please enter the correct page number",
	'site.member.without.login' : "You have not logged in, please login first",
	'site.member.login.timeout' : "Login timeout, please re login",
	'uploadify.error.type' : "Upload files only support {types} type",
	'uploadify.error.queuesizelimit.1' : "A maximum of ",
	'uploadify.error.queuesizelimit.2' : " files can be uploaded",
	'uploadify.error.filesizelimit' : "File size can not exceed ",
	'uploadify.error.filename.1' : "File [",
	'uploadify.error.filename.2' : "] Abnormal size!",
	'uploadify.error.uploadfail' : "File upload failed, please try again!",
	'uploadify.filetypedesc' : "Image File",
	'uploadify.error.invalidfiletype' : "Upload images only support {types} format"
};