
$("#markFontcolor").change(function(){
	$(this).css("background-color",$(this).val());
	$("input[name='markFontcolor']").val($(this).val());
});

$("input[name='markWord']").keyup(function(){
	refreshView();
});

$(".refreshView").change(function(){
	refreshView();
});

$("input[name='markStatus']").click(function(){
	changeStatus($(this).val());
});

/**
 * 修改状态，元素隐藏显示
 */
var changeStatus = function(status) {
	switch(parseInt(status)) {
	case 0:
		$(".openWaterMark").hide();
		break;
	case 1:
		$(".openWaterMark").show();
		$(".fontType").show();
		$(".pictureType").hide();
		refreshView();
		break;
	case 2:
		$(".openWaterMark").show();
		$(".fontType").hide();
		$(".pictureType").show();
		refreshView();
		break;
	}
};

/**
 * 刷新预览
 */
var refreshView = function(){
	var status = $("input[name='markStatus']:checked").val();
	// 处理预览位置
	var position = $("select[name='markAlignLocation']").find("option:selected").val();
	var style = "top:10px;left:10px;";
	switch(parseInt(position)) {
	case 0:
		style = "top:10px;left:10px;";
		break;
	case 1:
		style = "bottom:10px;left:10px;";
		break;
	case 2:
		style = "top:10px;right:10px;";
		break;
	case 3:
		style = "bottom:10px;right:10px;";
		break;
	}
	$(".watermark").attr("style",style);
	// 滤镜
	var markTransparency = parseInt($("#markTransparency").val());
	$(".watermark").find(".fontType").css("opacity", markTransparency/100);
	$(".watermark").find(".pictureType").css("opacity", markTransparency/100);
	// 显示类型、及其他参数设置
	switch(parseInt(status)) {
	case 0:
		break;
	case 1:
		var fontColor = $("#markFontcolor").val();
		var fontSize = $("select[name='markFontsize']").find("option:selected").val();
		var fontContent = $("input[name='markWord']").val();
		$(".watermark").find(".fontType").html(fontContent).show();
		$(".watermark").find(".fontType").css({"font-size":fontSize + "px","color": "#" + fontColor});
		$(".watermark").find(".pictureType").hide();
		break;
	case 2:
		$(".watermark").find(".pictureType").attr("src",$("#markImage").val() || "/images/sys/blank.gif");
		$(".watermark").find(".fontType").hide();
		$(".watermark").find(".pictureType").show();
		break;
	}
};