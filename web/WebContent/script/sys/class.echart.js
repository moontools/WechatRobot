/**
 * echart图表封装
 */
function EchartClass(options) {
	this._config = {
		echartEl: null,
			
		startDate: null,
		startDateEl: null,
		
		endDate: null,
		endDateEl: null,
		
		dateRange: 7,
		
		dataRequestUrl: "",
		
		toolTipFormatter: "",
		xAxisName: "",
		yAxisName: ""
	};
	
	//按照当前选择日期绘制图表
	this.draw = function() {
		this._config.echartEl.exEcharts(this._getOption());
	};
	
	//按照最近几天的日期绘制图表
	this.drawLatest = function(dateRange) {
		this._changeDateRange(dateRange);
		this.draw();
	};
	
	//改变最近几天的日期范围
	this._changeDateRange = function(dateRange) {
		this._config.dateRange = dateRange || this._config.dateRange;
		
		this._config.endDate = new Date();
		this._config.startDate = new Date(	this._config.endDate.getFullYear(),
											this._config.endDate.getMonth(),
											this._config.endDate.getDate() - this._config.dateRange);
		
		var startDate = this._config.startDate;
		var startDateEl = this._config.startDateEl;
		var endDate = this._config.endDate;
		var endDateEl = this._config.endDateEl;
		require(["BSDP/js/bootstrap-datepicker"], function(){
			startDateEl.datepicker("update", startDate);
			endDateEl.datepicker("update", endDate);
		});
	};
	
	//绑定日期选择控件
	this._bindDatePickerChange = function() {
		var _this = this;
		_this._config.startDateEl.bind("propertychange change", function(){
			_this._config.startDate = new Date(_this._config.startDateEl.val());
		});
		_this._config.endDateEl.bind("propertychange change", function(){
			_this._config.endDate = new Date(_this._config.endDateEl.val());
		});
	};
	
	//js的Date类转为字符串，格式如2016-05-25
	this._dateToString = function(date) {
		var year = date.getFullYear(),
			month = date.getMonth() + 1,
			date = date.getDate();
		return year + "-" + (month >= 10 ? month : "0" + month) + "-" + (date >= 10 ? date : "0" + date);
	};
	
	//日期列表
	this._getDateList = function() {
		var dateList = [];
		for(var i=0; ; i++) {
			var dateCounter = new Date(	this._config.startDate.getFullYear(),
										this._config.startDate.getMonth(),
										this._config.startDate.getDate() + i);
			
			if (dateCounter.getTime() > this._config.endDate.getTime()) break;
			dateList.push(this._dateToString(dateCounter));
		}
		return dateList;
	}
	
	//请求获取数据
	this._getDataList = function(dateList) {
		dateList = dateList || this._getDateList();
		var dataList = [];
		
		var startDate = this._dateToString(this._config.startDate);
		var endDate = this._dateToString(this._config.endDate);
		
		ajaxRequest({
			async: false,
			url: this._config.dataRequestUrl,
			data: {startDate: startDate, endDate: endDate},
			success: function(result) {
				if (result.type == "success") {
					if (result.data && result.data.statisticData) {
						for (var i=0; i<dateList.length; i++) {
							dataList.push(result.data.statisticData[dateList[i]] || 0);
						}
					}
				}
			}
		});
		return dataList;
	}
	
	//获取echart配置
	this._getOption = function() {
		var dateList = this._getDateList();
		var dataList = this._getDataList(dateList);
		return {
		    tooltip: {
		    	trigger: 'axis',
		        formatter: this._config.toolTipFormatter
		    },
		    xAxis: {
		    	type: 'category',
		        name: this._config.xAxisName,
		        splitNumber: 2,
		        boundaryGap: false,
		        splitLine: {show: false},
		        data: dateList
		    },
		    grid: {
		    	left: '3%',
		        right: '4%',
		        bottom: '3%',
		        containLabel: true
		    },
		    yAxis: {
		    	boundaryGap: false,
		        type: 'value',
		        name: this._config.yAxisName
		    },
		    series: [
		        {
		            type: 'line',
		            itemStyle:{
		            	normal: {
		            		color:'#348D1E',
		            		lineStyle:{
		            			color: "#348D1E"
		            		}
		            	}
		            },
		            data: dataList
		        }
		    ]
		};
	};
	
	//初始化
	this._init = function() {
		this._config = $.extend(this._config, options);
		this._changeDateRange();
		this._bindDatePickerChange();
		this.draw();
	};
	this._init();
}