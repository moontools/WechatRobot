/**
 * ajax请求封装支持提示用户操作过期等常见错误
 */ 
function ajaxRequest(options){
	options = options || {};
	return $.ajax({
		type: options.type || "POST",
		async: options.async === undefined || options.async == true,
		dataType: options.dataType || "json",
		url: options.url || "",
		data: options.data || {},
		success: function(jsonResult, textStatus){
			if (jsonResult.type != "success" && jsonResult.data.errorType == "unlogin") {
				showError("账号未登录", function(){
					location.href = "/sys/toLogin.do";
				});
			} else {
				if (typeof options.success == "function") {
					options.success(jsonResult, textStatus);
				}
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			if (textStatus == "error") {
				showError("登录超时，请重新登录", function(){
					location.href = "/sys/toLogin.do";
				});
			} else {
				if (typeof options.error == "function") {
					options.error(XMLHttpRequest, textStatus, errorThrown);
				}
			}
		},
		complete: function(XMLHttpRequest, textStatus){
			if (typeof options.complete == "function") {
				options.complete(XMLHttpRequest, textStatus);
			}
		}
	});
}

/**
 * sweetalert封装，显示提示
 * @param msg
 */
function showTip(msg){
	require(["sweetalert"], function(){
		setTimeout(function(){
			swal({
				title: "",
				text: msg,
				confirmButtonText: "确定"
			});
		}, 100);
	});
}

/**
 * sweetalert封装，显示成功信息
 * @param msg
 * @param callback
 */
function showSuccess(msg, callback){
	require(["sweetalert"], function(){
		setTimeout(function(){
			swal({
				title: "",
				type: "success",
				confirmButtonText: "确定",
				closeOnConfirm: true,
				text: msg
			}, function(){
				if (typeof callback == "function") {
					setTimeout(function(){
						callback();
					}, 100);
				}
			});
		}, 100);
	});
}

/**
 * sweetalert封装，显示错误信息
 * @param msg
 * @param callback
 */
function showError(msg, callback){
	setTimeout(function(){
		require(["sweetalert"], function(){
			new swal({
				title: "",
				type: "error",
				confirmButtonText: "确定",
				closeOnConfirm: true,
				text: msg
			}, function(){
				if (typeof callback == "function") {
					setTimeout(function(){
						callback();
					}, 100);
				}
			});
		});
	}, 100);
}

/**
 * sweetalert封装，显示确认信息
 * @param msg
 * @param callback
 */
function showConfirm(msg, callback){
	require(["sweetalert"], function(){
		new swal({
			title: "",
			type: "warning",
			confirmButtonText: "确定",
			showCancelButton: true,
			cancelButtonText: "取消",
			closeOnConfirm: true,
			text: msg
		}, function(){
			if (typeof callback == "function") {
				setTimeout(function(){
					callback();
				}, 100);
			}
		});
	});
}

/**
 * sweetalert封装，显示输入信息
 * @param msg
 * @param callback
 */
function showInput(msg, callback){
	require(["sweetalert"], function(){
		swal({
			title: "",
			type: "input",
			confirmButtonText: "确定",
			showCancelButton: true,
			cancelButtonText: "取消",
			closeOnConfirm: false,
			text: msg
		}, function(value){
			if (typeof callback == "function") {
				return callback(value);
			} else {
				return true;
			}
		});
	});
}

/**
 * 一些插件的封装
 */
(function($){
	/**
	 * 封装表单提交ajaxSubmit
	 */
	$.fn.exAjaxSubmit = function(options){
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxSubmit({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (jsonResult.type != "success" && jsonResult.data.errorType == "unlogin") {
						showError("账号未登录", function(){
							location.href = "/sys/toLogin.do";
						});
					} else {
						if (typeof options.success == "function") {
							options.success(jsonResult, textStatus);
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError("登录超时，请重新登录", function(){
							location.href = "/sys/toLogin.do";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	/**
	 * 封装ajax表单绑定
	 */
	$.fn.exAjaxForm = function(options) {
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxForm({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (jsonResult.type != "success" && jsonResult.data.errorType == "unlogin") {
						showError("账号未登录", function(){
							location.href = "/sys/toLogin.do";
						});
					} else {
						if (typeof options.success == "function") {
							options.success(jsonResult, textStatus);
						}
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError("登录超时，请重新登录", function(){
							location.href = "/sys/toLogin.do";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	/**
	 * 封装validate
	 */
	$.fn.exValidate = function(options,afterRequire){
		var _this = $(this);
		
		//默认配置
		options = $.extend({
			errorElement: "span",
			errorClass: "sign_red",
			errorPlacement: function(error, element) {
				element.parent("div").after(error);
			}
		}, options);
		
		require(["validate","validatemessage"], function(){
			if(afterRequire){
				afterRequire();
			}
			$.validator.addMethod("trim", function(value, element) {
				$(element).val($.trim(value));
				return true;
			});
			
			//return _this.validate(options);
			_this.validate(options);
		});
		return _this;
	};
	
	/**
	 * 封装日期选择器datepicker
	 */
	$.fn.exDatepicker = function(options){
		var _this = $(this);
		require(["BSDP/js/bootstrap-datepicker"], function(){
			require(["BSDP/locales/bootstrap-datepicker.zh-CN.min"], function(){
				var options = $.extend({language: "zh-CN", format: "yyyy-mm-dd"}, options);
				_this.datepicker(options);
			});
		});
		return _this;
	};
	
	/**
	 * 全选封装
	 */
	$.fn.exICheck = function(options){
		var _this = $(this);
		require(["icheck"], function(){
			var options = $.extend({
				checkboxClass: "icheckbox_square-green",
				radioClass: "iradio_square-green"
			}, options);
			
			_this.find(".i-checks").iCheck(options);
			_this.find(".i-checks-all").iCheck(options);
			_this.find(".i-checks-all").on("ifChecked", function(){
				$(".i-checks").iCheck('check');
			}).on("ifUnchecked", function(){
				$(".i-checks").iCheck('uncheck');
			});
		});
		return _this;
	};
})(jQuery);