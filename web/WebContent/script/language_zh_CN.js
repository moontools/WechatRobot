var language = {
	'site.page.error.pageno' : "请输入正常的页数",
	'site.member.without.login' : "您尚未登录，请先登录",
	'site.member.login.timeout' : "登录超时，请重新登录",
	'uploadify.error.type' : "上传文件只支持{types}格式",
	'uploadify.error.queuesizelimit.1' : "一次最多只能上传",
	'uploadify.error.queuesizelimit.2' : "个文件",
	'uploadify.error.filesizelimit' : "文件大小不能超过",
	'uploadify.error.filename.1' : "文件 [",
	'uploadify.error.filename.2' : "] 大小异常！",
	'uploadify.error.uploadfail' : "文件上传失败，请重试！",
	'uploadify.filetypedesc' : "图片文件",
	'uploadify.error.invalidfiletype' : "上传图片只支持{types}格式"
};