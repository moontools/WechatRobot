﻿// JavaScript Document
//*******************************************
//功能：tabs页签
//作者：Qupy
//时间：2012-2-16
//*******************************************
(function($) {
    $.fn.extend({
        tabs: function(options) {
            var opts = $.extend({}, $.fn.tabs.defaults, options);

            return this.each(function() {
                //当前实例
                var $this = $(this);
                //支持Metadata插件，抽取元数据
                var o = $.meta ? $.extend({}, opts, $this.data()) : opts;
							
				
				var ulTool=$this.find("ul[tool='true']");
				ulTool.addClass("neocard");
				
				//ulTool.children().addClass("ul.neocard li");
				ulTool.children().each(function(index){
					//如果索引和设置的相同，则不将这个div设置为隐藏
					var li=$(this);
					if(o.showIndex!=index){						
						var div=$(li.attr("link"));
						div.css("display","none");
					}else{
						li.addClass("selected");
					}									
				}).click(function(){
					//alert("true");
					var thisDivId=$(this).attr("link");
					//alert("Link");
					var li= ulTool.children(".selected").removeClass("selected");
					var div=$(li.attr("link"));
					div.css("display","none");					
					var divNow=$(thisDivId);
					divNow.css("display","block");
					$(this).addClass("selected");
					$(this).css({'color':'','background-color':''});  	
					if(o.onClick && typeof (o.onClick)=='function'){
						o.onClick($(this).text(),$(this));
					}
				})
                
            });
        }
    });
})(jQuery);

//绑定默认配置
$.fn.tabs.defaults = {
    showIndex:0,
	onClick:null
};
