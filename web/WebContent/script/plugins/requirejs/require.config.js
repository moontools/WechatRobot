requirejs.config({
	urlArgs: "ver=1.0",
	baseUrl: '/script/plugins',
    paths: {
    	jquery: "jquery-1.12.1",
    	bootstrap: "bootstrap/js/bootstrap",
    	
    	metisMenu: "metisMenu/jquery.metisMenu",
    	slimscroll: "slimscroll/jquery.slimscroll",
    	pace: "pace",
    	inspinia: "inspinia",
    	icheck: "iCheck/icheck.min",
    	validate: "validate/jquery.validate",
    	validatemessage: "validate/messages_zh",
    	sweetalert: "sweetalert/sweetalert.min",
    	ueditorconfig : "ueditor/ueditor.config",
    	ueditor : "ueditor/ueditor.all",
    	echarts : "echarts/echarts.min",
    	echarts2 : "echarts2/echarts-all",
    	datepicker97: "My97DatePicker/WdatePicker",
    	
        BS: "bootstrap/",
        BSDP: "datetimepicker/"
        
    },
    //模块依赖配置
    map: {
    	"*": {
    		"css": "requirejs/css"
    	}
    },
    //文件依赖配置
    shim: {
    	"bootstrap": ["jquery"],
    	"BSDP/js/bootstrap-datepicker": ["jquery"],
    	"metisMenu": ["jquery"],
    	"slimscroll": ["jquery"],
    	"inspinia": ["bootstrap", "metisMenu", "slimscroll"],
    	"validate": ["jquery"],
    	"validatemessage": ["jquery","validate"],
    	"ueditor" : ["jquery","ueditorconfig"]
    }
});