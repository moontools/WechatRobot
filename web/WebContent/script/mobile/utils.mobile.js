/**
 * ajax请求封装支持提示用户操作过期等常见错误
 */ 
function ajaxRequest(options){
	options = options || {};
	return $.ajax({
		type: options.type || "POST",
		async: options.async === undefined || options.async == true,
		dataType: options.dataType || "json",
		url: options.url || "",
		data: options.data || {},
		success: function(jsonResult, textStatus){
			if (typeof options.success == "function") {
				if (jsonResult.type == "failure" && jsonResult.data.errorType == "memberUnlogin") {
					showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
						location.href = "/mobile/login/toLogin.htm?needBack=1";
					});
					return false;
				}
				options.success(jsonResult, textStatus);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown){
			if (textStatus == "error") {
				showError(getLanguage("site.member.login.timeout","登录超时，请重新登录"), function(){
					location.href = "/mobile/login/toLogin.htm?needBack=1";
				});
			} else {
				if (typeof options.error == "function") {
					options.error(XMLHttpRequest, textStatus, errorThrown);
				}
			}
		},
		complete: function(XMLHttpRequest, textStatus){
			if (typeof options.complete == "function") {
				options.complete(XMLHttpRequest, textStatus);
			}
		}
	});
}

/**
 * 手机端页面滑动到底部触发回调函数
 * @param callback
 */
function bindToScrollBottom(callback){
	if (typeof callback == "function") {
		$(window).bind("scroll", function(){
			//滑动到底部判断
			function scrollToBottom() {
				var fixedHeight = 15;	//手机浏览器版本不同，最多有13像素的计算误差补偿
				return $(document).height() >= $(window).height() && 
						$(window).scrollTop() + fixedHeight >= $(document).height() - $(window).height();
			}
			
			if (scrollToBottom()) {
				callback();
			}
		});
	}
}

/**
 * 显示提示
 * @param msg
 */
function showTip(msg){
	//TODO
	alert(msg);
}

/**
 * 显示成功信息
 * @param msg
 * @param callback
 */
function showSuccess(msg, callback){
	showAlert(msg,callback);
}

/**
 * 显示错误信息
 * @param msg
 * @param callback
 */
function showError(msg, callback){
	showAlert(msg,callback);
}

/**
 * 显示确认信息
 * @param msg
 * @param callback
 */
function showConfirm(msg, callback){
	//TODO
	if (confirm(msg)) {
		if (typeof callback == "function") {
			callback();
		}
	}
}

/**
 * sweetalert封装，显示确认信息
 * @param msg
 * @param callback
 */
function showInput(msg, callback){
	//TODO
}

/**
 * 一些插件的封装
 */
(function($){
	/**
	 * 封装表单提交ajaxSubmit
	 */
	$.fn.exAjaxSubmit = function(options){
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxSubmit({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (typeof options.success == "function") {
						if (jsonResult.type == "failure" && jsonResult.data.errorType == "memberUnlogin") {
							showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
								location.href = "/mobile/login/toLogin.htm?needBack=1";
							});
							return false;
						}
						options.success(jsonResult, textStatus);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError("登录超时，请重新登录", function(){
							//location.href = "/login/toLogin.htm";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	$.fn.exHtml5uploader = function(options){
		var _this = $(this);
		require(["/script/plugins/jquery.exif.js"], function(){
			require(["/script/plugins/jquery.html5uploader.js"], function(){
				 _this.html5uploader(options);
			});
		});
		return _this;
	}
	
	$.fn.exMobileDate = function(options){
		var _this = $(this);
		require(["/script/plugins/mobiscroll-master/js/mobiscroll.custom-2.6.2.min.js"], function(){
			_this.mobiscroll().date(options);
		});
		return _this;
	}
	
	/**
	 * 封装ajax表单绑定
	 */
	$.fn.exAjaxForm = function(options) {
		var _this = $(this);
		require(["jquery.form"], function(){
			_this.ajaxForm({
				type: options.type || "POST",
				async: options.async === undefined || options.async == true,
				dataType: options.dataType || "json",
				url: options.url || _this[0].action || "",
				data: options.data || {},
				beforeSubmit: function(){
					if (typeof options.beforeSubmit == "function") {
						return options.beforeSubmit();
					}
				},
				success: function(jsonResult, textStatus){
					if (typeof options.success == "function") {
						if (jsonResult.type == "failure" && jsonResult.data.errorType == "memberUnlogin") {
							showError(getLanguage("site.member.without.login","您尚未登录，请先登录"), function(){
								location.href = "/mobile/login/toLogin.htm?needBack=1";
							});
							return false;
						}
						options.success(jsonResult, textStatus);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					if (textStatus == "error") {
						showError("登录超时，请重新登录", function(){
							//location.href = "/login/toLogin.htm";
						});
					} else {
						if (typeof options.error == "function") {
							options.error(XMLHttpRequest, textStatus, errorThrown);
						}
					}
				},
				complete: function(XMLHttpRequest, textStatus){
					if (typeof options.complete == "function") {
						options.complete(XMLHttpRequest, textStatus);
					}
				}
			});
		});
		return _this;
	};
	
	/**
	 * 封装datepicker
	 */
	/*$.fn.exDatepicker = function(options){
		var _this = $(this);
		_this.prop("readonly", options == null || options.readOnly == true)
		
		require(["datepicker97"], function(){
			_this.bind("focus", function(){
				WdatePicker(options || {});
			});
		});
		return _this;
	};*/
})(jQuery);