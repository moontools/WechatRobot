/**
 * Created by Lincoln on 2017/8/9.
 */
$.extend({
    alertLL: function (title, content,funL) {
        $("#boxAlert").each(function () {
            $(this).remove();
        });
        var html = '<div class="boxgrand"></div>';
        $("body").append("<div id='boxAlert' style=';z-index:99999'></div>");
        $("#boxAlert").append(html);
        $("#boxAlert").append('<div class="box-tanchu"><ul><li><b>' + title + '</b></li><li>' + content + '</li><li>' + '</li><a href="javascript:void(0)" style="color: white" id="lincolnCheck"><li class="box-btn">确认</li></a></ul></div>');
        $("#lincolnCheck").click(function(){
            $('#boxAlert').remove();
            funL();
        });
    },
    alertL: function (title, content) {
        $("#boxAlert").each(function () {
            $(this).remove();
        });
        var html = '<div class="boxgrand"></div>';
        $("body").append("<div id='boxAlert' style=';z-index:99999'></div>");
        $("#boxAlert").append(html);
        $("#boxAlert").append('<div class="box-tanchu"><ul><li><b>' + title + '</b></li><li>' + content + '</li><li>' + '</li><a href="javascript:void(0)" style="color: white" onclick="$(\'#boxAlert\').remove();"><li class="box-btn">确认</li></a></ul></div>');
    },
    comformL: function (title, content,funL,btn1,btn2) {
        if(btn1==null||btn1==""){
            btn1 = "确定";
        }
        if(btn2==null||btn2==""){
            btn2 = "取消";
        }
        $("#boxAlert").each(function () {
            $(this).remove();
        });
        var html = '<div class="boxgrand"></div>';
        $("body").append("<div id='boxAlert' style=';z-index:99999'></div>");
        $("#boxAlert").append(html);
        $("#boxAlert").append('<div class="box-tanchu"><ul><li><b>' + title + '</b></li><li>' + content + '</li><li>' + '</li><a href="javascript:void(0)" style="color: white" id="lincolncheck"><li class="box-btn">'+btn1+'</li></a><a href="javascript:void(0)" style="color: white" onclick="$(\'#boxAlert\').remove();"><li class="box-btn">'+btn2+'</li></a></ul></div>');
        $("#lincolncheck").click(function(){
            funL();
        });
    },
    comformLL: function (title, content,funL,cancel,btn1,btn2) {
        if(btn1==null||btn1==""){
            btn1 = "确定";
        }
        if(btn2==null||btn2==""){
            btn2 = "取消";
        }
        $("#boxAlert1").each(function () {
            $(this).remove();
        });
        var html = '<div class="boxgrand"></div>';
        $("body").append("<div id='boxAlert1' style=';z-index:99999'></div>");
        $("#boxAlert1").append(html);
        $("#boxAlert1").append('<div class="box-tanchu"><ul><li><b>' + title + '</b></li><li>' + content + '</li><li>' + '</li><a href="javascript:void(0)" style="color: white" id="lincolncheck"><li class="box-btn">'+btn1+'</li></a><a id="lincolncancel" href="javascript:void(0)" style="color: white"><li class="box-btn">'+btn2+'</li></a></ul></div>');
        $("#lincolncheck").click(function(){
            funL();
        });
        $("#lincolncancel").click(function(){
            cancel();
        });
    }
})
/**
 <link rel="stylesheet" href="box.css" type="text/css" />
 <div id="coutantssan" class="coutantssaa">
 </div>
 <div id="wdtx" class="box-tanchu">
 <ul>
 <li class="box-top"><b>提现</b></li>
 <li>提现金额：129元</li>
 <li>提现银行卡尾号：1235</li>
 <li class="box-btn">
 确认
 </li>
 </ul>
 </div>
 **/