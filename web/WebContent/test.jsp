<%@page import="com.lincoln.framework.utils.FrameworkLogger"%>
<%@page import="com.lincoln.framework.utils.StringUtilsEx"%>
<%@page import="java.io.BufferedReader"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	BufferedReader rd = request.getReader();
	String tempLine = rd.readLine();
	StringBuffer tempStr = new StringBuffer();
	String crlf = System.getProperty("line.separator");
	while (tempLine != null) {
		tempStr.append(tempLine);
		tempStr.append(crlf);
		tempLine = rd.readLine();
	}
	String responseContent = tempStr.toString();
	System.err.println("响应内容：" + responseContent);
	System.err.println("请求参数：" + StringUtilsEx.getJsonString(request.getParameterMap()));
%>
