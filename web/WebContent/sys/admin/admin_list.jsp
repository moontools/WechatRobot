<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="ex" uri="/framework/tld"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>
    <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <ol class="breadcrumb">
          <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
          <li> <strong>用户管理</strong> </li>
        </ol>
      </div>
      <div class="col-lg-2"> </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-content">
              <form role="form" class="form-inline" id="searchForm" action="toList.do" method="post">
                <div class="form-group m_l_10">
                  <label for="exampleInputEmail2" class="">账号： </label>
                    <input type="text"  placeholder="" id="name" name="name" value="<c:out value="${name}"/>" class="form-control" data-form-un="1456383357267.2239">
                </div>
                <div class="form-group m_l_10">
                  <label for="exampleInputEmail2" class="">使用者：</label>
                    <input type="text" placeholder="" id="realName" name="realName" value="<c:out value="${realName}"/>"  class="form-control" data-form-pw="1456383357267.2239">
                </div>
                <div class="form-group m_l_10">
                  <label for="exampleInputEmail2" class="">所属角色：</label>
                  <select class="form-control m-b-5 " id="roleId"  name="roleId">
                    <option value="0">请选择</option>
                 	<c:forEach var="sysRole"  items="${sysRoleList}" >
                    <option value="${sysRole.id}" title="${sysRole.name}" <c:if test="${sysRole.id == roleId}">selected="selected"</c:if>>${sysRole.name}</option>
                    </c:forEach>
                  </select>
                </div>
                <c:if test="${sysRoleRights.checkButtonRights(1) }">
                <button type="submit"  class="btn btn-primary m_l_10">查询</button>
                </c:if>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-content clearfix">
              <c:if test="${sysRoleRights.checkButtonRights(2) }">
              <div class=""> <a  href="<c:url value="toEdit.do"/>" class="btn btn-primary ">新增用户</a> </div>
              </c:if>
              <table class="table table-bordered table-hover m_t_6 table-center">
                <thead>
                  <tr>
                    <th>账号</th>
                    <th>使用者</th>
                    <th width="150">联系电话</th>
                    <th>所属角色</th>
                    <th>备注</th>
                    <th width="80">状态</th>
                    <th width="150">操作</th>
                  </tr>
                </thead>
                <tbody>
                <c:if test="${resultPage.getResult() == null || resultPage.getResult().size() == 0}">
                <tr>
					<td colspan="7" style="text-align: center;">暂无数据</td>
				</tr>
				</c:if>
                <c:forEach var="sysAdmin"  items="${resultPage.getResult()}" >
                  <tr>
                  	<td>${sysAdmin.name}</td>
                    <td>${sysAdmin.realName}</td>
                    <td>${sysAdmin.phoneNum}</td>
                    <td>${sysAdmin.sysRole.name}</td>
                    <td>${sysAdmin.remark}</td>
                    <td><c:if test="${sysAdmin.status == 0}"><i title="禁用" class="fa fa-times text-danger"></i></c:if><c:if test="${sysAdmin.status == 1}"><i title="启用" class="fa fa-check text-navy"></i></c:if></td>
                    <td class="opera_icon">
                    <c:if test="${sysAdmin.status == 0 && sysAdmin.sysRole != null}">
                    <c:if test="${sysRoleRights.checkButtonRights(5) }">
                    	<a title="启用" class="resume" href="javascript:;" data-id="<c:out value="${sysAdmin.id}"/>"><i class="fa fa-check-circle"></i></a>
                    </c:if>
                    </c:if>
                    <c:if test="${sysAdmin.status == 1 && sysAdmin.sysRole != null}">
                    <c:if test="${sysRoleRights.checkButtonRights(5) }">
                    	<a title="禁用" class="stop" href="javascript:;" data-id="<c:out value="${sysAdmin.id}"/>"><i class="fa fa-minus-circle"></i></a>
                    </c:if>
                    </c:if>
                    <c:if test="${sysRoleRights.checkButtonRights(2) }">
                    	<a title="修改" href="<c:url value="toEdit.do"><c:param name="id" value="${sysAdmin.id}"></c:param></c:url>"><i class="fa fa-edit"></i></a>
                    </c:if>
                    	<c:if test="${sysAdmin.sysRole != null }">
                    	<c:if test="${sysRoleRights.checkButtonRights(3) }">
                    	<a title="删除" class="delete" href="javascript:;" data-id="<c:out value="${sysAdmin.id}"/>"><i class="fa fa-trash-o"></i></a>
                    	</c:if>
                    	</c:if>
                    </td>
                  </tr>
                  </c:forEach>
                </tbody>
              </table>
              <c:if test="${resultPage.getResult() != null && resultPage.getResult().size() > 0}">
              <ex:page target="searchForm"  pageObj="${resultPage}" />
              </c:if>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script  type="text/javascript">
    var submiting = false;
	$(document).ready(function(){
		
		$('.stop').click(function() {
			if(submiting){
				return true;
			}
			var id = $(this).attr("data-id");
			
			showConfirm("确定禁用该用户么", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='adminClose.do' />?id=" + id,
					success:function(result){
						if(result.type == "success") {
							showSuccess("禁用成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
		
		$('.delete').click(function() {
			if(submiting){
				return true;
			}
			var id = $(this).attr("data-id");
			
			showConfirm("确定删除吗", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='delete.do' />?id=" + id,
					success:function(result){
						if(result.type == "success") {
							showSuccess("删除成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
		
		$('.resume').click(function() {
			if(submiting){
				return true;
			}
			var id = $(this).attr("data-id");
			
			showConfirm("确定启用该用户么", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='adminOpen.do' />?id=" + id,
					success:function(result){
						if(result.type == "success") {
							showSuccess("启用成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
	});
    </script>
<c:import url="/sys/common/sys_footer.jsp" ></c:import>