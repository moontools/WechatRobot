<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="ex" uri="/framework/tld"%>
<c:import url="/sys/common/sys_header.jsp"></c:import>
    <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <ol class="breadcrumb">
          <li><i class="fa fa-arrow-circle-o-right"></i>系统管理 </li>
          <li> 用户管理 </li>
          <li class="active"> <strong><c:if test="${sysAdmin.id > 0}">修改</c:if><c:if test="${sysAdmin.id == 0}">新增</c:if>用户</strong> </li>
        </ol>
      </div>
      <div class="col-lg-2"> </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-content">
              <form method="post" class="form-horizontal" id="searchForm" action="edit.do">
              	<input type="hidden" name="id" id="id" value="<c:out value="${sysAdmin.id}"/>">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>账号：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="text" class="form-control" id="name" name="name" <c:if test="${sysAdmin.id > 0}">readOnly</c:if> value="${sysAdmin.name}"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><c:if test= "${sysAdmin.id == 0}"><span class="text-danger">*&nbsp;</span></c:if>密码：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="password" id="password" name="password"  class="form-control" ></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                
                <c:if test= "${sysAdmin.id == 0}">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>确认密码：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="password" id="checkPassword" name="checkPassword" class="form-control" ></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                </c:if>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">使用者：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="text" id="realName" name="realName" class="form-control" value="${sysAdmin.realName}"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                
                <c:if test= "${sysAdmin.id == 0 || sysAdmin.sysRole != null}">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>角色：</label>
                  <div class="col-sm-10">
                  <div class="row"><div class="col-md-4">
                    <select class="form-control" id="roleId"  name="roleId">
                    	<option value="0">请选择</option>
                      <c:forEach var="sysRole"  items="${sysRoleList}" >
                    	<option value="${sysRole.id}" title="${sysRole.name}" <c:if test="${sysRole.id == sysAdmin.sysRole.id}">selected="selected"</c:if>>${sysRole.name}</option>
                      </c:forEach>
                    </select>
                    </div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                </c:if>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">电话：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="text" class="form-control" id="phoneNum" name="phoneNum" value="${sysAdmin.phoneNum}"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">备注：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="text" class="form-control" id="remark" name="remark" value="${sysAdmin.remark}"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                  <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" type="submit">提 交</button>
                    <button class="btn btn-white m_l_10" type="button"  onclick="location.href='<c:url value='backList.do' />';" >取 消</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
<ex:javascript src="/script/common/check.js"></ex:javascript>
 <script type="text/javascript">
 $(document).ready(function(){
	 <c:if test= "${sysAdmin.id == 0}">
	 $("#name").focus();
	 </c:if>
	 <c:if test= "${sysAdmin.id > 0}">
	 $("#password").focus();
	 </c:if>
 });
		 require(["validate"] ,function(){
			$("#searchForm").exValidate({
				rules: {
					name: {
						required: true,
						maxlength:18
					},
					<c:if test= "${sysAdmin.id == 0}">
					password: {
						required: true,
						ValidatePassword : true,
						maxlength:18
					},
					checkPassword : {
						required:true,
						equalTo:"#password"
					},
					</c:if>
					<c:if test= "${sysAdmin.id > 0}">
					password: {
						ValidatePassword : true
					},
					</c:if>
					realName: {
						maxlength:18
					},
					<c:if test= "${sysAdmin.id == 0 || sysAdmin.sysRole != null}">
					roleId:{
						roleId:true
					},
					</c:if>
					phoneNum: {
						maxlength:20
					},
					remark:{
						maxlength:100
					}
				},
				messages: {
					name: {
						required: "请输入账号",
						maxlength:"输入账号长度不能大于18个字符"
					},
					<c:if test= "${sysAdmin.id == 0}">
					password: {
						required: "请输入密码",
						ValidatePassword: "密码不能包含空格",
						maxlength:"输入密码长度不能大于18个字符"
					},
					checkPassword: {
						required: "请输入确认密码",
						equalTo : "确认密码与密码不一致"
					},
					</c:if>
					<c:if test= "${sysAdmin.id > 0}">
					password: {
						ValidatePassword: "密码不能包含空格"
					},
					</c:if>
					realName: {
						maxlength:"输入使用者长度不能大于18个字符"
					},
					<c:if test= "${sysAdmin.id == 0 || sysAdmin.sysRole != null}">
					roleId:{
						roleId:"请选择用户角色"
					},
					</c:if>
					phoneNum: {
						maxlength:"输入电话长度不能大于13个字符"
					},
					remark: {
						maxlength:"输入备注长度不能大于100个字符",
					}
				},
				submitHandler: function(form) {
					$(form).exAjaxSubmit({
						dataType : "json",
						success:function(result){
							if(result.type == "success") {
								showSuccess("保存成功",function(){
									location.href = "<c:url value="backList.do" />"
								});
							} else {
								if(result.data.focusId || "") {
									showError(result.msg,function(){
										$("#" + result.data.focusId).focus();
									});
								} else {
									showError(result.msg,function(){});
								}
							}
						}
					});
					return false;
				}
			});
			
	 		jQuery.validator.addMethod("ValidatePassword", function(value, element) {
				return !(/\s/g.test(value));
			});
	 		
	 		<c:if test= "${sysAdmin.id == 0 || sysAdmin.sysRole != null}">
	 		jQuery.validator.addMethod("roleId", function(value, element) {
				return ( value != 0 && $.trim(value).length > 0);
			}, "请选择用户角色");
	 		</c:if>
	 		
		});
 
 </script>
<c:import url="/sys/common/sys_footer.jsp" ></c:import>