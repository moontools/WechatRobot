<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <ol class="breadcrumb">
      <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
      <li> 角色管理 </li>
      <li> <strong>权限设置</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content clearfix">
          <form method="post" action="<c:url value="saveRoleConfig.do"/>" id="role_config_form">
          <div class=" clearfix">
          	<p class="f_l m_t_10">为${sysRole.name}设置权限</p>
          	<div class="f_r">
          	  <button type="submit" class="btn btn-primary">提交</button>
          	  <button type="button" class="btn btn-white m_l_10" onclick="location.href='<c:url value="toList.do"/>';">返回列表</button>
          	</div>
       	  </div>
          <table class="table table-bordered table-hover m_t_6 table-striped table-center">
            <thead>
              <tr>
                <th width="50">选择</th>
                <th width="80">级别</th>
                <th width="180">菜单名称</th>
                <th>设置</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="menuItem" items="${sysMenuList}">
              	<tr data-id="${menuItem.id}" data-parentid="${menuItem.parentMenu.id}">
              	  <td><input type="checkbox" name="menuId" value="${menuItem.id}" class="checkable i-checks" data-type="${menuItem.level}"></td>
              	  <c:if test="${menuItem.level == 1}">
              	  	<td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/sys/grate_a.gif">${menuItem.level + 1}级</td>
              	  </c:if>
              	  <c:if test="${menuItem.level == 0}">
              	  	<td>${menuItem.level + 1}级&nbsp;&nbsp;&nbsp;&nbsp;</td>
              	  </c:if>
              	  <td>${menuItem.name}</td>
              	  <td class="module_list left">
              	  	<c:if test="${menuItem.level == 1}">
              	  	  <div class="col-sm-10">
              	  	  	<c:forEach var="moduleName" items="${menuItem.getMenuButtonsList()}" varStatus="status">
              	  	  	  <label class="checkbox-inline">
              	  	  	    <input type="checkbox" id="inlineCheckbox1" class="checkable" data-type="2" name="module" value="${menuItem.id}_${status.count}">${moduleName}
              	  	  	  </label>
              	  	  	</c:forEach>
              	  	  </div>
              	  	</c:if>
              	  </td>
              	</tr>
              </c:forEach>
              <tr>
                <td colspan="4" class="left">将
                  <select name="editable_length" aria-controls="editable" class="form-control input-sm input_w" id="role_select">
                  	<option value="0" title="空权限">空权限</option>
               		<c:forEach var="sysRoleItem" items="${sysRoleList}">
               		  <option title="${sysRoleItem.name}" value="${sysRoleItem.id}" ${sysRoleItem.id == sysRole.id ? "selected" : ""}>
               		  	${sysRoleItem.name}
               		  </option>
               		</c:forEach>
                  </select>的权限赋予当前角色
                </td>
              </tr>
            </tbody>
          </table>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="footer">
  <div> <strong>Copyright</strong> 中资源 &copy; 2014-2015 </div>
</div>

<script type="text/javascript">
$(function(){
	$("#role_config_form").exValidate({
		submitHandler: function(form) {
			$(form).exAjaxSubmit({
				type: "post",
				data: {id: ${sysRole.id}},
				dataType: "json",
				success: function(result){
					if (result.type == "success"){
						showSuccess(result.msg, function(){
							location.href = "<c:url value='toList.do'/>";
						});
					} else {
						showError(result.msg);
					}
				},
				error: function(){
					showError("提交失败");
				}
			});
			return false;
		}
	});
	
	//初始化复选框选项
	function initCheckStatus(authorizationCode){
		$(".checkable").prop("checked", false);
		var authorizationCodeArr = authorizationCode.split(",") || [];
		for (var i=0; i<authorizationCodeArr.length; i++) {
			var authorizationCodeItem = authorizationCodeArr[i];
			if (authorizationCodeItem) {
				$(".checkable[value="+ authorizationCodeItem +"]").prop("checked", true);
			}
		}
	}
	initCheckStatus("${sysRole.getAuthorizationCode()}");
	
	
	//处理复选框点击事件
	$(".checkable").bind("click", function(){
		var _this = $(this);
		var checkType = parseInt(_this.attr("data-type"));
		
		switch (checkType) {
			case 0: //一级菜单
				var dataId = _this.closest("tr").attr("data-id");
				if (_this.is(":checked")) {
					$("tr[data-parentid="+ dataId +"] input:checkbox").prop("checked", true);
				} else {
					$("tr[data-parentid="+ dataId +"] input:checkbox").prop("checked", false);
				}
				break;
			case 1: //二级菜单
				var dataParentId = _this.closest("tr").attr("data-parentid");
				_this.closest("tr").find(".module_list input:checkbox").prop("checked", _this.prop("checked"));
			
				if (_this.is(":checked")) {
					$("tr[data-id="+ dataParentId +"] input:checkbox").prop("checked", true);
				} else {
					if ($("tr[data-parentid="+ dataParentId +"] input:checkbox:checked").length == 0) {
						$("tr[data-id="+ dataParentId +"] input:checkbox").prop("checked", false);
					}
				}
				break;
			case 2: //模块功能
				var dataParentId = _this.closest("tr").attr("data-parentid");
				if (_this.is(":checked")) {
					_this.closest("tr").find("input[data-type=1]").prop("checked", true);
					$("tr[data-id="+ dataParentId +"] input:checkbox").prop("checked", true);
				} else {
					if (_this.closest(".module_list").find("input:checkbox:checked").length == 0) {
						_this.closest("tr").find("input[data-type=1]").prop("checked", false);
						if ($("tr[data-parentid="+ dataParentId +"] input:checkbox:checked").length == 0) {
							$("tr[data-id="+ dataParentId +"] input:checkbox").prop("checked", false);
						}
					}
				}
				break;
			default: break;
		}
	});
	
	//处理权限复制
	$("#role_select").bind("change", function(){
		var selectId = $(this).val();
		if (selectId == 0) {
			initCheckStatus("");
			return;
		}
		
		ajaxRequest({
			url: "<c:url value='getAuthorizationByRoleId.do'/>",
			data: {id: selectId},
			success: function(result){
				initCheckStatus(result.data.authorizationCode);
			}
		});
	});
});
</script>

<c:import url="/sys/common/sys_footer.jsp" ></c:import>