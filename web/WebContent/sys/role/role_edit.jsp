<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <ol class="breadcrumb">
      <li><i class="fa fa-arrow-circle-o-right"></i>系统管理</li>
      <li>角色管理</li>
      <li class="active"> <strong>${param.id==0 ? "新增" : "修改"}角色</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <form method="post" class="form-horizontal" action="saveNewRole.do" id="role_add_form" >
            <div class="form-group">
              <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>角色名称：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  	<input type="hidden" name="id" value="${sysRole.id}">
                	<input type="text" class="form-control" name="roleName" value="${sysRole.name}" maxlength="30">
                  </div>
                  <div></div>
                </div>
              </div>
            </div><div class="form-group">
              <label class="col-sm-2 control-label">备注：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  	<input type="hidden" name="id" value="${sysRole.id}">
                	<input type="text" class="form-control" name="roleRemark" value="${sysRole.remark}" maxlength="30">
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <div class="col-sm-4 col-sm-offset-2">
                <button class="btn btn-primary" type="submit">提 交</button>
                <button class="btn btn-white m_l_10" type="button" onclick="location.href='<c:url value="toList.do"/>';">取 消</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
	
	require(["validate"], function(){
		$.validator.addMethod("trimRequired", function(value, element) {
			$(element).val($.trim(value));
			return $.trim(value).length > 0;
		});
	});
	
	$("#role_add_form").exValidate({
		rules: {
			roleName: "trimRequired"
		},
		messages: {
			roleName: {
				trimRequired: "请输入角色名称"
			}
		},
		submitHandler: function(form) {
			$(form).exAjaxSubmit({
				type: "post",
				dataType: "json",
				success: function(result){
					if (result.type == "success"){
						showSuccess(result.msg, function(){
							location.href = "<c:url value='toList.do'/>";
						});
					} else {
						showError(result.msg);
					}
				},
				error: function(){
					showError("提交失败");
				}
			});
			return false;
		}
	});
	
	$("input[type=text]:visible:first").focus();
});
</script>

<c:import url="/sys/common/sys_footer.jsp" ></c:import>