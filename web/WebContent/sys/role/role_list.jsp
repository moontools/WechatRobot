<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <ol class="breadcrumb">
      <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
      <li> <strong>角色管理</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content clearfix">
        <form role="form" class="form-inline" action="<c:url value="toList.do" />" method="post" id="roleListForm">
        <c:if test="${sysRoleRights.checkButtonRights(2) }">
          <div><a href="<c:url value="toEdit.do"/>?id=0" class="btn btn-primary">新增角色</a></div>
        </c:if>
          <table class="table table-bordered table-hover m_t_6 table-center">
            <thead>
              <tr>
                <th>角色名称</th>
                <th>备注</th>
                <th width="150">操作</th>
              </tr>
            </thead>
            <tbody>
              <c:if test="${sysRolePage.getResult() == null || sysRolePage.getResult().size() == 0}">
				<tr><td colspan="3" style="text-align: center;">暂无数据</td></tr>
			  </c:if>
              <c:forEach var="sysRole" items="${sysRolePage.getResult()}">
              	<tr>
                  <td><c:out value="${sysRole.name}"/></td>
                  <td><c:out value="${sysRole.remark}"/></td>
                  <td class="opera_icon">
                  <c:if test="${sysRoleRights.checkButtonRights(2) }">
                  	<a href="<c:url value="toEdit.do"/>?id=${sysRole.id}"><i class="fa fa-edit" title="修改"></i></a>
                  	<a href="<c:url value="toConfig.do"/>?id=${sysRole.id}"><i class="fa fa-cog" title="权限设置"></i></a>
                  </c:if>
                  <c:if test="${sysRoleRights.checkButtonRights(3) }">
                  	<a class="delete delete_btn" href="javascript:;" data-id="${sysRole.id}"><i class="fa fa-trash-o" title="删除"></i></a>
                  </c:if>
                  </td>
                </tr>
              </c:forEach>
            </tbody>
          </table>
        </form>
        <c:if test="${sysRolePage.getResult() != null && sysRolePage.getResult().size() > 0}">
          <ex:page target="roleListForm"  pageObj="${sysRolePage}"/>
        </c:if>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
	$(".delete_btn").each(function(){
		var _this = $(this);
		_this.bind("click", function(){
			showConfirm("确定删除此角色吗", function(){
				ajaxRequest({
					url: "<c:url value='delete.do'/>",
					data: {id: _this.attr("data-id")},
					success: function(result){
						if (result.type == "success") {
							showSuccess("删除成功", function(){
								location.reload(true);
							});
						} else {
							showError(result.msg || "删除失败");
						}
					}
				});
			});
		});
	});
});
</script>

<c:import url="/sys/common/sys_footer.jsp" ></c:import>