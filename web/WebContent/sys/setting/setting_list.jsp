<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
	<ol class="breadcrumb">
		<li><i class="fa fa-arrow-circle-o-right"></i>营销管理</li>
		<li>网站背景设置</li>
	</ol>
</div>
<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<form method="post" class="form-horizontal" action="<c:url value="edit.do"/>" id="materialEditForm">

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>平台开户行：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" name="payBank" autocomplete="off" value="${payBank.value}">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>平台开户行分行地址：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" name="paySubBank" autocomplete="off" value="${paySubBank.value}">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>平台收款账号：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" name="payBankNo" autocomplete="off" value="${payBankNo.value}">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>平台银行卡持卡人：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" name="payBankName" autocomplete="off" value="${payBankName.value}">
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>注意事项：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<textarea class="form-control" name="payAttention" autocomplete="off" value="${payAttention.value}"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>微信二维码：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input name="wxewm" id="wxewm" value="${wxewm.value }" type="hidden">
										<div class="col-sm-10">
											<div style="line-height: 39px;" class="clearfix">
												<a class="btn btn-primary" id="uploadify">上传图片</a>
                                            <span class="text-muted m_l_10"
												  style="line-height: 39px;"> 建议图片尺寸不超过460*460 </span>
											</div>
											<div class="img_box2 avatar_div" id="avatar_divPic"
												 style="${wxewm.value == null ||wxewm.value == "" ? "display: none;" : "" }">
												<img alt="image" id="picUrl" height="110" width="110"
													 class="img-circle"
													 src="${wxewm.value == null ||wxewm.value == "" ? "/images/sys/blank.gif" : wxewm.value }">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<button class="btn btn-primary" type="submit">提 交</button>
								<button class="btn btn-white m_l_10" type="button" onclick="location.href='<c:url value='toList.do' />';">取 消</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<ex:javascript src="/script/common/check.js"></ex:javascript>
<ex:javascript src="/script/common/uploadify.js"></ex:javascript>
	<script>
		$(document).ready(function() {
			$('.input-date').exDatepicker();

			$("#uploadify").imageUploadify({
				uploader: "<c:url value="/upload/sysImageUpload.do"/>",
				float:"left",
				afterSuccess : function(result){
					var imageUrl = result.data.imageUrl;
					$("#wxewm").val(imageUrl);
					$("#picUrl").attr("src",imageUrl);
					$(".avatar_div").show();
				}
			});
		});
		require(["validate"] ,function(){
			$("#materialEditForm").exValidate({
				rules: {
				},
				submitHandler: function(form) {
					$(form).exAjaxSubmit({
						dataType : "json",
						success:function(result){
							if(result.type == "success") {
								showSuccess("保存成功",function(){
									location.href = "<c:url value="toList.do" />"
								});
							} else {
                                showError(result.msg,function(){});
							}
						}
					});
					return false;
				}
			});

			$.validator.addMethod("requireInput", function(value, element) {
				$(element).val($.trim(value));
				return $.trim(value).length > 0;
			});

		});

		function changeType(){
			var type = $("select[name='type']").val();
			if(type == 0) {
				$(".enterprise").hide();
			} else {
				$(".enterprise").show();
			}
		}
	</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>