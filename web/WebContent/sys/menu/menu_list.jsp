<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <ol class="breadcrumb">
      <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
      <li> <strong>菜单管理</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content clearfix">
        <c:if test="${sysRoleRights.checkButtonRights(2) }">
          <div class=""><a href="<c:url value="toEdit.do?id=0"/>" class="btn btn-primary">新增菜单</a></div>
        </c:if>
          <table class="table table-bordered table-hover m_t_6 table-center">
            <thead>
              <tr>
                <th>菜单级别</th>
                <th>菜单名称</th>
                <th>菜单排序</th>
                <th>菜单链接</th>
                <th width="106" class="left">操作</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var="menuItem" items="${sysMenuList}">
              	<tr>
                  <td>
                  	<c:if test="${menuItem.level > 0}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="/images/sys/grate_a.gif"></c:if>${menuItem.level + 1}级
                  </td>
                  <td>${menuItem.name}</td>
                  <td>${menuItem.sort}</td>
                  <td>${menuItem.uri}</td>
                  <td class="opera_icon left">
                  	<c:if test="${sysRoleRights.checkButtonRights(2) }">
                  	<a href="<c:url value="toEdit.do"/>?id=${menuItem.id}"><i class="fa fa-edit" title="修改"></i></a>
                  	<a href="javascript:;" class="set_order" data-id="${menuItem.id}"><i class="fa fa-sort-alpha-desc" title="排序"></i></a>
                  	</c:if>
                  	<c:if test="${sysRoleRights.checkButtonRights(3) }">
                  	<c:if test="${menuItem.getChildMenus().size() == 0}">
                  	  <a class="delete delete_btn" href="javascript:;" data-id="${menuItem.id}"><i class="fa fa-trash-o" title="删除"></i></a>
                  	</c:if>
                  	</c:if>
                  </td>
              	</tr>
              </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
	$(".delete_btn").each(function(){
		var _this = $(this);
		_this.bind("click", function(){
			showConfirm("确定删除此菜单吗", function(){
				ajaxRequest({
					url: "<c:url value='delete.do'/>",
					data: {id: _this.attr("data-id")},
					success: function(result){
						if (result.type == "success") {
							showSuccess("删除成功", function(){
								location.reload(true);
							});
						} else {
							showError(result.msg || "删除失败");
						}
					}
				});
			});
		});
	});
	
	//设置排序值
	function setOrder(id){
		showInput("请输入排序值：", function(value){
			var reg = /^[0-9][0-9]*$/;
			if (reg.test(value)) {
				var sortValue = parseInt(value);
				
				ajaxRequest({
					url: "<c:url value='setSort.do'/>",
					data: {id: id, sort: sortValue},
					success: function(result){
						if (result.type == "success") {
							showSuccess("修改成功", function(){
								location.reload(true);
							});
						} else {
							showError(result.msg || "修改失败");
						}
					}
				});
				
			} else {
				swal.showInputError("排序值输入有误");
				return false;
			}
		});
	}
	
	$(".set_order").each(function(){
		var _this = $(this);
		_this.bind("click", function(){
			setOrder(_this.attr("data-id"));
		});
	});
});
</script>

<c:import url="/sys/common/sys_footer.jsp" ></c:import>