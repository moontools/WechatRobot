<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>

<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <ol class="breadcrumb">
      <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
      <li> 菜单管理</li>
      <li> <strong>${param.id==0 ? "新增" : "修改"}菜单</strong> </li>
    </ol>
  </div>
  <div class="col-lg-2"> </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
          <div class="s_title">
            <h5>菜单信息</h5>
          </div>
          <form method="post" class="form-horizontal" action="save.do" id="menu_form">
            <div class="form-group">
              <label class="col-sm-2 control-label">上级菜单：</label>
              <div class="col-sm-10">
                <div class="row">
	              <div class="col-md-4">
	                <select class="form-control" name="parentMenuId" id="parent_menu_select" <c:if test="${param.id > 0}">disabled</c:if>>
	                  <option value="0">无</option>
	                  <c:forEach var="sysMenuItem" items="${sysMenuList}">
	                  	<c:if test="${sysMenuItem.getLevel() == 0}">
	                  	  <option value="${sysMenuItem.getId()}" <c:if test="${sysMenuItem.id == sysMenu.parentMenu.id}">selected</c:if>>
	                  	  	${sysMenuItem.getName()}
	                  	  </option>
	                  	</c:if>
	                  </c:forEach>
	                </select>
	              </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>菜单排序：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  	<input class="form-control" type="text" name="sort" value="${sysMenu.getSort()}">
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>菜单名称：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  	<input class="form-control" type="text" name="name" value="${sysMenu.getName()}" maxlength="10">
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span class="text-danger" id="uri_tip" <c:if test="${param.id == 0}">style="display:none;"</c:if>>*&nbsp;</span>菜单链接：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  	<input class="form-control" type="text" name="uri" value="${sysMenu.getUri()}">
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div id="module_div" style="${sysMenu.parentMenu == null ? 'display:none;' : ''}">
          	<div class="s_title">
              <h5>功能模块</h5>
          	</div>
            
            <div class="form-group">
              <label class="col-sm-2 control-label"> 模块名称：</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                    <input class="form-control" type="text" id="module_name" placeholder="请填写模块名称" maxlength="10">
                  </div>
                  <button class="btn btn-primary" type="button" id="add_module_btn">添加模块</button>
                </div>
              </div>
            </div>
            
            <div class="hr-line-dashed"></div>
            <div class="form-group">
              <label class="col-sm-2 control-label">&nbsp;</label>
              <div class="col-sm-10">
                <div class="row">
                  <div class="col-md-4">
                  <table class="table table-bordered table-center">
	                <thead>
	                  <tr>
	                    <th>功能序列</th>
	                    <th>模块名称</th>
	                    <th>操作</th>
	                  </tr>
	                </thead>
	                <tbody id="module_list">
	                  <c:forEach var="moduleItem" items="${sysMenuButtons}">
	                  	<tr>
	                      <td class="module_id"></td>
	                      <td class="module_name">${moduleItem}</td>
	                      <td class="opera_icon">
	                        <a href="javascript:;" class="delete delete_modult_btn"><i class="fa fa-trash-o"></i></a>
	                      </td>
	                      <input type="hidden" name="moduleName" value="${moduleItem}">
	                  	</tr>
	                  </c:forEach>
	                </tbody>
	              </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="hr-line-dashed"></div>
            </div>
            <div class="form-group">
              <div class="col-sm-4 col-sm-offset-2">
                <button class="btn btn-primary" type="submit">提 交</button>
                <button class="btn btn-white m_l_10" type="button" onclick="location.href='<c:url value="toList.do"/>';">取 消</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
	//选择父菜单下拉框事件
	$("#parent_menu_select").bind("change", function(){
		var selectId = $(this).val();
		if (selectId == 0) {
			$("#uri_tip").hide();
			$("#module_div").hide();
		} else {
			$("#uri_tip").show();
			$("#module_div").show();
		}
	});
	
	//添加模块按钮事件
	$("#add_module_btn").bind("click", function(){
		if ($("#module_list tr").length >= 20) {
			showError("模块数量不能超过20个");
			return;
		}
		
		var moduleName = $.trim($("#module_name").val());
		if (moduleName == "") {
			showError("请填写模块名称");
			$("#module_name").focus();
			return;
		}
		
		var existName = false;
		$("#module_list .module_name").each(function(){
			if ($(this).html() == moduleName) {
				existName = true;
			}
		});
		if (existName) {
			showError("模块名称重复");
			return;
		}
		
		appendNewModule(moduleName);
		$("#module_name").val("");
	});
	
	//模块列表删除事件
	$("#module_list").on("click", ".delete_modult_btn", function(){
		var _this = $(this);
		_this.closest("tr").remove();
		formatOrderNo();
	});
	
	//增加新的模块功能
	function appendNewModule(moduleName){
		var newDom = $("<tr></tr>");
		newDom.append("<td class='module_id'></td>");
		newDom.append("<td class='module_name'>" + moduleName + "</td>");
		newDom.append("<td class='opera_icon'><a href='javascript:;' class='delete delete_modult_btn'><i class='fa fa-trash-o'></i></a></td>");
		newDom.append("<input type='hidden' name='moduleName' value='" + moduleName + "'>");
		$("#module_list").append(newDom);
		formatOrderNo();
	}
	
	//模块列表排序
	function formatOrderNo(){
		$("#module_list tr").each(function(i){
			var _this = $(this);
			_this.find(".module_id").html(i + 1);
		});
	}
	formatOrderNo();
	
	//表单验证及提交
	require(["validate"], function(){
		$.validator.addMethod("trimRequired", function(value, element) {
			$(element).val($.trim(value));
			return $.trim(value).length > 0;
		});
		$.validator.addMethod("requiredDepends", function(value){
			return $("#parent_menu_select").val() == 0 || value != "";
		});
	});
	
	$("#menu_form").exValidate({
		rules: {
			sort: {
				required: true,
				digits: true,
				min: 0
			},
			name: "trimRequired",
			uri: "requiredDepends"
		},
		messages: {
			sort: {
				required: "请输入排序值",
				digits: "请输入整数",
				min: "排序值需大于等于0"
			},
			name: {
				trimRequired: "请输入菜单名称"
			},
			uri: {
				requiredDepends: "请输入链接"
			}
		},
		submitHandler: function(form) {
			$(form).exAjaxSubmit({
				type: "post",
				dataType: "json",
				success: function(result){
					if (result.type == "success"){
						showSuccess(result.msg, function(){
							location.href = "<c:url value='toList.do'/>";
						});
					} else {
						showError(result.msg);
					}
				},
				error: function(){
					showError("提交失败");
				}
			});
			return false;
		}
	});
});
</script>

<c:import url="/sys/common/sys_footer.jsp" ></c:import>