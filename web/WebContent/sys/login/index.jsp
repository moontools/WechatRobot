<%@page import="com.lincoln.framework.utils.ConfigSetting"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<title>金典实分销管理系统</title>
<link href="/style/sys/bootstrap.css" rel="stylesheet">
<link href="/style/sys/font-awesome/f-style/font-awesome.css" rel="stylesheet">
<link href="/style/sys/animate.css" rel="stylesheet">
<link href="/style/sys/style.css" rel="stylesheet">
</head>
<body class="gray-bg">
	<div>
		<div class="head clearfix" style="padding-left: 123px">
			<div class="logo1" class="span2">
				<img src="/images/sys/logo.jpg" alt="" width="45" height="45"> 
				<span class="span_p">金典实分销管理系统</span>
			</div>
		</div>

		<div class="loginColumns animated fadeInDown">
			<div class="row min_w_1200">
				<div class="login_l">
					<div class="login_imgbox">
						<img src="/images/sys/login_l.png">
					</div>
				</div>
				<div class="login_r">
					<div class="ibox-content login_box">
						<div class="login_title">
							登录<em>sign in</em>
						</div>
						<form id="loginForm" class="m-t" method="post" enctype="application/x-www-form-urlencoded" action="<c:url value="/sys/login.do"></c:url>">
							<div class="form-group">
								<input type="text" id="name" name="name"  class="form-control" placeholder="用户名">
							</div>
							<div class="form-group">
								<input type="password" id="password" name="password" class="form-control" placeholder="密码">
							</div>
							<div class="form-group safe_code_div" style="<c:if test="${errorTimes < 3}">display: none;</c:if>">
								<input type="text" maxlength="4" id="safecode" name="safecode" class="form-control auth_code" placeholder="验证码"> 
								<a class="m_l_10 codeimg" onclick="return false;" href="javascript:;">
									<img onclick="this.src='<c:url value="/sys/getSafeCode.do"></c:url>?t='+Math.random();" src="<c:url value="/sys/getSafeCode.do"></c:url>" height="34" width="100">
								</a>
							</div>
							<button type="submit" class="btn btn-primary block full-width m-b">登&nbsp;录</button>
							<p class="text-danger" style="display: none;"></p>
						</form>
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="text-center">${icp }</div>
			</div>
		</div>
	</div>
<script type="text/javascript" src="/script/plugins/jquery-1.12.1.js"></script>
<script type="text/javascript" src="/script/plugins/jquery.form.js"></script>
<script type="text/javascript" src="/script/common/cookies.js"></script>
<script type="text/javascript" src="/script/common/utils.js"></script>
<script type="text/javascript" src="/script/sys/utils.sys.js"></script>
	<script type="text/javascript">
		$("#loginForm").ajaxForm({
			dataType : "json",
			beforeSubmit:function(){
				var name = $("#name").val();
				var password = $("#password").val();
				var safeCode = $("#safecode").val();
				var errorTimes = $(document).getCookie("cookies_error_times");
				if($.trim(name) == "") {
					$(".text-danger").html('<i class="fa fa-times-circle"></i> 请输入用户名').show();
					$("#name").focus();
					return false;
				}
				if(password.length == 0) {
					$(".text-danger").html('<i class="fa fa-times-circle"></i> 请输入密码').show();
					$("#password").focus();
					return false;
				}
				if(errorTimes >= 3 && $.trim(safeCode) == "") {
					$(".text-danger").html('<i class="fa fa-times-circle"></i> 请输入验证码').show();
					$("#safecode").focus();
					return false;
				}
				$(".text-danger").hide(100);
				return true;
			},
			success:function(result){
				if(result.type == "success") {
					$(document).delCookie("cookies_error_times");
					location.href = "<c:url value="/sys/order/see.do"></c:url>";
				} else {
					$(".text-danger").html('<i class="fa fa-times-circle"></i> ' + result.msg).show(100);
					$(document).setCookie("cookies_error_times",result.data.errorTimes || 0);
					if(result.data.errorTimes >= 3) {
						$(".safe_code_div").show();
					}
					if(result.data.focusId || "") {
						$("#" + result.data.focusId).focus();
					}
					
					$(".codeimg").find("img").click();
				}
			}
		});
		
		$(document).ready(function(){
			$("#name").focus();
		});
	</script>
</body>
</html>
