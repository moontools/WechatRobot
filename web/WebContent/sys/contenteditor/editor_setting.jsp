<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<ol class="breadcrumb">
			<li><i class="fa fa-arrow-circle-o-right"></i>平台设置</li>
			<li><strong>内容编辑</strong></li>
		</ol>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
				<form method="post" class="form-horizontal" action="<c:url value="edit.do"/>" id="editForm">
					<div class="form-group">
                  			<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>选择内容：</label>
                 			<div class="col-sm-10">
                  			<div class="row"><div class="col-md-4">
                    			<select class="form-control m-b " id="type"  name="type">
                 				<c:forEach var="item" items="${contentEditorTypeMap}">
                  					<option value="${item.key}" >${item.value}</option>
                				</c:forEach>
                  				</select>
                    			</div></div>
                  		   </div>
                	</div>
                	<div class="hr-line-dashed"></div>
                	
                	<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>中文说明：</label>
							<div class="col-sm-10">
								<script id="contentCh" name="contentCh" type="text/plain" style="min-height: 300px;width: 600px;">${contentEditor.contentCh}</script>
							</div>
					</div>
					<div class="hr-line-dashed"></div>
					
					<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>英文说明：</label>
							<div class="col-sm-10">
								<script id="contentEn" name="contentEn" type="text/plain" style="min-height: 300px;width: 600px;">${contentEditor.contentEn}</script>
							</div>
					</div>
					<div class="hr-line-dashed"></div>
					
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-primary" type="submit">提 交</button>
						</div>
					</div>
						
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
<ex:javascript src="/script/common/ueditor.js"></ex:javascript>
<script type="text/javascript">
var contentEditorCh;
var contentEditorEn;
$(document).ready(function(){
	$("#contentCh").simpleueditor({textarea : "contentCh",
		toolbars:[[ 'bold', 'italic', 'underline', 'forecolor', 'backcolor','fontsize',
	                '|','justifyleft', 'justifycenter', 'justifyright','lineheight', 
	                '|','insertimage','selectlib', 'link', 'unlink','inserttable' ]]},function(editor){
	        			contentEditorCh = editor;
	});
	
	$("#contentEn").simpleueditor({textarea : "contentEn",
		toolbars:[[ 'bold', 'italic', 'underline', 'forecolor', 'backcolor','fontsize',
	                '|','justifyleft', 'justifycenter', 'justifyright','lineheight', 
	                '|','insertimage','selectlib', 'link', 'unlink','inserttable' ]]},function(editor){
	                	contentEditorEn = editor;
	});
	
	$("#type").on("change", function(){
		var type = $(this).val();
		ajaxRequest({
			url: "<c:url value='getContentEditor.do'/>",
			data: {type: type},
			success: function(result){
				if (result.type == "success") {
					contentEditorCh.setContent(result.data.contentEditor.contentCh);
					contentEditorEn.setContent(result.data.contentEditor.contentEn);
				} else {
					showError(result.msg || "获取设置数据失败");
				}
			}
		});
	});
});

require(["validate"] ,function(){
	$("#editForm").exValidate({
		rules: {
		},
		messages: {
		},
		submitHandler: function(form) {
			$(form).exAjaxSubmit({
				dataType : "json",
				beforeSubmit :function(){
					var content = $.trim(contentEditorCh.getContent());
					if(content.length == 0){
						showError("中文说明不能为空");
						contentEditorCh.focus();
						return false;
					}
					if(content.length > 65535){
						showError("中文说明内容过长，请审核");
						contentEditorCh.focus();
						return false;
					}
					content = $.trim(contentEditorEn.getContent());
					if(content.length == 0){
						showError("英文说明不能为空");
						contentEditorEn.focus();
						return false;
					}
					if(content.length > 65535){
						showError("英文说明内容过长，请审核");
						contentEditorEn.focus();
						return false;
					}
					return true;
				},
				success:function(result){
					if(result.type == "success") {
						showSuccess("保存成功",function(){
						});
					} else {
						if(result.data.focusId || "") {
							showError(result.msg,function(){
								$("#" + result.data.focusId).focus();
							});
						} else {
							showError(result.msg,function(){});
						}
					}
				}
			});
			return false;
		}
	});

	
});
</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>