<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/framework/tld" prefix="ex" %>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li><i class="fa fa-arrow-circle-o-right"></i>今日一览</li>
            <li><strong>浏览列表</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content clearfix">
                    <table class="table table-bordered table-hover m_t_6 table-center">
                        <thead>
                        <tr>
                            <th width="80">下分金额统计</th>
                            <th width="80">上分金额统计</th>
                            <th width="80">下注金额统计</th>
                            <th width="80">下注订单统计</th>
                            <th width="80">中奖订单统计</th>
                            <th width="80">盈亏</th>
                        </tr>
                        </thead>
                        <tbody id="adList">
                            <tr>
                                <td><c:out value="${requestScope.extract}"/></td>
                                <td><c:out value="${requestScope.recharge}"/></td>
                                <td><c:out value="${requestScope.betting}"/></td>
                                <td><c:out value="${requestScope.order}"/></td>
                                <td><c:out value="${requestScope.brojbk}"/></td>
                                <td><c:out value="${requestScope.dei}"/></td>
                            </tr>

                        </tbody>
                    </table>
                    <c:if test="${model != null && model.size() > 0}">
                        <ex:page pageObj="${model.extract }" target="adForm"></ex:page>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function del(id){
        showConfirm("是否删除", function () {
            ajaxRequest({
                url: "/sys/member/delete.do",
                data: {'id': id},
                success: function (result) {
                    if (result.type == "success") {
                        showSuccess("删除成功", function () {
                            location.href = "<c:url value='backList.do' />";
                        });
                    } else {
                        showError(result.msg);
                    }
                }
            });
        });
    }
    $(document).ready(function () {
        $('#adList').exICheck();
        $('.input-daterange').exDatepicker();
    });
</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>



