<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/framework/tld" prefix="ex" %>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li><i class="fa fa-arrow-circle-o-right"></i>商品管理</li>
            <li><strong>商品列表</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table>
                        <tr>
                            <td>
                                <form role="form" class="form-inline" action="<c:url value="toList.do" />" method="post"
                                      id="Form" style="display: inline">
                                    <div class="form-group m_l_10">
                                        <input type="text" name="name" value="${name }" placeholder="用户名"
                                               class="form-control">
                                    </div>
                                    <div class="form-group m_l_10">
                                        <input type="text" name="score" value="${score }" placeholder="余额"
                                               class="form-control">
                                    </div>
                                        <a onclick="$('#Form').submit();" href="javascript:void(0);"
                                           class="btn btn-primary m_l_10">查询</a>
                                        <a href="toEdit.do"
                                           class="btn btn-primary m_l_10">新增</a>
                                    <button type="submit" hidden="true"></button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content clearfix">
                    <table class="table table-bordered table-hover m_t_6 table-center">
                        <thead>
                        <tr>
                            <th width="100">id</th>
                            <th width="80">商品名</th>
                            <th width="130">商品信息</th>
                            <th width="80">商品价格</th>
                            <th width="120">操作</th>
                        </tr>
                        </thead>
                        <tbody id="adList">
                        <c:if test="${page.getResult() == null || page.getResult().size() == 0}">
                            <tr>
                                <td colspan="100" style="text-align: center;">暂无数据</td>
                            </tr>
                        </c:if>
                        <c:forEach items="${page.getResult() }" var="entity">
                            <tr>
                                <td><c:out value="${entity.id }"></c:out></td>
                                <td><c:out value="${entity.name}"></c:out></td>
                                <td><c:out value="${entity.info}"/></td>
                                <td><c:out value="${entity.price}"/></td>
                                <td class="opera_icon">
                                        <a title="编辑" href="<c:url value="toEdit.do"><c:param name="id" value="${entity.id }"></c:param></c:url>"><i class="fa fa-edit"></i></a>
                                        <a title="删除" text="del" href="javascript:void(0)" onclick="del(${entity.id});"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${page.getResult() != null && page.getResult().size() > 0}">
                        <ex:page pageObj="${page }" target="adForm"></ex:page>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function del(id){
        showConfirm("是否删除", function () {
            ajaxRequest({
                url: "/sys/commodity/delete.do",
                data: {'id': id},
                success: function (result) {
                    if (result.type == "success") {
                        showSuccess("删除成功", function () {
                            location.href = "<c:url value='backList.do' />";
                        });
                    } else {
                        showError(result.msg);
                    }
                }
            });
        });
    }
    $(document).ready(function () {
        $('#adList').exICheck();
        $('.input-daterange').exDatepicker();
    });
</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>