<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<div class="row wrapper border-bottom white-bg page-heading">
<div class="col-lg-10">
	<ol class="breadcrumb">
		<li><i class="fa fa-arrow-circle-o-right"></i>下注管理</li>
		<li class="active"><strong><c:if test="${entity.id <= 0}">新增</c:if><c:if test="${entity.id > 0}">修改</c:if>用户</strong></li>
	</ol>
</div>
<div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<form method="post" class="form-horizontal" action="<c:url value="edit.do"/>" id="editForm">
						<input type="hidden" name="id" value="${entity.id }">
						<div class="hr-line-dashed"></div>

						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>用户名：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" maxlength="100" name="name" id="name" autocomplete="off" value="${entity.name}">
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>积分：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" maxlength="100" name="score" id="score" autocomplete="off" value="${entity.score}">
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>手机号：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" maxlength="100" name="phone" id="phone" autocomplete="off" value="${entity.phone}">
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>openId：</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-md-4">
										<input type="text" class="form-control" maxlength="100" name="openId" id="openId" autocomplete="off" value="${entity.openId}">
									</div>
								</div>
							</div>
						</div>
						<div class="hr-line-dashed"></div>

						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<button class="btn btn-primary" type="submit">提 交</button>
								<button class="btn btn-white m_l_10" type="button" onclick="location.href='<c:url value='backList.do' />';">取 消</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<ex:javascript src="/script/common/check.js"></ex:javascript>
<ex:javascript src="/script/common/uploadify.js"></ex:javascript>
	<script>
		$(document).ready(function() {
			$('.input-date').exDatepicker();

		});
		require(["validate"] ,function(){
			$("#editForm").exValidate({
				rules: {
					name: {
						requireInput: true,
						maxlength:18
					}
				},
				submitHandler: function(form) {
					$(form).exAjaxSubmit({
						dataType : "json",
						success:function(result){
							if(result.type == "success") {
								showSuccess("保存成功",function(){
									location.href = "<c:url value="backList.do" />"
								});
							} else {
                                showError(result.msg,function(){});
							}
						}
					});
					return false;
				}
			});
			$.validator.addMethod("requireInput", function(value, element) {
				$(element).val($.trim(value));
				return $.trim(value).length > 0;
			});
		});
	</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>