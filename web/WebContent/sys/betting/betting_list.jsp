<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/framework/tld" prefix="ex" %>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li><i class="fa fa-arrow-circle-o-right"></i>下注管理</li>
            <li><strong>下注列表</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table>
                        <tr>
                            <td>
                                <form role="form" class="form-inline" action="<c:url value="toList.do" />" method="post"
                                      id="form" style="display: inline">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content clearfix">
                    <table class="table table-bordered table-hover m_t_6 table-center">
                        <thead>
                        <tr>
                            <th width="100">id</th>
                            <th width="80">会员名</th>
                            <th width="130">下注金额</th>
                            <th width="80">下注内容</th>
                            <th width="80">下注结果</th>
                            <th width="80">下注时间</th>
                        </tr>
                        </thead>
                        <tbody id="list">
                        <c:if test="${page.getResult() == null || page.getResult().size() == 0}">
                            <tr>
                                <td colspan="100" style="text-align: center;">暂无数据</td>
                            </tr>
                        </c:if>
                        <c:forEach items="${page.getResult() }" var="entity">
                            <tr>
                                <td><c:out value="${entity.id }"></c:out></td>
                                <td><c:out value="${entity.member.name}"></c:out></td>
                                <td><c:out value="${entity.score}"/></td>
                                <td><c:out value="${entity.content}"/></td>
                                <td>
                                    <c:if test="${entity.result == 0}">
                                        未中奖
                                    </c:if>
                                    <c:if test="${entity.result == 1}">
                                        已中奖
                                    </c:if>
                                </td>
                                <td><c:out value="${entity.createTime}"/></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${page.getResult() != null && page.getResult().size() > 0}">
                        <ex:page pageObj="${page }" target="form"></ex:page>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#adList').exICheck();
        $('.input-daterange').exDatepicker();
    });
</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>



