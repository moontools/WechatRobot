<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="ex" uri="/framework/tld"%>
<c:import url="/sys/common/sys_header.jsp"></c:import>
    <div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <ol class="breadcrumb">
          <li> <i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
          <li class="active"> <strong>修改密码</strong> </li>
        </ol>
      </div>
      <div class="col-lg-2"> </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-content">
              <form method="get" class="form-horizontal" id="searchForm" action="edit.do">
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>原密码：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="password" class="form-control" name="oldPassword" id="oldPassword"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>新密码：</label>
                  <div class="col-sm-10">
                   <div class="row"><div class="col-md-4"><input type="password" class="form-control" name="newPassword" id="newPassword"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"><span class="text-danger">*&nbsp;</span>确认密码：</label>
                  <div class="col-sm-10">
                    <div class="row"><div class="col-md-4"><input type="password" class="form-control" name="checkPassword" id="checkPassword"></div></div>
                  </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                  <div class="col-sm-4 col-sm-offset-2">
                    <button class="btn btn-primary" type="submit">保 存</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
    require(["validate"] ,function(){
		$("#searchForm").exValidate({
			rules: {
				oldPassword: {
					required: true,
					ValidatePassword: true,
					maxlength:18
				},
				newPassword: {
					required: true,
					ValidatePassword: true,
					maxlength:18
				},
				checkPassword: {
					required: true,
					maxlength:18,
					equalTo:"#newPassword"
				}
			},
			messages: {
				oldPassword: {
					required: "请输入原密码",
					ValidatePassword: "密码不能包含空格",
					maxlength:"输入原密码长度不能大于18个字符"
				},
				newPassword: {
					required: "请输入新密码",
					ValidatePassword: "密码不能包含空格",
					maxlength:"输入新密码长度不能大于18个字符"
				},
				checkPassword: {
					required: "请输入确认密码",
					maxlength:"输入确认密码长度不能大于18个字符",
					equalTo:"新密码和确认密码必须一致"
				}
			},
			submitHandler: function(form) {
				$(form).exAjaxSubmit({
					dataType : "json",
					success:function(result){
						if(result.type == "success") {
							showSuccess("保存成功",function(){
								location.href = "<c:url value="/sys/password/toEdit.do" />"
							});
						} else {
							if(result.data.focusId || "") {
								showError(result.msg,function(){
									$("#" + result.data.focusId).focus();
								});
							} else {
								showError(result.msg,function(){});
							}
						}
					}
				});
				return false;
			}
		});
		
		jQuery.validator.addMethod("ValidatePassword", function(value, element) {
			return !(/\s/g.test(value));
		});
		
	});
 </script>
<c:import url="/sys/common/sys_footer.jsp" ></c:import>