<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="ex" uri="/framework/tld"%>
<c:import url="/sys/common/sys_header.jsp">
</c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-10">
        <ol class="breadcrumb">
          <li><i class="fa fa-arrow-circle-o-right"></i> 系统管理 </li>
          <li> <strong>操作日志</strong> </li>
        </ol>
      </div>
      <div class="col-lg-2"> </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
      
        <div class="col-lg-12">
          <div class="ibox float-e-margins">
            <div class="ibox-content clearfix">
              <div class="clearfix"><div class=" f_l">
              <c:if test="${sysRoleRights.checkButtonRights(3) }">
               <a href="javascript:;"   class="btn btn-primary mutiDelete">批量删除</a>
               <a href="javascript:;"   class="btn btn-primary m_l_10 month_delete">删除3个月前数据</a> 
              </c:if>
               <c:if test="${sysRoleRights.checkButtonRights(2) }">
               <a href="
                <c:url value="export.do">
               	<c:param name="searchVal" value="${searchVal}"></c:param>
               	<c:param name="searchKey" value="${searchKey}"></c:param>
               	<c:param name="order" value="${order}"></c:param>
               	<c:param name="orderBy" value="${orderBy}"></c:param>
               </c:url>" class="btn btn-primary m_l_10">导出数据
              </a>
              </c:if>
              </div><div class="f_r">
              <form role="form" class="form-inline" id="searchForm" action="toList.do" method="post">
              <c:if test="${sysRoleRights.checkButtonRights(1) }">
              <button type="submit" class="btn btn-primary m_l_10 f_r">搜索</button> 
              </c:if>
              <div class="f_r m_l_10"><input class="form-control" value="<c:out  value="${searchVal}"/>" type="text" id="searchVal" name="searchVal"></div><label class="f_r">
              <select class="form-control input-sm" aria-controls="DataTables_Table_0" name="searchKey" id="searchKey">
              <option value="">请选择</option>
              <option value="sysAdmin.name" <c:if test="${searchKey == 'sysAdmin.name'}">selected="selected"</c:if>>管理员</option>
              <option value="content" <c:if test="${searchKey == 'content'}">selected="selected"</c:if>>操作动作</option>
              <option value="ipAddress" <c:if test="${searchKey == 'ipAddress'}">selected="selected"</c:if>>IP</option>
              </select>
              </label>
              </form>
              </div></div>
              <table class="table table-bordered table-hover m_t_6 table-center">
                <thead>
                  <tr>
                    <th width="35"></th>
                    <th><ex:sortcolumn columnName="管理员" sortAttr="sysAdmin.name" targetForm="searchForm" pageObj="${resultPage}"></ex:sortcolumn></th>
                    <th>操作动作</th>
                    <th width="180"><ex:sortcolumn columnName="操作时间" sortAttr="operateTime" targetForm="searchForm" pageObj="${resultPage}"></ex:sortcolumn></th>
                    <th width="180">IP</th>
                    <th width="60">操作</th>
                  </tr>
                </thead>
                <tbody id="logList">
                	<c:if test="${resultPage.getResult() == null || resultPage.getResult().size() == 0}">
                	<tr>
						<td colspan="6" style="text-align: center;">暂无数据</td>
					</tr>
					</c:if>
                  <c:forEach var="sysAdminLog"  items="${resultPage.getResult()}" >
                  <tr>
                  <td><input type="checkbox" name="ids" value="<c:out value="${sysAdminLog.id}"/>" class="i-checks" /></td>
                    <td>${sysAdminLog.sysAdmin.name}</td>
                    <td>${sysAdminLog.content}</td>
                    <td><fmt:formatDate value="${sysAdminLog.operateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                    <td>${sysAdminLog.ipAddress}</td>
                    <td class="opera_icon">
                    <c:if test="${sysRoleRights.checkButtonRights(3) }">
                    <a title="删除" class="delete"  href="javascript:;" data-id="<c:out value="${sysAdminLog.id}"/>"><i class="fa fa-trash-o"></i></a>
                    </c:if>
                    </td>
                  </tr>
                  </c:forEach>
                </tbody>
              </table>
              <c:if test="${resultPage.getResult() != null && resultPage.getResult().size() > 0}">
              <ex:page target="searchForm"  pageObj="${resultPage}" />
              </c:if>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script  type="text/javascript">
    var submiting = false;
    $(document).ready(function(){
    	
    	$('#logList').exICheck();
    	
    	$('.delete').click(function() {
    		if(submiting){
				return true;
			}
			var id = $(this).attr("data-id");
			
			showConfirm("确定删除吗", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='delete.do' />?id=" + id,
					success:function(result){
						if(result.type == "success") {
							showSuccess("删除成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
    	
    	$('.mutiDelete').click(function() {
			
    		if(submiting){
				return true;
			}
    		var ids = [];
    		$("input[name=ids]").each(function () {
    			if($(this).is(':checked')){
    				ids.push($(this).val());
    			}
    		});
    		if(ids.length == 0){
    			showError("请选择记录");
    			submiting = false;
    			return false;
    		}
			showConfirm("确定删除吗", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='mutiDelete.do' />?ids=" + ids.toString(),
					success:function(result){
						if(result.type == "success") {
							showSuccess("删除成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
    	
    	$('.month_delete').click(function() {	
    		if(submiting){
				return true;
			}
			showConfirm("确定删除三个月前数据吗", function(){
				submiting = true;
				ajaxRequest({
					url : "<c:url value='preMonthDelete.do' />",
					success:function(result){
						if(result.type == "success") {
							showSuccess("删除成功",function(){
								location.href = "<c:url value="backList.do" />";
							});
						} else {
							showError(result.msg);
							submiting = false;
						}
					}
				});
			});
		});
    
    });

    </script>
<c:import url="/sys/common/sys_footer.jsp" ></c:import>