<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/framework/tld" prefix="ex" %>
<c:import url="/sys/common/sys_header.jsp"></c:import>
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAmdin"></c:set>
<c:set value="${sysLoginAmdin.sysRoleRights }" var="sysRoleRights"></c:set>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li><i class="fa fa-arrow-circle-o-right"></i>充值管理</li>
            <li><strong>充值列表</strong></li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <table>
                        <tr>
                            <td>
                                <form role="form" class="form-inline" action="<c:url value="toList.do" />" method="post"
                                      id="Form" style="display: inline">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content clearfix">
                    <table class="table table-bordered table-hover m_t_6 table-center">
                        <thead>
                        <tr>
                            <th width="100">id</th>
                            <th width="80">用户名</th>
                            <th width="90">充值金额</th>
                            <th width="120">充值来源</th>
                            <th width="80">订单状态</th>
                            <th width="120">操作</th>
                        </tr>
                        </thead>
                        <tbody id="adList">
                        <c:if test="${page.getResult() == null || page.getResult().size() == 0}">
                            <tr>
                                <td colspan="100" style="text-align: center;">暂无数据</td>
                            </tr>
                        </c:if>
                        <c:forEach items="${page.getResult() }" var="entity">
                            <tr>
                                <td><c:out value="${entity.id }"></c:out></td>
                                <td><c:out value="${entity.member.name}"/></td>
                                <td><c:out value="${entity.score}"/></td>
                                <td><c:out value="${entity.pay}"/></td>
                                <!--<td><c:out value="${entity.status}"/></td>-->
                                <td>
                                    <c:if test="${entity.status == 0}">
                                        <b style="color:#ff9900">未处理</b>
                                    </c:if>
                                    <c:if test="${entity.status == 1}">
                                        <b style="color:#66cc33">已处理</b>
                                    </c:if>
                                    <c:if test="${entity.status == 2}">
                                        <b style="color:red">已拒绝</b>
                                    </c:if>
                                </td>
                                <td class="opera_icon">
                                    <c:if test="${entity.status == 0}">
                                        <a onclick="allow(${entity.id})">确认</a>&nbsp;&nbsp;&nbsp;
                                        <a style="color:red" onclick="refuse(${entity.id},${entity.score})">拒绝</a>
                                    </c:if>
                                    <c:if test="${entity.status == 1 || entity.status == 2}">
                                        <a title="删除" text="del" href="javascript:void(0)" onclick="del(${entity.id});"><i class="fa fa-trash"></i></a>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <c:if test="${page.getResult() != null && page.getResult().size() > 0}">
                        <ex:page pageObj="${page }" target="Form"></ex:page>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function allow(id,score){
        showConfirm("是否确认此订单", function () {
            ajaxRequest({
                url: "/sys/recharge/allow.do",
                data: {'id': id},
                success: function (result) {
                    if (result.type == "success") {
                        showSuccess("确认成功", function () {
                            location.href = "<c:url value='backList.do' />";
                        });
                    } else {
                        showError(result.msg);
                    }
                }
            });
        });
    }

    function refuse(id){
        showConfirm("是否拒绝此订单", function () {
            ajaxRequest({
                url: "/sys/recharge/refuse.do",
                data: {'id': id},
                success: function (result) {
                    if (result.type == "success") {
                        showSuccess("确认成功", function () {
                            location.href = "<c:url value='backList.do' />";
                        });
                    } else {
                        showError(result.msg);
                    }
                }
            });
        });
    }

    function del(id){
        showConfirm("是否删除", function () {
            ajaxRequest({
                url: "/sys/recharge/delete.do",
                data: {'id': id},
                success: function (result) {
                    if (result.type == "success") {
                        showSuccess("删除成功", function () {
                            location.href = "<c:url value='backList.do' />";
                        });
                    } else {
                        showError(result.msg);
                    }
                }
            });
        });
    }
    $(document).ready(function () {
        $('#adList').exICheck();
        $('.input-daterange').exDatepicker();
    });
</script>
<c:import url="/sys/common/sys_footer.jsp"></c:import>