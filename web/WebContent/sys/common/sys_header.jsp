<%@page import="com.lincoln.framework.common.WebUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/framework/tld" prefix="ex"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
<title>微信机器人后台管理系统</title>
<ex:css src="/style/sys/bootstrap.css"></ex:css>
<ex:css src="/style/sys/font-awesome/f-style/font-awesome.css" ></ex:css>
<ex:css src="/style/sys/plugins/iCheck/custom.css" ></ex:css>
<ex:css src="/style/sys/animate.css" ></ex:css>
<ex:css src="/style/sys/style.css" ></ex:css>
<!-- Sweet Alert -->
<ex:css src="/style/sys/plugins/sweetalert/sweetalert.css" ></ex:css>
<!-- Data Tables -->
<ex:css src="/style/sys/plugins/dataTables/dataTables.bootstrap.css" ></ex:css>
<ex:css src="/style/sys/plugins/dataTables/dataTables.responsive.css" ></ex:css>
<ex:css src="/style/sys/plugins/dataTables/dataTables.tableTools.min.css" ></ex:css>
<ex:css src="/style/sys/plugins/datapicker/datepicker3.css" ></ex:css>
<!-- Mainly scripts --> 
<ex:javascript src="/script/plugins/requirejs/require.js"></ex:javascript>
<ex:javascript src="/script/plugins/requirejs/require.config.js"></ex:javascript>
<ex:javascript src="/script/plugins/jquery-1.12.1.js"></ex:javascript>
<ex:javascript src="/script/common/cookies.js"></ex:javascript>
<ex:javascript src="/script/common/utils.js"></ex:javascript>
<ex:javascript src="/script/sys/utils.sys.js"></ex:javascript>
</head>
<body class="<%=WebUtils.getCookieValue("bar_class") == null ? "" : WebUtils.getCookieValue("bar_class")%>">
<c:set value="${sessionScope.session_sys_admin_login }" var="sysLoginAdmin"></c:set>
<c:set value="${sysLoginAdmin.sysRoleRights }" var="sysRoleRights"></c:set>
<c:import url="/common/dialog.jsp"></c:import>
<div id="wrapper">
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse">
		    <ul class="nav metismenu" id="side-menu">
				<li class="nav-header">
				<div class="dropdown profile-element"> 
					<a href="<c:url value="/sys/index/toIndex.do"/>"> <img alt="image" class="img-circle" src="/images/sys/logo.png" width="50" height="50"> </a>
					<a data-toggle="dropdown" class="dropdown-toggle"> 
						<span class="clear"> 
							<span class="block m-t-xs"> 
								<strong class="font-bold"><c:out value="${sysLoginAdmin.name }"></c:out> </strong> 
							</span> 
							<span class="text-muted text-xs block">${sysLoginAdmin.name == "admin" ? "超级管理员" : sysLoginAdmin.roleName} <b class="caret"></b></span> 
						</span> 
					</a>
				    <ul class="dropdown-menu animated fadeInRight m-t-xs">
				      <li><a href="<c:url value="/sys/logout.do" />">退出系统</a></li>
				    </ul>
				  </div>
				</li>
				<c:forEach items="${sysRoleRights.firstMenuList }" var="first">
					<c:if test="${sysRoleRights.checkFunctionRights(first.id) }">
						<li data-id="${first.id }" class="<c:if test="${sysRequestParentMenuId == first.id }">active</c:if>">
							<a href="javascript:;">
								<i class="fa ${first.cssClass }"></i><span class="nav-label"><c:out value="${first.name }" /></span> <span class="fa arrow"></span>
							</a>
							<ul class="nav nav-second-level collapse <c:if test="${sysRequestParentMenuId == first.id }">in</c:if>">
								<c:forEach items="${sysRoleRights.childMenuList.get(first.id) }" var="child">
									<c:if test="${sysRoleRights.checkMenuRights(child.uri)}">
										<li data-id="${child.id }" data-uri="${child.uri }" class="<c:if test="${sysRequestMenuId == child.id }">active</c:if>" ><a href="<c:url value="${child.uri }" />"><c:out value="${child.name }" /></a></li>
									</c:if>
								</c:forEach>
							</ul>
						</li>
					</c:if>
				</c:forEach>
			</ul>
		</div>
	</nav>
  	<div id="page-wrapper" class="gray-bg">
		<div class="row border-bottom">
			<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="javascript:;"><i class="fa fa-bars"></i> </a> </div>
				<ul class="nav navbar-top-links navbar-right">
					<li> <a href="<c:url value="/sys/password/toEdit.do"/>"><i class="fa fa-key"></i>修改密码</a> </li>
					<li> <a href="<c:url value="/sys/logout.do" />"><i class="fa fa-sign-out"></i>退出</a> </li>
				</ul>
			</nav>
		</div>