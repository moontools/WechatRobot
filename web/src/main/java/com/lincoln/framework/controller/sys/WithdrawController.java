package com.lincoln.framework.controller.sys;

import com.lincoln.framework.entity.Member;
import com.lincoln.framework.entity.Withdraw;
import com.lincoln.framework.exception.ClientException;
import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.service.WithdrawService;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.framework.utils.WebParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 下分Controller
 * Created by luo on 2017/4/19.
 */
@Controller
@RequestMapping("/sys/withdraw")
public class WithdrawController extends SysBaseController {
    @Autowired
    WithdrawService service;

    @Autowired
    MemberService memberService;

    /**
     * 查询数据
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/toList.do")
    public String toList(HttpServletRequest request, Model model) throws Exception {
        String sortBy = "createTime";
        this.saveRequestParameter();
        Page<Withdraw> page = this.saveQueryInfoAndReturnPage("desc", sortBy);

        double score = WebParamUtils.getDoubleParameter(request,"score",0);
        String source = request.getParameter("source");
        Integer status = WebParamUtils.getIntParameter(request,"status");

        long member = WebParamUtils.getIntParameter(request,"member",0);
        if(member>0){
            page.getQueryMap().put("memberId",member);
        }
        if(score>0){
            page.getQueryMap().put("score",score);
        }
        if(!StringUtilsEx.isNullOrEmpty(source)){
            page.getQueryMap().put("source",source);
        }
        if(status!=null){
            page.getQueryMap().put("status",status);
        }
        page = service.findPage(page);
        service.sumTodayWithdraw(null);
        model.addAttribute("page",page);
        return "sys/withdraw/withdraw_list";
    }

    /**
     * 删除
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    public void delete(HttpServletRequest request) {
        try {
            int id = WebParamUtils.getIntParameter(request, "id");

            Withdraw entity = service.getById(id);

            if (entity != null) {
                /**
                 * 判断传入的id是否是未处理的订单
                 */
                if(entity.getStatus() == Withdraw.STATUS_WAIT){
                    this.returnError("删除失败");
                }else{
                    service.delete(entity);
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            this.returnError("删除失败，请联系管理员");
        }
        this.returnSuccess();
    }

    /**
     * 确认订单
     */
    @RequestMapping("/allow.do")
    @ResponseBody
    public void allow(HttpServletRequest request) throws ClientException {

        int id = WebParamUtils.getIntParameter(request,"id",0);

        if(id<=0){
            throw new ClientException("参数错误!");
        }

        Withdraw withdraw = service.getById(id);
        if(withdraw == null){
            throw new ClientException("对象不存在!");
        }
        if(withdraw.getStatus()!=Withdraw.STATUS_WAIT){
            throw new ClientException("对象已操作!");
        }
        /**
         * 获取member的名字,openId,手机号
         */
        withdraw.setStatus(Withdraw.STATUS_YES);
        service.save(withdraw);
        this.returnSuccess();
    }

    /**
     * 拒绝订单
     */
    @RequestMapping("/refuse.do")
    @ResponseBody
    public void refuse(HttpServletRequest request) throws ClientException {
        int id = WebParamUtils.getIntParameter(request,"id",0);
        if(id<=0){
            throw new ClientException("参数错误!");
        }

        Withdraw withdraw = service.getById(id);
        if(withdraw == null){
            throw new ClientException("对象不存在!");
        }
        if(withdraw.getStatus()!=Withdraw.STATUS_WAIT){
            throw new ClientException("对象已操作!");
        }
        Member member = withdraw.getMember();
        member.setWithdraw(member.getWithdraw()-withdraw.getScore());
        memberService.save(member);
        withdraw.setStatus(Withdraw.STATUS_NO);
        service.save(withdraw);
        this.returnSuccess();
    }

}
