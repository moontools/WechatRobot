package com.lincoln.framework.controller.sys;

import com.lincoln.framework.entity.Member;
import com.lincoln.framework.exception.ClientException;
import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.framework.utils.WebParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 用户(member)Controller
 * Created by luo on 2017/4/19.
 */
@Controller
@RequestMapping("/sys/member")
public class MemberController extends SysBaseController {
    @Autowired
    MemberService service;

    /**
     * 查询数据
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/toList.do")
    public String toList(HttpServletRequest request, Model model) throws Exception {
        String sortBy = "createTime";
        this.saveRequestParameter();
        Page<Member> page = this.saveQueryInfoAndReturnPage("desc", sortBy);

        String name = request.getParameter("name");
        String score = request.getParameter("score");
        String phone = request.getParameter("phone");
        String surplusScore = request.getParameter("surplusScore");
        if(!StringUtilsEx.isNullOrEmpty(name)){
            page.getQueryMap().put("name",name);
        }
        if(!StringUtilsEx.isNullOrEmpty(score)){
            page.getQueryMap().put("score",score);
        }
        if(!StringUtilsEx.isNullOrEmpty(phone)){
            page.getQueryMap().put("phone",phone);
        }
        if(!StringUtilsEx.isNullOrEmpty(surplusScore)){
            page.getQueryMap().put("surplusScore",surplusScore);
        }
        page = service.findPage(page);
        model.addAttribute("page",page);
        return "sys/member/member_list";
    }
    /**
     * 跳转编辑页面
     *
     * @return
     * @throws ClientException
     */
    @RequestMapping("/toEdit.do")
    public String toEdit(HttpServletRequest request, Model model) throws ClientException {
        int id = WebParamUtils.getIntParameter(request, "id", 0);
        Member entity = new Member();
        if (id > 0) {
            entity = service.getById(id);
            if (entity == null) {
                throw new ClientException("操作对象不存在");
            }
        }
        model.addAttribute("entity", entity);
        return "sys/member/member_edit";
    }

    /**
     * 保存修改
     *
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/edit.do")
    public void edit(@ModelAttribute Member entity, HttpServletRequest request) throws Exception {
        int id = WebParamUtils.getIntParameter(request, "id");
        Member save = null;
        if (id > 0) {
            save = service.getById(id);
            if (save == null) {
                throw new ClientException("操作对象不存在");
            }
            save.setScore(entity.getScore());
            save.setWithdraw(entity.getWithdraw());
            save.setAllUse(entity.getAllUse());
            save.setEarn(entity.getEarn());
            save.setStatus(entity.getStatus());
        } else {
            save = entity;
            save.setCreateTime(new Date());
        }
        save.setUpdateTime(new Date());
        service.save(save);
        this.returnSuccess();
    }
    /**
     * 删除
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    public void delete(HttpServletRequest request) {
        try {
            int id = WebParamUtils.getIntParameter(request, "id");
            Member entity = service.getById(id);
            if (entity != null) {
                service.delete(entity);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            this.returnError("删除失败，请联系管理员");
        }
        this.returnSuccess();
    }
}
