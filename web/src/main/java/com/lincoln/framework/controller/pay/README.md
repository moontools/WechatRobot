#金大咖

1、https://git.oschina.net/wp_zwm/chewo https://git.oschina.net/ 注册账号后，通知我提交加入项目开发人员

2、	changelog、initbasedata、chineseregion 3个初始化内容 

	changelog 项目所有数据库变更提交该文件，防止数据库同步问题，注释格式：# yyyy-MM-dd 某人 删除、提交了***** 
	initbasedata 初始化内容，用于频繁变更初始化数据，如菜单数据 
	chineseregion 中国地区表，初始化即可

3、	com.zzy.framework.controller.sys 系统端controller
	com.zzy.framework.controller.api api controller
	com.zzy.framework.entity 持久化实体类 
	com.zzy.framework.bean 封装实体类 
	com.zzy.framework.service 忽略了dao层，直接将业务逻辑service与dao合并为service层 
	com.zzy.framework.task 定时任务 
	com.zzy.framework.task.schedule 延时任务
	
	由于系统上传都是临时文件/temp，所以需要转移至正式目录/uploads，遇到富文本编辑器内容，可使用一下方法直接替换转移
	WebUtils.updateImage
	
	其他可直接参看代码

4、编码规范，
	a、所有方法类都增加注释，有利于后续维护修改
	