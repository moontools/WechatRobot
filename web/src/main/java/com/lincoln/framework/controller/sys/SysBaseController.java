package com.lincoln.framework.controller.sys;

import com.lincoln.framework.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;

import com.lincoln.framework.common.WebUtils;
import com.lincoln.framework.service.SysAdminLogService;

/**
 * 系统端controller基础类
 * 提供了系统操作日志等基础方法
 * 
 * @author hht
 */
abstract class SysBaseController<T> extends BaseController<T> {

    
    @Autowired
    private SysAdminLogService sysAdminLogService;
    

    /**
     * 记录操作日志
     * @param content
     */
	protected void addSysAdminLog(String content) {
	    sysAdminLogService.log(content, WebUtils.getSysLoginAdmin().getId(), WebUtils.getClientIp());
	}

}
