package com.lincoln.framework.controller.sys;

import com.lincoln.framework.common.WebUtils;
import com.lincoln.framework.entity.SysAdmin;
import com.lincoln.framework.service.SysAdminService;
import com.lincoln.framework.utils.MD5;
import com.lincoln.framework.utils.StringUtilsEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统修改密码controller
 * 
 * @author jared
 */
@Controller
@RequestMapping("/sys/password")
public class PasswordController extends SysBaseController{
	
	@Autowired
	private SysAdminService sysAdminService;
	
	@RequestMapping(path= "/toEdit.do")
	public String toEdit(HttpServletRequest request, ModelMap modelMap) throws Exception {
		return "/sys/password/password_edit";
	}
	
	@RequestMapping(path= "/edit.do")
	public void edit(HttpServletRequest request, ModelMap modelMap) throws Exception {
		SysAdmin sysAdmin = sysAdminService.getSysAdminById(WebUtils.getSysLoginAdmin().getId());
		if (sysAdmin == null) {
			this.returnError("用户名或密码错误，请重新输入");
			return ;
		}
		String oldPassword = request.getParameter("oldPassword").trim();
		String newPassword = request.getParameter("newPassword").trim();
		String checkPassword = request.getParameter("checkPassword").trim();
		if(!sysAdmin.getPassword().equals(StringUtilsEx.secretEncode(oldPassword))){
			this.returnError("旧密码不正确");
			return ;
		}
		if(!newPassword.equals(checkPassword)){
			this.returnError("新密码与确认密码不一致");
			return ;
		}
		sysAdmin.setPassword(StringUtilsEx.secretEncode(newPassword));
		sysAdmin.setMd5Password(MD5.hex_md5(newPassword));
		sysAdminService.saveSysAdmin(sysAdmin);
		this.addSysAdminLog("修改密码");
		this.returnSuccess();
	}
}
