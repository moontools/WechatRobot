package com.lincoln.framework.controller.sys;

import com.lincoln.framework.annotation.MenuAuth;
import com.lincoln.framework.entity.SysAdmin;
import com.lincoln.framework.entity.SysRole;
import com.lincoln.framework.exception.EntityNotFoundException;
import com.lincoln.framework.service.SysAdminService;
import com.lincoln.framework.service.SysRoleService;
import com.lincoln.framework.utils.MD5;
import com.lincoln.framework.utils.ParameterChecker;
import com.lincoln.framework.utils.StringUtilsEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 系统角色管理controller
 * 
 * @author jared
 */
@Controller
@RequestMapping("/sys/sysAdmin")
public class SysAdminContoller extends SysBaseController<SysAdmin>{
	
	@Autowired
	private SysAdminService sysAdminService;
	
	@Autowired
	private SysRoleService sysRoleService;
	
	/**用户*/
	private SysAdmin sysAdmin;
	
	@RequestMapping(path= "/toList.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_1)
	public String toList(HttpServletRequest request, ModelMap modelMap) throws Exception {
		this.saveRequestParameter();
		modelMap.addAttribute("sysRoleList", sysRoleService.findAll()); 
		modelMap.addAttribute("resultPage", sysAdminService.getSysAdminPage(this.saveQueryInfoAndReturnPage("desc", "id")));
		return "/sys/admin/admin_list";
	}
	
	@RequestMapping(path = "/toEdit.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	public String toEdit(HttpServletRequest request, ModelMap modelMap) throws Exception {
		if (!ParameterChecker.isNullOrEmpty(request.getParameter("id"))) {
			int id = Integer.valueOf(request.getParameter("id"));
			sysAdmin = sysAdminService.getSysAdminById(id);
			if(sysAdmin == null){
				throw new EntityNotFoundException("该管理员不存在");
			}
			modelMap.addAttribute("password", StringUtilsEx.secretDecode(sysAdmin.getPassword()));
		} else {
			sysAdmin = new SysAdmin();
		}
		modelMap.addAttribute("sysRoleList", sysRoleService.findAll()); 
		modelMap.addAttribute("sysAdmin", sysAdmin);
		return "/sys/admin/admin_edit";
	}
	
	/**
	 * 管理员编辑修改
	 * @param request
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/edit.do")
	@ResponseBody
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	public void edit(HttpServletRequest request, ModelMap modelMap) throws Exception {
		if (!ParameterChecker.isNullOrEmpty(request.getParameter("id")) && !request.getParameter("id").equals("0")) {
			int id = Integer.valueOf(request.getParameter("id"));
			sysAdmin = sysAdminService.getSysAdminById(id);
			if(sysAdmin == null){
				throw new Exception("该管理员不存在");
			}
		} else {
			sysAdmin = new SysAdmin();
			if (ParameterChecker.isNullOrEmpty(request.getParameter("password"))){
				this.returnError("密码不能包含空格");
				return;
			}
		}
		SysAdmin sysAdminTemp = sysAdminService.getSysAdminByName(request.getParameter("name").trim());
		if (sysAdminTemp !=  null) {
			if (sysAdminTemp.getId() != sysAdmin.getId()){
				this.returnError("该角色已存在，请审核");
				return;
			}
		}
		sysAdmin.setName(request.getParameter("name").trim());
		if (!ParameterChecker.isNullOrEmpty(request.getParameter("password"))) {
			sysAdmin.setPassword(StringUtilsEx.secretEncode(request.getParameter("password").trim()));
			sysAdmin.setMd5Password(MD5.hex_md5(request.getParameter("password").trim()));
		}
		sysAdmin.setPhoneNum(request.getParameter("phoneNum").trim());
		sysAdmin.setRealName(request.getParameter("realName").trim());
		sysAdmin.setRemark(request.getParameter("remark").trim());
		sysAdmin.setStatus(SysAdmin.STATUS_ENABLE);
		if (sysAdmin.getId() == 0 || sysAdmin.getSysRole() != null) {
			if (!ParameterChecker.isNullOrEmpty(request.getParameter("roleId")) && !request.getParameter("roleId").equals("0")) {
				SysRole sysRole = sysRoleService.getSysRoleById(Integer.valueOf(request.getParameter("roleId")));
				if(sysRole == null){
					this.returnError("该角色已删除，请刷新页面");
					return;
				}
				sysAdmin.setSysRole(sysRole);
			}
		} else {
			this.returnError("超管禁止设置角色");
			return;
		}
		this.addSysAdminLog((sysAdmin.getId() > 0? "修改":"添加") + sysAdmin.getName() + "用户信息");
		sysAdminService.saveSysAdmin(sysAdmin);
		this.returnSuccess();
	}
	
	/**
	 * 关闭账号
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/adminClose.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_5)
	@ResponseBody
	public void doClose(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		if (getSysAdmin()) {
			if (sysAdmin.getSysRole() != null) {
				sysAdmin.setStatus(SysAdmin.STATUS_DISABLE);
				sysAdminService.saveSysAdmin(sysAdmin);
			} else {
				this.returnError("超级管理员禁止禁用");
				return;
			}
		} else {
			return;
		}
		this.addSysAdminLog("禁用" + sysAdmin.getName() + "用户");
		this.returnSuccess();
	}
	
	/**
	 * 开启账号
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/adminOpen.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_5)
	@ResponseBody
	public void doOpen(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		if (getSysAdmin()) {
			if (sysAdmin.getSysRole() != null) {
				sysAdmin.setStatus(SysAdmin.STATUS_ENABLE);
				sysAdminService.saveSysAdmin(sysAdmin);
			} else {
				this.returnError("超级管理员禁止启用");
				return;
			}
		} else {
			return;
		}
		this.addSysAdminLog("启用" + sysAdmin.getName() + "用户");
		this.returnSuccess();
	}
	
	private boolean getSysAdmin(){
		HttpServletRequest request = this.getRequest();
		if (!ParameterChecker.isNullOrEmpty(request.getParameter("id"))) {
			int id = Integer.valueOf(request.getParameter("id"));
			sysAdmin = sysAdminService.getSysAdminById(id);
			if (sysAdmin == null) {
				this.returnError("该用户已删除");
				return false;
			} else {
				return true;
			}
		}else{
			this.returnError("非法访问");
			return false;
		}
	}
	
	/**
	 * 删除账号
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/delete.do")
	@ResponseBody
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_3)
	public void delete(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		if (getSysAdmin()) {
			if (sysAdmin.getSysRole() != null) {
			    this.addSysAdminLog("删除" + sysAdmin.getName() + "用户");
				sysAdminService.deleteSysAdmin(sysAdmin);
			} else {
				this.returnError("超级管理员禁止删除");
				return;
			}
		} else {
			this.returnError("删除用户不存在，请重试", true);
			return;
		}
		this.returnSuccess();
	}
}
