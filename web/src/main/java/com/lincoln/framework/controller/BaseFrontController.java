package com.lincoln.framework.controller;

import com.alibaba.fastjson.JSONObject;
import com.lincoln.framework.utils.JSONUtilsEx;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Lincoln on 2017/10/17.
 */
public class BaseFrontController {
    private final static String[] agent = {"Android", "iPhone", "iPod", "iPad", "Windows Phone", "MQQBrowser"};

    public ModelAndView returnError(String error) {
        ModelAndView mv = new ModelAndView("front/common/error");
        mv.addObject("error", error);
        return mv;
    }

    public String returnApiError(String error) {
        Map<String,Object> map = new HashMap<>();
        map.put("success", 0);
        map.put("msg", error);
        return JSONUtilsEx.fromObject(map, null).toString();
    }

    public String returnApiSuccess(String key,Object data) {
        Map<String,Object> map = new HashMap<>();
        map.put("success", 1);
        map.put(key, data);
        String result = JSONObject.toJSONString(map);
        System.out.println(result);
        return result;
    }
    public String returnApiSuccess(Object data) {
        Map<String,Object> map = new HashMap<>();
        map.put("success", 1);
        map.put("data", data);
        return JSONUtilsEx.fromObject(map, null).toString();
    }
    public String returnApiSuccess() {
        Map<String,Object> map = new HashMap<>();
        map.put("success", 1);
        return JSONUtilsEx.fromObject(map, null).toString();
    }
}
