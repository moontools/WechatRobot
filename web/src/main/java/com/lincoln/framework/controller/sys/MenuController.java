package com.lincoln.framework.controller.sys;

import com.lincoln.framework.annotation.MenuAuth;
import com.lincoln.framework.entity.SysMenu;
import com.lincoln.framework.exception.EntityNotFoundException;
import com.lincoln.framework.service.SysMenuService;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.framework.utils.WebParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 系统端菜单controller
 * @author ssx
 */
@Controller
@RequestMapping("/sys/menu")
public class MenuController extends SysBaseController<SysMenu> {
    
	@Autowired
	private SysMenuService sysMenuService;
	
	private List<SysMenu> sysMenuList;
	
	private SysMenu sysMenu;
	
	private int id;
	
	/**
	 * 跳转列表页面
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping("/toList.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_1)
	public String toList(HttpServletRequest request, Model model) {
		sysMenuList = sysMenuService.findAllInTree();
		
		model.addAttribute("sysMenuList", sysMenuList);
		return "sys/menu/menu_list";
	}
	
	/**
	 * 跳转编辑页面
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/toEdit.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	public String toEdit(HttpServletRequest request, Model model) throws Exception {
		this.initSysMenu(request, true);
		sysMenuList = sysMenuService.findByParentIdAndLevel(0, 0);
		
		model.addAttribute("sysMenuList", sysMenuList);
		model.addAttribute("sysMenu", sysMenu);
		model.addAttribute("sysMenuButtons", sysMenu.getMenuButtonsList());
		return "sys/menu/menu_edit";
	}
	
	/**
	 * 保存菜单
	 * @param request
	 * @throws Exception 
	 */
	@RequestMapping("/save.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	public void save(HttpServletRequest request) throws Exception {
		this.initSysMenu(request, true);
		
		int parentMenuId = 0;
		if (sysMenu.getId() == 0) {
			parentMenuId = WebParamUtils.getIntParameter(request, "parentMenuId");
			sysMenu.setParentMenu(sysMenuService.getSysMenuById(parentMenuId));
		}
		
		String menuName = request.getParameter("name");
		String menuUri = request.getParameter("uri");
		if (sysMenuService.checkExistByName(sysMenu.getId(), parentMenuId, menuName)) {
			this.returnError("名称重复，请修改菜单名称");
			return;
		}
		if (!StringUtilsEx.isNullOrEmpty(menuUri) && sysMenuService.checkExistByUri(sysMenu.getId(), menuUri)) {
			this.returnError("链接重复，请修改菜单链接");
			return;
		}
		
		sysMenu.setLevel(sysMenu.getParentMenu() != null ? 1 : 0);
		sysMenu.setSort(WebParamUtils.getIntParameter(request, "sort"));
		sysMenu.setName(menuName);
		sysMenu.setUri(sysMenu.getLevel() == 0 ? "" : menuUri);
		formatMenuButtons(request);
		
		sysMenuService.saveSysMenu(sysMenu);
		this.addSysAdminLog((id > 0 ? "修改" : "新增") + "菜单");
		this.returnSuccess("保存成功");
	}
	
	/**
	 * 设置菜单排序
	 * @param request
	 * @throws EntityNotFoundException
	 */
	@RequestMapping("/setSort.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	public void setSort(HttpServletRequest request) throws EntityNotFoundException {
		this.initSysMenu(request, false);
		
		sysMenu.setSort(WebParamUtils.getIntParameter(request, "sort"));
		sysMenuService.saveSysMenu(sysMenu);
		this.returnSuccess();
	}
	
	/**
	 * 删除菜单
	 * @param request
	 * @throws EntityNotFoundException 
	 */
	@RequestMapping("/delete.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_3)
	public void delete(HttpServletRequest request) throws EntityNotFoundException {
		this.initSysMenu(request, false);
		
		sysMenuService.deleteSysMenu(sysMenu);
		this.addSysAdminLog("删除菜单");
		this.returnSuccess();
	}
	
	/**
	 * 初始化系统菜单对象
	 * @param request
	 * @param needInitial
	 * @throws EntityNotFoundException
	 */
	private void initSysMenu(HttpServletRequest request, boolean needInitial) throws EntityNotFoundException {
		if (request.getParameter("id") != null) {
			id = WebParamUtils.getIntParameter(request, "id");
		}
		
		if (id == 0 && needInitial) {
			sysMenu = new SysMenu();
			return;
		}
		
		sysMenu = sysMenuService.getSysMenuById(id);
		if (sysMenu == null) {
			throw new EntityNotFoundException("系统菜单不存在");
		}
	}
	
	/**
	 * 将模块名称设置到sysMenu对象中
	 * @param request
	 * @throws Exception
	 */
	private void formatMenuButtons(HttpServletRequest request) throws Exception{
		String[] moduleNames = request.getParameterValues("moduleName");
		if (moduleNames != null && moduleNames.length > 0) {
			for (int i=0; i<20; i++) {
				String methodName = "setButton" + (i+1);
				if (i < moduleNames.length) {
					SysMenu.class.getDeclaredMethod(methodName, String.class).invoke(sysMenu, moduleNames[i]);
				} else {
					SysMenu.class.getDeclaredMethod(methodName, String.class).invoke(sysMenu, (String)null);
				}
			}
		}
	}
}
