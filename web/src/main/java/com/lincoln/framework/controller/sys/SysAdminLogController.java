package com.lincoln.framework.controller.sys;

import com.lincoln.framework.annotation.MenuAuth;
import com.lincoln.framework.bean.ExcelBean;
import com.lincoln.framework.entity.SysAdminLog;
import com.lincoln.framework.service.SysAdminLogService;
import com.lincoln.framework.utils.ExcelUtils;
import com.lincoln.framework.utils.ParameterChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 系统管理员日志controller
 * 
 * @author jared
 */
@Controller
@RequestMapping("/sys/sysAdminLog")
public class SysAdminLogController extends SysBaseController<SysAdminLog>{
	
	@Autowired
	private SysAdminLogService sysAdminLogService;
	
	private SysAdminLog sysAdminLog;
	
	/**
	 * 系统管理日志
	 * @param request
	 * @param modelMap
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path= "/toList.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_1)
	public String toList(HttpServletRequest request, ModelMap modelMap) throws Exception {
		this.saveRequestParameter();
		modelMap.addAttribute("resultPage", sysAdminLogService.getSysAdminLogPage(this.saveQueryInfoAndReturnPage("desc", "operateTime")));
		return "/sys/adminlog/adminlog_list";
	}
	
	/**
	 * 删除账号日志
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/delete.do")
	@ResponseBody
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_3)
	public void doDelete(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		if (getSysAdminLog()) {
			sysAdminLogService.deleteSysAdminLog(sysAdminLog);
		} else {
			this.returnError("该操作日志已删除");
			return;
		}
		this.returnSuccess();
	}
	
	private boolean getSysAdminLog(){
		HttpServletRequest request = this.getRequest();
		if (!ParameterChecker.isNullOrEmpty(request.getParameter("id"))) {
			int id = Integer.valueOf(request.getParameter("id"));
			sysAdminLog = sysAdminLogService.getSysAdminLogById(id);
			if (sysAdminLog == null) {
				this.returnError("该操作日志已删除");
				return false;
			} else {
				return true;
			}
		}else{
			this.returnError("非法访问");
			return false;
		}
	}
	
	/**
	 * 批量删除账号日志
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/mutiDelete.do")
	@ResponseBody
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_3)
	public void domutiDelete(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		String idsStr = request.getParameter("ids");
		if(ParameterChecker.isNullOrEmpty(idsStr)){
			this.returnError("非法访问");
			return;
		}
		String[] ids = idsStr.split(",");
		List<String> idStrList = new ArrayList<String>(Arrays.asList(ids));
		for(int i = 0; i < idStrList.size() ; i++){
			sysAdminLogService.deleteSysAdminLogById(Integer.valueOf(idStrList.get(i)));
		}
		this.returnSuccess();
	}
	
	/**
	 * 批量删除账号日志
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(path = "/preMonthDelete.do")
	@ResponseBody
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_3)
	public void preMonthDelete(HttpServletRequest request, ModelMap modelMap)  throws Exception{
		sysAdminLogService.delerePreThreeMothLog();
		this.returnSuccess();
	}
	
	/**
	 * 导出系统日志数据
	 */
	@RequestMapping(path = "/export.do")
	@MenuAuth(buttonRight = MenuAuth.MENU_BUTTON_2)
	@ResponseBody
	public void export(HttpServletRequest request, ModelMap modelMap)throws Exception{
		List<String> titlesList = new ArrayList<String>();
		titlesList.add("管理员");
		titlesList.add("操作动作");
		titlesList.add("操作时间");
		titlesList.add("IP");
		String[] titles = new String[titlesList.size()];
		titlesList.toArray(titles);
		SimpleDateFormat df2 = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String filename = "操作日志数据" + df2.format(new Date());
		ExcelBean excelBean = new ExcelBean(filename + ".xls", filename, titles);
		List<SysAdminLog> logList = sysAdminLogService.findAll(request.getParameter("searchKey"), request.getParameter("searchVal"),request.getParameter("order"), request.getParameter("orderBy"));
		for(SysAdminLog log : logList){
			String[] data = new String[titlesList.size()];
			data[0] = log.getSysAdmin().getName();
			data[1] = log.getContent();
			data[2] = formatter.format(log.getOperateTime());
			data[3] = log.getIpAddress();
			excelBean.add(data);
		}
		ExcelUtils.export(excelBean);
	}
	
}
