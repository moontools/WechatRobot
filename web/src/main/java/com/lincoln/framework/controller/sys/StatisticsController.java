package com.lincoln.framework.controller.sys;

import com.lincoln.framework.service.BettingService;
import com.lincoln.framework.service.RechargeService;
import com.lincoln.framework.service.WithdrawService;
import com.lincoln.framework.utils.WebParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 今日一览Controller
 * Created by luo on 2017/4/19.
 */
@Controller
@RequestMapping("/sys/statistics")
public class StatisticsController extends SysBaseController {

    @Autowired
    WithdrawService withdrawService;

    @Autowired
    RechargeService rechargeService;

    @Autowired
    BettingService bettingService;

    /**
     * 查询数据
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/toList.do")
    public String toList(HttpServletRequest request, Model model) {
        this.saveRequestParameter();
        Long member = WebParamUtils.getLongParameter(request,"mid",0);
        if(member <= 0){
            member = null;
        }
        /**
         * 下分总计(Today)
         */
        model.addAttribute("withdraw", withdrawService.sumTodayWithdraw(member));
         /**
         * 上分总计(Today)
         */
        model.addAttribute("recharge", rechargeService.sumTodayRecharge(member));
        /**
         * 下注总计(Today)
         */
        model.addAttribute("betting", bettingService.sumTodayBatting(member));
        /**
         * 查询订单总数(Today)
         */
        model.addAttribute("order", bettingService.countOrder(member));
        /**
         * 查询今天中奖的订单数(Today)
         */
        model.addAttribute("brojbk", bettingService.queryTodayWinBatting(member));
        /**
         * 查询今天中奖的金额(Today)
         */
        model.addAttribute("dei", bettingService.countTodayWin(member));

        return "sys/statistics/statistics_list";
    }

}
