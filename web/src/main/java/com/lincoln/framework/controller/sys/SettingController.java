package com.lincoln.framework.controller.sys;

import com.lincoln.framework.entity.Setting;
import com.lincoln.framework.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by luo on 2017/4/19.
 */
@Controller
@RequestMapping("/sys/setting")
public class SettingController extends SysBaseController {
    @Autowired
    SettingService settingService;

    @RequestMapping("/toList.do")
    public String toList(HttpServletRequest request, Model model) throws Exception {
        Setting payBank = settingService.findByKey("payBank");
        Setting paySubBank = settingService.findByKey("paySubBank");
        Setting payBankNo = settingService.findByKey("payBankNo");
        Setting payBankName = settingService.findByKey("payBankName");
        Setting payAttention = settingService.findByKey("payAttention");
        Setting wxewm = settingService.findByKey("wxewm");
        model.addAttribute("payBank",payBank);
        model.addAttribute("paySubBank",paySubBank);
        model.addAttribute("payBankNo",payBankNo);
        model.addAttribute("payBankName",payBankName);
        model.addAttribute("payAttention",payAttention);
        model.addAttribute("wxewm",wxewm);
        return "sys/setting/setting_list";
    }
    @RequestMapping("/contact.do")
    public String contact(HttpServletRequest request, Model model) throws Exception {
        Setting contact = settingService.findByKey("contact");
        model.addAttribute("contact",contact);
        return "sys/setting/contact";
    }
    /**
     * 保存修改
     *
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/edit.do")
    public void edit(HttpServletRequest request) throws Exception {
        String payBank = request.getParameter("payBank");
        String paySubBank = request.getParameter("paySubBank");
        String payBankNo = request.getParameter("payBankNo");
        String payBankName = request.getParameter("payBankName");
        String payAttention = request.getParameter("payAttention");
        String wxewm = request.getParameter("wxewm");
        settingService.save("payBank",payBank);
        settingService.save("paySubBank",paySubBank);
        settingService.save("payBankNo",payBankNo);
        settingService.save("payBankName",payBankName);
        settingService.save("payAttention",payAttention);
        settingService.save("wxewm",wxewm);
        this.returnSuccess();
    }
    /**
     * 保存修改
     *
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping("/editContact.do")
    public void editContact(HttpServletRequest request) throws Exception {
        String contact = request.getParameter("contact");
        settingService.save("contact",contact);
        this.returnSuccess();
    }
}
