package com.lincoln.framework.controller.sys;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 首页controller
 */
@Controller
@RequestMapping("/sys/index")
public class IndexController extends SysBaseController {

	/**
	 * 系统断首页
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/toIndex.do")
	public String toIndex(HttpServletRequest request, Model model) throws Exception {
		return "sys/index/index";
	}

}
