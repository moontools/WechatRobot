package com.lincoln.framework.controller.sys;

import com.lincoln.framework.entity.Betting;
import com.lincoln.framework.exception.ClientException;
import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.service.BettingService;
import com.lincoln.framework.service.MemberService;
import com.lincoln.framework.utils.StringUtilsEx;
import com.lincoln.framework.utils.WebParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 下注Controller
 * Created by luo on 2017/4/19.
 */
@Controller
@RequestMapping("/sys/betting")
public class BettingController extends SysBaseController {
    @Autowired
    BettingService service;

    @Autowired
    MemberService mservice;

    /**
     * 查询数据
     *
     * @param request
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/toList.do")
    public String toList(HttpServletRequest request, Model model) throws Exception {
        String sortBy = "createTime";
        this.saveRequestParameter();
        Page<Betting> page = this.saveQueryInfoAndReturnPage("desc", sortBy);

        double score = WebParamUtils.getDoubleParameter(request, "score", 0);
        String content = request.getParameter("content");
        String createTime = request.getParameter("createTime");
        long member = WebParamUtils.getLongParameter(request, "member", 0);
        if (member > 0) {
            page.getQueryMap().put("memberId", member);
        }
        if (score > 0) {
            page.getQueryMap().put("score", score);
        }
        if (!StringUtilsEx.isNullOrEmpty(content)) {
            page.getQueryMap().put("content", content);
        }

        if (!StringUtilsEx.isNullOrEmpty(createTime)) {
            page.getQueryMap().put("createTime", createTime);
        }
        page = service.findPage(page);
        model.addAttribute("page", page);
        return "sys/betting/betting_list";
    }

    /**
     * 删除
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    public void delete(HttpServletRequest request) {
        try {
            int id = WebParamUtils.getIntParameter(request, "id", 0);
            Betting entity = service.getById(id);
            if (entity != null) {
                service.delete(entity);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            this.returnError("删除失败，请联系管理员");
        }
        this.returnSuccess();
    }
}
