package com.lincoln.framework.common;

import com.lincoln.framework.utils.JSONUtilsEx;
import com.lincoln.framework.utils.FrameworkLogger;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 消息帮助类，返回统一格式内容
 * 
 * @author zwm
 */
public class MessageHandlerEx {

	private static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(MessageHandlerEx.class);

	/** 响应request */
	private HttpServletRequest request;

	/** 响应response */
	private HttpServletResponse response;

	private static final String RETURN_SUCCESS = "success";

	private static final String RETURN_FAILURE = "failure";

	/**
	 * 构造函数初始化
	 * 
	 * @param request
	 * @param response
	 */
	public MessageHandlerEx(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	/**
	 * 初始化消息帮助类
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static MessageHandlerEx instance(HttpServletRequest request, HttpServletResponse response) {
		MessageHandlerEx helper = new MessageHandlerEx(request, response);
		return helper;
	}

	/**
	 * ajax返回错误消息
	 * 
	 * @param message
	 * @throws IOException
	 */
	public void ajaxErrorReturn(String message) {
		this.ajaxErrorReturn(message, new JSONObject());
	}


	/**
	 * ajax返回错误消息
	 * 
	 * @param message
	 * @param extData
	 * @throws IOException
	 */
	public void ajaxErrorReturn(String message, Object extData) {
		JSONObject dataJson = getObjectJsonData(extData);
		this.ajaxReturn(message, true, dataJson);
	}

	/**
	 * ajax返回成功消息
	 * 
	 * @param message
	 * @throws IOException
	 */
	public void ajaxSuccessReturn(String message) {
		this.ajaxReturn(message, false, null);
	}

	/**
	 * ajax返回成功消息
	 * 
	 * @param message
	 * @param extData
	 * @throws IOException
	 */
	public void ajaxSuccessReturn(String message, Object extData) {
		JSONObject dataJson = getObjectJsonData(extData);
		this.ajaxReturn(message, false, dataJson);
	}

	/**
	 * ajax调用消息返回
	 * 
	 * @param message
	 * @param hasError
	 * @throws IOException
	 */
	private void ajaxReturn(String message, boolean hasError, Object dataJson) {

		JSONObject object = new JSONObject();

		object.put("data", dataJson);
		object.put("msg", message);
		object.put("type", hasError ? RETURN_FAILURE : RETURN_SUCCESS);
		this.response.setCharacterEncoding("utf-8");
		this.response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter printWriter = null;
		try {
			printWriter = this.response.getWriter();
		} catch (IOException e) {
			MessageHandlerEx.logger.error(e.getMessage(), e);
			return;
		}

		printWriter.write(object.toString());
		printWriter.flush();
		printWriter.close();
		logger.log(object.toString());
	}

	/**
	 * 通过list获取json
	 * 
	 * @param object
	 * @return
	 */
	public static JSONObject getObjectJsonData(Object object) {
		JsonConfig config = JSONUtilsEx.getJsonConfig(null);
		return JSONObject.fromObject(object, config);
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
}
