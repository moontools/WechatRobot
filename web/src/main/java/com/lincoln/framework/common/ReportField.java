/**
 * 
 */
package com.lincoln.framework.common;

import com.lincoln.framework.common.Constant;

/**
 * 报表字段对应pojo
 * 
 * @author hht
 */
public class ReportField {

    /**
     * 默认做图指标
     */
    public final static int ISDEFAULT_YES = 1;
    
    /**
     * 非默认做图指标
     */
    public final static int ISDEFAULT_NO = 0;

    /**
     * 字段中文名称
     */
    private String nameZh;

    /**
     * 字段英文名称
     */
    private String nameEn;
    
    /**
     * 字段标记
     */
    private String tag;


    /**
     * 是否默认做图指标
     */
    private int isDefault;
    
    /**
     * 该字段显示在图表时，选择使用的Y轴，默认选择左侧的轴,编号为0，右侧为1
     * @return
     */
    private int yAxisIndex;

    
    //get,set

    public String getTag() {
        return tag;
    }

    public String getNameZh() {
        return nameZh;
    }

    public void setNameZh(String nameZh) {
        this.nameZh = nameZh;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(int isDefault) {
        this.isDefault = isDefault;
    }

    public int getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(int yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }
    
    public String getLocaleName(){
        if(WebUtils.getLocale().equals(Constant.LOCALE_ZH)){
            return this.getNameZh();
        }
        return this.getNameEn();
    }
}
