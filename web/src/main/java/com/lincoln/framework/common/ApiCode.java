package com.lincoln.framework.common;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * api返回码
 */
public class ApiCode {
	
	private ApiCode() {

	}
	
	/** 返回码：-1、系统错误 */
	public final static int SYSTEM_ERROR = -1;
	
	/** 返回码：0、请求成功 */
	public final static int REQUEST_SUCCESS = 0;
	
	/** 返回码：10001、缺少checksum参数 */
	public final static int CHECKSUM_LACK = 10001;
	
	/** 返回码：10002、checksum参数错误 */
	public final static int CHECKSUM_WRONG = 10002;
	
	/** 返回码：10003、api请求参数不完整 */
	public final static int PARAMETER_WRONG = 10003;

	/** 返回码：10004、验证码发送失败 */
	public final static int CODE_SEND_WRONE = 10004;

	/** 返回码：10005、验证码错误 */
	public final static int CODE_WRONE = 10005;

	/** 返回码：10005、手机号不能为空 */
	public final static int PHONE_NONE = 10006;

	/** 返回码：10005、密码不能为空 */
	public final static int PASSWORD_NONE = 10007;

	/** 返回码：10005、验证码不能为空 */
	public final static int CODE_NONE = 10008;

	/** 返回码：10005、验证码错误,请重新获取验证码 */
	public final static int SESSION_NONE = 10009;

	/** 返回码：11001、会员账号不存在 */
	public final static int LOGIN_USERNAME_NONE = 11001;
	
	/** 返回码：11002、密码错误 */
	public final static int LOGIN_PASSWOED_WRONG = 11002;

	/** 返回码：11003、微信未注册 */
	public final static int LOGIN_WXOPENID_NONE = 11003;
	
	/** 返回码：12001、会员ID不存在 */
	public final static int LOGIN_MEMBERID_NONE = 12001;
	
	/** 返回码：13001、会员原密码不正确 */
	public final static int MODIFY_PASSWORD_OLDPASSWORD_WRONG = 13001;

	/** 返回码：13002、会员不存在 */
	public final static int MEMBER_NONE = 13002;

	/** 返回码：13003、会员已存在 */
	public final static int MEMBER_HAVEN = 13003;

	/** 返回码：13004、用户已填写邀请人 */
	public final static int MEMBER_INVITE_HAVEN = 13004;

	/** 返回码：14001、找不到该话题 */
	public final static int SUBJECT_NONE = 14001;

	/** 返回码：14002、找不到话题 用于getPage*/
	public final static int SUBJECTPAGE_NONE = 14001;

	/** 返回码：15001、找不到评论 用于getPage*/
	public final static int SUBJECCOMMENTTPAGE_NONE = 15001;

	/** 返回码：16001、找不到车辆 用于getPage*/
	public final static int CAR_NONE = 16001;

	/** 返回码：17001、找不到优惠券 用于getPage*/
	public final static int COUPON_NONE = 17001;

	/** 返回码：17002、优惠券编号已存在*/
	public final static int COUPON_HAVEN = 17002;
	
	/** 返回码：17003、优惠券已使用 */
	public final static int COUPON_USED = 17003;

	/** 返回码：17004、优惠券编号已使用*/
	public final static int MEMBER_COUPON_USED = 17003;

	/** 返回码：18001、找不到广告 用于getPage*/
	public final static int ADVERTISEMENT_NONE = 18001;

	/** 返回码：18002、广告编号已存在 */
	public final static int ADVERTISEMENT_HAVEN = 18002;
	
	/** 返回码：190001、会员卡已使用 */
	public final static int PREPAYCARD_USED = 190001;
	
	/** 返回码：190002、会员卡不存在 */
	public final static int PREPAYCARD_NONE = 190002;

	/**返回码：20001、商家已存在*/
	public final static int BUSINESS_EXIST = 20001;

	/**返回码：20002、商家不存在*/
	public final static int BUSINESS_NONE = 20002;

	/**返回码：20003、您已被禁用！*/
	public final static int BUSINESS_UNUSER= 20003;

	/**返回码：20004、商家暂停营业！*/
	public final static int BUSINESS_STOP = 20004;

	/**返回码：21001、保养信息不存在*/
	public final static int MAINTENANCE_NONE = 21001;
	
	/**返回码：22001、会员订单不存在*/
	public final static int MEMBERORDER_NONE = 22001;

	/**返回码：23001、银行卡不存在*/
	public final static int BANKCARD_NONE = 23001;
	
	public final static int PAYORDER_NONE = 25001;

	/**返回码：24001、token返回失败*/
	public final static int RYTOKEN_NONE = 24001;

	/**返回码：27001、 消息不存在*/
	public final static int MESSAGE_NONE = 27001;

	/**返回码：26001、 订单不存在*/
	public final static int ORDER_NONE = 26001;

	/**返回码：26001、 订单已评价*/
	public final static int ORDER_PINGJIA = 26002;

	/**返回码：26001、 w文章不存在*/
	public final static int ARTICLE_NONE = 27001;
	/**返回码：28001、 该充值配置不存在*/
	public final static int RECHARGE_NONE = 28001;
	/**返回码：29001、 余额不足*/
	public final static int BLANCE_LESS = 29001;
	/**返回码：30001、 最低提现15元*/
	public final static int MONEY_LIMIT = 30001;
	/**返回码：31001、 您不是新用户*/
	public final static int OLD_MEMBER = 31001;

	/**
	 * api返回码与描述对应map
	 */
	public final static Map<Integer, String> CODE_MAP = new LinkedHashMap<Integer, String>() {
		{
			put(SYSTEM_ERROR, "系统错误");
			put(REQUEST_SUCCESS, "请求成功");
			put(CHECKSUM_LACK, "缺少checksum参数");
			put(CHECKSUM_WRONG, "checksum参数错误");
			put(PARAMETER_WRONG, "api请求参数不完整");
			put(LOGIN_USERNAME_NONE, "会员账号不存在");
			put(LOGIN_PASSWOED_WRONG, "密码错误");
			put(LOGIN_MEMBERID_NONE, "会员ID不存在");
			put(MODIFY_PASSWORD_OLDPASSWORD_WRONG,"旧密码错误");
			put(SUBJECT_NONE,"找不到该话题");
			put(SUBJECTPAGE_NONE,"找不到话题");
			put(SUBJECCOMMENTTPAGE_NONE,"找不到评论");
			put(CAR_NONE,"找不到车辆");
			put(COUPON_NONE,"找不到优惠券");
			put(COUPON_HAVEN,"优惠券编号已存在");
			put(ADVERTISEMENT_NONE,"找不到广告");
			put(ADVERTISEMENT_HAVEN,"广告编号已存在");
			put(MEMBER_NONE,"会员不存在");
			put(MEMBER_COUPON_USED,"优惠券编号已使用");
			put(PREPAYCARD_USED,"会员卡已使用");
			put(PREPAYCARD_NONE,"会员卡不存在");
			put(MEMBER_HAVEN,"会员已存在");
			put(BUSINESS_EXIST,"商家已存在");
			put(BUSINESS_NONE,"商家不存在");
			put(COUPON_USED,"优惠券已使用");
			put(CODE_SEND_WRONE,"验证码发送失败");
			put(CODE_WRONE,"验证码错误");
			put(MEMBERORDER_NONE,"会员订单不存在");
			put(PAYORDER_NONE,"流水不存在");
			put(BANKCARD_NONE,"银行卡不存在");
			put(RYTOKEN_NONE,"token返回失败");
			put(LOGIN_WXOPENID_NONE,"微信未注册");
			put(MESSAGE_NONE,"消息不存在");
			put(ORDER_NONE,"订单不存在");
			put(MEMBER_INVITE_HAVEN,"用户已填写邀请人");
			put(ARTICLE_NONE,"文章不存在");
			put(ORDER_PINGJIA,"订单已评价");
			put(RECHARGE_NONE,"该充值配置不存在");
			put(BLANCE_LESS,"余额不足");
			put(PHONE_NONE,"手机号不能为空");
			put(PASSWORD_NONE,"密码不能为空");
			put(CODE_NONE,"验证码不能为空 ");
			put(SESSION_NONE,"验证码错误,请重新获取验证码");
			put(MONEY_LIMIT,"最低提现15元");
			put(OLD_MEMBER,"您不是新用户,无法购买该洗车产品");
			put(BUSINESS_UNUSER,"您已被禁用，请联系客服!");
			put(BUSINESS_STOP,"该商家已经被禁用，请联系客服。");
		}
	};
	
}
