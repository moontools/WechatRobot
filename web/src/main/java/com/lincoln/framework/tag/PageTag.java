package com.lincoln.framework.tag;

import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.utils.ConfigSetting;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * 自定义分页标签
 * @author	jared
 */
public class PageTag extends TagSupport{
	
	/**
	 * 分页显示最大页数数字
	 */
	public final int PAGE_DISPLAY_MAX_NUM = 10;
	
	/**
	 * 分页显示最大页数差
	 */
	public final int PAGE_DISPLAY_MAX_BETEEN = 4;
	
	/**
	 * 表单目标对象
	 */
	private String target;
	
	/**
	 * 分页对象
	 */
	private Page pageObj;
	
	public int doStartTag() throws JspException { 
	        JspWriter out = pageContext.getOut();
	        try{
	        	out.print(getPageStr(pageObj, target));
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	        return super.doStartTag();  
	  
	}
	
	private String getPageStr(Page resultPage, String pageForm) {
		int maxPage = (int) ((resultPage.getTotalCount() - 1) / resultPage.getPageSize()) + 1;
		int currentPage = resultPage.getPageNo();
		int minIndex = 0;
		int maxIndex = 0;
		boolean omittedFirstFlag = false;	//首页是否省略
		boolean omittedEndFlag = false;		//尾页是否省略
		StringBuilder sb = new StringBuilder();
		sb.append("<script type=\"text/javascript\" src=\"/script/common/page.js?v=" + ConfigSetting.SYS_VERSION +"\"></script>");
		sb.append("<div class=\"dataTables_paginate paging_simple_numbers \" id=\"DataTables_Table_0_paginate\"> <span class=\"totalpage\">共" + maxPage +"页</span>");
		sb.append("<ul class=\"pagination vertical_align_m\">");
		if (currentPage <= 1) {
			sb.append("<li class=\"paginate_button previous disabled\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\" id=\"DataTables_Table_0_previous\"><a >上一页</a></li>");
		} else {
			sb.append(" <li class=\"paginate_button previous\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\" id=\"DataTables_Table_0_previous\"><a href=\"javascript:resultPage.skip2Page(" + resultPage.getPrePage() + ");\">上一页</a></li>");
		}
		if (maxPage > PAGE_DISPLAY_MAX_NUM + 1 && currentPage > (PAGE_DISPLAY_MAX_BETEEN + 1)){
			omittedFirstFlag = true;
		}
		if (maxPage - currentPage > PAGE_DISPLAY_MAX_BETEEN){
			if (currentPage  > PAGE_DISPLAY_MAX_BETEEN ){
				if(currentPage != (PAGE_DISPLAY_MAX_BETEEN + 1)){
					maxIndex = currentPage + PAGE_DISPLAY_MAX_BETEEN;
				}else{
					maxIndex = currentPage + PAGE_DISPLAY_MAX_BETEEN + 1;
				}
			} else if (maxPage > PAGE_DISPLAY_MAX_NUM){	
				maxIndex = PAGE_DISPLAY_MAX_NUM;
			} else {
				maxIndex = maxPage;
			}
			if (maxPage > PAGE_DISPLAY_MAX_NUM) {
				omittedEndFlag = true;
			}
		} else {
			maxIndex = maxPage;
		}
		if (maxIndex == maxPage && omittedFirstFlag) {
			if (maxIndex - currentPage <= PAGE_DISPLAY_MAX_BETEEN) {
				minIndex = maxIndex - PAGE_DISPLAY_MAX_NUM + 1;
			}else
				minIndex = currentPage + PAGE_DISPLAY_MAX_NUM - maxIndex;
		} else if (omittedFirstFlag) {
			minIndex = currentPage - PAGE_DISPLAY_MAX_BETEEN;
		}else{
			minIndex = 1;
		}
		if (omittedFirstFlag) {
			sb.append("<li class=\"paginate_button \" aria-controls=\"DataTables_Table_0\" tabindex=\"0\"><a href=\"javascript:resultPage.skip2Page(1);\">1</a></li>");
			if(currentPage != (PAGE_DISPLAY_MAX_BETEEN + 1) && currentPage != (PAGE_DISPLAY_MAX_BETEEN + 2)){
				sb.append("<li class=\"paginate_button disabled\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\"><a>...</a></li>");
			}
		}
		sb.append(getPageString(minIndex, maxIndex, currentPage));
		if(omittedEndFlag){
			if(maxIndex + 1 != maxPage){
				sb.append("<li class=\"paginate_button disabled\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\"><a>...</a></li>");
			}
			sb.append("<li class=\"paginate_button \" aria-controls=\"DataTables_Table_0\" tabindex=\"0\"><a href=\"javascript:resultPage.skip2Page(" + maxPage +");\">" + maxPage + "</a></li>");
		}
		if (currentPage >= maxPage) {
			sb.append("<li class=\"paginate_button next disabled\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\" id=\"DataTables_Table_0_next\"><a>下一页</a></li>");
		} else {
			sb.append("<li class=\"paginate_button next\" aria-controls=\"DataTables_Table_0\" tabindex=\"0\" id=\"DataTables_Table_0_next\"><a href=\"javascript:resultPage.skip2Page(" + resultPage.getNextPage() + ");\">下一页</a></li>");
		}
		sb.append("</ul>");
		sb.append("<div class=\"changepage\">");
		sb.append("<div class=\"changepage_c\">跳转到第");
		sb.append("<input type=\"text\" id=\"pageNum\">");
        sb.append("页</div>");
        sb.append("<a onclick=\"jumpPage();\" href=\"javascript:void(0);\" class=\"btn btn-primary m_l_10\">跳转</a></div>");
        sb.append("</div>");
        sb.append("</div>");
		sb.append("<script type=\"text/javascript\">");
		sb.append("initSkipResultPage(\"" + pageForm + "\")");
		sb.append("</script>");
		return sb.toString();
	}
	
	/**
	 * 获取需要显示的页面标签的字符串
	 * @param minPage 显示的最小页面
	 * @param maxPage 显示的最大页面
	 * @return
	 */
	private String getPageString(int minPage, int maxPage, int currentPage){
		String pageStr = "";
		for(int i = minPage; i <= maxPage ; i++){
			pageStr += "<li class=\"paginate_button" + (i == currentPage? " active": "") +" \" aria-controls=\"DataTables_Table_0\" tabindex=\"0\"><a href=\"javascript:resultPage.skip2Page(" + i + ");\">" + i + "</a></li>";
		}
		return pageStr;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Page getPageObj() {
		return pageObj;
	}

	public void setPageObj(Page pageObj) {
		this.pageObj = pageObj;
	}  

}
