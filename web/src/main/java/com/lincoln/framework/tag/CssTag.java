package com.lincoln.framework.tag;

import com.lincoln.framework.utils.ConfigSetting;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * 自定义css标签
 * @author jared
 *
 */
public class CssTag extends TagSupport{
	
	/**
	 * css引入的href对象
	 */
	private String src;
	
	public int doStartTag() throws JspException { 
        JspWriter out = pageContext.getOut();
        try{
        	out.print("<link href=\"" + src + "?v=" + ConfigSetting.SYS_VERSION + "\" type=\"text/css\" rel=\"stylesheet\" />");
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return super.doStartTag();  
  
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

}
