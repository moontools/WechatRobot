package com.lincoln.framework.tag;

import java.io.IOException;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
/**
 * 下拉框选项选择标签
 * @author jared
 *
 */
public class SelectTag extends TagSupport{
	
	/**
	 * 下拉框选项内容
	 */
	private Map<Object, String> targetList;
	
	/**
	 * 选中的额值
	 */
	private String selectValue;
	
	/**
	 * 样式类型
	 */
	private String className;
	
	/**
	 * 设置下拉框的名称
	 */
	private String name;
	
	public int doStartTag() throws JspException { 
        JspWriter out = pageContext.getOut();
        try{
        	//判断结果集信息
			StringBuilder sb = new StringBuilder();
			sb.append("<select class=\"" + className + "\" id=\"" + name + "\" name=\"" + name + "\">");
			sb.append("<option value=\"-1\">请选择</option>");
			for (Object key : targetList.keySet()) {
				sb.append("<option value=\"" + key.toString() + "\" " + (key.toString().equals(selectValue)? "selected=\"selected\"" : "") + ">" + targetList.get(key) +"</option>");
		    }
			sb.append("</select>");
        	out.print(sb);
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return super.doStartTag();  
  
	}

	public Map<Object, String> getTargetList() {
		return targetList;
	}

	public void setTargetList(Map<Object, String> targetList) {
		this.targetList = targetList;
	}

	public String getSelectValue() {
		return selectValue;
	}

	public void setSelectValue(String selectValue) {
		this.selectValue = selectValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
}
