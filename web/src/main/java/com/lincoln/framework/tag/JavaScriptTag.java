package com.lincoln.framework.tag;

import com.lincoln.framework.utils.ConfigSetting;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * 自定义javascript标签
 * @author jared
 *
 */
public class JavaScriptTag extends TagSupport{
	
	/**
	 * 引入的js的src对象
	 */
	private String src;
	
	public int doStartTag() throws JspException { 
        JspWriter out = pageContext.getOut();
        try{
        	out.print("<script type=\"text/javascript\" src=\"" + src + "?v=" + ConfigSetting.SYS_VERSION +"\"></script>");
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        return super.doStartTag();  
  
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}
	
}
