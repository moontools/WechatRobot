package com.lincoln.framework.tag;

import com.lincoln.framework.hibernate.Page;
import com.lincoln.framework.utils.ConfigSetting;
import com.lincoln.framework.utils.ParameterChecker;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * pc页面分页控件
 * @author ssx
 */
public class SitePageTag extends TagSupport {
	
	/**
	 * 分页显示最大页数数字
	 */
	public final int PAGE_DISPLAY_MAX_NUM = 10;
	
	/**
	 * 分页显示最大页数差
	 */
	public final int PAGE_DISPLAY_MAX_BETEEN = 4;
	
	/**
	 * 表单目标对象
	 */
	private String target;
	
	/**
	 * 分页对象
	 */
	private Page pageObj;
	
	/**
	 * div样式
	 */
	private String divClass;
	
	public int doStartTag() throws JspException { 
		JspWriter out = pageContext.getOut();
		try{
			out.print(getPageStr(pageObj, target,divClass));
		} catch (IOException e) {
		    e.printStackTrace();
		}
		return super.doStartTag();
	}
	
	private String getPageStr(Page resultPage, String pageForm , String divClass) {
		int maxPage = (int) ((resultPage.getTotalCount() - 1) / resultPage.getPageSize()) + 1;
		int currentPage = resultPage.getPageNo();
		divClass = ParameterChecker.isNullOrEmpty(divClass) ? "page_box" : divClass;
		StringBuilder sb = new StringBuilder();
		sb.append("<script type=\"text/javascript\" src=\"/script/common/page.js?v=" + ConfigSetting.SYS_VERSION +"\"></script>");
		sb.append("<div class=\""+ divClass +"\"><div class=\"page\">");
		
		if (currentPage <= 1) {
			sb.append("<a href=\"javascript:;\" class=\"arrowll\"></a>");
		} else {
			sb.append("<a href=\"javascript:resultPage.skip2Page(1);\" class=\"arrowll\"></a>");
		}
		for (int i=1; i<=maxPage; i++) {
			String aCss = i==currentPage ? "nb on" : "nb";
			if (i==1 || i==maxPage || (i<currentPage+3 && i>currentPage-3)) {
				sb.append("<a href=\"javascript:resultPage.skip2Page(" + i + ");\" class=\"" + aCss + "\">" + i + "</a>");
			} else if (i==currentPage+3 || i==currentPage-3) {
				sb.append("<a href=\"javascript:resultPage.skip2Page(" + i + ");\" class=\"pagemore\"> ... </a>");
			}
		}
		if (currentPage >= maxPage) {
			sb.append("<a href=\"javascript:;\" class=\"arrowrr\"></a>");
		} else {
			sb.append("<a href=\"javascript:resultPage.skip2Page(" + maxPage + ");\" class=\"arrowrr\"></a>");
		}
		
		sb.append("&nbsp;<span>到第 </span><input class=\"inputpage\" id=\"pageNum\" type=\"text\"><span>页</span>&nbsp;");
		sb.append("<a href=\"javascript:;\" onclick=\"jumpPage();\" class=\"btn_b\">确定</a>");
		sb.append("</div></div>");
        
		sb.append("<script type=\"text/javascript\">");
		sb.append("initSkipResultPage(\"" + pageForm + "\")");
		sb.append("</script>");
		return sb.toString();
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Page getPageObj() {
		return pageObj;
	}

	public void setPageObj(Page pageObj) {
		this.pageObj = pageObj;
	}

	public String getDivClass() {
		return divClass;
	}

	public void setDivClass(String divClass) {
		this.divClass = divClass;
	}
	
}
