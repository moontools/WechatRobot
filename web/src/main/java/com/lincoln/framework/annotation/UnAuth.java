package com.lincoln.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 不做校验
 * 
 * @author zwm
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface UnAuth {

}
