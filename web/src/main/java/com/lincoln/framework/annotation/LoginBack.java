package com.lincoln.framework.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * <h1>登录返回</h1>
 * <p>
 * 登录返回，未设置参数，登录后直接返回注解使用当前方法，设置backUrl，直接跳转url;backRefererUrl设置为true,返回当前方法referer-url
 * </p>
 * @author zwm
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginBack {

	/**
	 * 指定返回url
	 */
	String backUrl() default "";
	
	/**
	 * 是否返回referer-url
	 */
	boolean backRefererUrl() default false;
	
}
