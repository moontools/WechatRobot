package com.lincoln.framework.task.schedule;

import com.lincoln.framework.utils.FrameworkLogger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务
 * 
 * @author zwm
 * 
 */
@Component("sumJob")
public class SumJob{

	private static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(SumJob.class);


	@Scheduled(cron = "0 0 0 * * MON")
	public void sumTotal() {
		System.out.println("收益统计定时器");
		logger.log("定时任务执行....");
	}

}
