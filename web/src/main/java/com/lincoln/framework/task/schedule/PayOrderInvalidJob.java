package com.lincoln.framework.task.schedule;

/**
 * 商品定时过期处理
 * 
 * @author zwm
 * 
 * @version 2015-3-19 下午8:13:14
 */
public class PayOrderInvalidJob extends ScheduleJob {

	/**
	 * 订单id
	 */
	private int orderId;

	/**
	 * 订单失效最大时间2小时(单位：毫秒)
	 */
	public static int MAX_OVER_TIME = 2 * 60 * 60 * 1000;

	@Override
	public void run() {
//		logger.log("订单过期定时任务处理【orderId：" + this.getOrderId() + "】start");
//		try {
//			PayOrderService payOrderService = SpringContextUtil.getBean("payOrderService");
//			PayOrder order = payOrderService.getPayOrderById(orderId);
//			if (order != null && order.getStatus() == PayOrder.STATUS_WAITPAY) {
//				payOrderService.cancelOrderById(order.getId());
//			}
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//		}
//		logger.log("订单过期定时任务处理【orderId：" + this.getOrderId() + "】end");
	}
	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

}
