package com.lincoln.framework.task;

import com.lincoln.framework.common.Constant;
import com.lincoln.framework.utils.FrameworkLogger;
import com.lincoln.framework.utils.StringUtilsEx;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 定时任务
 * 
 * @author zwm
 * 
 */
@Component("taskJob")
public class TaskJob {

	private static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(TaskJob.class);


	@Scheduled(cron = "0 0 0 * * ?")
	public void TestJob() {
		logger.log("定时任务执行....");
	}

	/**
	 * 每日清理临时文件
	 */
	@Scheduled(cron = "0 0 3 * * ?")
	public void clearTempFile() {
		String tempPath = StringUtilsEx.getContextPath() + Constant.TEMP_DIR;
		File file = new File(tempPath);
		if (file.isDirectory()) {
			for (File itemFile : file.listFiles()) {
				deleteFile(itemFile);
			}
		}
	}

	/**
	 * 每周一结算
	 */
	@Scheduled(cron = "0 0 0 * * MON")
	public void dealGain() {
		logger.log("执行每周一结算");
	}

	/**
	 * 文件删除
	 * 
	 * @param file
	 */
	private static void deleteFile(File file) {
		if (file.exists()) {
			if (file.isDirectory()) {
				for (File itemFile : file.listFiles()) {
					deleteFile(itemFile);
				}
			} else {
				long nowTime = System.currentTimeMillis();
				if (nowTime - file.lastModified() > 1000 * 60 * 60 * 12) {
					file.delete();
				}
			}
		}
	}
}
