package com.lincoln.framework.task.schedule;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 定时初始化执行任务处理
 * 
 * @author zwm
 * 
 * @version 2015-3-19 下午9:05:36
 */
public class InitScheduleJobProcessor implements ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		initShopOrderInvalidJob();
	}

	/**
	 * 初始化订单过期任务
	 */
	private static void initShopOrderInvalidJob() {
//		PayOrderService payOrderService = SpringContextUtil.getBean("payOrderService");
//		payOrderService.deleteOverTimeWaitPayOrder();
//		Date now = new Date();
//		Date limitTime = new Date(now.getTime() - PayOrderInvalidJob.MAX_OVER_TIME);
//		List<PayOrder> orders = payOrderService.findPayOrders(new Integer[] { PayOrder.STATUS_WAITPAY }, limitTime,
//				new Integer[] { PayOrder.ORDER_TYPE_BUY_WASH_TIMES, PayOrder.ORDER_TYPE_MAINTENANCE });
//		for (PayOrder shopOrder : orders) {
//			PayOrderInvalidJob job = new PayOrderInvalidJob();
//			job.setOrderId(shopOrder.getId());
//			ScheduledExecutorHelper.excuteDelayJob(job, PayOrderInvalidJob.MAX_OVER_TIME - now.getTime()
//					+ shopOrder.getCreateTime().getTime(), TimeUnit.MILLISECONDS);
//		}
	}
}
