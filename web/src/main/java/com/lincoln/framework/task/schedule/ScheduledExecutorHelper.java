package com.lincoln.framework.task.schedule;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务线程帮助类
 * 
 * @author zwm
 * 
 * @version 2015-3-19 下午7:43:29
 */
public class ScheduledExecutorHelper {

	private static ScheduledExecutorService service;

	/**
	 * 线程池配置
	 */
	private static int CORE_POOL_SIZE = 30;

	static {
		service = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
	}

	/**
	 * 延时一段时间执行执行,只执行一次
	 * 
	 * @param job
	 *            任务详情
	 * @param delayTime
	 *            延时时间
	 * @param timeUnit
	 *            延时时间时间类型 void
	 */
	public static void excuteDelayJob(ScheduleJob job, long delayTime, TimeUnit timeUnit) {
		service.schedule(job, delayTime, timeUnit);
	}

	/**
	 * 设置循环执行任务
	 * 
	 * @param job
	 *            任务详情
	 * @param initDelay
	 *            初次执行延时时间
	 * @param delayTime
	 *            每次执行延迟时间
	 * @param timeUnit
	 *            延迟时间类型
	 */
	public static void excuteFixedRateJob(ScheduleJob job, long initDelay, long delayTime, TimeUnit timeUnit) {
		service.scheduleAtFixedRate(job, initDelay, delayTime, timeUnit);
	}

	/**
	 * 设置循环执行任务,delayTime时间差的线型执行
	 * 
	 * @param job
	 *            任务详情
	 * @param initDelay
	 *            初次执行延时时间
	 * @param delayTime
	 *            每次执行延迟时间
	 * @param timeUnit
	 *            延迟时间类型
	 */
	public static void excuteFixedDelayJob(ScheduleJob job, long initDelay, long delayTime, TimeUnit timeUnit) {
		service.scheduleWithFixedDelay(job, initDelay, delayTime, timeUnit);
	}
}
