package com.lincoln.framework.task.schedule;

import com.lincoln.framework.utils.FrameworkLogger;


/**
 * 线程任务基础类
 * 
 * @author zwm
 * 
 * @version 2015-3-19 下午7:31:48
 */
public abstract class ScheduleJob implements Runnable {

	/**
	 * 日志对象
	 */
	protected static FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(ScheduleJob.class);
}
