package com.lincoln.framework.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.lincoln.framework.bean.QueryInfo;

public class QueryUtils {
	
	/**保存在session中的查询map对象名称*/
	private static final String MANAGE_SESSION_QUERY_MAP = QueryUtils.class + ".Manage.SessionQueryMap";
	
	/**
	 * 管理中心,从session中获取QueryInfo对象,如果不存在的话，创建一个
	 */
	public static QueryInfo getQueryInfoFromSession (HttpServletRequest request, String className) {
		Map<String, QueryInfo> queryMap = getManageQueryinfoMapFromSession(request);
		QueryInfo queryInfo = queryMap.get(className);
		if (queryInfo == null) {
			queryInfo = new QueryInfo();
			queryMap.put(className, queryInfo);
		}
		queryInfo.setUri(request.getRequestURI());
		return queryInfo;
	}
	
	/**
	 * 管理中心,从当前会话中获取查询map。如果不存在创建之 
	 */
	private static Map<String, QueryInfo> getManageQueryinfoMapFromSession (HttpServletRequest request ) {
		HttpSession session = request.getSession();
		Object map = session.getAttribute(MANAGE_SESSION_QUERY_MAP);
		if (map == null) {
			map = new HashMap<String, QueryInfo>();
			session.setAttribute(MANAGE_SESSION_QUERY_MAP, map);
		}
		return (HashMap<String, QueryInfo>) map;
	}
}
