package com.lincoln.framework.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import com.lincoln.framework.common.WebUtils;

/**
 * 时间通用方法静态类
 * <p>
 * 提供了系统常用的时间操作方法
 * </p>
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * <p>
 * Company:
 * </p>
 * 
 * @author anderson
 * @version 1.0
 */
public class DateUtils {
    
    /**
     * 获取长时间格式
     * @return
     */
    private static SimpleDateFormat getLongFormat(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
    
    /**
     * 获取日期格式
     * @return
     */
    private static SimpleDateFormat getMyFormat(){
        return new SimpleDateFormat("yyyy-MM-dd");
    }
    
    /**
     * 获取年月份
     * @return
     */
    private static SimpleDateFormat getYearMonthFormat(){
        return new SimpleDateFormat("yyyy-MM");
    }
    
    /**
     * 获取月份
     * @return
     */
    private static SimpleDateFormat getMonthFormat(){
        return new SimpleDateFormat("MM");
    }
    
    
    /**
     * 获取月份
     * @return
     */
    private static SimpleDateFormat getYearFormat(){
        return new SimpleDateFormat("yyyy");
    }
    
    
    /**
     * 获取年的格式，返回yyyy字符串格式
     * @param date
     * @return
     */
    public static String getYear(Date date){
        return getYearFormat().format(date);
    }
    
    /**
     * 获取年的格式，返回yyyy-MM字符串格式
     * @param date
     * @return
     */
    public static String getYearMonth(Date date){
        return getYearMonthFormat().format(date);
    }
    

	/**
	 * 取得指定月份的第一天
	 * 
	 * @param strdate
	 *            String
	 * @return String
	 */
	public static String getMonthBegin(String strdate) {
		java.util.Date date = StringToDate(strdate);
		return formatDateByFormat(date, "yyyy-MM") + "-01";
	}

	/**
	 * 取得指定月份的最后一天
	 * 
	 * @param strdate
	 *            String
	 * @return String
	 */
	public static String getMonthEnd(String strdate) {
		java.util.Date date = StringToDate(getMonthBegin(strdate));

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		return formatDate(calendar.getTime());
	}

	/**
	 * 取得指定月份的总天数
	 * 
	 * @param strdate
	 *            String
	 * @return String
	 */
	public static int getMonthDaynum(String strdate) {
		String enddate = getMonthEnd(strdate);
		return Integer.parseInt(enddate.substring(enddate.length() - 2, enddate.length()));
	}

	/**
	 * 得到strDate日期的周的星期一
	 * 
	 * @param strDate
	 * @return
	 */
	public static String getWeekBegin(String strDate) {
		java.util.Date date = StringToDate(strDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);// 1为周日.
		if (dayOfWeek == 1) {
			dayOfWeek = 6;
		} else {
			dayOfWeek = dayOfWeek - 2;
		}
		calendar.add(Calendar.DAY_OF_YEAR, 0 - dayOfWeek);
		return formatDate(calendar.getTime());
	}

	/**
	 * 常用的格式化日期
	 * 
	 * @param date
	 *            Date
	 * @return String
	 */
	public static String formatDate(java.util.Date date) {
		return formatDateByFormat(date, "yyyy-MM-dd");
	}

	/**
	 * 是否在同一分钟
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static boolean isSameMinute(java.util.Date d1, java.util.Date d2) {
		boolean result;
		if (d1.getTime() - d2.getTime() >= 60 * 1000 || d1.getTime() - d2.getTime() < -60 * 1000) {
			result = false;
		} else {
			result = d1.getMinutes() == d2.getMinutes();
		}
		return result;
	}

	/**
	 * 常用的格式化日期
	 * 
	 * @param date
	 *            Date
	 * @return String
	 */
	public static String formatDate2(java.util.Date date) {
		return formatDateByFormat(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 常用的格式化日期
	 * 
	 * @param date
	 *            Date
	 * @return String
	 */
	public static String formatDate3(java.util.Date date) {
		return formatDateByFormat(date, "yyyy-MM-dd HH:mm");
	}

	/**
	 * 以指定的格式来格式化日期
	 * 
	 * @param date
	 *            Date
	 * @param format
	 *            String
	 * @return String
	 */
	public static String formatDateByFormat(java.util.Date date, String format) {
		String result = "";
		if (date != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(format);
				result = sdf.format(date);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * 取得系统当前时间,类型为Timestamp
	 * 
	 * @return Timestamp
	 */
	public static Timestamp getNowTimestamp() {
		java.util.Date d = new java.util.Date();
		Timestamp numTime = new Timestamp(d.getTime());
		return numTime;
	}

	/**
	 * 取得系统当前时间,类型为String
	 * 
	 * @return String
	 */
	public static String getNowTimeString() {
		return TimestampToString(getNowTimestamp());
	}

	/**
	 * 取得系统的当前时间,类型为String
	 * 
	 * @return String
	 */
	public static String getNowMonth() {
		return getNowTimeString().substring(0, 7);
	}

	/**
	 * 取得系统的当前年份,类型为String
	 * 
	 * @return String
	 */
	public static String getCurrentYear() {
		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		return sdf.format(now);
	}
	
	/**
     * 取得系统的当前年份,类型为String
     * 
     * @return String
     */
    public static String getLastYear() {
        Calendar ca = Calendar.getInstance();
        ca.add(Calendar.YEAR, -1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        return sdf.format(ca.getTime());
    }

	/**
	 * 取得系统的当前月份,类型为String
	 * 
	 * @return String
	 */
	public static String getMonthNow() {
		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		return sdf.format(now);
	}

	/**
	 * 取得工资年月,类型为String
	 * 
	 * @return String
	 */
	public static String getSalaryMonth() {
		java.util.Date now = getNowDate();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.add(Calendar.MONTH, -1);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		return formatter.format(calendar.getTime());
	}

	/**
	 * 取得系统的当前时间,类型为java.sql.Date
	 * 
	 * @return java.sql.Date
	 */
	public static java.sql.Date getNowDate() {
		java.util.Date d = new java.util.Date();
		return new java.sql.Date(d.getTime());
	}

	/**
	 * 获取当前日期
	 * 
	 * @return
	 */
	public static String getDateNow() {
		java.util.Date now = new java.util.Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		return sdf.format(now);
	}

	/**
	 * 获取当天日期
	 */
	public static java.util.Date getToday() {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.parse(df.format(new java.util.Date()));
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * 从Timestamp类型转化为yyyy/mm/dd类型的字符串
	 * 
	 * @param date
	 * @param strDefault
	 * @return
	 */
	public static String TimestampToString(Timestamp date, String strDefault) {
		String strTemp = strDefault;
		if (date != null) {
			// SimpleDateFormat formatter= new SimpleDateFormat ("yyyy/MM/dd");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	/**
	 * 从Timestamp类型转化为yyyy/mm/dd类型的字符串,如果为null,侧放回""
	 * 
	 * @param date
	 * @return
	 */
	public static String TimestampToString(Timestamp date) {
		return TimestampToString(date, null);
	}

	/**
	 * date型转化为String 格式为yyyy/MM/dd
	 * 
	 * @param date
	 * @param strDefault
	 * @return
	 */
	public static String DateToString(java.sql.Date date, String strDefault) {
		String strTemp = strDefault;
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	/**
	 * date型转化为String 格式为yyyy/MM/dd
	 * 
	 * @param date
	 * @return
	 */
	public static String DateToString(java.util.Date date) {
		String strTemp = "";
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	/**
	 * date型转化为String 格式为hh/mm/ss
	 * 
	 * @param date
	 * @return
	 */
	public static String DateToString2(java.util.Date date) {
		String strTemp = "";
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	/**
	 * date型转化为String 格式为hh/mm/ss
	 * 
	 * @param date
	 * @return
	 */
	public static String DateToString3(java.util.Date date) {
		String strTemp = "";
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	/**
	 * date型转化为String 格式为yyyy-MM
	 * 
	 * @param date
	 * @param strDefault
	 * @return
	 */
	public static String DateToString3(java.sql.Date date, String strDefault) {
		String strTemp = strDefault;
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	public static String DateToString2(java.sql.Date date, String strDefault) {
		String strTemp = strDefault;
		if (date != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			strTemp = formatter.format(date);
		}
		return strTemp;
	}

	public static String DateToString(java.sql.Date date) {
		return DateToString(date, null);
	}

	/**
	 * String转化为Timestamp类型
	 * 
	 * @param strDate
	 * @return
	 */
	public static Timestamp StringToTimestamp(String strDate) {
		if (strDate != null && !strDate.equals("")) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date d = formatter.parse(strDate);
				Timestamp numTime = new Timestamp(d.getTime());
				return numTime;
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * String转化为java.sql.date类型，
	 * 
	 * @param strDate
	 * @return
	 */
	public static java.sql.Date StringToDate(String strDate) {
		if (strDate != null && !strDate.equals("")) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date d = formatter.parse(strDate);
				java.sql.Date numTime = new java.sql.Date(d.getTime());
				return numTime;
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * String转化为java.sql.date类型，
	 * 
	 * @param strDate
	 * @return
	 */
	public static java.sql.Date StringToDateLong(String strDate) {
		if (strDate != null && !strDate.equals("")) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				java.util.Date d = formatter.parse(strDate);
				java.sql.Date numTime = new java.sql.Date(d.getTime());
				return numTime;
			} catch (Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * 得到上个月的日期
	 * 
	 * @param date
	 * @return
	 */
	public static Date getPreDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, -1);
		return new Date(calendar.getTime().getTime());
	}

	/**
	 * 得到昨天的日期
	 * 
	 * @return
	 */
	public static java.util.Date getYesterDayDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1);
		return new java.util.Date(calendar.getTime().getTime());
	}
	
	/**
     * 得到指定日期的昨天
     * 
     * @param date
     * @return
     */
    public static java.util.Date getYesterDayByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, -1);
        return new java.util.Date(calendar.getTime().getTime());
    }

	/**
	 * 得到上个月的月份（返回格式MM）
	 * 
	 * @return
	 */
	public static String getLastMonth() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		return getMonthFormat().format(calendar.getTime());
	}
	
	 /**
     * 得到上个月的年月份（返回格式yyyy-MM）
     */
    public static String getLastYearMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);
        return getYearMonthFormat().format(calendar.getTime());
    }

	/**
	 * 得到上个月的月份（返回格式MM）
	 * 
	 * @return
	 */
	public static java.util.Date getPreThreeMonthDayDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -6);
		return new java.util.Date(calendar.getTime().getTime());
	}

	/**
	 * 得到下一个月的字符串
	 * 
	 * @param strDate
	 * @return
	 * @throws ParseException
	 */
	public static String getPreMonthStr(String strDate) {
		Date fromDate = StringToDate(strDate);
		Date toDate = getPreDate(fromDate);
		return DateToString(toDate);
	}

	/**
	 * 得到当前日期的字符串
	 * 
	 * @return
	 */
	public static String getNowDateString() {
		return DateToString(getNowDate());
	}

	/**
	 * 得到当前日期的字符串
	 * 
	 * @return
	 */
	public static String getNowDateBeginString() {
		return getNowDateString() + " 00:00:00";
	}

	public static String getNowDateEndString() {
		return getNowDateString() + " 23:59:59";
	}

	public static long getLast7dayTime() {
		long time = StringToDateLong(getNowDateBeginString()).getTime();
		return (time - 6 * 24 * 60 * 60 * 1000);
	}

	/**
	 * 得到日期在月份的第几周
	 * 
	 * @param strDate
	 * @return
	 */
	public static int getWeekOfMonth(String strDate) {
		Date date = StringToDate(strDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setFirstDayOfWeek(Calendar.MONDAY);
		calendar.setTime(date);

		return calendar.get(Calendar.WEEK_OF_MONTH);
	}

	/**
	 * 传入日期加1
	 * 
	 * @param date
	 * @return (date + 1)
	 */
	public static String getQueryDate(String date) {
		java.util.Date d = StringToDate(date);
		return formatDate(getDateAfter(d, 1));
	}

	/**
	 * 给指定日期加减天数，返回结果日期
	 * 
	 * @param date
	 * @param day
	 * @return
	 */
	public static java.util.Date getDateAfter(java.util.Date date, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, day);
		java.util.Date newdate = calendar.getTime();
		return newdate;
	}
	
	/**
	 * 给指定日期加减月数，返回结果日期
	 * 
	 * @param date
	 * @param month
	 * @return
	 */
	public static java.util.Date getDateAfterMonth(java.util.Date date, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, month);
		return calendar.getTime();
	}

	/**
	 * 返回当前时间(格式：yyyy-MM-dd hh:mm:ss)
	 */
	public static String getLongNow() {
		Calendar calendar = Calendar.getInstance();
		return getLongFormat().format(calendar.getTime());
	}

	/**
	 * 将字符串时间转换为java.util.date时间格式(字符串格式：yyyy-MM-dd hh:mm:ss)
	 * 
	 * @param str
	 * @return
	 */
	public static java.util.Date getDateByLongFormat(String str) {
		if (str == null || str.length() < 1)
			return null;
		java.util.Date result = null;
		try {
			result = getLongFormat().parse(str);
		} catch (Exception e) {
		}
		return result;
	}

	/**
	 * 返回时间(格式：yyyy-MM-dd hh:mm:ss)
	 */
	public static String getLongTime(long time) {
		return getLongFormat().format(time);
	}

	/**
	 * 返回当前时间(格式：yyyy-MM-dd)
	 */
	public static String getNow() {
		Calendar calendar = Calendar.getInstance();
		return getMyFormat().format(calendar.getTime());
	}

	/**
	 * 返回当前年月（格式：yyyy-MM）
	 * 
	 * @return
	 */
	public static String getYearMonth() {
		Calendar calendar = Calendar.getInstance();
		return getYearMonthFormat().format(calendar.getTime());
	}

	/**
	 * 返回当前小时数(24小时)
	 * 
	 * @return
	 */
	public static int getHour() {
		Calendar calendars = Calendar.getInstance();
		int hour = calendars.get(Calendar.HOUR_OF_DAY);
		return hour;
	}

	/**
	 * 获得昨天的日期
	 */
	public static String getYesterday() {
		java.util.Date d = new java.util.Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(calendar.getTime());
	}

	/**
	 * 获得昨天的月份
	 */
	public static String getYesterdayMonth() {
		java.util.Date d = new java.util.Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
		return sdf.format(calendar.getTime());
	}

	/**
	 * 获得一个员工在公司的完整年数
	 * 
	 * @param date
	 * @return
	 */
	public static int getYearCount(java.util.Date date, java.util.Date date2) {
		int count = 0;
		// 获得员工在公司的完整年数
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		int year = Integer.parseInt(sdf.format(date));
		int cyear = Integer.parseInt(sdf.format(date2));
		count = cyear - year - 1;
		count = count < 0 ? 0 : count;
		return count;
	}

	public static String getChineseDateStr(String strdate) {
		Date date = StringToDate(strdate);
		String datearr[] = strdate.split("-");
		String week[] = { "日", "一", "二", "三", "四", "五", "六" };
		String s = datearr[0] + "年" + datearr[1] + "月" + datearr[2] + "日  星期";
		@SuppressWarnings("deprecation")
		int weekday = date.getDay();
		return s + week[weekday];
	}

	public static String getChineseDateStr2(String strdate) {
		String datearr[] = strdate.split("-");
		if (datearr[1].startsWith("0"))
			datearr[1] = datearr[1].substring(1, datearr[1].length());
		String s = datearr[1] + "月" + datearr[2] + "日 ";
		return s;
	}

	public static String getChineseDateStr3(String strdate) {
		String datearr[] = strdate.split("-");
		String s = datearr[0] + "年" + datearr[1] + "月" + datearr[2] + "日 ";
		return s;
	}

	/**
	 * 转换unix long时间为Date对象
	 * 
	 * @param time
	 * @return
	 */
	public static java.util.Date fromUnixLongToDate(long time) {
		time = fromServerTimeToTimeStamp(time);
		return new java.util.Date(time);
	}

	/**
	 * 得到某年的所有自然周：注意，calendar将周日做为周的第一天
	 * 
	 * @param year
	 * @return String[2]:String[0] 每周一字符串 String[1] 每周日字符串
	 */
	public static String[] getAllWeekStr(int year) {
		String[] result = { "", "" };
		String firstday = String.valueOf(year) + "-01-01";
		String lastday = String.valueOf(year) + "-12-31";
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, year);// 设置年
		c.set(Calendar.WEEK_OF_YEAR, 1);// 设置周
		Calendar d = (Calendar) c.clone();
		c.set(Calendar.DAY_OF_WEEK, 1);// 设置周的第一天
		if (getMyFormat().format(new Date(c.getTimeInMillis())).equals(firstday)) {// 如果01-01刚好是周日
			result[0] = result[0] + firstday + "|";
			result[1] = result[1] + firstday + "|";
		}
		c.add(Calendar.DAY_OF_YEAR, 1);
		d.set(Calendar.DAY_OF_WEEK, 7);// 设置周的最后一天
		d.add(Calendar.DAY_OF_YEAR, 1);
		String weekmonday = getMyFormat().format(new Date(c.getTimeInMillis()));// 得到第一周的周一
		String weeksunday = getMyFormat().format(new Date(d.getTimeInMillis()));// 得到第一周的周日
		if (weekmonday.compareTo(firstday) > 0)
			result[0] = result[0] + weekmonday + "|";
		else
			result[0] = result[0] + firstday + "|";
		result[1] = result[1] + weeksunday + "|";
		weekmonday = firstday;
		while (weekmonday.startsWith(String.valueOf(year))) {// 循环得到所有周的周一
			c.add(Calendar.DAY_OF_YEAR, 7);
			weekmonday = getMyFormat().format(new Date(c.getTimeInMillis()));
			if (!weekmonday.startsWith(String.valueOf(year)))
				break;
			result[0] = result[0] + weekmonday + "|";
		}
		while (weeksunday.startsWith(String.valueOf(year))) {// 循环得到所有周的周日
			d.add(Calendar.DAY_OF_YEAR, 7);
			weeksunday = getMyFormat().format(new Date(d.getTimeInMillis()));
			if (!weeksunday.startsWith(String.valueOf(year))) {
				result[1] = result[1] + lastday + "|";
				break;
			}
			result[1] = result[1] + weeksunday + "|";
			if (weeksunday.equals(lastday)) {
				break;
			}
		}
		result[0] = result[0].substring(0, result[0].length() - 1);
		result[1] = result[1].substring(0, result[1].length() - 1);
		return result;
	}

	/**
	 * 得到当前日期年月日字符串 result[0]:year result[1]:month result[2]:day
	 */
	public static String[] getYearMonthDay() {
		String[] result = new String[3];
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date());
		int month = c.get(Calendar.MONTH) + 1;
		int day = c.get(Calendar.DAY_OF_MONTH);
		if (month < 10) {
			result[1] = String.valueOf("0" + month);
		} else {
			result[1] = String.valueOf(month);
		}
		if (day < 10) {
			result[2] = String.valueOf("0" + day);
		} else {
			result[2] = String.valueOf(day);
		}
		result[0] = String.valueOf(c.get(Calendar.YEAR));
		return result;
	}

	/**
	 * 返回10位的int型当前时间
	 * 
	 * @return
	 */
	public static long getCurrTime() {
		return System.currentTimeMillis() / 1000;
	}

	/**
	 * 根据date获得邮件显示用准确时间
	 * 
	 * @param date
	 *            源date
	 * @return 邮件显示用准确时间
	 */
	public static String getMailDate(java.util.Date date) {
		String mailDateStr = formatDate(date);
		String curDateStr = getNowDateString();
		if (curDateStr.equals(mailDateStr)) {
			// 当天的邮件，只显示小时和分钟
			return new SimpleDateFormat("HH:mm").format(date);
		} else {
			// 不是当天的邮件，则只显示年、月、日
			return mailDateStr;
		}
	}

	/**
	 * 显示移站通时间
	 * 
	 * @param date
	 *            源date
	 * @return 移站通时间
	 */
	public static String getMobilePlusDate(java.util.Date date) {
		String dateStr = formatDate(date);
		String yearStr = new SimpleDateFormat("yyyy").format(date);
		String curDateStr = getNowDateString();
		String curYearStr = new SimpleDateFormat("yyyy").format(new java.util.Date());
		String yesterdayDateStr = getYesterday();
		String timeStr = new SimpleDateFormat("HH:mm").format(date);
		if (curDateStr.equals(dateStr)) {
			return timeStr;
		} else if (yesterdayDateStr.equals(dateStr)) {
			return "昨天 " + timeStr;
		} else if (yearStr.equals(curYearStr)) {
			return getChineseDateStr2(dateStr) + " " + timeStr;
		} else {
			return getChineseDateStr3(dateStr) + " " + timeStr;
		}
	}

	/**
	 * 获得当前时间的指定相差月份的时间
	 * 
	 * @param differenceMonth
	 *            相差的月份，正数则往前推，负数则往后推
	 * @return
	 */
	public static String getPrevOrNextMonthTime(int differenceMonth) {
		String time = getLongFormat().format(getPrevOrNextMonthDate(differenceMonth));
		return time;
	}

	/**
	 * 获得当前时间的指定相差月份的时间
	 * 
	 * @param differenceMonth
	 * @return
	 */
	public static java.util.Date getPrevOrNextMonthDate(int differenceMonth) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, differenceMonth);
		return c.getTime();
	}

	/**
	 * 处理服务器时间（少了3位）
	 * 
	 * @param serverTime
	 * @return
	 */
	public static long fromServerTimeToTimeStamp(long serverTime) {
		return serverTime * 1000;
	}

	/**
	 * 根据服务器返回时间，修改描述
	 * 
	 * @param serverTime
	 * @return
	 */
	public static String getDescriptionOfServerTime(long serverTime) {
		Locale locale = Locale.CHINA;
		ResourceBundle bundle = ResourceBundle.getBundle("webq", locale);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new java.util.Date());
		Date serverDate = new Date(fromServerTimeToTimeStamp(serverTime));
		try {
			SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			java.util.Date today = format.parse(format.format(cal.getTime()));

			cal.add(Calendar.DAY_OF_MONTH, -1);
			java.util.Date yesterday = format.parse(format.format(cal.getTime()));

			String formatServerTime = "";
			if (serverDate.after(yesterday) && serverDate.before(today)) {
				formatServerTime += bundle.getString("common_yesterday") + "" + formatTime.format(serverDate);
			} else if (serverDate.after(today)) {
				formatServerTime += bundle.getString("common_today") + "" + formatTime.format(serverDate);
			} else {
				formatServerTime += format.format(serverDate);
			}
			return formatServerTime;
		} catch (ParseException e) {
		}
		return "";
	}

	/**
	 * 比较时间大小
	 */
	public static int compare(java.util.Date date1, java.util.Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		return cal2.getTime().compareTo(cal1.getTime());
	}

	/**
	 * 大于当前时间返回 1 小于当前时间 返回-1
	 * 
	 * @param date
	 * @return
	 */
	public static int compare(java.util.Date date) {
		return compare(new java.util.Date(), date);
	}

	/**
	 * 时间差
	 */
	public static int yearDiff(java.util.Date date1, java.util.Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		return cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
	}

	/**
	 * 是否转月
	 * 
	 * @return
	 */
	public static boolean isMonthDiff() {
		String nowMonth = formatDateByFormat(new java.util.Date(), "yyyy-MM");
		String yesterDayMonth = getYesterdayMonth();
		return !nowMonth.equals(yesterDayMonth);
	}

	/**
	 * 比较是否同一天
	 * 
	 * @param time1
	 * @param time2
	 * @return
	 */
	public static boolean isSameDate(long time1, long time2) {
		if (String.valueOf(time1).length() == 10) {
			time1 = time1 * 1000;
		}
		if (String.valueOf(time2).length() == 10) {
			time2 = time2 * 1000;
		}
		String time1Str = getMyFormat().format(new java.util.Date(time1));
		String time2Str = getMyFormat().format(new java.util.Date(time2));
		return time1Str.equals(time2Str);
	}

	/**
	 * 格式化日期（yyyy-MM-dd）
	 * 
	 * @param time
	 * @return
	 */
	public static String formatLongToDate(long time) {
		if (String.valueOf(time).length() == 10) {
			time = time * 1000;
		}
		return getMyFormat().format(new java.util.Date(time));
	}

	/**
	 * 获取时间差(返回相差秒数)
	 */
	public static long getTimeDiff(java.util.Date date) {
		return (date.getTime() - System.currentTimeMillis()) / 1000;
	}

	/**
	 * 获取两个时间相差的天数
	 */
	public static long getDayBetween(java.util.Date startDate, java.util.Date endDate) {
		Calendar calst = Calendar.getInstance();
		Calendar caled = Calendar.getInstance();
		calst.setTime(startDate);
		caled.setTime(endDate);
		calst.set(java.util.Calendar.HOUR_OF_DAY, 0);
		calst.set(java.util.Calendar.MINUTE, 0);
		calst.set(java.util.Calendar.SECOND, 0);
		caled.set(java.util.Calendar.HOUR_OF_DAY, 0);
		caled.set(java.util.Calendar.MINUTE, 0);
		caled.set(java.util.Calendar.SECOND, 0);
		int days = ((int) (caled.getTime().getTime() / 1000) - (int) (calst.getTime().getTime() / 1000)) / 3600 / 24;
		return days;
	}

	/**
	 * 获取距离当前日子几天
	 * 
	 * @param date
	 * @return
	 */
	public static long getFromNowDays(java.util.Date date) {
		long compareTime = date.getTime();
		long currentTime = System.currentTimeMillis();
		long timeDistance = currentTime - compareTime;
		return timeDistance / (24 * 60 * 60 * 1000);
	}

	/**
	 * 获取几天前日起
	 * 
	 * @return
	 */
	public static String getSeveralDayAgo(int num) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(new java.util.Date());
		ca.add(Calendar.DAY_OF_YEAR, 0 - num);
		java.util.Date lastMonth = ca.getTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(lastMonth);
	}

	public static java.util.Date getDateAfterYear(java.util.Date date, int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, year);
		java.util.Date newdate = calendar.getTime();
		return newdate;
	}

	/**
	 * 获取星期几
	 * 
	 * @param date
	 * @return
	 */
	public static String getWeekOfDate(java.util.Date date) {
		String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (w < 0)
			w = 0;

		return weekDays[w];
	}

	/**
	 * 按照格式，解析字符串
	 */
	public static java.util.Date parse(String dateStr, String pattern) throws ParseException {
		return new SimpleDateFormat(pattern).parse(dateStr);
	}

	/**
	 * yyyy-MM-dd字符串转换Date对象
	 * 
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 *             java.util.Date
	 */
	public static java.util.Date parseDate(String dateStr) throws ParseException {
		return getMyFormat().parse(dateStr);
	}
	/**
	 * yyyy-MM-dd HH:mm:ss字符串转换Date对象
	 * 
	 * @param dateStr
	 * @return
	 * @throws ParseException
	 *             java.util.Date
	 */
	public static java.util.Date parseDateTime(String dateStr) throws ParseException {
		return getLongFormat().parse(dateStr);
	}
	
	/**
	 * 获取传入时间过去了多久
	 * @param date
	 * @return
	 */
	public static String getDatePassBy(java.util.Date date) {
		Date now = new Date();
		long lastSecond = (now.getTime() - date.getTime()) / 1000;
		
		if (lastSecond < 0) {
			return "";
		}
		
		if (lastSecond < 60) {
			return lastSecond + WebUtils.getLocaleMessage("common.descrtion.time.secago");
		} else if (lastSecond < 60 * 60) {
			return (lastSecond / 60) + WebUtils.getLocaleMessage("common.descrtion.time.minago");
		} else if (lastSecond < 60 * 60 * 24) {
			return (lastSecond / (60 * 60)) + WebUtils.getLocaleMessage("common.descrtion.time.hourago");
		} else {
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		}
	}
	
	/**
	 * 获取特定日期结束时间
	 * @throws ParseException 
	 */
	public static Date getEndOfDateTime(Date date) {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 23:59:59");    
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	/**
	 * 获取特定日期结束时间
	 * @throws ParseException 
	 */
	public static Date getStartOfDateTime(Date date) {
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd 00:00:00");    
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(df.format(date));
		} catch (ParseException e) {
			e.printStackTrace();
		} 
		return null;
	}

	/**
	 * 取得今日日期0:00:00
	 * @return
	 */
	public static Date getTodayDateStart(){
		return fomatDateStart(new Date());
	}
	/**
	 * 取得今日日期23:59:59:999
	 * @return
	 */
	public static Date getTodayDateEnd(){
		return fomatDateEnd(new Date());
	}

	/**
	 * 取得传入日期的0:00
	 * @param date
	 * @return
     */
	public static Date fomatDateStart(Date date){
		try {
			if (date == null) {
				return getTodayDateStart();
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
			return simpleDateFormat.parse(simpleDateFormat.format(date));
		}catch (Exception e){
			return null;
		}
	}
	/**
	 * 取得传入日期的23:59
	 * @param date
	 * @return
	 */
	public static Date fomatDateEnd(Date date){
		try {
			if (date == null) {
				return getTodayDateEnd();
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
			SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss:SSS");
			String d = simpleDateFormat.format(date);
			d = d + " 23:59:59:999";
			return simpleDateFormat1.parse(d);
		}catch (Exception e){
			return null;
		}
	}
}
