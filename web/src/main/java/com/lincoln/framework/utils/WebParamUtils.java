package com.lincoln.framework.utils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

public class WebParamUtils {
    
    /**
     * Gets a parameter as a string.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return The value of the parameter or null if the parameter was not found
     *         or if the parameter is a zero-length string.
     */
    public static String getParameter(HttpServletRequest request, String name) {
        return getParameter(request, name, false);
    }

    /**
     * Gets a parameter as a string.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @param emptyStringsOK
     *            Return the parameter values even if it is an empty string.
     * @return The value of the parameter or null if the parameter was not
     *         found.
     */
    public static String getParameter(HttpServletRequest request, String name, boolean emptyStringsOK) {

        String temp = request.getParameter(name);
        if (temp != null) {
            if (temp.equals("") && !emptyStringsOK) {
                return null;
            } else {

                return temp.trim();
            }
        } else {
            return null;
        }
    }

    public static String getParameter(HttpServletRequest request, String name, String emptyStringsOK) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("null")) {
            return temp.trim();
        } else {
            return emptyStringsOK;
        }
    }

    public static String[] getParameters(HttpServletRequest request, String name) {
        String[] temp = request.getParameterValues(name);
        ArrayList param = new ArrayList();

        if (temp == null) {
            return null;
        }

        if (temp.length < 1) {
            return null;
        }

        for (int i = 0; i < temp.length; i++) {
            if (!temp[i].trim().equals(""))
                param.add(temp[i].trim());
        }

        if (param.size() == 0) {
            return null;
        }

        String[] values = new String[param.size()];
        for (int i = 0; i < param.size(); i++) {

            values[i] = param.get(i).toString();

        }
        return values;
    }

    public static String[] getParametersNullArray(HttpServletRequest request, String name) {
        String[] temp = request.getParameterValues(name);

        if (temp != null) {
            return temp;
        } else {
            return new String[0];
        }
    }

    /**
     * Gets a parameter as a boolean.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return True if the value of the parameter was "true", false otherwise.
     */
    public static boolean getBooleanParameter(HttpServletRequest request, String name) {
        return getBooleanParameter(request, name, false);
    }

    /**
     * Gets a parameter as a boolean.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return True if the value of the parameter was "true", false otherwise.
     */
    public static boolean getBooleanParameter(HttpServletRequest request, String name, boolean defaultVal) {
        String temp = request.getParameter(name);
        if ("true".equals(temp) || "on".equals(temp)) {
            return true;
        } else if ("false".equals(temp) || "off".equals(temp)) {
            return false;
        } else {
            return defaultVal;
        }
    }

    /**
     * Gets a parameter as an int.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return The int value of the parameter specified or the default value if
     *         the parameter is not found.
     */
    public static int getIntParameter(HttpServletRequest request, String name, int defaultNum) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            int num = defaultNum;
            try {
                num = Integer.parseInt(temp.trim());
            } catch (Exception ignored) {
            }
            return num;
        } else {
            return defaultNum;
        }
    }

    public static int getIntParameter(HttpServletRequest request, String name) {
        return getIntParameter(request, name, 0);
    }
    
    /**
     * 获取short类型
     * @param request
     * @param name
     * @return
     */
    public static short getShortParameter(HttpServletRequest request, String name) {
        return (short)getIntParameter(request, name, 0);
    }

    /**
     * Gets a list of int parameters.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @param defaultNum
     *            The default value of a parameter, if the parameter can't be
     *            converted into an int.
     */
    public static int[] getIntParameters(HttpServletRequest request, String name, int defaultNum) {
        String[] paramValues = request.getParameterValues(name);
        ArrayList param = new ArrayList();
        if (paramValues == null) {
            return null;
        }
        if (paramValues.length < 1) {
            // return new int[0];
            return null;
        }

        for (int i = 0; i < paramValues.length; i++) {
            if (!paramValues[i].trim().equals(""))
                param.add(paramValues[i].trim());
        }

        if (param.size() == 0) {
            return null;
        }

        int[] values = new int[param.size()];
        for (int i = 0; i < param.size(); i++) {
            try {
                values[i] = Integer.parseInt(param.get(i).toString());
            } catch (Exception e) {
                values[i] = defaultNum;
            }
        }
        return values;
    }

    /**
     * Gets a parameter as a double.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return The double value of the parameter specified or the default value
     *         if the parameter is not found.
     */
    public static double getDoubleParameter(HttpServletRequest request, String name, double defaultNum) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            double num = defaultNum;
            try {
                num = Double.parseDouble(temp.trim());
            } catch (Exception ignored) {
            }
            return num;
        } else {
            return defaultNum;
        }
    }

    public static BigDecimal getBigDecimalParameter(HttpServletRequest request, String name, int defaultNum) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            BigDecimal num = new BigDecimal(defaultNum);
            try {
                double numDouble = Double.parseDouble(temp);
                num = new BigDecimal(numDouble);
            } catch (Exception ignored) {
            }
            return num;
        } else {
            return new BigDecimal(defaultNum);
        }
    }

    public static BigDecimal getBigDecimalParameter(HttpServletRequest request, String name) {
        return getBigDecimalParameter(request, name, 0);
    }

    public static BigDecimal[] getBigDecimalParameters(HttpServletRequest request, String name, BigDecimal defaultNum) {
        String[] paramValues = request.getParameterValues(name);
        ArrayList param = new ArrayList();
        if (paramValues == null) {
            return null;
        }
        if (paramValues.length < 1) {
            return null;
        }

        for (int i = 0; i < paramValues.length; i++) {
            if (!paramValues[i].trim().equals(""))
                param.add(paramValues[i].trim());
        }

        if (param.size() == 0) {
            return null;
        }

        BigDecimal[] values = new BigDecimal[param.size()];

        for (int i = 0; i < param.size(); i++) {
            try {
                values[i] = BigDecimal.valueOf(Double.parseDouble(param.get(i).toString()));
            } catch (Exception e) {
                values[i] = defaultNum;
            }
        }
        return values;

    }

    public static Timestamp getTimestampParameter(HttpServletRequest request, String name, Timestamp defaultDate) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            Timestamp numTime = defaultDate;
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                java.util.Date d = formatter.parse(temp);
                numTime = new Timestamp(d.getTime());
            } catch (Exception ignored) {
            }
            return numTime;
        } else {
            return defaultDate;
        }
    }

    public static java.sql.Date getDateParameter(HttpServletRequest request, String name, java.sql.Date defaultDate) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            java.sql.Date numTime = defaultDate;
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
                java.util.Date d = formatter.parse(temp);
                numTime = new java.sql.Date(d.getTime());
            } catch (Exception ignored) {
            }
            return numTime;
        } else {
            return defaultDate;
        }
    }

    public static java.sql.Date getDateParameter(HttpServletRequest request, String name) {
        return getDateParameter(request, name, null);
    }

    public static Timestamp getTimestampParameter(HttpServletRequest request, String name) {
        return getTimestampParameter(request, name, null);
    }

    public static Timestamp getNowTimestamp() {
        java.util.Date d = new java.util.Date();
        Timestamp numTime = new Timestamp(d.getTime());
        return numTime;

    }

    /**
     * Gets a parameter as a long.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @return The long value of the parameter specified or the default value if
     *         the parameter is not found.
     */
    public static long getLongParameter(HttpServletRequest request, String name, long defaultNum) {
        String temp = request.getParameter(name);
        if (temp != null && !temp.equals("")) {
            long num = defaultNum;
            try {
                num = Long.parseLong(temp);
            } catch (Exception ignored) {
            }
            return num;
        } else {
            return defaultNum;
        }
    }

    /**
     * Gets a list of long parameters.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @param defaultNum
     *            The default value of a parameter, if the parameter can't be
     *            converted into a long.
     */
    public static long[] getLongParameters(HttpServletRequest request, String name, long defaultNum) {
        String[] paramValues = request.getParameterValues(name);
        ArrayList param = new ArrayList();
        if (paramValues == null) {
            return null;
        }
        if (paramValues.length < 1) {
            // return new long[0];
            return null;
        }

        for (int i = 0; i < paramValues.length; i++) {
            if (!paramValues[i].trim().equals(""))
                param.add(paramValues[i].trim());
        }

        if (param.size() == 0) {
            return null;
        }

        long[] values = new long[param.size()];
        for (int i = 0; i < param.size(); i++) {
            try {
                values[i] = Long.parseLong(param.get(i).toString());
            } catch (Exception e) {
                values[i] = defaultNum;
            }
        }
        return values;
    }

    /**
     * Gets a parameter as a string.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the parameter you want to get
     * @param emptyStringsOK
     *            Return the parameter values even if it is an empty string.
     * @return The value of the parameter or null if the parameter was not
     *         found.
     */

    /**
     * Gets an attribute as a boolean.
     * 
     * @param request
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param name
     *            The name of the attribute you want to get
     * @return True if the value of the attribute is "true", false otherwise.
     */

    /**
     * Gets an attribute as a long.
     * 
     * @param data
     *            The HttpServletRequest object, known as "request" in a JSP
     *            page.
     * @param strDefault
     *            The name of the attribute you want to get
     * @return The long value of the attribute or the default value if the
     *         attribute is not found or is a zero length string.
     */

    public static String getObjectString(Object data, String strDefault) {
        return (data == null) ? strDefault : data.toString();
    }

    public static String getObjectStringGB(Object data, String strDefault) {
        return (data == null) ? strDefault : data.toString();
    }

    public static String getEditObjectString(Object data, String strDefault) {
        String strTemp = (data == null) ? strDefault : data.toString();
        return "\"" + strTemp + "\"";
    }

    public static String getEditObjectDateToString(Object data) {
        return getEditObjectDateToString(data, "");
    }

    public static String getEditObjectDateToString(Object data, String strDefault) {
        return "\"" + fromDataToString(data, strDefault) + "\"";
    }

    /**
     * 
     * 方法描述：将双精度类型转化为字符串
     * 
     * @param data
     * @return String
     */
    public static String getBigDecimalToString(BigDecimal data) {
        return getBigDecimalToString(data, "");
    }

    /**
     * 
     * 方法描述：取得客户端ip
     * 
     * @param request
     * @param defaultip
     * @return String
     */
    public static String getRemoteip(HttpServletRequest request, String defaultip) {
        String ip = "";
        try {
            ip = request.getRemoteAddr();
            if (ip == null) {
                ip = defaultip;
            }
            return ip;
        } catch (Exception e) {
            e.printStackTrace();
            return defaultip;
        }
    }

    public static String getBigDecimalToString(BigDecimal data, String strDefault) {
        if (data == null) {
            return strDefault;
        } else {
            NumberFormat nf = NumberFormat.getNumberInstance();
            nf.setMinimumFractionDigits(2);
            nf.setMaximumFractionDigits(2);
            return String.valueOf(nf.format(data.doubleValue()));
        }
    }

    public static String getDateToString(Object data) {
        return fromDataToString(data, "");
    }

    public static String fromDataToString(Object data, String strDefault) {
        String strTemp = strDefault;
        if (data != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            strTemp = formatter.format((Timestamp) data);
        }
        return strTemp;
    }

    public static String getEditString(Object data, String strDefault) {
        String strTemp = (data == null) ? strDefault : data.toString();
        return strTemp;
    }

    public static String getEditString(Object data) {
        return getEditString(data, "");
    }
}
