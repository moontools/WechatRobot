package com.lincoln.framework.bean;

import com.lincoln.framework.annotation.MenuAuth;
import com.lincoln.framework.entity.SysAdmin;
import com.lincoln.framework.entity.SysRole;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统端登录管理员数据
 * 
 * @author zwm
 */
public class SysLoginAdmin implements Serializable {

	private int id;

	private int status;

	private String name;

	private String lastLoginIp;

	private Date lastLoginTime;

	private String phoneNumber;

	private boolean isSupperAdmin;

	private String roleName;

	private int roleId;

	private SysRoleRights sysRoleRights;

	public SysLoginAdmin(SysAdmin admin) {
		this.id = admin.getId();
		this.status = admin.getStatus();
		this.name = admin.getName();
		this.isSupperAdmin = admin.getSysRole() == null;
		this.lastLoginIp = admin.getLastLoginIp();
		this.lastLoginTime = admin.getLastLoginTime();

		SysRole role = admin.getSysRole();
		if (role != null) {
			this.roleId = role.getId();
			sysRoleRights = new SysRoleRights(role);
			this.roleName = role.getName();
		}
	}

	/**
	 * 校验菜单权限
	 * 
	 * @param roleId
	 * @param auth
	 * @return boolean
	 */
	public boolean checkMenuAuth(String uri, MenuAuth auth) {
		if (this.isSupperAdmin) {
			return true;
		}
		if (auth.buttonRight() == MenuAuth.MENU_BUTTON_NO) {
			return sysRoleRights.checkMenuRights(uri);
		} else {
			return sysRoleRights.checkButtonRights(uri, auth.buttonRight());
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isSupperAdmin() {
		return isSupperAdmin;
	}

	public void setSupperAdmin(boolean isSupperAdmin) {
		this.isSupperAdmin = isSupperAdmin;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public SysRoleRights getSysRoleRights() {
		return sysRoleRights;
	}

	public void setSysRoleRights(SysRoleRights sysRoleRights) {
		this.sysRoleRights = sysRoleRights;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

}
