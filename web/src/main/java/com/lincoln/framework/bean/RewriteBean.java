package com.lincoln.framework.bean;

import java.io.Serializable;

/**
 * 重写实体类
 * @author zwm
 */
/**
 * 
 * @author zwm
 */
public class RewriteBean implements Serializable {

	private int shopId;

	private String domainName;

	/**
	 * 0-牛皮 、1-机械、2-化料
	 */
	private int shopType;

	/**
	 * 0-屠房、1-代理商、2-进出口商、3-制革厂、4-贸易商、5-下游厂商、6-融资服务、7-物流企业、8-其他
	 */
	private int shopTrade;

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getShopTrade() {
		return shopTrade;
	}

	public void setShopTrade(int shopTrade) {
		this.shopTrade = shopTrade;
	}

}
