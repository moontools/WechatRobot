package com.lincoln.framework.bean;

import com.lincoln.framework.exception.ClientException;

import java.util.HashMap;
import java.util.Map;

/**
 * 系统消息实体类
 * 
 * @author zwm
 */
public class SysMessageBean {

	/**
	 * 系统消息推送类型：0、文章审核通过
	 */
	public static final int TYPE_ARTICLE_AUDIT_PASS = 0;

	/**
	 * 系统消息推送类型：1、文章审核不通过
	 */
	public static final int TYPE_ARTICLE_AUDIT_NOPASS = 1;

	/**
	 * 系统消息推送类型：2、店铺审核通过
	 */
	public static final int TYPE_SHOP_AUDIT_PASS = 2;

	/**
	 * 系统消息推送类型：3、店铺审核不通过
	 */
	public static final int TYPE_SHOP_AUDIT_NOPASS = 3;

	/**
	 * 系统消息推送类型：4、供应信息审核通过
	 */
	public static final int TYPE_PROVIDE_AUDIT_PASS = 4;

	/**
	 * 系统消息推送类型：5、供应信息审核不通过
	 */
	public static final int TYPE_PROVIDE_AUDIT_NOPASS = 5;

	/**
	 * 系统消息推送类型：6、需求审核通过
	 */
	public static final int TYPE_AUTHORIZETRANSACTION_AUDIT_PASS = 6;

	/**
	 * 系统消息推送类型：7、需求审核不通过
	 */
	public static final int TYPE_AUTHORIZETRANSACTION_AUDIT_NOPASS = 7;

	/**
	 * 系统消息推送类型：8、担保交易审核通过
	 */
	public static final int TYPE_GUARANTEETRANSACTION_AUDIT_PASS = 8;

	/**
	 * 系统消息推送类型：9、担保交易审核不通过
	 */
	public static final int TYPE_GUARANTEETRANSACTION_AUDIT_NOPASS = 9;
	
	/**
	 * 系统消息推送类型：10、店铺商品审核通过
	 */
	public static final int TYPE_SHOP_PRODUCT_AUDIT_PASS = 10;
	/**
	 * 系统消息推送类型：11、店铺商品审核不通过
	 */
	public static final int TYPE_SHOP_PRODUCT_AUDIT_NOPASS = 11;

	/**
	 * 系统消息：标题 key(系统消息类型) => value(名称)
	 */
	public static final Map<Integer, String> SYS_MESSAGE_TITLES = new HashMap<Integer, String>() {
		{
			this.put(TYPE_ARTICLE_AUDIT_PASS, "资讯文章审核结果");
			this.put(TYPE_ARTICLE_AUDIT_NOPASS, "资讯文章审核结果");
			this.put(TYPE_SHOP_AUDIT_NOPASS, "店铺审核拒绝通知");
			this.put(TYPE_PROVIDE_AUDIT_NOPASS, "供应信息审核拒绝通知");
			this.put(TYPE_AUTHORIZETRANSACTION_AUDIT_NOPASS, "需求审核结果");
			this.put(TYPE_AUTHORIZETRANSACTION_AUDIT_PASS, "需求审核结果");
			this.put(TYPE_GUARANTEETRANSACTION_AUDIT_NOPASS, "担保交易审核结果");
			this.put(TYPE_GUARANTEETRANSACTION_AUDIT_PASS, "担保交易审核结果");
			this.put(TYPE_SHOP_PRODUCT_AUDIT_NOPASS, "店铺商品审核拒绝通知");
		}
	};

	/**
	 * 系统消息： 消息内容 key(系统消息类型) => value(内容)
	 */
	public static final Map<Integer, String> SYS_MESSAGE_CONTENTS = new HashMap<Integer, String>() {
		{
			this.put(TYPE_ARTICLE_AUDIT_PASS,
					"尊敬的用户，您发布的资讯文章《{title}》已通过审核，点击以下链接查看文章：<a href='{url}' target='_blank'>{title}</a>");
			this.put(TYPE_ARTICLE_AUDIT_NOPASS, "尊敬的用户，您发布的资讯文章《{title}》审核被拒绝，请修改后重新提交，如有疑问请咨询平台客服。");
			this.put(TYPE_SHOP_AUDIT_NOPASS, "尊敬的用户，您申请的店铺“{shopName}”由于“{reason}”审核被拒绝，请修改信息后重新提交，如有疑问请咨询平台客服。");
			this.put(TYPE_PROVIDE_AUDIT_NOPASS, "尊敬的用户，您发布的供应（流水号：{serialNo}）由于“{reason}”审核被拒绝，请修改信息后重新提交，如有疑问请咨询平台客服。");
			this.put(TYPE_AUTHORIZETRANSACTION_AUDIT_NOPASS,
					"尊敬的用户，您申请的需求（流水号：{serialNo}）由于“{reason}”审核被拒绝，如有疑问请咨询平台客服。");
			this.put(TYPE_AUTHORIZETRANSACTION_AUDIT_PASS, "尊敬的用户，您申请的需求（流水号：{serialNo}）已通过审核。");
			this.put(TYPE_GUARANTEETRANSACTION_AUDIT_NOPASS,
					"尊敬的用户，您申请的担保交易（流水号：{serialNo}）由于“{reason}”审核被拒绝，如有疑问请咨询平台客服。");
			this.put(TYPE_GUARANTEETRANSACTION_AUDIT_PASS, "尊敬的用户，您申请的担保交易（流水号：{serialNo}）已通过审核。");
			this.put(TYPE_SHOP_PRODUCT_AUDIT_NOPASS, "尊敬的用户，您发布的商品（商品编号：{serialNo}）由于“{reason}”审核被拒绝，请修改信息后重新提交，如有疑问请咨询平台客服。");
		}
	};

	/**
	 * 系统消息标题
	 */
	private String title;

	/**
	 * 系统消息内容
	 */
	private String content;

	/**
	 * 系统消息类型
	 */
	private int type;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	/**
	 * 初始化系统消息
	 * 
	 * @param type
	 * @param parameters
	 * @throws ClientException
	 */
	public SysMessageBean(int type, Map<String, String> parameters) throws ClientException {
		if (!SYS_MESSAGE_TITLES.keySet().contains(type)) {
			throw new ClientException("系统消息类型不存在");
		}
		this.title = SYS_MESSAGE_TITLES.get(type);
		this.content = SYS_MESSAGE_CONTENTS.get(type);
		this.content = replaceKeyValue(this.content, parameters);
	}

	/**
	 * 处理字符串需要替换内容
	 * 
	 * @param content
	 * @param keyValues
	 *            void
	 */
	public static String replaceKeyValue(String content, Map<String, String> keyValues) {
		for (String key : keyValues.keySet()) {
			content = content.replaceAll("\\{(\\s*)" + key + "(\\s*)\\}", keyValues.get(key));
		}
		content = content.replaceAll("\\{(?:[^}]*)\\}", "");
		return content;
	}
}
