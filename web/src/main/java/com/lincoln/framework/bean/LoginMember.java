package com.lincoln.framework.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 登录会员信息
 * 
 * @author zwm
 */
public class LoginMember implements Serializable {

	/**
	 * 会员id
	 */
	private int id;

	/**
	 * 会员类型： 0 个人会员 1 企业会员
	 */
	private int type;

	/**
	 * 会员账号登录名
	 */
	private String account;

	/**
	 * 真实名称
	 */
	private String realName;

	/**
	 * 最后上线IP
	 */
	private String lastLoginIp;

	/**
	 * 最后上线时间
	 */
	private Date lastLoginTime;

	/**
	 * 店铺id
	 */
	private int shopId;

	/**
	 * 店铺类型；0、皮类，1、机械类，2、化料类
	 */
	private int shopType = -1;
	
	/**
	 * 消息统计
	 */
	private int msgCount = 0;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public int getShopId() {
		return shopId;
	}

	public void setShopId(int shopId) {
		this.shopId = shopId;
	}

	public int getShopType() {
		return shopType;
	}

	public void setShopType(int shopType) {
		this.shopType = shopType;
	}

	public int getMsgCount() {
		return msgCount;
	}

	public void setMsgCount(int msgCount) {
		this.msgCount = msgCount;
	}

}
