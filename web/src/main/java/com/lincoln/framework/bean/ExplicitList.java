package com.lincoln.framework.bean;

import java.util.List;

/**
 * 单元默认值
 * 
 * @author zwm
 */
public class ExplicitList {
	/**
	 * 单元范围：结束单元行坐标
	 */
	private short firstRow;

	/**
	 * 单元范围：结束单元列坐标
	 */
	private short firstCol;

	/**
	 * 单元范围：结束单元列坐标
	 */
	private short endRow;

	/**
	 * 单元范围：结束单元列坐标
	 */
	private short endCol;

	/**
	 * 默认值
	 */
	private String[] listData;

	/**
	 * 关联数据
	 */
	private List<ExplicitList> list;

	public ExplicitList(short firstRow, short firstCol, short endRow, short endCol, String[] listData) {
		this.endCol = endCol;
		this.endRow = endRow;
		this.firstCol = firstCol;
		this.firstRow = firstRow;
		this.listData = listData;
	}

	public ExplicitList() {
		// TODO Auto-generated constructor stub
	}

	public short getFirstRow() {
		return firstRow;
	}

	public void setFirstRow(short firstRow) {
		this.firstRow = firstRow;
	}

	public short getFirstCol() {
		return firstCol;
	}

	public void setFirstCol(short firstCol) {
		this.firstCol = firstCol;
	}

	public short getEndRow() {
		return endRow;
	}

	public void setEndRow(short endRow) {
		this.endRow = endRow;
	}

	public short getEndCol() {
		return endCol;
	}

	public void setEndCol(short endCol) {
		this.endCol = endCol;
	}

	public String[] getListData() {
		return listData;
	}

	public void setListData(String[] listData) {
		this.listData = listData;
	}

	public List<ExplicitList> getList() {
		return list;
	}

	public void setList(List<ExplicitList> list) {
		this.list = list;
	}
}
