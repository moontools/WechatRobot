package com.lincoln.framework.bean;

import com.lincoln.framework.bean.ExplicitList;

import java.util.ArrayList;
import java.util.List;

/**
 * Excel Bean
 * 
 * @author rubekid
 * 
 */
public class ExcelBean {

	private String name;

	private String sheetName;

	private String[] titles;

	private List<String[]> dataList;

	private List<ExplicitList> explicitLists;

	private boolean headBold = true;

	private int columnWidth = 6000;
	
    public ExcelBean(String name, String sheetName, List<String> titleList) {
        String[] titles = new String[titleList.size()];
        titleList.toArray(titles);
        this.name = name;
        this.sheetName = sheetName;
        this.titles = titles;
        this.dataList = new ArrayList<String[]>();
        this.explicitLists = new ArrayList<ExplicitList>();
    }

	public ExcelBean(String name, String sheetName, String[] titles) {
		this.name = name;
		this.sheetName = sheetName;
		this.titles = titles;
		this.dataList = new ArrayList<String[]>();
		this.explicitLists = new ArrayList<ExplicitList>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public String[] getTitles() {
		return titles;
	}

	public void setTitles(String[] titles) {
		this.titles = titles;
	}

	public List<String[]> getDataList() {
		return dataList;
	}

	public void setDataList(List<String[]> dataList) {
		this.dataList = dataList;
	}

	public boolean isHeadBold() {
		return headBold;
	}

	public void setHeadBold(boolean headBold) {
		this.headBold = headBold;
	}

	public int getColumnWidth() {
		return columnWidth;
	}

	public void setColumnWidth(int columnWidth) {
		this.columnWidth = columnWidth;
	}

	public void add(String[] data) {
		this.dataList.add(data);
	}

	public List<ExplicitList> getExplicitLists() {
		return explicitLists;
	}

	public void setExplicitLists(List<ExplicitList> explicitLists) {
		this.explicitLists = explicitLists;
	}

	public void addExplicitList(short firstRow, short firstCol, short endRow, short endCol, String[] listData) {
		if (explicitLists == null) {
			explicitLists = new ArrayList<ExplicitList>();
		}
		explicitLists.add(new ExplicitList(firstRow, firstCol, endRow, endCol, listData));
	}
	
	public void addExplicitList(ExplicitList explicitList) {
		if (explicitLists == null) {
			explicitLists = new ArrayList<ExplicitList>();
		}
		explicitLists.add(explicitList);
	}
}