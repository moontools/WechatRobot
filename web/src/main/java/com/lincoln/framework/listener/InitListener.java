package com.lincoln.framework.listener;

import com.lincoln.framework.utils.FrameworkLogger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * 初始化监听器
 * 
 * Application Lifecycle Listener implementation class InitListener
 */
@WebListener
public class InitListener implements ServletContextListener {

	private static final FrameworkLogger logger = FrameworkLogger.initFrameworkLogger(InitListener.class);

	/**
	 * Default constructor.
	 */
	public InitListener() {
	}

	/**
	 * @TODO
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
	}

	/**
	 * @TODO
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
	}

}
