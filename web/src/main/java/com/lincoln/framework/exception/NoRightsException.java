package com.lincoln.framework.exception;

/**
 * 无权限异常
 * 
 * @author zwm
 */
public class NoRightsException extends ClientException {

	public NoRightsException(String msg) {
		super(msg);
	}

	public NoRightsException(String msg, String errorType) {
		super(msg, errorType);
	}

	public NoRightsException(String msg, String errorType, boolean needRefresh) {
		super(msg, errorType, needRefresh);
	}

}
