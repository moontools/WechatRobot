package com.lincoln.framework.exception;

/**
 * 实体不存在异常
 * @author ssx
 */
@SuppressWarnings("serial")
public class EntityNotFoundException extends ClientException {
	
	public EntityNotFoundException(String msg){
		super(msg);
	}	
}
