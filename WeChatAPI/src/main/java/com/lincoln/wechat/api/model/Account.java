package com.lincoln.wechat.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.lincoln.wechat.api.enums.AccountType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

import static com.lincoln.wechat.api.constant.Constant.API_SPECIAL_USER;

/**
 * 微信用户
 *
 * @author biezhi
 * @date 2018/1/19
 */
@Data
public class Account implements Serializable {
    @SerializedName("Alias")
    private String alias;

    @SerializedName("AppAccountFlag")
    private Integer appAccountFlag;

    @SerializedName("AttrStatus")
    private Long attrStatus;

    @SerializedName("ChatRoomId")
    private Long chatRoomId;

    @SerializedName("City")
    private String city;

    @SerializedName("ContactFlag")
    private Integer contactFlag;

    @SerializedName("DisplayName")
    private String displayName;

    /**
     * 群id
     */
    @SerializedName("EncryChatRoomId")
    private String encryChatRoomId;

    /**
     * 微信头像URL
     */
    @SerializedName("HeadImgUrl")
    private String headImgUrl;

    @SerializedName("HideInputBarFlag")
    private Integer hideInputBarFlag;

    @SerializedName("IsOwner")
    private Integer isOwner;

    /**
     * 微信头像URL
     */
    @SerializedName("KeyWord")
    private String keyWord;

    @SerializedName("MemberCount")
    private Integer memberCount;

    /**
     * 群成员
     */
    @SerializedName("MemberList")
    private List<Member> members;

    /**
     * 微信昵称
     */
    @SerializedName("NickName")
    private String nickName;

    @SerializedName("OwnerUin")
    private Long ownerUin;

    @SerializedName("PYInitial")
    private String pyInitial;

    @SerializedName("PYQuanPin")
    private String pyQuanPin;

    @SerializedName("Province")
    private String province;

    /**
     * 备注名
     */
    @SerializedName("RemarkName")
    private String remarkName;

    @SerializedName("RemarkPYInitial")
    private String remarkPYInitial;

    @SerializedName("RemarkPYQuanPin")
    private String remarkPYQuanPin;

    /**
     * 性别
     */
    @SerializedName("Sex")
    private Integer sex;

    @SerializedName("Signature")
    private String signature;

    @SerializedName("SnsFlag")
    private Integer snsFlag;

    @SerializedName("StarFriend")
    private Integer starFriend;

    @SerializedName("Statues")
    private Integer statues;

    @SerializedName("Uin")
    private Long uin;

    @SerializedName("UniFriend")
    private Integer uniFriend;

    /**
     * 用户唯一标识
     */
    @SerializedName("UserName")
    private String userName;

    @SerializedName("VerifyFlag")
    private Integer verifyFlag;

    @SerializedName("WebWxPluginSwitch")
    private Integer webWxPluginSwitch;

    @SerializedName("HeadImgFlag")
    private Integer headImgFlag;

    @SerializedName("ChatRoomOwner")
    private String chatRoomOwner;

    ///////////////////群聊相关/////////////////////

    /**
     * 通过id查询本群的成员
     * @return
     */
    public Member findMemberById(String id){
        if(members == null||members.size()<=0){
            return null;
        }
        for(Member member:members){
            if(member.getUserName().equals(id)){
                return member;
            }
        }
        return null;
    }

    /**
     * 账户类型，群、好友、公众号、特殊账号
     */
    @Expose
    private AccountType accountType;

    public AccountType getAccountType() {
        if (null != this.accountType) {
            return this.accountType;
        }
        if (verifyFlag > 0 && verifyFlag % 8 == 0) {
            this.accountType = AccountType.TYPE_MP;
        }
        if (API_SPECIAL_USER.contains(this.userName)) {
            this.accountType = AccountType.TYPE_SPECIAL;
        }
        if (this.userName.startsWith("@@")) {
            this.accountType = AccountType.TYPE_GROUP;
        }
        return AccountType.TYPE_FRIEND;
    }

}
