package com.lincoln.wechat.api.client;

import com.lincoln.wechat.api.request.ApiRequest;
import com.lincoln.wechat.api.response.ApiResponse;

import java.io.IOException;

public interface Callback<T extends ApiRequest, R extends ApiResponse> {

    void onResponse(T request, R response);

    void onFailure(T request, IOException e);

}