package com.lincoln.wechat.api.request;

import com.lincoln.wechat.api.constant.Constant;
import com.lincoln.wechat.api.response.ApiResponse;
import okhttp3.Headers;

/**
 * @author biezhi
 * @date 2018/1/18
 */
public class StringRequest extends ApiRequest<StringRequest, ApiResponse> {

    public StringRequest(String url) {
        super(url, ApiResponse.class);
    }
    public void removeContentType(){
        this.headers = Headers.of("User-Agent", Constant.USER_AGENT);
        this.setContentType("text/javascript");
    }
}